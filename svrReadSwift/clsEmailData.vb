﻿Imports DocumentFormat.OpenXml.Packaging
Imports DocumentFormat.OpenXml.Spreadsheet

Public Class clsEmailData

    Private Const _cApprovalEmailPath As String = "https://outlook.office365.com/ews/exchange.asmx"
    Private Const _cOutlookEmailPath As String = "https://outlook.office365.com/ews/exchange.asmx"
    Private Const _cApprovalEmailAddress As String = "payment.approval@dolfin.com"
    Private Const _cApprovalEmailPassword As String = "hTrtc_5$$gte!"
    Private Const _cApprovalEmailFirstLine As String = "Payment Authorisation is required for the below. Kindly confirm payment is correct and follow below instructions"
    Private Const _cOperationsFilesEmailAddress As String = "OperationsFiles@dolfin.com"
    Private Const _cOperationsFilesEmailPassword As String = "Htou^44_h3$87G"
    Private Const _cBFLPath As String = "C:\Temp\ExportFiles\BFL\"
    Private ReadOnly _filePathVolopaTemplate As String = clsIMS.GetFilePath("VolopaTemplate")
    Private ReadOnly _filePathVolopaFTP As String = clsIMS.GetFilePath("VolopaFTP")

    Function CheckApproveMovementEmail(ByVal LstPayment As Dictionary(Of String, String), ByVal varComplainceScore As Integer) As Microsoft.Exchange.WebServices.Data.ExchangeService
        Dim Service As Microsoft.Exchange.WebServices.Data.ExchangeService = Nothing

        Try
            Service = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2013)
            Service.TraceEnabled = True
            Service.UseDefaultCredentials = False
            Service.Credentials = New Microsoft.Exchange.WebServices.Data.WebCredentials(_cApprovalEmailAddress, _cApprovalEmailPassword)
            Service.Url = New Uri(_cApprovalEmailPath)

            Dim SearchFilterCollection As New List(Of Microsoft.Exchange.WebServices.Data.SearchFilter)
            SearchFilterCollection.Add(New Microsoft.Exchange.WebServices.Data.SearchFilter.ContainsSubstring(Microsoft.Exchange.WebServices.Data.ItemSchema.Subject, LstPayment("ID")))
            ' Create the search filter.
            Dim searchFilter As Microsoft.Exchange.WebServices.Data.SearchFilter = New Microsoft.Exchange.WebServices.Data.SearchFilter.SearchFilterCollection(Microsoft.Exchange.WebServices.Data.LogicalOperator.Or, SearchFilterCollection.ToArray())

            ' Create a view with a page size of 50.
            Dim view As New Microsoft.Exchange.WebServices.Data.ItemView(1)

            'Identify the Subject and DateTimeReceived properties to return.
            'Indicate that the base property will be the item identifier
            view.PropertySet = (New Microsoft.Exchange.WebServices.Data.PropertySet(Microsoft.Exchange.WebServices.Data.BasePropertySet.IdOnly, Microsoft.Exchange.WebServices.Data.ItemSchema.Subject, Microsoft.Exchange.WebServices.Data.ItemSchema.DateTimeReceived))
            view.PropertySet.RequestedBodyType = Microsoft.Exchange.WebServices.Data.BodyType.Text

            ' Order the search results by the DateTimeReceived in descending order.
            view.OrderBy.Add(Microsoft.Exchange.WebServices.Data.ItemSchema.DateTimeReceived, Microsoft.Exchange.WebServices.Data.SortDirection.Descending)

            ' Set the traversal to shallow. (Shallow is the default option; other options are Associated and SoftDeleted.)
            view.Traversal = Microsoft.Exchange.WebServices.Data.ItemTraversal.Shallow

            ' Send the request to search the Inbox and get the results.
            Dim findResults As Microsoft.Exchange.WebServices.Data.FindItemsResults(Of Microsoft.Exchange.WebServices.Data.Item) = Service.FindItems(Microsoft.Exchange.WebServices.Data.WellKnownFolderName.Inbox, searchFilter, view)

            ' Process each item.
            For Each item As Microsoft.Exchange.WebServices.Data.Item In findResults.Items
                If TypeOf item Is Microsoft.Exchange.WebServices.Data.EmailMessage Then
                    item.Load()

                    Dim varEmailSender As String = TryCast(item, Microsoft.Exchange.WebServices.Data.EmailMessage).Sender.Address.ToString
                    Dim varEmailSentTime As Date = TryCast(item, Microsoft.Exchange.WebServices.Data.EmailMessage).DateTimeSent
                    Dim EmailSenderDomainName As String = Environment.UserDomainName & "\" & Left(varEmailSender, InStr(1, varEmailSender, "@", vbTextCompare) - 1)
                    Dim varMainBody As String = TryCast(item, Microsoft.Exchange.WebServices.Data.EmailMessage).Body.ToString

                    If varEmailSentTime >= CDate(LstPayment("ModifiedTime")) Then
                        If InStr(1, varEmailSender, "@", vbTextCompare) <> 0 And InStr(1, varMainBody, "OPS PaymentApproval", vbTextCompare) <> 0 Then
                            Dim varMainBodyCheckFirstLine As String = Left(varMainBody, InStr(1, varMainBody, "OPS PaymentApproval", vbTextCompare) - 1)
                            varMainBodyCheckFirstLine = System.Text.RegularExpressions.Regex.Replace(Replace(varMainBodyCheckFirstLine, vbCrLf, ""), "<(?:[^>=]|='[^']*'|=""[^""]*""|=[^'""][^\s>]*)*>", "")
                            varMainBodyCheckFirstLine = Replace(varMainBodyCheckFirstLine, " P {margin-top:0;margin-bottom:0;} ", "")
                            Dim Approved As Boolean = False
                            Dim LengthCheckMail As Double = 3 'Len(varMainBodyCheckFirstLine) 'varMainBody.IndexOf(cApprovalEmailFirstLine, StringComparison.OrdinalIgnoreCase)

                            If LCase(EmailSenderDomainName) <> LCase(LstPayment("Username")) Then
                                If LengthCheckMail > 0 Then
                                    If varComplainceScore = 0 And Not CBool(LstPayment("AboveThreshold")) Then
                                        If (CInt(LstPayment("PaymentTypeID")) = cPayDescReceieveFree Or CInt(LstPayment("PaymentTypeID")) = cPayDescDeliverFree) And (clsIMS.CanUser(EmailSenderDomainName, cUserAuthoriseCompliance) Or clsIMS.CanUser(EmailSenderDomainName, cUserAuthorisePaymentsTeam)) Then
                                            If (Mid(varMainBodyCheckFirstLine, 1, LengthCheckMail).IndexOf("yes", StringComparison.OrdinalIgnoreCase) >= 0) Then
                                                Dim varID As String = Replace(LstPayment("ID"), "ID:", "")
                                                If IsNumeric(varID) Then
                                                    clsIMS.ActionMovementRD(5, CDbl(varID), EmailSenderDomainName)
                                                    clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Authorised free of payment transaction " & varID & " by " & EmailSenderDomainName, CDbl(varID))
                                                    Approved = True
                                                Else
                                                    clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, "CheckApproveMovementEmail - Cannot Authorised free of payment transaction id " & varID & " by " & EmailSenderDomainName, 0)
                                                End If
                                            End If
                                        ElseIf CInt(LstPayment("PaymentTypeID")) = cPayDescExIn And (clsIMS.CanUser(EmailSenderDomainName, cUserAuthoriseCompliance) Or clsIMS.CanUser(EmailSenderDomainName, cUserAuthorisePaymentsTeam)) Then
                                            If (Mid(varMainBodyCheckFirstLine, 1, LengthCheckMail).IndexOf("yes", StringComparison.OrdinalIgnoreCase) >= 0) Then
                                                Dim varID As String = Replace(LstPayment("ID"), "IN:", "")
                                                If IsNumeric(varID) Then
                                                    clsIMS.ActionMovementCR(1, CDbl(varID), EmailSenderDomainName)
                                                    clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Authorised incoming funds " & varID & " by " & EmailSenderDomainName, CDbl(varID))
                                                    Approved = True
                                                Else
                                                    clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, "CheckApproveMovementEmail - Cannot Authorised free of payment transaction id " & varID & " by " & EmailSenderDomainName, 0)
                                                End If
                                            End If
                                        ElseIf ((CInt(LstPayment("PaymentTypeID")) = cPayDescExClientOut Or CInt(LstPayment("PaymentTypeID")) = cPayDescExOut) And clsIMS.CanUser(EmailSenderDomainName, cUserAuthorise3rdPartyPayments)) Or
                                       ((CInt(LstPayment("PaymentTypeID")) <> cPayDescExClientOut And CInt(LstPayment("PaymentTypeID")) <> cPayDescExOut) And clsIMS.CanUser(EmailSenderDomainName, cUserAuthorise)) Then
                                            If (Mid(varMainBodyCheckFirstLine, 1, LengthCheckMail).IndexOf("yes", StringComparison.OrdinalIgnoreCase) >= 0) Then
                                                If Not CBool(LstPayment("AboveThreshold")) Then
                                                    clsIMS.ActionMovement(1, CDbl(LstPayment("ID")), CBool(LstPayment("SendSwift")), CBool(LstPayment("SendIMS")), EmailSenderDomainName)
                                                    clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Authorised transaction " & CDbl(LstPayment("ID")) & " by " & EmailSenderDomainName, CDbl(LstPayment("ID")))
                                                    Approved = True
                                                Else
                                                    If IsDate(LstPayment("AuthorisedTime")) Then
                                                        If varEmailSentTime >= CDate(LstPayment("AuthorisedTime")) Then
                                                            clsIMS.ActionMovement(1, CDbl(LstPayment("ID")), CBool(LstPayment("SendSwift")), CBool(LstPayment("SendIMS")), EmailSenderDomainName)
                                                            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "2nd Authorised transaction " & CDbl(LstPayment("ID")) & " by " & EmailSenderDomainName, CDbl(LstPayment("ID")))
                                                            Approved = True
                                                        Else
                                                            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "2nd authorisation required for this above threshold transaction " & CDbl(LstPayment("ID")), CDbl(LstPayment("ID")))
                                                        End If
                                                    ElseIf varEmailSentTime < CDate(LstPayment("AuthorisedTime")) Then
                                                        clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "2nd authorisation required for this above threshold transaction " & CDbl(LstPayment("ID")), CDbl(LstPayment("ID")))
                                                    End If
                                                End If
                                            End If
                                        End If
                                    ElseIf (varComplainceScore > 1 And clsIMS.CanUserAuthoriseCompliance(EmailSenderDomainName, varComplainceScore)) Or
                                    (varComplainceScore = 1 And (clsIMS.CanUser(EmailSenderDomainName, cUserAuthoriseCompliance) Or clsIMS.CanUser(EmailSenderDomainName, cUserAuthorisePaymentsTeam))) Then
                                        If (Mid(varMainBodyCheckFirstLine, 1, LengthCheckMail).IndexOf("yes", StringComparison.OrdinalIgnoreCase) >= 0) Then
                                            clsIMS.ActionMovementPR(4, CDbl(LstPayment("ID")), EmailSenderDomainName)
                                            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Authorised transaction (payments team) " & CDbl(LstPayment("ID")) & " by " & EmailSenderDomainName, CDbl(LstPayment("ID")))
                                            Approved = True
                                        End If
                                    ElseIf varComplainceScore = 0 And CBool(LstPayment("AboveThreshold")) Then
                                        If (Mid(varMainBodyCheckFirstLine, 1, LengthCheckMail).IndexOf("yes", StringComparison.OrdinalIgnoreCase) >= 0) Then
                                            If clsIMS.CanUser(EmailSenderDomainName, cUserAboveThreshold) And Not IsDate(LstPayment("AuthorisedTime")) Then
                                                clsIMS.ActionMovement(2, CDbl(LstPayment("ID")), CBool(LstPayment("SendSwift")), CBool(LstPayment("SendIMS")), EmailSenderDomainName)
                                                clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Above Threshold - Authorised transaction " & CDbl(LstPayment("ID")) & " by " & EmailSenderDomainName, CDbl(LstPayment("ID")))
                                                Approved = True
                                            ElseIf (((CInt(LstPayment("PaymentTypeID")) = cPayDescExClientOut Or CInt(LstPayment("PaymentTypeID")) = cPayDescExOut) And clsIMS.CanUser(EmailSenderDomainName, cUserAuthorise3rdPartyPayments)) Or
                                                    ((CInt(LstPayment("PaymentTypeID")) <> cPayDescExClientOut And CInt(LstPayment("PaymentTypeID")) <> cPayDescExOut) And clsIMS.CanUser(EmailSenderDomainName, cUserAuthorise))) And varEmailSentTime >= CDate(LstPayment("AuthorisedTime")) Then
                                                clsIMS.ActionMovement(1, CDbl(LstPayment("ID")), CBool(LstPayment("SendSwift")), CBool(LstPayment("SendIMS")), EmailSenderDomainName)
                                                clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Above Threshold - 2nd above threshold authorisation of transaction " & CDbl(LstPayment("ID")) & " by " & EmailSenderDomainName, CDbl(LstPayment("ID")))
                                                Approved = True
                                            ElseIf (CBool(LstPayment("AboveThreshold")) And varEmailSentTime < CDate(LstPayment("AuthorisedTime"))) Then
                                                clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Above Threshold - 2nd authorisation required for this above threshold transaction " & CDbl(LstPayment("ID")), CDbl(LstPayment("ID")))
                                            End If
                                        End If
                                    End If

                                    If varComplainceScore > 1 And clsIMS.CanUserAuthoriseCompliance(EmailSenderDomainName, varComplainceScore) And Not Approved And Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("Escalate", StringComparison.OrdinalIgnoreCase) >= 0 Then
                                        LstPayment.Add("RiskScore", varComplainceScore + 1)
                                        clsIMS.RiskAction(LstPayment, True)
                                        'If LstPayment("RiskScore") <> 6 Then
                                        'clsIMS.ActionMovementPR(10, CDbl(LstPayment("ID")), EmailSenderDomainName)
                                        'End If
                                        clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Escalated transaction (ComplianceAPI) " & CDbl(LstPayment("ID")) & " by " & EmailSenderDomainName, CDbl(LstPayment("ID")))
                                    ElseIf CInt(LstPayment("StatusID")) <> cStatusComplianceRejected And CInt(LstPayment("StatusID")) <> cStatusPaymentsTeamRejected And varComplainceScore > 1 And clsIMS.CanUserAuthoriseCompliance(EmailSenderDomainName, varComplainceScore) And Not Approved And Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("No", StringComparison.OrdinalIgnoreCase) >= 0 Or
                                        varComplainceScore = 1 And (clsIMS.CanUser(EmailSenderDomainName, cUserAuthoriseCompliance) Or clsIMS.CanUser(EmailSenderDomainName, cUserAuthorisePaymentsTeam)) And Not Approved And Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("No", StringComparison.OrdinalIgnoreCase) >= 0 Then
                                        clsIMS.ActionMovementPR(5, CDbl(LstPayment("ID")), EmailSenderDomainName)
                                        clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Payments Team refused transaction " & CDbl(LstPayment("ID")) & " by " & EmailSenderDomainName, CDbl(LstPayment("ID")))
                                    ElseIf varComplainceScore = 0 And Not Approved And Not CBool(LstPayment("AboveThreshold")) And Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("No", StringComparison.OrdinalIgnoreCase) >= 0 Then
                                        If ((CInt(LstPayment("PaymentTypeID")) = cPayDescExClientOut Or CInt(LstPayment("PaymentTypeID")) = cPayDescExOut) And clsIMS.CanUser(EmailSenderDomainName, cUserAuthorise3rdPartyPayments)) Or
                                       ((CInt(LstPayment("PaymentTypeID")) <> cPayDescExClientOut And CInt(LstPayment("PaymentTypeID")) <> cPayDescExOut) And clsIMS.CanUser(EmailSenderDomainName, cUserAuthorise)) Then
                                            clsIMS.ActionMovement(3, CDbl(LstPayment("ID")), CBool(LstPayment("SendSwift")), CBool(LstPayment("SendIMS")), EmailSenderDomainName)
                                            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Refused transaction " & CDbl(LstPayment("ID")) & " by " & EmailSenderDomainName, CDbl(LstPayment("ID")))
                                        End If
                                    End If
                                Else
                                    clsIMS.AddAudit(cAuditStatusWarning, cAuditGroupNameEmail, "No detail in mail found", Replace(LstPayment("ID"), "ID:", ""))
                                End If
                            Else
                                clsIMS.AddAudit(cAuditStatusWarning, cAuditGroupNameEmail, "You are not allowed to authorise your own payment - " & EmailSenderDomainName, Replace(LstPayment("ID"), "ID:", ""))
                            End If
                        ElseIf (CInt(LstPayment("PaymentTypeID")) <> cPayDescReceieveFree And CInt(LstPayment("PaymentTypeID")) <> cPayDescDeliverFree) Then
                            clsIMS.AddAudit(cAuditStatusWarning, cAuditGroupNameEmail, "Please check validity of email address and OPS PaymentApproval is in email body", Replace(LstPayment("ID"), "ID:", ""))
                        End If
                    Else
                        clsIMS.AddAudit(cAuditStatusWarning, cAuditGroupNameEmail, "Awaiting re-authorisation email or authorisation via FLOW because it has been edited after the authorisation email(s) has been sent out", Replace(LstPayment("ID"), "ID:", ""))
                    End If
                End If
            Next

        Catch ex As Microsoft.Exchange.WebServices.Data.AutodiscoverLocalException
            'Seems that user does not work on Exchange 2007 SP1 or later?
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, Err.Description & " - error in CheckApproveMovementEmail", Replace(LstPayment("ID"), "ID:", ""))
        End Try
        Return Service
    End Function

    Public Sub FinishHTML(ByRef HTMLBody As String)
        HTMLBody = "<table Class=Style1 border=1 cellspacing=0 cellpadding=0 align=left style='border-collapse:collapse;border:none;margin-left:6.75pt;margin-right:6.75pt'>" & HTMLBody & "</table>"
    End Sub

    Public Sub BuildHTMLString(ByRef varTitle As String, ByRef varValue As String, ByRef HTMLBody As String, Optional ByVal TitleHeader As Boolean = False)
        On Error Resume Next
        If TitleHeader Then
            HTMLBody = HTMLBody & "<tr><td width = 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'><span style ='font-size:medium;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#FF6666'>" & varTitle & ":<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'><span style ='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & varValue & "<o:p></o:p></span></p></td></tr>"
        Else
            HTMLBody = HTMLBody & "<tr><td width = 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'><span style ='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>" & varTitle & ":<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'><span style ='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & varValue & "<o:p></o:p></span></p></td></tr>"
        End If
    End Sub

    Public Sub CreateHTMLFile(ByVal SearchTypeShortName As String, ByVal varID As Integer, ByVal varFilePath As String, ByVal HTMLBody As String)
        Try
            Dim varDirectory As String = GetPaymentDirectory(varID)
            If varDirectory = "" Then
                varDirectory = varFilePath
                IO.Directory.CreateDirectory(varDirectory)
            End If
            Using varStreamWriter As IO.StreamWriter = New IO.StreamWriter(varDirectory & "\" & SearchTypeShortName & "Search" & varID & ".html", False)
                varStreamWriter.Write(HTMLBody)
            End Using
            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Successfully saved Search" & varID & ".html")
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, Err.Description & " - error in CreateHTMLFile")
        End Try
    End Sub

    Function SendApproveMovementEmailCompliance3rdParty(ByVal LstPayment As Dictionary(Of String, String), ByVal LstRecipients As List(Of String)) As Microsoft.Exchange.WebServices.Data.ExchangeService
        Dim Service As Microsoft.Exchange.WebServices.Data.ExchangeService = Nothing

        Try
            Dim CreateBody As String = ""

            CreateBody = CreateBody & "<html xmlns:v=""urn:schemas-microsoft-com:vml"" xmlns:o=""urn:schemas-microsoft-com:office:office"" xmlns:w=""urn:schemas-microsoft-com:office:word"" xmlns:m=""http://schemas.microsoft.com/office/2004/12/omml"" xmlns=""http://www.w3.org/TR/REC-html40"">" & vbCrLf & "<head>" & vbCrLf & "<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">" & vbCrLf & "<meta name=""Generator"" content=""Microsoft Word 15 (filtered medium)"">" & vbCrLf & "<!--[if !mso]><style>v\:* {behavior:url(#default#VML);}" & vbCrLf &
            "o\* {behaviorurl(#default#VML);}w\:* {behavior:url(#default#VML);}.shape {behavior:url(#default#VML);}</style><![endif]--><style><!--" & vbCrLf &
            "/* Font Definitions */@font-face	{font-family:Calibri;	panose-1:2 15 5 2 2 2 4 3 2 4;}@font-face	{font-family:Tahoma;	panose-1:2 11 6 4 3 5 4 4 2 4;}@font-face	{font-family:""Avenir LT 35 Light"";	panose-1:2 11 3 3 2 0 0 2 0 3;}" & vbCrLf &
            "@font-face	{font-family:""Segoe UI"";	panose-1:2 11 5 2 4 2 4 2 2 3;}" & vbCrLf &
            "/* Style Definitions */p.MsoNormal, li.MsoNormal, div.MsoNormal	{margin:0cm;	margin-bottom:.0001pt;	font-size:11.0pt;	font-family:""Calibri"",""sans-serif"";	mso-fareast-language:EN-US;}a: link, span.MsoHyperlink	{mso-style-priority: 99;" & vbCrLf &
            "color:blue;	text-decoration:underline;}a:visited, span.MsoHyperlinkFollowed	{mso-style-priority: 99;	color:purple;	text-decoration:underline;}p	{mso-style-priority:99;	mso-margin-top-alt:auto;	margin-right:0cm;" & vbCrLf &
            "mso-margin-bottom-altauto;	margin-left0cm;	font-size:12.0pt;	font-family:""Times New Roman"",""serif"";}p.MsoAcetate, li.MsoAcetate, div.MsoAcetate	{mso-style-priority:99;	mso-style-link:""Balloon Text Char"";	margin:0cm;" & vbCrLf &
            "margin-bottom:.0001pt;	font-size:8.0pt;	font-family:""Tahoma"",""sans-serif"";	mso-fareast-language:EN-US;}span.EmailStyle17	{mso-style-type: personal-compose;	font-family: ""Arial"",""sans-serif"";	font-variant:normal!important;" & vbCrLf &
            "color:windowtext;	text-transform:none;	text-decoration:none none;	vertical-align: baseline;}span.link	{mso-style-name:link;}span.txt	{mso-style-name:txt;}span.BalloonTextChar	{mso-style-name:""Balloon Text Char"";	mso-style-priority:99;" & vbCrLf &
            "mso-style-link:""Balloon Text"";	font-family:""Tahoma"",""sans-serif"";}.MsoChpDefault	{mso-style-type:export-only;	font-family: ""Calibri"",""sans-serif"";	mso-fareast-language:EN-US;}@page WordSection1	{size: 612.0pt 792.0pt;	margin:72.0pt 72.0pt 72.0pt 72.0pt;}" & vbCrLf &
            "div.WordSection1	{page:WordSection1;}--></style><!--[if gte mso 9]><xml><o:shapedefaults v: ext = ""edit"" spidmax=""1026"" /></xml><![endif]--><!--[if gte mso 9]><xml><o:shapelayout v: ext = ""edit""><o:idmap v: ext = ""edit"" data=""1"" />" & vbCrLf &
            "</o:shapelayout></xml><![endif]--></head><body lang=EN-GB link=blue vlink=purple><div class=WordSection1>" & vbCrLf

            CreateBody = CreateBody & "<p><span style=""font-family:&quot;Calibri&quot;,sans-serif"">" & _cApprovalEmailFirstLine & "<o:p></o:p></span></p>" & vbCrLf &
            "<p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><table class=Style1 border=1 cellspacing=0 cellpadding=0 align=left style='border-collapse:collapse;border:none;margin-left:6.75pt;margin-right:6.75pt'><tr>" & vbCrLf &
            "<td width=236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Payment Type:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border:solid windowtext 1.0pt;border-left:none;background:white;padding:0cm 5.4pt 0cm 5.4pt'>" & vbCrLf &
            "<p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style ='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>3rd Party Client Payment<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width=236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            AddEmailBody("Payment Portfolio", LstPayment("Portfolio"), CreateBody)
            AddEmailBody("Payment Amount", LstPayment("Amount"), CreateBody)
            AddEmailBody("Payment CCY", LstPayment("CCY"), CreateBody)
            If CInt(IIf(LstPayment("BenTypeID") = "", 0, LstPayment("BenTypeID"))) = 0 Then
                AddEmailBody("Payment Beneficiary First Name", LstPayment("BenFirstname"), CreateBody)
                AddEmailBody("Payment Beneficiary Middle Name", LstPayment("BenMiddlename"), CreateBody)
                AddEmailBody("Payment Beneficiary Surname", LstPayment("BenSurname"), CreateBody)
            Else
                AddEmailBody("Payment Beneficiary Business Name", LstPayment("BenName"), CreateBody)
            End If
            AddEmailBody("Payment Bank Name", LstPayment("BenBankName"), CreateBody)
            AddEmailBody("Payment Bank Country", LstPayment("BenBankCountry"), CreateBody)
            AddEmailBody("Payment Beneficiary Sanction Check", LstPayment("CheckedSanctions"), CreateBody)
            AddEmailBody("Payment Beneficiary is not a PEP", LstPayment("CheckedPEP"), CreateBody)
            AddEmailBody("Customer Due Diligence Performed", LstPayment("CheckedCDD"), CreateBody)
            AddEmailBody("Payment Issues Disclosed/Rationale", LstPayment("IssuesDisclosed"), CreateBody)

            CreateBody = CreateBody & "<span style ='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Link to KYC folder:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style ='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>\\EgnyteDrive\DolfinGroup\Shared\Dolfin\Business%20Development\Sales\Clients\Private\<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "</table><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p>" & vbCrLf &
            "<p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal>" & vbCrLf &
            "<span style ='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal><u>Email Request<o:p></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p>" & vbCrLf

            '"<span style ='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal><u>Email Request<o:p></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p>" & vbCrLf

            CreateBody = CreateBody & "<p>" & Replace(LstPayment("RequestedEmail"), vbCrLf, "</p><p>") & "</p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p>" & vbCrLf

            CreateBody = CreateBody & "<p Class= ""MsoNormal""><u>Instructions<o:p></o:p></u></p>" & vbCrLf &
            "<p class=""MsoNormal""><u><o:p><span style=""text-decoration:none"">&nbsp;</span></o:p></u></p>" & vbCrLf &
            "<p class=""MsoNormal""><b>If you agree payment please reply all and state 'Yes'<o:p></o:p></b></p>" & vbCrLf &
            "<p class=""MsoNormal""><b>If you disagree payment please reply all and state 'No'<o:p></o:p></b></p>" & vbCrLf & "</div>" & vbCrLf &
            "<p style=""FONT-SIZE: 14px; MARGIN-BOTTOM: 10px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; COLOR: rgb(33,33,33); LINE-HEIGHT: 22px"">" & vbCrLf &
            "<span class=""link email signature_email-target sig-hide"" style=""TEXT-DECORATION: none; COLOR: rgb(229,62,48); DISPLAY: inline"">Read Swift</span>" & vbCrLf & "<br>" & vbCrLf & "</p>" & vbCrLf & "<p style=""FONT-SIZE: 12px; MARGIN-BOTTOM: 18px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; MARGIN-TOP: 16px"">" & vbCrLf & "</p>" & vbCrLf & "<p style=""FONT-SIZE: 14px; MARGIN-BOTTOM: 0px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; LINE-HEIGHT: 22px"">" & vbCrLf & "<span class=""txt office-sep sep"" style=""COLOR: rgb(188,190,192); DISPLAY: inline; MARGIN-RIGHT: 8px""></span>" & vbCrLf & "<span class=""txt office-sep sep"" style=""COLOR: rgb(129,130,133); DISPLAY: inline"">" & vbCrLf & "</span><br>" & vbCrLf & "<span class=""txt office-sep sep"" style=""COLOR: rgb(188,190,192); DISPLAY: inline; MARGIN-RIGHT: 10px"">T</span>" & vbCrLf & "<span class=""txt office-sep sep"" style=""COLOR: rgb(129,130,133); DISPLAY: inline"">" & vbCrLf & "&nbsp;&#43;44 20 3700 3888</span> </p>" & vbCrLf & "<p style=""FONT-SIZE: 14px; MARGIN-BOTTOM: 10px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; MARGIN-TOP: 10px; LINE-HEIGHT: 14px"">" & vbCrLf &
            "<a class=""link signature_website-target sig-hide"" style=""TEXT-DECORATION: none; COLOR: rgb(229,62,48); DISPLAY: inline"" href=""http://www.dolfin.com"">dolfin.com</a>" & vbCrLf & "</p>" & vbCrLf & "<p style=""FONT-SIZE: 14px; MARGIN-BOTTOM: 10px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; MARGIN-TOP: 10px; LINE-HEIGHT: 14px"">" & vbCrLf & "<br>" & vbCrLf & "<font color=""#424242""><small>Dolfin Financial (UK) Ltd is a private limited company registered in England and Wales with registration number 07431519 and whose registered office is at 50 Berkeley Street, London W1J 8HA. Dolfin Financial (UK) Ltd is" & vbCrLf & " authorised and regulated by the Financial Conduct Authority.<br>" & vbCrLf & "<br>" & vbCrLf & "The content of this email and any attachments is confidential, may be privileged, is subject to copyright and should only be read, copied and used by the intended recipient. If you are not the intended recipient please notify us by return email or telephone" & vbCrLf &
            " and erase all copies and do not disclose the email Or any part of it to any person. We may monitor email traffic data And also the content of emails for security And regulatory compliance purposes.</small></font>" & vbCrLf & "</p>" & vbCrLf & "<p></p>" & vbCrLf & "</body>" & vbCrLf & "</html>" & vbCrLf


            Service = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2010)
            Service.TraceEnabled = True
            Service.UseDefaultCredentials = False
            Service.Credentials = New Microsoft.Exchange.WebServices.Data.WebCredentials(_cApprovalEmailAddress, _cApprovalEmailPassword)
            Service.Url = New Uri(_cApprovalEmailPath)

            Dim EmailMsg As New Microsoft.Exchange.WebServices.Data.EmailMessage(Service)

            For varemailitem = 0 To LstRecipients.Count - 1
                EmailMsg.ToRecipients.Add(LstRecipients(varemailitem))
            Next

            Dim vFilePath As String = GetPaymentDirectory(LstPayment("ID"))
            If vFilePath <> "" Then
                Dim varFiles() As String = IO.Directory.GetFiles(vFilePath)
                If varFiles.Count > 0 Then
                    For i As Integer = 0 To varFiles.Count - 1
                        EmailMsg.Attachments.AddFileAttachment(varFiles(i))
                    Next
                End If
            End If

            EmailMsg.Subject = "**PAYMENTS TEAM - 3rd PARTY PAYMENT AUTHORISATION REQUIRED** - Order Ref: " & LstPayment("OrderRef") & ", ID: " & LstPayment("ID") & ", CCY: " & LstPayment("CCY")

            Dim MyEmailAddress As String = clsIMS.GetSQLItem("select U_EmailAddress from dolfinpaymentusers where U_Username = '" & LstPayment("Username") & "'")
            If InStr(1, MyEmailAddress, "@", vbTextCompare) <> 0 Then
                EmailMsg.CcRecipients.Add(MyEmailAddress)
            End If

            'add ops and alex to payments team emails
            'EmailMsg.CcRecipients.Add("operations@dolfin.com")
            EmailMsg.Body = New Microsoft.Exchange.WebServices.Data.MessageBody(CreateBody)
            EmailMsg.Send()
            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "SwiftRead Emailed " & LstRecipients(0) & " and possibly others for authorisation in SendApproveMovementEmailCompliance3rdParty", LstPayment("ID"))

        Catch ex As Microsoft.Exchange.WebServices.Data.AutodiscoverLocalException
            'Seems that user does not work on Exchange 2007 SP1 or later?
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, Err.Description & " - error in SwiftRead SendApproveMovementEmailCompliance3rdParty")
        End Try
        Return Service
    End Function

    Function SendApproveMovementEmailCompliance(ByVal LstPayment As Dictionary(Of String, String), ByVal Escalated As Boolean, ByVal LstRecipients As List(Of String), ByVal RiskScore As Integer) As Microsoft.Exchange.WebServices.Data.ExchangeService
        Dim Service As Microsoft.Exchange.WebServices.Data.ExchangeService = Nothing

        Try
            Service = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2010)
            Service.TraceEnabled = True
            Service.UseDefaultCredentials = False
            Service.Credentials = New Microsoft.Exchange.WebServices.Data.WebCredentials(_cApprovalEmailAddress, _cApprovalEmailPassword)
            Service.Url = New Uri(_cApprovalEmailPath)

            Dim EmailMsg As New Microsoft.Exchange.WebServices.Data.EmailMessage(Service)


            Dim CreateBody As String = ""

            CreateBody = CreateBody & "<html xmlns:v=""urn:schemas-microsoft-com:vml"" xmlns:o=""urn:schemas-microsoft-com:office:office"" xmlns:w=""urn:schemas-microsoft-com:office:word"" xmlns:m=""http://schemas.microsoft.com/office/2004/12/omml"" xmlns=""http://www.w3.org/TR/REC-html40"">" & vbCrLf & "<head>" & vbCrLf & "<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">" & vbCrLf & "<meta name=""Generator"" content=""Microsoft Word 15 (filtered medium)"">" & vbCrLf & "<!--[if !mso]><style>v\:* {behavior:url(#default#VML);}" & vbCrLf &
            "o\* {behaviorurl(#default#VML);}w\:* {behavior:url(#default#VML);}.shape {behavior:url(#default#VML);}</style><![endif]--><style><!--" & vbCrLf &
            "/* Font Definitions */@font-face	{font-family:Calibri;	panose-1:2 15 5 2 2 2 4 3 2 4;}@font-face	{font-family:Tahoma;	panose-1:2 11 6 4 3 5 4 4 2 4;}@font-face	{font-family:""Avenir LT 35 Light"";	panose-1:2 11 3 3 2 0 0 2 0 3;}" & vbCrLf &
            "@font-face	{font-family:""Segoe UI"";	panose-1:2 11 5 2 4 2 4 2 2 3;}" & vbCrLf &
            "/* Style Definitions */p.MsoNormal, li.MsoNormal, div.MsoNormal	{margin:0cm;	margin-bottom:.0001pt;	font-size:11.0pt;	font-family:""Calibri"",""sans-serif"";	mso-fareast-language:EN-US;}a: link, span.MsoHyperlink	{mso-style-priority: 99;" & vbCrLf &
            "color:blue;	text-decoration:underline;}a:visited, span.MsoHyperlinkFollowed	{mso-style-priority: 99;	color:purple;	text-decoration:underline;}p	{mso-style-priority:99;	mso-margin-top-alt:auto;	margin-right:0cm;" & vbCrLf &
            "mso-margin-bottom-altauto;	margin-left0cm;	font-size:12.0pt;	font-family:""Times New Roman"",""serif"";}p.MsoAcetate, li.MsoAcetate, div.MsoAcetate	{mso-style-priority:99;	mso-style-link:""Balloon Text Char"";	margin:0cm;" & vbCrLf &
            "margin-bottom:.0001pt;	font-size:8.0pt;	font-family:""Tahoma"",""sans-serif"";	mso-fareast-language:EN-US;}span.EmailStyle17	{mso-style-type: personal-compose;	font-family: ""Arial"",""sans-serif"";	font-variant:normal!important;" & vbCrLf &
            "color:windowtext;	text-transform:none;	text-decoration:none none;	vertical-align: baseline;}span.link	{mso-style-name:link;}span.txt	{mso-style-name:txt;}span.BalloonTextChar	{mso-style-name:""Balloon Text Char"";	mso-style-priority:99;" & vbCrLf &
            "mso-style-link:""Balloon Text"";	font-family:""Tahoma"",""sans-serif"";}.MsoChpDefault	{mso-style-type:export-only;	font-family: ""Calibri"",""sans-serif"";	mso-fareast-language:EN-US;}@page WordSection1	{size: 612.0pt 792.0pt;	margin:72.0pt 72.0pt 72.0pt 72.0pt;}" & vbCrLf &
            "div.WordSection1	{page:WordSection1;}--></style><!--[if gte mso 9]><xml><o:shapedefaults v: ext = ""edit"" spidmax=""1026"" /></xml><![endif]--><!--[if gte mso 9]><xml><o:shapelayout v: ext = ""edit""><o:idmap v: ext = ""edit"" data=""1"" />" & vbCrLf &
            "</o:shapelayout></xml><![endif]--></head><body lang=EN-GB link=blue vlink=purple><div class=WordSection1>" & vbCrLf

            CreateBody = CreateBody & "<p><span style=""font-family:&quot;Calibri&quot;,sans-serif"">Compliance, please see payment booked in FLOW:<o:p></o:p></span></p>" & vbCrLf &
            "<p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><table class=Style1 border=1 cellspacing=0 cellpadding=0 align=left style='border-collapse:collapse;border:none;margin-left:6.75pt;margin-right:6.75pt'><tr>" & vbCrLf &
            "<td width=236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Payment Type:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border:solid windowtext 1.0pt;border-left:none;background:white;padding:0cm 5.4pt 0cm 5.4pt'>" & vbCrLf &
            "<p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style ='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & LstPayment("PaymentType") & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width=236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            AddEmailBody("Payment Portfolio", LstPayment("Portfolio"), CreateBody)
            AddEmailBody("Payment Amount", LstPayment("Amount"), CreateBody)
            AddEmailBody("Payment CCY", LstPayment("CCY"), CreateBody)
            If CInt(LstPayment("BenTypeID")) = 0 Then
                AddEmailBody("Payment Beneficiary First Name", LstPayment("BenFirstname"), CreateBody)
                AddEmailBody("Payment Beneficiary Middle Name", LstPayment("BenMiddlename"), CreateBody)
                AddEmailBody("Payment Beneficiary Surname", LstPayment("BenSurname"), CreateBody)
            Else
                AddEmailBody("Payment Beneficiary Business Name", LstPayment("BenName"), CreateBody)
            End If
            AddEmailBody("Payment Beneficiary Country", LstPayment("BenCountry"), CreateBody)
            AddEmailBody("Payment Beneficiary Relationship", LstPayment("BenRelationship"), CreateBody)
            AddEmailBody("Payment Beneficiary is PEP", IIf(LstPayment("isPEP") = "1" Or LCase(LstPayment("isPEP")) = "true", "Yes", "No"), CreateBody)
            AddEmailBody("Payment Beneficiary has sanction (Current)", IIf(LstPayment("isSanctionsCurrent") = "1" Or LCase(LstPayment("isSanctionsCurrent")) = "true", "Yes", "No"), CreateBody)
            AddEmailBody("Payment Beneficiary has sanction (Previous)", IIf(LstPayment("isSanctionsPrevious") = "1" Or LCase(LstPayment("isSanctionsPrevious")) = "true", "Yes", "No"), CreateBody)
            AddEmailBody("Payment Beneficiary has adverse media", IIf(LstPayment("isAdverseMedia") = "1" Or LCase(LstPayment("isAdverseMedia")) = "true", "Yes", "No"), CreateBody)
            AddEmailBody("Payment Bank Name", LstPayment("BenBankName"), CreateBody)
            AddEmailBody("Payment Bank Country", LstPayment("BenBankCountry"), CreateBody)
            AddEmailBody("Payment Bank has sanction (Current)", IIf(LstPayment("BankisSanctionsCurrent") = "1" Or LCase(LstPayment("BankisSanctionsCurrent")) = "true", "Yes", "No"), CreateBody)
            AddEmailBody("Payment Bank has sanction (Previous)", IIf(LstPayment("BankisSanctionsPrevious") = "1" Or LCase(LstPayment("BankisSanctionsPrevious")) = "true", "Yes", "No"), CreateBody)
            AddEmailBody("Payment Issues Disclosed/Rationale", LstPayment("IssuesDisclosed"), CreateBody)
            AddEmailBody("SCORES", "", CreateBody)
            AddEmailBody("PEP Score", LstPayment("ScorePEP"), CreateBody)
            AddEmailBody("Recipient Score", LstPayment("ScoreRecipient"), CreateBody)
            AddEmailBody("Bank Score", LstPayment("ScoreBank"), CreateBody)
            AddEmailBody("Country Score", LstPayment("ScoreCountry"), CreateBody)
            AddEmailBody("CCY Score", LstPayment("ScoreCCY"), CreateBody)
            AddEmailBody("Amt Score", LstPayment("ScoreAmt"), CreateBody)
            AddEmailBody("Cumulative Amt Score", LstPayment("ScoreCumulativeAmt"), CreateBody)
            AddEmailBody("Type Score", LstPayment("ScoreType"), CreateBody)
            AddEmailBody("Total Score", LstPayment("Score"), CreateBody)
            AddEmailBody("Risk Score", LstPayment("RiskScoreDescription") & " (" & LstPayment("RiskScore") & ")", CreateBody)


            Dim vFilePath As String = GetPaymentDirectory(LstPayment("ID"))
            If vFilePath <> "" Then
                Dim varFiles() As String = IO.Directory.GetFiles(vFilePath)
                If varFiles.Count > 0 Then
                    For i As Integer = 0 To varFiles.Count - 1
                        If InStr(1, varFiles(i), "IndSearch", vbTextCompare) <> 0 And InStr(1, varFiles(i), "html", vbTextCompare) <> 0 Then
                            AddEmailBody("Individual Search File", Replace(varFiles(i), " ", "%20"), CreateBody)
                        ElseIf InStr(1, varFiles(i), "BusSearch", vbTextCompare) <> 0 And InStr(1, varFiles(i), "html", vbTextCompare) <> 0 Then
                            AddEmailBody("Business Search File", Replace(varFiles(i), " ", "%20"), CreateBody)
                        ElseIf InStr(1, varFiles(i), "BankSearch", vbTextCompare) <> 0 And InStr(1, varFiles(i), "html", vbTextCompare) <> 0 Then
                            AddEmailBody("Bank Search File", Replace(varFiles(i), " ", "%20"), CreateBody)
                            'Else
                            '    EmailMsg.Attachments.AddFileAttachment(varFiles(i))
                        End If
                    Next
                End If
            End If

            CreateBody = CreateBody & "<span style ='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Link to KYC folder:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style ='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>\\EgnyteDrive\DolfinGroup\Shared\Dolfin\Business%20Development\Sales\Clients\Private\<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "</table><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p>" & vbCrLf &
            "<p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal>" & vbCrLf &
            "<span style ='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal><u>Email Request<o:p></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p>" & vbCrLf

            CreateBody = CreateBody & "<p>" & Replace(LstPayment("RequestedEmail"), vbCrLf, "</p><p>") & "</p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p>" & vbCrLf

            CreateBody = CreateBody & "<p Class= ""MsoNormal""><u><o:p></o:p></u></p>" & vbCrLf &
            "<p class=""MsoNormal""><u><o:p><span style=""text-decoration:none"">&nbsp;</span></o:p></u></p>" & vbCrLf &
            "<p class=""MsoNormal""><b><o:p></o:p></b></p>" & vbCrLf &
            "<p class=""MsoNormal""><b><o:p></o:p></b></p>" & vbCrLf &
            "<p class=""MsoNormal""><b><o:p></o:p></b></p>" & vbCrLf & "</div>" & vbCrLf &
            "<p style=""FONT-SIZE: 14px; MARGIN-BOTTOM: 10px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; COLOR: rgb(33,33,33); LINE-HEIGHT: 22px"">" & vbCrLf &
            "<span class=""link email signature_email-target sig-hide"" style=""TEXT-DECORATION: none; COLOR: rgb(229,62,48); DISPLAY: inline"">Read Swift</span>" & vbCrLf & "<br>" & vbCrLf & "</p>" & vbCrLf & "<p style=""FONT-SIZE: 12px; MARGIN-BOTTOM: 18px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; MARGIN-TOP: 16px"">" & vbCrLf & "</p>" & vbCrLf & "<p style=""FONT-SIZE: 14px; MARGIN-BOTTOM: 0px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; LINE-HEIGHT: 22px"">" & vbCrLf & "<span class=""txt office-sep sep"" style=""COLOR: rgb(188,190,192); DISPLAY: inline; MARGIN-RIGHT: 8px""></span>" & vbCrLf & "<span class=""txt office-sep sep"" style=""COLOR: rgb(129,130,133); DISPLAY: inline"">" & vbCrLf & "</span><br>" & vbCrLf & "<span class=""txt office-sep sep"" style=""COLOR: rgb(188,190,192); DISPLAY: inline; MARGIN-RIGHT: 10px"">T</span>" & vbCrLf & "<span class=""txt office-sep sep"" style=""COLOR: rgb(129,130,133); DISPLAY: inline"">" & vbCrLf & "&nbsp;&#43;44 20 3700 3888</span> </p>" & vbCrLf & "<p style=""FONT-SIZE: 14px; MARGIN-BOTTOM: 10px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; MARGIN-TOP: 10px; LINE-HEIGHT: 14px"">" & vbCrLf &
            "<a class=""link signature_website-target sig-hide"" style=""TEXT-DECORATION: none; COLOR: rgb(229,62,48); DISPLAY: inline"" href=""http://www.dolfin.com"">dolfin.com</a>" & vbCrLf & "</p>" & vbCrLf & "<p style=""FONT-SIZE: 14px; MARGIN-BOTTOM: 10px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; MARGIN-TOP: 10px; LINE-HEIGHT: 14px"">" & vbCrLf & "<br>" & vbCrLf & "<font color=""#424242""><small>Dolfin Financial (UK) Ltd is a private limited company registered in England and Wales with registration number 07431519 and whose registered office is at 50 Berkeley Street, London W1J 8HA. Dolfin Financial (UK) Ltd is" & vbCrLf & " authorised and regulated by the Financial Conduct Authority.<br>" & vbCrLf & "<br>" & vbCrLf & "The content of this email and any attachments is confidential, may be privileged, is subject to copyright and should only be read, copied and used by the intended recipient. If you are not the intended recipient please notify us by return email or telephone" & vbCrLf &
            " and erase all copies and do not disclose the email Or any part of it to any person. We may monitor email traffic data And also the content of emails for security And regulatory compliance purposes.</small></font>" & vbCrLf & "</p>" & vbCrLf & "<p></p>" & vbCrLf & "</body>" & vbCrLf & "</html>" & vbCrLf

            For varemailitem = 0 To LstRecipients.Count - 1
                If InStr(1, LstRecipients(varemailitem), "@", vbTextCompare) <> 0 Then
                    EmailMsg.ToRecipients.Add(LstRecipients(varemailitem))
                End If
            Next varemailitem

            EmailMsg.Subject = "PAYMENT BOOKED** - Order Ref: " & LstPayment("OrderRef") & ", ID: " & LstPayment("ID") & ", CCY: " & LstPayment("CCY")

            'add ops and alex to payments team emails
            'EmailMsg.CcRecipients.Add("operations@dolfin.com")
            EmailMsg.BccRecipients.Add("David.Harper@dolfin.com")
            EmailMsg.Body = New Microsoft.Exchange.WebServices.Data.MessageBody(CreateBody)
            EmailMsg.Send()
            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Successful email sent in SendApproveMovementEmailCompliance - ReadSwift", LstPayment("ID"))

        Catch ex As Microsoft.Exchange.WebServices.Data.AutodiscoverLocalException
            'Seems that user does not work on Exchange 2007 SP1 or later?
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, Err.Description & " - error in SendApproveMovementEmailCompliance - ReadSwift", LstPayment("ID"))
            clsIMS.ActionMovementPR(9, LstPayment("ID"), "complianceAPI")
        End Try
        Return Service
    End Function

    Function SendVolopaEmail(ByVal ReportTitle As String, ByVal varSQL As String) As Microsoft.Exchange.WebServices.Data.ExchangeService
        Dim Service As Microsoft.Exchange.WebServices.Data.ExchangeService = Nothing

        Try
            Dim varFileName As String = ""
            Dim varAccMgtEmailAddress As String = ""


            If PopulateVolopaExcelSheetOpenXML(varFileName, ReportTitle, varSQL) Then
                Service = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2010)
                Service.TraceEnabled = True
                Service.UseDefaultCredentials = False
                Service.Credentials = New Microsoft.Exchange.WebServices.Data.WebCredentials(_cApprovalEmailAddress, _cApprovalEmailPassword)
                Service.Url = New Uri(_cApprovalEmailPath)

                Dim EmailMsg As New Microsoft.Exchange.WebServices.Data.EmailMessage(Service)

                Dim CreateBody As String = ""

                CreateBody = CreateBody & "Please see attached " & ReportTitle & " Volopa payments allocations for today..."

                EmailMsg.Attachments.AddFileAttachment(varFileName)
                EmailMsg.Subject = "Dolfin Volopa Allocations (" & ReportTitle & ")"
                If clsIMS.LiveDB() Then
                    varAccMgtEmailAddress = clsIMS.GetSQLItem("Select top 1 U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username = 'accountmanagement'")
                Else
                    varAccMgtEmailAddress = "David.Harper@dolfin.com"
                End If

                EmailMsg.ToRecipients.Add(varAccMgtEmailAddress)
                'EmailMsg.CcRecipients.Add("David.Harper@dolfin.com")

                EmailMsg.Body = New Microsoft.Exchange.WebServices.Data.MessageBody(CreateBody)
                EmailMsg.Send()
                'clsIMS.ExecuteString(clsIMS.GetNewOpenConnection, "update DolfinPayment set P_IsReceiptGenerated = 1 from DolfinPayment dp inner join vwDolfinPaymentVolopa dpv on dp.p_id = dpv.id")
                clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameVolopa, "Successful email sent in SendVolopaEmail - ReadSwift (sent to: " & varAccMgtEmailAddress & ")")
            End If
        Catch ex As Microsoft.Exchange.WebServices.Data.AutodiscoverLocalException
            'Seems that user does not work on Exchange 2007 SP1 or later?
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameVolopa, Err.Description & " - error in SendVolopaEmail - ReadSwift")
        End Try
        Return Service
    End Function

    Private Sub AddEmailBody(ByRef varTitle As String, ByRef varValue As String, ByRef CreateBody As String)
        CreateBody = CreateBody & "<span style ='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>" & varTitle & ":</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'>" & vbCrLf &
            "<p Class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
        If varTitle = "Total Score" Or varTitle = "Risk Score" Then
            CreateBody = CreateBody & "<span style ='font-size:12.0pt;font-family:""Arial"",""sans-serif""'><b>" & varValue & "</b><o:p></o:p></span></p></td></tr>" & vbCrLf
        Else
            CreateBody = CreateBody & "<span style ='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & varValue & "<o:p></o:p></span></p></td></tr>" & vbCrLf
        End If
        CreateBody = CreateBody & "<tr><td width=236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
    End Sub

    Sub CorporateActionsAddAppt()
        Dim Service As Microsoft.Exchange.WebServices.Data.ExchangeService = Nothing
        Dim Lst As Dictionary(Of String, String)

        Try
            Service = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2010)
            Service.TraceFlags = Microsoft.Exchange.WebServices.Data.TraceFlags.None
            Service.TraceEnabled = True
            Service.UseDefaultCredentials = False
            Service.Credentials = New Microsoft.Exchange.WebServices.Data.WebCredentials(_cOperationsFilesEmailAddress, _cOperationsFilesEmailPassword)
            Service.Url = New Uri(_cOutlookEmailPath)

            Lst = clsIMS.GetDataToList("select cac_id,cac_name,cac_reference,cac_relatedreference,cac_accountno,cac_description,cac_swiftcode,cac_filename,cac_indicator1,cac_indicator2,cac_isin,cast(cac_settledamount as money) as cac_settledamount,cac_exdate
                                        from DolfinPaymentCorporateActionsCalendar where cac_runtype = 1 and cac_entereddate is null")

            If Lst IsNot Nothing Then
                For i = 0 To (Lst.Count / 13) - 1
                    Dim appt As New Microsoft.Exchange.WebServices.Data.Appointment(Service)
                    appt.Subject = "ExDate: " & Lst.Item("cac_description_" & i.ToString).ToString & " - " & Lst.Item("cac_name_" & i.ToString).ToString & " (" & Lst.Item("cac_isin_" & i.ToString).ToString & ")"
                    appt.Body = "Filename: " & Lst.Item("cac_filename_" & i.ToString).ToString & vbNewLine & vbNewLine &
                    "Swift Code: " & Lst.Item("cac_swiftcode_" & i.ToString).ToString & vbNewLine & vbNewLine &
                    "Account No: " & Lst.Item("cac_accountno_" & i.ToString).ToString & vbNewLine & vbNewLine &
                    "Amount: " & Lst.Item("cac_settledamount_" & i.ToString).ToString & vbNewLine & vbNewLine &
                    "Reference: " & Lst.Item("cac_reference_" & i.ToString).ToString & vbNewLine & vbNewLine &
                    "Related Reference: " & Lst.Item("cac_relatedreference_" & i.ToString).ToString
                    appt.Start = New DateTime(CDate(Lst.Item("cac_exdate_" & i.ToString)).Year, CDate(Lst.Item("cac_exdate_" & i.ToString)).Month, CDate(Lst.Item("cac_exdate_" & i.ToString)).Day, 9, 0, 0)
                    appt.End = appt.Start.AddMinutes(1)
                    appt.Save()
                    clsIMS.ExecuteString(clsIMS.GetNewOpenConnection, "update DolfinPaymentCorporateActionsCalendar set cac_entereddate = getdate(),cac_enteredby = 'SwiftRead' where cac_id = " & Lst.Item("cac_id_" & i.ToString).ToString)
                Next
            End If

            Lst = clsIMS.GetDataToList("select cac_id,cac_name,cac_accountno,cac_description,cac_swiftcode,cac_isin,cac_dolfindeadlinedate,cast(cac_settledamount as money) as cac_settledamount
                                        from DolfinPaymentCorporateActionsCalendar where cac_runtype = 2 and cac_entereddate is null")

            If Lst IsNot Nothing Then
                For i = 0 To (Lst.Count / 8) - 1
                    Dim appt As New Microsoft.Exchange.WebServices.Data.Appointment(Service)
                    appt.Subject = "DEADLINE DAY: " & Lst.Item("cac_description_" & i.ToString).ToString & " - " & Lst.Item("cac_name_" & i.ToString).ToString & " (" & Lst.Item("cac_isin_" & i.ToString).ToString & ")"
                    appt.Body = "Swift Code: " & Lst.Item("cac_swiftcode_" & i.ToString).ToString & vbNewLine & vbNewLine &
                    "Account No: " & Lst.Item("cac_accountno_" & i.ToString).ToString & vbNewLine & vbNewLine &
                    "Amount: " & Lst.Item("cac_settledamount_" & i.ToString).ToString & vbNewLine & vbNewLine
                    appt.Start = New DateTime(CDate(Lst.Item("cac_dolfindeadlinedate_" & i.ToString)).Year, CDate(Lst.Item("cac_dolfindeadlinedate_" & i.ToString)).Month, CDate(Lst.Item("cac_dolfindeadlinedate_" & i.ToString)).Day, 9, 0, 0)
                    appt.End = appt.Start.AddMinutes(1)
                    appt.Save()
                    clsIMS.ExecuteString(clsIMS.GetNewOpenConnection, "update DolfinPaymentCorporateActionsCalendar set cac_entereddate = getdate(),cac_enteredby = 'SwiftRead' where cac_id = " & Lst.Item("cac_id_" & i.ToString).ToString)
                Next
            End If

        Catch ex As Microsoft.Exchange.WebServices.Data.AutodiscoverLocalException
            'Seems that user does not work on Exchange 2007 SP1 or later?
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, Err.Description & " - error in CorporateActionsAddAppt")
        End Try

    End Sub

    Public Function PopulateVolopaExcelSheetOpenXML(ByRef varFileName As String, ByRef ReportTitle As String, ByRef varSQL As String) As Boolean
        Dim varRow As Integer
        Dim cell As Cell
        Dim xlsWB As SpreadsheetDocument

        Try
            PopulateVolopaExcelSheetOpenXML = False

            Dim Lst As List(Of String) = clsIMS.GetMultipleDataToList(varSQL)
            Dim varStartRow As Integer = 6

            If Lst IsNot Nothing Then
                varFileName = _filePathVolopaTemplate & "VolopaPayments" & Now.ToString("yyyyMMddHHmm") & ".xlsx"
                If IO.File.Exists(varFileName) Then
                    IO.File.Delete(varFileName)
                End If
                IO.File.Copy(_filePathVolopaTemplate & "VolopaPayments.xlsx", varFileName)
                xlsWB = SpreadsheetDocument.Open(varFileName, True)

                Using (xlsWB)
                    Dim worksheetPart As WorksheetPart = xlsWB.WorkbookPart.GetPartById("rId1")
                    cell = InsertCellInWorksheet("B", 3, worksheetPart)
                    cell.CellValue = New CellValue(ReportTitle & " Report")
                    cell.DataType = CellValues.String
                    cell = InsertCellInWorksheet("B", 4, worksheetPart)
                    cell.CellValue = New CellValue("DATE: " & Now().ToString("dd/MM/yyyy"))
                    cell.DataType = CellValues.String

                    For varRow = 0 To Lst.Count - 1 Step 5
                        cell = InsertCellInWorksheet("B", varStartRow + (varRow / 5), worksheetPart)
                        cell.CellValue = New CellValue(CDate(Lst(varRow)).ToString("dd/MM/yyyy"))
                        cell.DataType = CellValues.String
                        cell = InsertCellInWorksheet("C", varStartRow + (varRow / 5), worksheetPart)
                        cell.CellValue = New CellValue(Lst(varRow + 1))
                        cell.DataType = CellValues.String
                        cell = InsertCellInWorksheet("D", varStartRow + (varRow / 5), worksheetPart)
                        cell.CellValue = New CellValue(Lst(varRow + 2))
                        cell.DataType = CellValues.String
                        cell = InsertCellInWorksheet("E", varStartRow + (varRow / 5), worksheetPart)
                        cell.CellValue = New CellValue(Lst(varRow + 3))
                        cell.DataType = CellValues.String
                        cell = InsertCellInWorksheet("F", varStartRow + (varRow / 5), worksheetPart)
                        cell.CellValue = New CellValue(Lst(varRow + 4))
                        cell.DataType = CellValues.String
                        cell = InsertCellInWorksheet("G", varStartRow + (varRow / 5), worksheetPart)
                        cell.CellValue = New CellValue("Dolfin")
                        cell.DataType = CellValues.String
                    Next
                    xlsWB.Save()

                End Using
                IO.File.Copy(varFileName, _filePathVolopaFTP & Dir(varFileName))
            End If

        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, Err.Description & " - error in PopulateVolopaExcelSheetOpenXML")
            PopulateVolopaExcelSheetOpenXML = False
        Finally
            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Successfully created Volopa Excel sheet - PopulateVolopaExcelSheetOpenXML")
            PopulateVolopaExcelSheetOpenXML = True
        End Try
    End Function

    Private Function InsertCellInWorksheet(ByVal columnName As String, ByVal rowIndex As UInteger, ByVal worksheetPart As WorksheetPart) As Cell
        Dim worksheet As Worksheet = worksheetPart.Worksheet
        Dim sheetData As SheetData = worksheet.GetFirstChild(Of SheetData)()
        Dim cellReference As String = (columnName + rowIndex.ToString())

        Dim row As Row
        row = sheetData.Elements(Of Row).Where(Function(r) r.RowIndex.Value = rowIndex).First()
        Return row.Elements(Of Cell).Where(Function(c) c.CellReference.Value = cellReference).First()
    End Function

    Public Sub BFLSave()
        Dim Service As Microsoft.Exchange.WebServices.Data.ExchangeService = Nothing

        Try
            If IO.Directory.Exists(_cBFLPath) Then
                Dim varFiles As String() = IO.Directory.GetFiles(_cBFLPath, Now.Date.ToString("yyyyMMdd") & "*.txt", System.IO.SearchOption.TopDirectoryOnly)
                If BFLGetAttachments(varFiles) Then
                    'If varFiles.Length <> 6 Then
                    'For Each vfile As String In varFiles
                    'IO.File.Delete(vfile)
                    'Next
                    Service = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2010)
                    Service.TraceFlags = Microsoft.Exchange.WebServices.Data.TraceFlags.None
                    Service.TraceEnabled = True
                    Service.UseDefaultCredentials = False
                    Service.Credentials = New Microsoft.Exchange.WebServices.Data.WebCredentials(_cOperationsFilesEmailAddress, _cOperationsFilesEmailPassword)
                    Service.Url = New Uri(_cOutlookEmailPath)


                    Dim SearchFilterCollection As New List(Of Microsoft.Exchange.WebServices.Data.SearchFilter)
                    SearchFilterCollection.Add(New Microsoft.Exchange.WebServices.Data.SearchFilter.ContainsSubstring(Microsoft.Exchange.WebServices.Data.ItemSchema.Subject, "Eclipse Report:"))

                    ' Create the search filter.
                    Dim searchFilter As Microsoft.Exchange.WebServices.Data.SearchFilter = New Microsoft.Exchange.WebServices.Data.SearchFilter.SearchFilterCollection(Microsoft.Exchange.WebServices.Data.LogicalOperator.Or, SearchFilterCollection.ToArray())

                    ' Create a view with a page size of 50.
                    Dim view As New Microsoft.Exchange.WebServices.Data.ItemView(10)

                    'Identify the Subject and DateTimeReceived properties to return.
                    'Indicate that the base property will be the item identifier
                    view.PropertySet = (New Microsoft.Exchange.WebServices.Data.PropertySet(Microsoft.Exchange.WebServices.Data.BasePropertySet.IdOnly, Microsoft.Exchange.WebServices.Data.ItemSchema.Subject, Microsoft.Exchange.WebServices.Data.ItemSchema.DateTimeReceived))

                    ' Order the search results by the DateTimeReceived in descending order.
                    view.OrderBy.Add(Microsoft.Exchange.WebServices.Data.ItemSchema.DateTimeReceived, Microsoft.Exchange.WebServices.Data.SortDirection.Descending)

                    ' Set the traversal to shallow. (Shallow is the default option; other options are Associated and SoftDeleted.)
                    view.Traversal = Microsoft.Exchange.WebServices.Data.ItemTraversal.Shallow

                    ' Send the request to search the Inbox and get the results.
                    Dim findResults As Microsoft.Exchange.WebServices.Data.FindItemsResults(Of Microsoft.Exchange.WebServices.Data.Item) = Service.FindItems(Microsoft.Exchange.WebServices.Data.WellKnownFolderName.Inbox, searchFilter, view)

                    ' Process each item.
                    Dim varAscii As Integer = 0
                    For Each item As Microsoft.Exchange.WebServices.Data.Item In findResults.Items
                        If TypeOf item Is Microsoft.Exchange.WebServices.Data.EmailMessage Then
                            item.Load()
                            Dim varEmailSentTime As Date = TryCast(item, Microsoft.Exchange.WebServices.Data.EmailMessage).DateTimeSent
                            If varEmailSentTime.Date.ToString("dd/MM/yyyy") = Now.Date.ToString("dd/MM/yyyy") Then
                                Dim fileattached2 As Microsoft.Exchange.WebServices.Data.FileAttachment
                                For i = 0 To item.Attachments.Count - 1
                                    fileattached2 = item.Attachments(i)
                                    If item.Attachments(0).Name = "ECMIR13.txt" Then
                                        If Not IO.File.Exists(_cBFLPath & varEmailSentTime.Date.ToString("yyyyMMdd") & Microsoft.VisualBasic.Strings.Chr(65 + varAscii) & item.Attachments(0).Name) Then
                                            fileattached2.Load(_cBFLPath & varEmailSentTime.Date.ToString("yyyyMMdd") & Microsoft.VisualBasic.Strings.Chr(65 + varAscii) & item.Attachments(0).Name)
                                            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, _cBFLPath & varEmailSentTime.Date.ToString("yyyyMMdd") & Microsoft.VisualBasic.Strings.Chr(65 + varAscii) & item.Attachments(0).Name & " - file saved from BFLSave - ReadSwift")
                                            varAscii += 1
                                        End If
                                    ElseIf Not IO.File.Exists(_cBFLPath & varEmailSentTime.Date.ToString("yyyyMMdd") & item.Attachments(0).Name) Then
                                        fileattached2.Load(_cBFLPath & varEmailSentTime.Date.ToString("yyyyMMdd") & item.Attachments(0).Name)
                                        clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, _cBFLPath & varEmailSentTime.Date.ToString("yyyyMMdd") & item.Attachments(0).Name & " - file saved from BFLSave - ReadSwift")
                                    End If
                                    fileattached2 = Nothing
                                Next
                            End If
                        End If

                    Next
                Else
                    clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, _cBFLPath & Now.Date.ToString("yyyyMMdd") & " - files already saved from BFLSave - ReadSwift")
                End If
            Else
                clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, _cBFLPath & " - directory does not exist - BFLSave - ReadSwift")
            End If
        Catch ex As Microsoft.Exchange.WebServices.Data.AutodiscoverLocalException
            'Seems that user does not work on Exchange 2007 SP1 or later?
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, Err.Description & " - error in BFLSave")
        End Try
        Service = Nothing
    End Sub

    Public Function BFLGetAttachments(ByVal varFiles As String()) As Boolean
        Dim BFLGetAttachmentsCount As Integer = 0
        BFLGetAttachments = False

        If varFiles.Count > 0 Then
            For Each vFile In varFiles
                If vFile.Contains("ECMIR13.txt") Then
                    BFLGetAttachmentsCount += 1
                ElseIf vFile.Contains("ecmir15.txt") Then
                    BFLGetAttachmentsCount += 1
                ElseIf vFile.Contains("ECMIR34.txt") Then
                    BFLGetAttachmentsCount += 1
                ElseIf vFile.Contains("ECMIR16.txt") Then
                    BFLGetAttachmentsCount += 1
                End If
            Next

            'need 2x ECMIR13
            If BFLGetAttachmentsCount < 5 Then
                BFLGetAttachments = True
            End If
        Else
            BFLGetAttachments = True
        End If

    End Function

    Function SendGPPTransactionEmail(ByVal varDocNo As String, ByVal FullFileName As String, ByVal varAccMgtEmailAddress As String, ByVal varSubject As String, ByVal varBody As String) As Microsoft.Exchange.WebServices.Data.ExchangeService
        Dim Service As Microsoft.Exchange.WebServices.Data.ExchangeService = Nothing

        Try
            Service = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2010)
            Service.TraceEnabled = True
            Service.UseDefaultCredentials = False
            Service.Credentials = New Microsoft.Exchange.WebServices.Data.WebCredentials(_cApprovalEmailAddress, _cApprovalEmailPassword)
            Service.Url = New Uri(_cApprovalEmailPath)

            Dim EmailMsg As New Microsoft.Exchange.WebServices.Data.EmailMessage(Service)

            Dim CreateBody As String = ""
            CreateBody = varBody
            EmailMsg.Subject = varSubject

            EmailMsg.ToRecipients.Add(varAccMgtEmailAddress)
            EmailMsg.CcRecipients.Add("David.Harper@dolfin.com")
            EmailMsg.CcRecipients.Add("Roger.ababao@dolfin.com")

            If FullFileName <> "" Then
                EmailMsg.Attachments.AddFileAttachment(FullFileName)
            End If

            EmailMsg.Body = New Microsoft.Exchange.WebServices.Data.MessageBody(CreateBody)
            EmailMsg.Send()
            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameGPP, "Transaction email sent from SendFailedGPPTransaction - ReadSwift (sent to: " & varAccMgtEmailAddress & ")")
        Catch ex As Microsoft.Exchange.WebServices.Data.AutodiscoverLocalException
            'Seems that user does not work on Exchange 2007 SP1 or later?
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameGPP, Err.Description & " - error in SendFailedGPPTransaction - ReadSwift")
        End Try
        Return Service
    End Function

End Class