﻿Public Class clsSwiftEdit

    Private _varSwiftType As String
    Private _varSwiftCode As String
    Private _rSBNRUMMSafeAssigned As Boolean = False
    Private _cancelSwift As Boolean = False
    Private _varSwiftFileEditSourcePath As String = clsIMS.GetFilePath("SWIFTEditSource")
    Private _varSwiftFileEditDestPath As String = clsIMS.GetFilePath("SWIFTEditDest")
    Private _varSwiftFileEditArchivePath As String = clsIMS.GetFilePath("SWIFTEditArchive")
    Private _varSwiftFileEditedPath As String = clsIMS.GetFilePath("SWIFTEditEdited")
    Private _varGPPFilePath As String = clsIMS.GetFilePath("GPP")

    Public Sub EditSwift()
        Dim objSwiftReader As IO.StreamReader = Nothing
        Dim objSwiftWriter As IO.StreamWriter = Nothing
        Dim varLine As String = ""
        Dim varDocNo As String = ""

        clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEditSwift, "Starting Edit Swift - EditSwift")
        Try
            If IO.Directory.Exists(_varSwiftFileEditSourcePath) And IO.Directory.Exists(_varSwiftFileEditDestPath) And IO.Directory.Exists(_varSwiftFileEditArchivePath) And IO.Directory.Exists(_varSwiftFileEditedPath) Then
                clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEditSwift, "Checking " & clsIMS.GetFilePath("SWIFTEditSource") & " for Swift messages - EditSwift")
                For Each SwiftFinFile As String In System.IO.Directory.GetFiles(_varSwiftFileEditSourcePath)

                    Dim SwiftFinFileNew = Replace(SwiftFinFile, ".fin", "_Edited.fin")
                    If IO.File.Exists(SwiftFinFileNew) Then
                        IO.File.Delete(SwiftFinFileNew)
                    End If

                    Dim swiftstreamRead As New IO.FileStream(SwiftFinFile, IO.FileMode.Open, IO.FileAccess.Read)
                    objSwiftReader = New IO.StreamReader(swiftstreamRead)

                    Dim swiftstreamWrite As New IO.FileStream(SwiftFinFileNew, IO.FileMode.CreateNew, IO.FileAccess.Write)
                    objSwiftWriter = New IO.StreamWriter(swiftstreamWrite)

                    Dim vSwiftNum As Integer = 0
                    _rSBNRUMMSafeAssigned = False
                    _cancelSwift = False
                    Do While objSwiftReader.Peek() >= 0
                        varLine = objSwiftReader.ReadLine()

                        If Left(varLine, 2) = "-}" Then
                            If _varSwiftCode = "GPPLGB21" Then
                                If Left(varLine, 3) = "-}$" Then
                                    If vSwiftNum = 0 Then
                                        varLine = Right(varLine, Len(varLine) - 3)
                                    End If
                                Else
                                    objSwiftWriter.WriteLine(varLine)
                                End If
                                If varDocNo <> "" And Not _cancelSwift Then
                                    SendGPPFile(_varSwiftCode, varDocNo)
                                End If
                            Else
                                vSwiftNum = vSwiftNum + 1
                            End If
                        End If
                        EditSwiftFile(objSwiftWriter, SwiftFinFile, varLine, _varSwiftCode, varDocNo, _cancelSwift)
                    Loop
                    objSwiftReader.Close()
                    objSwiftReader = Nothing
                    objSwiftWriter.Close()
                    objSwiftWriter = Nothing

                    If vSwiftNum = 0 Then
                        FileAction(2, SwiftFinFileNew)
                        'IO.File.Delete(SwiftFinFileNew)
                        'clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEditSwift, "Removed " & SwiftFinFileNew & " - SwiftRead (FileAction)")
                    End If
                    FileAction(0, SwiftFinFile, _varSwiftFileEditArchivePath & Dir(SwiftFinFile))
                    FileAction(1, SwiftFinFileNew, _varSwiftFileEditedPath & Dir(SwiftFinFileNew))
                    FileAction(0, SwiftFinFileNew, _varSwiftFileEditDestPath & Dir(SwiftFinFileNew))
                Next
            Else
                clsIMS.AddAudit(3, cAuditGroupNameEditSwift, _varSwiftFileEditSourcePath & "," & _varSwiftFileEditArchivePath & "," & _varSwiftFileEditDestPath & " - directories has not been found - problem in EditSwift")
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, cAuditGroupNameEditSwift, Err.Description & " - error in EditSwift")
        End Try
    End Sub

    Public Sub EditSwiftFile(ByRef objSwiftWriter As IO.StreamWriter, ByRef SwiftFinFile As String, ByRef varLine As String, ByRef varSwiftCode As String, ByRef varDocNo As String, ByRef CancelSwift As Boolean)
        Const cSwiftDocNo As String = ":20C::SEME//"
        Const cSwiftFIAC As String = ":16S:FIAC"
        Const cSwiftSAFE As String = ":94F::SAFE//"
        Const cSwiftTRAD As String = ":22F::SETR//TRAD"

        If InStr(1, varLine, "{2:", vbTextCompare) <> 0 Then
            Dim varSwiftCodeStart = InStr(1, varLine, "{2:", vbTextCompare)
            _varSwiftType = Mid(varLine, varSwiftCodeStart + 4, 3)
            varSwiftCode = Mid(varLine, varSwiftCodeStart + 7, 8)
            varDocNo = ""
            _rSBNRUMMSafeAssigned = False
            CancelSwift = False
        End If

        If (_varSwiftType = 541 Or _varSwiftType = 543) And (varSwiftCode = "INGBPLPW" Or varSwiftCode = "IRVTGB2X" Or varSwiftCode = "RSBNRUMM" Or varSwiftCode = "GPPLGB21") Then
            If InStr(1, varLine, cSwiftDocNo, vbTextCompare) <> 0 Then
                varDocNo = Replace(varLine, cSwiftDocNo, "")
                varDocNo = Left(varDocNo, InStr(1, varDocNo, "/", vbTextCompare) - 1)
                clsIMS.AddAudit(1, 30, "Editing swift for SwiftFile: " & SwiftFinFile & ", SwiftCode: " & varSwiftCode & ", DocNo: " & varDocNo & " - EditSwiftFile (SwiftRead)")
            End If

            If varSwiftCode = "INGBPLPW" Then
                If InStr(1, varLine, cSwiftSAFE, vbTextCompare) <> 0 Then
                    Dim varISINCheck As String = clsIMS.GetSQLItem("Select t_isin from Transactions inner join titles on tr_tcode = t_code where tr_docno Like '" & varDocNo & "' and left(isnull(t_isin,'AA'),2) in ('US','DE','NL','CH','NO','BE','FR','ES','RU','IT','NO')")
                    Dim varSafeLine As String = getSafeLine(varISINCheck)
                    If varSafeLine <> "" Then
                        objSwiftWriter.WriteLine(varSafeLine)
                        clsIMS.AddAudit(1, 30, "Editing swift for SwiftFile: " & SwiftFinFile & ", SwiftCode: " & varSwiftCode & ", DocNo: " & varDocNo & ", ISIN: " & varISINCheck & ", Added line: " & varSafeLine & ", DocNo: " & varDocNo & " - EditSwiftFile (SwiftRead)")
                    Else
                        objSwiftWriter.WriteLine(varLine)
                        clsIMS.AddAudit(4, 30, "ISIN found but not changed for SwiftFile: " & SwiftFinFile & ", SwiftCode: " & varSwiftCode & ", DocNo: " & varDocNo & ", ISIN: " & varISINCheck & " - No isin data found to change in EditSwiftFile (SwiftRead)")
                    End If
                Else
                    objSwiftWriter.WriteLine(varLine)
                End If
            ElseIf varSwiftCode = "RSBNRUMM" Then
                If InStr(1, varLine, cSwiftFIAC, vbTextCompare) <> 0 Then
                    Dim varAccountNo As String = clsIMS.GetSQLItem("Select top 1 t_freefield1 from tranmaster inner join transactions on trm_setno = tr_setno inner join titles on trm_AccCode = t_code where t_Name1 Like '%Rosbank%Euroclear%' and tr_docno = '" & varDocNo & "' and isnull(t_freefield1,'')<>''")
                    If varAccountNo <> "" Then
                        objSwiftWriter.WriteLine(":97A::CASH//" & varAccountNo)
                        clsIMS.AddAudit(1, 30, "Editing swift for SwiftFile: " & SwiftFinFile & ", SwiftCode: " & varSwiftCode & ", DocNo: " & varDocNo & " - Added line: :97A::CASH//" & varAccountNo & " - EditSwiftFile (SwiftRead)")
                    Else
                        clsIMS.AddAudit(3, 30, "Editing swift for SwiftFile: " & SwiftFinFile & ", SwiftCode: " & varSwiftCode & ", DocNo: " & varDocNo & " - Could not retrieve the account number in t_FreeField1 - No data in EditSwiftFile (SwiftRead)")
                    End If
                    objSwiftWriter.WriteLine(varLine)
                    _rSBNRUMMSafeAssigned = True 'Added safe line, no other safe lines required
                ElseIf InStr(1, varLine, ":94F::SAFE//", vbTextCompare) = 0 And (InStr(1, varLine, ":97A::SAFE//", vbTextCompare) = 0 Or (InStr(1, varLine, ":97A::SAFE//", vbTextCompare) <> 0 And Not _rSBNRUMMSafeAssigned)) Then
                    objSwiftWriter.WriteLine(varLine)
                Else
                    clsIMS.AddAudit(1, 30, "Editing swift for SwiftFile: " & SwiftFinFile & ", SwiftCode: " & varSwiftCode & ", DocNo: " & varDocNo & " - Removed safe line: " & varLine)
                End If
            ElseIf varSwiftCode = "IRVTGB2X" Then
                If InStr(1, varLine, cSwiftTRAD, vbTextCompare) <> 0 Then
                    objSwiftWriter.WriteLine(varLine)
                    objSwiftWriter.WriteLine(":22F::STCO//NPAR")
                    clsIMS.AddAudit(1, 30, "Editing swift for SwiftFile: " & SwiftFinFile & ", SwiftCode: " & varSwiftCode & ", DocNo: " & varDocNo & " - Added line: :22F::STCO//NPAR - EditSwiftFile (SwiftRead)")
                Else
                    objSwiftWriter.WriteLine(varLine)
                End If
            ElseIf varSwiftCode = "GPPLGB21" Then
                If varLine = ":23G:CANC" Then
                    CancelSwift = True
                End If
            Else
                objSwiftWriter.WriteLine(varLine)
            End If
        Else
            objSwiftWriter.WriteLine(varLine)
        End If
    End Sub

    Public Function getSafeLine(ByVal varISINCheck As String) As String

        'PLEASE CHANGE QUERY IF ADDING MORE ITEMS TO THIS LIST
        Select Case Left(varISINCheck, 5)
            Case "US192"
                Return ":94F::SAFE//NCSD/FRNYUS33"
            Case Else
                Select Case Left(varISINCheck, 2)
                    Case "US"
                        Return ":94F::SAFE//NCSD/DTCYUS33"
                    Case "DE"
                        Return ":94F::SAFE//NCSD/DAKVDEFF"
                    Case "NL"
                        Return ":94F::SAFE//NCSD/NECINL2A"
                    Case "CH"
                        Return ":94F::SAFE//NCSD/INSECHZZ"
                    Case "SE"
                        Return ":94F::SAFE//NCSD/VPCSSESS"
                    Case "BE"
                        Return ":94F::SAFE//NCSD/CIKBBEBBCLR"
                    Case "FR"
                        Return ":94F::SAFE//NCSD/SICVFRPP"
                    Case "ES"
                        Return ":94F::SAFE//NCSD/IBRCESMMXXX"
                    Case "RU"
                        Return ":94F::SAFE//NCSD/NADCRUMM"
                    Case "IT"
                        Return ":94F::SAFE//NCSD/MOTIITMMXXX"
                    Case "NO"
                        Return ":94F::SAFE//NCSD/VPSNNOKK"
                End Select
        End Select

    End Function

    Private Sub SendGPPFile(ByRef varSwiftCode As String, ByVal varDocNo As String)
        Dim varGPPData As String = ""
        Dim GPPFileName As String = ""

        If clsIMS.GetGPPdata(varDocNo, varGPPData) Then
            GPPFileName = varDocNo & Now.ToString("_yyyyMMddHHmmssfff") & ".csv"
            CreateFile(_varGPPFilePath & "Upload\", GPPFileName, varGPPData)
        Else
            Dim varEmailAddress As String = ""

            varEmailAddress = clsIMS.GetSQLItem("Select top 1 U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username in ('operationsfiles')")
            Dim varBody As String = "Please check IMS SSI's in brokers for transaction " & varDocNo
            Dim varSubject As String = varDocNo & "GPP Mapping not found for DocNo: " & varDocNo
            clsEmail.SendGPPTransactionEmail(varDocNo, "", varEmailAddress, varSubject, varBody)
            clsIMS.AddAudit(3, cAuditGroupNameGPP, "GPP Mappings - cound not create file from DolfinPaymentGetGPPData for " & varDocNo & " - SendGPPFile - SwiftRead")
        End If
    End Sub

    Public Function CreateFile(ByVal FilePath As String, ByVal Filename As String, ByVal varData As String) As Boolean
        CreateFile = False
        Try
            If Not IO.Directory.Exists(FilePath) Then
                IO.Directory.CreateDirectory(FilePath)
            End If

            Using varStreamWriter As IO.StreamWriter = New IO.StreamWriter(FilePath & Filename, False)
                varStreamWriter.Write(varData)
            End Using
            CreateFile = True
            clsIMS.AddAudit(1, 30, "Created file and sent to " & FilePath & Filename & " (CreateGPPFile)", 0)

        Catch ex As Exception
            clsIMS.AddAudit(3, 30, ex.Message & " - Failure creating file (CreateFile)", 0)
        End Try
    End Function


End Class