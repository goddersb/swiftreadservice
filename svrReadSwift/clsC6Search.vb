﻿Imports Newtonsoft.Json
Imports System.Net.Http
Imports System.Net.Http.Headers
Imports System.Text
Imports System.Configuration

Public Class clsC6Search
    Private Sub CreatePersonColumns(ByRef TblPerson As DataTable)
        AddDataColumn(TblPerson, "score", "System.Int32")
        AddDataColumn(TblPerson, "id", "System.String")
        AddDataColumn(TblPerson, "title", "System.String")
        AddDataColumn(TblPerson, "alternativeTitle", "System.String")
        AddDataColumn(TblPerson, "forename", "System.String")
        AddDataColumn(TblPerson, "middlename", "System.String")
        AddDataColumn(TblPerson, "surname", "System.String")
        AddDataColumn(TblPerson, "dateOfBirth", "System.DateTime")
        AddDataColumn(TblPerson, "yearOfBirth", "System.String")
        AddDataColumn(TblPerson, "dateOfDeath", "System.DateTime")
        AddDataColumn(TblPerson, "yearOfDeath", "System.String")
        AddDataColumn(TblPerson, "isDeceased", "System.String")
        AddDataColumn(TblPerson, "gender", "System.String")
        AddDataColumn(TblPerson, "nationality", "System.String")
        AddDataColumn(TblPerson, "imageURL", "System.String")
        AddDataColumn(TblPerson, "telephoneNumber", "System.String")
        AddDataColumn(TblPerson, "faxNumber", "System.String")
        AddDataColumn(TblPerson, "mobileNumber", "System.String")
        AddDataColumn(TblPerson, "email", "System.String")
        AddDataColumn(TblPerson, "pepLevel", "System.Int32")
        AddDataColumn(TblPerson, "isPEP", "System.String")
        AddDataColumn(TblPerson, "isSanctionsCurrent", "System.String")
        AddDataColumn(TblPerson, "isSanctionsPrevious", "System.String")
        AddDataColumn(TblPerson, "isLawEnforcement", "System.String")
        AddDataColumn(TblPerson, "isFinancialregulator", "System.String")
        AddDataColumn(TblPerson, "isDisqualifiedDirector", "System.String")
        AddDataColumn(TblPerson, "isInsolvent", "System.String")
        AddDataColumn(TblPerson, "isAdverseMedia", "System.String")
    End Sub

    Private Sub PopulatePerson(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblPerson As DataTable, ByRef HTMLBody As String)
        On Error Resume Next
        Dim personrow As DataRow
        personrow = TblPerson.NewRow()
        clsEmail.BuildHTMLString("New Person", "", HTMLBody, True)
        personrow("score") = jsonBody.Item("score").ToString()
        clsEmail.BuildHTMLString("score", personrow("score").ToString(), HTMLBody)
        personrow("id") = jsonBody.Item("person").item("id").ToString()
        clsEmail.BuildHTMLString("id", personrow("id").ToString(), HTMLBody)
        personrow("title") = Left(jsonBody.Item("person").item("title").item("description").ToString(), 100)
        clsEmail.BuildHTMLString("title", personrow("title").ToString(), HTMLBody)
        personrow("alternativeTitle") = Left(jsonBody.Item("person").item("alternativeTitle").ToString(), 100)
        clsEmail.BuildHTMLString("alternativeTitle", personrow("alternativeTitle").ToString(), HTMLBody)
        personrow("forename") = Left(jsonBody.Item("person").item("forename").ToString(), 200)
        clsEmail.BuildHTMLString("forename", personrow("forename").ToString(), HTMLBody)
        personrow("middlename") = Left(jsonBody.Item("person").item("middlename").ToString(), 200)
        clsEmail.BuildHTMLString("middlename", personrow("middlename").ToString(), HTMLBody)
        personrow("surname") = Left(jsonBody.Item("person").item("surname").ToString(), 200)
        clsEmail.BuildHTMLString("surname", personrow("surname").ToString(), HTMLBody)
        personrow("dateOfBirth") = IIf(Not IsDate(jsonBody.Item("person").item("dateOfBirth")), DBNull.Value, jsonBody.Item("person").item("dateOfBirth"))
        clsEmail.BuildHTMLString("dateOfBirth", personrow("dateOfBirth").ToString(), HTMLBody)
        personrow("yearOfBirth") = Left(jsonBody.Item("person").item("yearOfBirth").ToString(), 5)
        clsEmail.BuildHTMLString("yearOfBirth", personrow("yearOfBirth").ToString(), HTMLBody)
        personrow("dateOfDeath") = IIf(Not IsDate(jsonBody.Item("person").item("dateOfDeath")), DBNull.Value, jsonBody.Item("person").item("dateOfBirth"))
        clsEmail.BuildHTMLString("dateOfDeath", personrow("dateOfDeath").ToString(), HTMLBody)
        personrow("yearOfDeath") = Left(jsonBody.Item("person").item("yearOfDeath").ToString(), 5)
        clsEmail.BuildHTMLString("yearOfDeath", personrow("yearOfDeath").ToString(), HTMLBody)
        personrow("isDeceased") = jsonBody.Item("person").item("isDeceased").ToString()
        clsEmail.BuildHTMLString("isDeceased", personrow("isDeceased"), HTMLBody)
        personrow("gender") = Left(jsonBody.Item("person").item("gender").ToString(), 10)
        clsEmail.BuildHTMLString("gender", personrow("gender").ToString(), HTMLBody)
        personrow("nationality") = Left(jsonBody.Item("person").item("nationality").item("nationality").ToString(), 100)
        clsEmail.BuildHTMLString("nationality", personrow("nationality").ToString(), HTMLBody)
        personrow("imageURL") = Left(jsonBody.Item("person").item("imageURL").ToString(), 200)
        clsEmail.BuildHTMLString("imageURL", personrow("imageURL").ToString(), HTMLBody)
        personrow("telephoneNumber") = Left(jsonBody.Item("person").item("telephoneNumber").ToString(), 50)
        clsEmail.BuildHTMLString("telephoneNumber", personrow("telephoneNumber").ToString(), HTMLBody)
        personrow("faxNumber") = Left(jsonBody.Item("person").item("faxNumber").ToString(), 50)
        clsEmail.BuildHTMLString("faxNumber", personrow("faxNumber").ToString(), HTMLBody)
        personrow("mobileNumber") = Left(jsonBody.Item("person").item("mobileNumber").ToString(), 50)
        clsEmail.BuildHTMLString("mobileNumber", personrow("mobileNumber").ToString(), HTMLBody)
        personrow("email") = Left(jsonBody.Item("person").item("email").ToString(), 100)
        clsEmail.BuildHTMLString("email", personrow("email").ToString(), HTMLBody)
        personrow("pepLevel") = IIf(Not IsNumeric(jsonBody.Item("person").item("pepLevel")), 0, jsonBody.Item("person").item("pepLevel"))
        clsEmail.BuildHTMLString("score", personrow("score").ToString(), HTMLBody)
        personrow("isPEP") = jsonBody.Item("person").item("isPEP").ToString()
        clsEmail.BuildHTMLString("isPEP", personrow("isPEP").ToString(), HTMLBody)
        personrow("isSanctionsCurrent") = jsonBody.Item("person").item("isSanctionsCurrent").ToString()
        clsEmail.BuildHTMLString("isSanctionsCurrent", personrow("isSanctionsCurrent").ToString(), HTMLBody)
        personrow("isSanctionsPrevious") = jsonBody.Item("person").item("isSanctionsPrevious").ToString()
        clsEmail.BuildHTMLString("isSanctionsPrevious", personrow("isSanctionsPrevious").ToString(), HTMLBody)
        personrow("isLawEnforcement") = jsonBody.Item("person").item("isLawEnforcement").ToString()
        clsEmail.BuildHTMLString("isLawEnforcement", personrow("isLawEnforcement").ToString(), HTMLBody)
        personrow("isFinancialregulator") = jsonBody.Item("person").item("isFinancialregulator").ToString()
        clsEmail.BuildHTMLString("isFinancialregulator", personrow("isFinancialregulator").ToString(), HTMLBody)
        personrow("isDisqualifiedDirector") = jsonBody.Item("person").item("isDisqualifiedDirector").ToString()
        clsEmail.BuildHTMLString("isDisqualifiedDirector", personrow("isDisqualifiedDirector").ToString(), HTMLBody)
        personrow("isInsolvent") = jsonBody.Item("person").item("isInsolvent").ToString()
        clsEmail.BuildHTMLString("isInsolvent", personrow("isInsolvent").ToString(), HTMLBody)
        personrow("isAdverseMedia") = jsonBody.Item("person").item("isAdverseMedia").ToString()
        clsEmail.BuildHTMLString("isAdverseMedia", personrow("isAdverseMedia").ToString(), HTMLBody)
        TblPerson.Rows.Add(personrow)
    End Sub

    Private Sub CreateAddressesColumns(ByRef TblAddresses As DataTable)
        AddDataColumn(TblAddresses, "id", "System.String")
        AddDataColumn(TblAddresses, "address1", "System.String")
        AddDataColumn(TblAddresses, "address2", "System.String")
        AddDataColumn(TblAddresses, "address3", "System.String")
        AddDataColumn(TblAddresses, "address4", "System.String")
        AddDataColumn(TblAddresses, "city", "System.String")
        AddDataColumn(TblAddresses, "county", "System.String")
        AddDataColumn(TblAddresses, "postcode", "System.String")
        AddDataColumn(TblAddresses, "country", "System.String")
    End Sub

    Private Sub PopulateAddresses(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblAddresses As DataTable, ByVal varTypeName As String, ByRef HTMLBody As String)
        On Error Resume Next
        For loopAddress As Integer = 0 To jsonBody.Item(varTypeName).Item("addresses").count - 1
            Dim addressrow As DataRow
            addressrow = TblAddresses.NewRow()
            addressrow("id") = jsonBody.Item(varTypeName).item("id").ToString()
            addressrow("address1") = Left(jsonBody.Item(varTypeName).item("addresses").item(loopAddress).item("address1").ToString(), 200)
            clsEmail.BuildHTMLString("address1", addressrow("address1").ToString(), HTMLBody)
            addressrow("address2") = Left(jsonBody.Item(varTypeName).item("addresses").item(loopAddress).item("address2").ToString(), 200)
            clsEmail.BuildHTMLString("address2", addressrow("address2").ToString(), HTMLBody)
            addressrow("address3") = Left(jsonBody.Item(varTypeName).item("addresses").item(loopAddress).item("address3").ToString(), 200)
            clsEmail.BuildHTMLString("address3", addressrow("address3").ToString(), HTMLBody)
            addressrow("address4") = Left(jsonBody.Item(varTypeName).item("addresses").item(loopAddress).item("address4").ToString(), 200)
            clsEmail.BuildHTMLString("address4", addressrow("address4").ToString(), HTMLBody)
            addressrow("city") = Left(jsonBody.Item(varTypeName).item("addresses").item(loopAddress).item("city").ToString(), 100)
            clsEmail.BuildHTMLString("city", addressrow("city").ToString(), HTMLBody)
            addressrow("county") = Left(jsonBody.Item(varTypeName).item("addresses").item(loopAddress).item("county").ToString(), 100)
            clsEmail.BuildHTMLString("county", addressrow("county").ToString(), HTMLBody)
            addressrow("postcode") = Left(jsonBody.Item(varTypeName).item("addresses").item(loopAddress).item("postcode").ToString(), 10)
            clsEmail.BuildHTMLString("postcode", addressrow("postcode").ToString(), HTMLBody)
            addressrow("country") = Left(jsonBody.Item(varTypeName).item("addresses").item(loopAddress).item("country").item("name").ToString(), 255)
            clsEmail.BuildHTMLString("country", addressrow("country").ToString(), HTMLBody)
            TblAddresses.Rows.Add(addressrow)
        Next
    End Sub

    Private Sub CreateAliasColumns(ByRef TblAlias As DataTable)
        AddDataColumn(TblAlias, "id", "System.String")
        AddDataColumn(TblAlias, "title", "System.String")
        AddDataColumn(TblAlias, "alternativeTitle", "System.String")
        AddDataColumn(TblAlias, "forename", "System.String")
        AddDataColumn(TblAlias, "middlename", "System.String")
        AddDataColumn(TblAlias, "surname", "System.String")
        AddDataColumn(TblAlias, "businessName", "System.String")
    End Sub

    Private Sub PopulateAlias(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblAlias As DataTable, ByVal varTypeName As String, ByRef HTMLBody As String)
        On Error Resume Next
        For loopAlias As Integer = 0 To jsonBody.Item(varTypeName).Item("aliases").count - 1
            Dim aliasrow As DataRow
            aliasrow = TblAlias.NewRow()
            aliasrow("id") = jsonBody.Item(varTypeName).item("id").ToString()
            If varTypeName = "person" Then
                aliasrow("title") = Left(jsonBody.Item(varTypeName).item("aliases").item(loopAlias).item("title").ToString(), 100)
                clsEmail.BuildHTMLString("title", aliasrow("title").ToString(), HTMLBody)
                aliasrow("alternativeTitle") = Left(jsonBody.Item(varTypeName).item("aliases").item(loopAlias).item("alternativeTitle").ToString(), 100)
                clsEmail.BuildHTMLString("alternativeTitle", aliasrow("alternativeTitle").ToString(), HTMLBody)
                aliasrow("forename") = Left(jsonBody.Item(varTypeName).item("aliases").item(loopAlias).item("forename").ToString(), 200)
                clsEmail.BuildHTMLString("forename", aliasrow("forename").ToString(), HTMLBody)
                aliasrow("middlename") = Left(jsonBody.Item(varTypeName).item("aliases").item(loopAlias).item("middlename").ToString(), 200)
                clsEmail.BuildHTMLString("middlename", aliasrow("middlename").ToString(), HTMLBody)
                aliasrow("surname") = Left(jsonBody.Item(varTypeName).item("aliases").item(loopAlias).item("surname").ToString(), 200)
                clsEmail.BuildHTMLString("surname", aliasrow("surname").ToString(), HTMLBody)
                aliasrow("businessName") = "n/a"
                clsEmail.BuildHTMLString("businessName", aliasrow("businessName").ToString(), HTMLBody)
            ElseIf varTypeName = "business" Then
                aliasrow("businessName") = Left(jsonBody.Item(varTypeName).item("aliases").item(loopAlias).item("businessName").ToString(), 200)
                clsEmail.BuildHTMLString("businessName", aliasrow("businessName").ToString(), HTMLBody)
            End If
            TblAlias.Rows.Add(aliasrow)
        Next
    End Sub

    Private Sub CreateArticlesColumns(ByRef TblArticles As DataTable)
        AddDataColumn(TblArticles, "id", "System.String")
        AddDataColumn(TblArticles, "originalURL", "System.String")
        AddDataColumn(TblArticles, "dateCollected", "System.DateTime")
        AddDataColumn(TblArticles, "c6URL", "System.String")
        AddDataColumn(TblArticles, "ArticleCategory1", "System.String")
        AddDataColumn(TblArticles, "ArticleCategory2", "System.String")
        AddDataColumn(TblArticles, "ArticleCategory3", "System.String")
    End Sub

    Private Sub PopulateArticles(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblArticles As DataTable, ByVal varTypeName As String, ByRef HTMLBody As String)
        On Error Resume Next
        For loopArticles As Integer = 0 To jsonBody.Item(varTypeName).Item("articles").count - 1
            Dim Articlesrow As DataRow
            Articlesrow = TblArticles.NewRow()
            Articlesrow("id") = jsonBody.Item(varTypeName).item("id").ToString()
            Articlesrow("originalURL") = Left(jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("originalURL").ToString(), 1000)
            clsEmail.BuildHTMLString("originalURL", Articlesrow("originalURL").ToString(), HTMLBody)
            Articlesrow("dateCollected") = IIf(Not IsDate(jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("dateCollected").ToString()), DBNull.Value, jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("dateCollected").ToString())
            clsEmail.BuildHTMLString("dateCollected", Articlesrow("dateCollected").ToString(), HTMLBody)
            Articlesrow("c6URL") = Left(jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("c6URL").ToString(), 1000)
            clsEmail.BuildHTMLString("c6URL", Articlesrow("c6URL").ToString(), HTMLBody)
            If jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("categories").count > 0 Then
                Articlesrow("ArticleCategory1") = Left(jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("categories").item(0).item("name").ToString, 50)
                clsEmail.BuildHTMLString("ArticleCategory1", Articlesrow("ArticleCategory1").ToString(), HTMLBody)
            End If

            If jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("categories").count > 1 Then
                Articlesrow("ArticleCategory2") = Left(jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("categories").item(1).item("name").ToString, 50)
                clsEmail.BuildHTMLString("ArticleCategory2", Articlesrow("ArticleCategory2").ToString(), HTMLBody)
            End If

            If jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("categories").count > 2 Then
                Articlesrow("ArticleCategory3") = Left(jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("categories").item(2).item("name").ToString, 50)
                clsEmail.BuildHTMLString("ArticleCategory3", Articlesrow("ArticleCategory3").ToString(), HTMLBody)
            End If
            TblArticles.Rows.Add(Articlesrow)
        Next
    End Sub

    Private Sub CreateSanctionsColumns(ByRef TblSanctions As DataTable)
        AddDataColumn(TblSanctions, "id", "System.String")
        AddDataColumn(TblSanctions, "sanctionType", "System.String")
        AddDataColumn(TblSanctions, "isCurrent", "System.String")
    End Sub

    Private Sub PopulateSanctions(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblSanctions As DataTable, ByVal varTypeName As String, ByRef HTMLBody As String)
        On Error Resume Next
        For loopSanctions As Integer = 0 To jsonBody.Item(varTypeName).Item("sanctions").count - 1
            Dim Sanctionsrow As DataRow
            Sanctionsrow = TblSanctions.NewRow()
            Sanctionsrow("id") = jsonBody.Item(varTypeName).item("id").ToString()
            Sanctionsrow("sanctionType") = Left(jsonBody.Item(varTypeName).item("sanctions").item(loopSanctions).item("sanctionType").item("description").ToString(), 50)
            clsEmail.BuildHTMLString("sanctionType", Sanctionsrow("sanctionType").ToString(), HTMLBody)
            Sanctionsrow("isCurrent") = jsonBody.Item(varTypeName).item("sanctions").item(loopSanctions).item("isCurrent").ToString()
            clsEmail.BuildHTMLString("isCurrent", Sanctionsrow("isCurrent").ToString(), HTMLBody)
            TblSanctions.Rows.Add(Sanctionsrow)
        Next
    End Sub

    Private Sub CreateNotesColumns(ByRef TblNotes As DataTable)
        AddDataColumn(TblNotes, "id", "System.String")
        AddDataColumn(TblNotes, "dataSource", "System.String")
        AddDataColumn(TblNotes, "text", "System.String")
    End Sub

    Private Sub PopulateNotes(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblNotes As DataTable, ByVal varTypeName As String, ByRef HTMLBody As String)
        On Error Resume Next
        For loopNotes As Integer = 0 To jsonBody.Item(varTypeName).Item("notes").count - 1
            Dim Notesrow As DataRow
            Notesrow = TblNotes.NewRow()
            Notesrow("id") = jsonBody.Item(varTypeName).item("id").ToString()
            Notesrow("dataSource") = Left(jsonBody.Item(varTypeName).item("notes").item(loopNotes).item("dataSource").item("name").ToString(), 100)
            clsEmail.BuildHTMLString("dataSource", Notesrow("dataSource").ToString(), HTMLBody)
            Notesrow("text") = jsonBody.Item(varTypeName).item("notes").item(loopNotes).item("text").ToString()
            clsEmail.BuildHTMLString("text", Notesrow("text").ToString(), HTMLBody)
            TblNotes.Rows.Add(Notesrow)
        Next
    End Sub

    Private Sub CreateLinkedBusinessesColumns(ByRef TblLinkedBusinesses As DataTable)
        AddDataColumn(TblLinkedBusinesses, "id", "System.String")
        AddDataColumn(TblLinkedBusinesses, "businessName", "System.String")
        AddDataColumn(TblLinkedBusinesses, "position", "System.String")
        AddDataColumn(TblLinkedBusinesses, "linkDescription", "System.String")
    End Sub

    Private Sub PopulateLinkedBusinesses(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblLinkedBusinesses As DataTable, ByVal varTypeName As String, ByRef HTMLBody As String)
        On Error Resume Next
        For loopLinkedBusinesses As Integer = 0 To jsonBody.Item(varTypeName).Item("linkedBusinesses").count - 1
            Dim LinkedBusinessesrow As DataRow
            LinkedBusinessesrow = TblLinkedBusinesses.NewRow()
            LinkedBusinessesrow("id") = jsonBody.Item(varTypeName).item("id").ToString()
            LinkedBusinessesrow("businessName") = Left(jsonBody.Item(varTypeName).item("linkedBusinesses").item(loopLinkedBusinesses).item("businessName").ToString(), 200)
            clsEmail.BuildHTMLString("businessName", LinkedBusinessesrow("businessName").ToString(), HTMLBody)

            If varTypeName = "person" Then
                LinkedBusinessesrow("position") = Left(jsonBody.Item(varTypeName).item("linkedBusinesses").item(loopLinkedBusinesses).item("position").ToString(), 200)
                clsEmail.BuildHTMLString("position", LinkedBusinessesrow("position").ToString(), HTMLBody)
            ElseIf varTypeName = "business" Then
                LinkedBusinessesrow("linkDescription") = Left(jsonBody.Item(varTypeName).item("linkedBusinesses").item(loopLinkedBusinesses).item("linkDescription").ToString(), 200)
                clsEmail.BuildHTMLString("linkDescription", LinkedBusinessesrow("linkDescription").ToString(), HTMLBody)
            End If
            TblLinkedBusinesses.Rows.Add(LinkedBusinessesrow)
        Next
    End Sub

    Private Sub CreateLinkedPersonsColumns(ByRef TblLinkedPersons As DataTable)
        AddDataColumn(TblLinkedPersons, "id", "System.String")
        AddDataColumn(TblLinkedPersons, "personId", "System.String")
        AddDataColumn(TblLinkedPersons, "name", "System.String")
        AddDataColumn(TblLinkedPersons, "association", "System.String")
        AddDataColumn(TblLinkedPersons, "position", "System.String")
    End Sub

    Private Sub PopulateLinkedPersons(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblLinkedPersons As DataTable, ByVal varTypeName As String, ByRef HTMLBody As String)
        On Error Resume Next
        For loopLinkedPersons As Integer = 0 To jsonBody.Item(varTypeName).Item("linkedPersons").count - 1
            Dim LinkedPersonsrow As DataRow
            LinkedPersonsrow = TblLinkedPersons.NewRow()
            LinkedPersonsrow("id") = jsonBody.Item(varTypeName).item("id").ToString()
            LinkedPersonsrow("personId") = jsonBody.Item(varTypeName).item("linkedPersons").item(loopLinkedPersons).item("personId").ToString()
            clsEmail.BuildHTMLString("personId", LinkedPersonsrow("personId").ToString(), HTMLBody)
            LinkedPersonsrow("name") = Left(jsonBody.Item(varTypeName).item("linkedPersons").item(loopLinkedPersons).item("name").ToString(), 200)
            clsEmail.BuildHTMLString("name", LinkedPersonsrow("name").ToString(), HTMLBody)

            If varTypeName = "person" Then
                LinkedPersonsrow("association") = Left(jsonBody.Item(varTypeName).item("linkedPersons").item(loopLinkedPersons).item("association").ToString(), 200)
                clsEmail.BuildHTMLString("association", LinkedPersonsrow("association").ToString(), HTMLBody)
                LinkedPersonsrow("position") = ""
                clsEmail.BuildHTMLString("position", LinkedPersonsrow("position").ToString(), HTMLBody)
            ElseIf varTypeName = "business" Then
                LinkedPersonsrow("association") = ""
                clsEmail.BuildHTMLString("association", LinkedPersonsrow("association").ToString(), HTMLBody)
                LinkedPersonsrow("position") = Left(jsonBody.Item(varTypeName).item("linkedPersons").item(loopLinkedPersons).item("position").ToString(), 200)
                clsEmail.BuildHTMLString("position", LinkedPersonsrow("position").ToString(), HTMLBody)
            End If
            TblLinkedPersons.Rows.Add(LinkedPersonsrow)
        Next
    End Sub

    Private Sub CreatePoliticalPositionsColumns(ByRef TblPoliticalPositions As DataTable)
        AddDataColumn(TblPoliticalPositions, "id", "System.String")
        AddDataColumn(TblPoliticalPositions, "description", "System.String")
        AddDataColumn(TblPoliticalPositions, "from", "System.DateTime")
        AddDataColumn(TblPoliticalPositions, "to", "System.DateTime")
        AddDataColumn(TblPoliticalPositions, "country", "System.String")
    End Sub

    Private Sub PopulatePoliticalPositions(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblPoliticalPositions As DataTable, ByVal varTypeName As String, ByRef HTMLBody As String)
        On Error Resume Next
        For loopPoliticalPositions As Integer = 0 To jsonBody.Item(varTypeName).Item("politicalPositions").count - 1
            Dim PProw As DataRow
            PProw = TblPoliticalPositions.NewRow()
            PProw("id") = jsonBody.Item(varTypeName).item("id").ToString()
            PProw("description") = Left(jsonBody.Item(varTypeName).item("politicalPositions").item(loopPoliticalPositions).item("description").ToString(), 200)
            clsEmail.BuildHTMLString("description", PProw("description").ToString(), HTMLBody)
            PProw("from") = IIf(Not IsDate(jsonBody.Item(varTypeName).item("politicalPositions").item(loopPoliticalPositions).item("from").ToString()), DBNull.Value, jsonBody.Item(varTypeName).item("politicalPositions").item(loopPoliticalPositions).item("from").ToString())
            clsEmail.BuildHTMLString("from", PProw("from").ToString(), HTMLBody)
            PProw("to") = IIf(Not IsDate(jsonBody.Item(varTypeName).item("politicalPositions").item(loopPoliticalPositions).item("to").ToString()), DBNull.Value, jsonBody.Item(varTypeName).item("politicalPositions").item(loopPoliticalPositions).item("to").ToString())
            clsEmail.BuildHTMLString("to", PProw("to").ToString(), HTMLBody)
            PProw("country") = Left(jsonBody.Item(varTypeName).item("politicalPositions").item(loopPoliticalPositions).item("country").item("name").ToString(), 100)
            clsEmail.BuildHTMLString("country", PProw("country").ToString(), HTMLBody)
            TblPoliticalPositions.Rows.Add(PProw)
        Next
    End Sub

    Private Sub CreateBusinessColumns(ByRef TblBusiness As DataTable)
        AddDataColumn(TblBusiness, "score", "System.Int32")
        AddDataColumn(TblBusiness, "id", "System.String")
        AddDataColumn(TblBusiness, "businessname", "System.String")
        AddDataColumn(TblBusiness, "telephoneNumber", "System.String")
        AddDataColumn(TblBusiness, "faxNumber", "System.String")
        AddDataColumn(TblBusiness, "website", "System.String")
        AddDataColumn(TblBusiness, "isPEP", "System.String")
        AddDataColumn(TblBusiness, "isSanctionsCurrent", "System.String")
        AddDataColumn(TblBusiness, "isSanctionsPrevious", "System.String")
        AddDataColumn(TblBusiness, "isLawEnforcement", "System.String")
        AddDataColumn(TblBusiness, "isFinancialregulator", "System.String")
        AddDataColumn(TblBusiness, "isDisqualifiedDirector", "System.String")
        AddDataColumn(TblBusiness, "isInsolvent", "System.String")
        AddDataColumn(TblBusiness, "isAdverseMedia", "System.String")
    End Sub

    Private Sub PopulateBusiness(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblBusiness As DataTable, ByRef HTMLBody As String)
        On Error Resume Next
        Dim businessrow As DataRow
        businessrow = TblBusiness.NewRow()
        clsEmail.BuildHTMLString("New Business", "", HTMLBody, True)
        businessrow("score") = jsonBody.Item("score").ToString()
        clsEmail.BuildHTMLString("score", businessrow("score").ToString(), HTMLBody)
        businessrow("id") = jsonBody.Item("business").item("id").ToString()
        clsEmail.BuildHTMLString("id", businessrow("id").ToString(), HTMLBody)
        businessrow("businessname") = Left(jsonBody.Item("business").item("businessName").ToString(), 200)
        clsEmail.BuildHTMLString("businessname", businessrow("businessname").ToString(), HTMLBody)
        businessrow("telephoneNumber") = Left(jsonBody.Item("business").item("telephoneNumber").ToString(), 50)
        clsEmail.BuildHTMLString("telephoneNumber", businessrow("telephoneNumber").ToString(), HTMLBody)
        businessrow("faxNumber") = Left(jsonBody.Item("business").item("faxNumber").ToString(), 50)
        clsEmail.BuildHTMLString("faxNumber", businessrow("faxNumber").ToString(), HTMLBody)
        businessrow("website") = Left(jsonBody.Item("business").item("website").ToString(), 500)
        clsEmail.BuildHTMLString("website", businessrow("website").ToString(), HTMLBody)
        businessrow("isPEP") = jsonBody.Item("business").item("isPEP").ToString()
        clsEmail.BuildHTMLString("isPEP", businessrow("isPEP").ToString(), HTMLBody)
        businessrow("isSanctionsCurrent") = jsonBody.Item("business").item("isSanctionsCurrent").ToString()
        clsEmail.BuildHTMLString("isSanctionsCurrent", businessrow("isSanctionsCurrent").ToString(), HTMLBody)
        businessrow("isSanctionsPrevious") = jsonBody.Item("business").item("isSanctionsPrevious").ToString()
        clsEmail.BuildHTMLString("isSanctionsPrevious", businessrow("isSanctionsPrevious").ToString(), HTMLBody)
        businessrow("isLawEnforcement") = jsonBody.Item("business").item("isLawEnforcement").ToString()
        clsEmail.BuildHTMLString("isLawEnforcement", businessrow("isLawEnforcement").ToString(), HTMLBody)
        businessrow("isFinancialregulator") = jsonBody.Item("business").item("isFinancialregulator").ToString()
        clsEmail.BuildHTMLString("isFinancialregulator", businessrow("isFinancialregulator").ToString(), HTMLBody)
        businessrow("isDisqualifiedDirector") = jsonBody.Item("business").item("isDisqualifiedDirector").ToString()
        clsEmail.BuildHTMLString("isDisqualifiedDirector", businessrow("isDisqualifiedDirector").ToString(), HTMLBody)
        businessrow("isInsolvent") = jsonBody.Item("business").item("isInsolvent").ToString()
        clsEmail.BuildHTMLString("isInsolvent", businessrow("isInsolvent").ToString(), HTMLBody)
        businessrow("isAdverseMedia") = jsonBody.Item("business").item("isAdverseMedia").ToString()
        clsEmail.BuildHTMLString("isAdverseMedia", businessrow("isAdverseMedia").ToString(), HTMLBody)
        TblBusiness.Rows.Add(businessrow)
    End Sub

    Public Async Function PersonSearchAsync(ByVal varID As Double, ByVal varThreshold As Integer, ByVal varPEP As Boolean, ByVal varPrevSanc As Boolean, ByVal varCurrSanc As Boolean,
                                            ByVal varLaw As Boolean, ByVal varFinReg As Boolean, ByVal varIns As Boolean, ByVal varDisqDir As Boolean, ByVal varAdMedia As Boolean,
                                            ByVal varForename As String, ByVal varMiddlename As String, ByVal varSurname As String, ByVal varDateofBirth As String,
                                            ByVal varYearOfBirth As String, ByVal varAddress As String, ByVal varCity As String, ByVal varCounty As String, ByVal varPostCode As String,
                                            ByVal varCountry As String, ByVal HTML_Body As HTML) As Task(Of Dictionary(Of String, String))
        Dim TblPerson As New DataTable
        Dim TblAddresses As New DataTable
        Dim TblAlias As New DataTable
        Dim TblArticles As New DataTable
        Dim TblSanctions As New DataTable
        Dim TblNotes As New DataTable
        Dim TblLinkedBusinesses As New DataTable
        Dim TblLinkedPersons As New DataTable
        Dim TblPoliticalPositions As New DataTable
        Dim HTMLBody As String = String.Empty

        Try
            Dim varRecordsFound As Integer = 0
            Dim LstPersonSearch As New Dictionary(Of String, String)
            Dim LstPerson As Dictionary(Of String, String) = Nothing
            Dim varJSONPerson As String = Await GetJSONPersonSearchAsync(varThreshold, varPEP, varPrevSanc, varCurrSanc, varLaw, varFinReg, varIns, varDisqDir, varAdMedia, varForename, varMiddlename, varSurname, varDateofBirth, varYearOfBirth, varAddress, varCity, varCounty, varPostCode, varCountry)
            Dim varScore As Integer = 0

            CreatePersonColumns(TblPerson)
            CreateAddressesColumns(TblAddresses)
            CreateAliasColumns(TblAlias)
            CreateArticlesColumns(TblArticles)
            CreateSanctionsColumns(TblSanctions)
            CreateNotesColumns(TblNotes)
            CreateLinkedBusinessesColumns(TblLinkedBusinesses)
            CreateLinkedPersonsColumns(TblLinkedPersons)
            CreatePoliticalPositionsColumns(TblPoliticalPositions)

            If varJSONPerson <> "" Then
                Dim LstjsonResult = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(varJSONPerson)

                For j As Integer = 0 To LstjsonResult.Item("matches").count - 1
                    Dim jsonToPerson = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(LstjsonResult.Item("matches").item(j).ToString())

                    PopulatePerson(jsonToPerson, TblPerson, HTMLBody)
                    PopulateAddresses(jsonToPerson, TblAddresses, "person", HTMLBody)
                    PopulateAlias(jsonToPerson, TblAlias, "person", HTMLBody)
                    PopulateArticles(jsonToPerson, TblArticles, "person", HTMLBody)
                    PopulateSanctions(jsonToPerson, TblSanctions, "person", HTMLBody)
                    PopulateNotes(jsonToPerson, TblNotes, "person", HTMLBody)
                    PopulateLinkedBusinesses(jsonToPerson, TblLinkedBusinesses, "person", HTMLBody)
                    PopulateLinkedPersons(jsonToPerson, TblLinkedPersons, "person", HTMLBody)
                    PopulatePoliticalPositions(jsonToPerson, TblPoliticalPositions, "person", HTMLBody)
                Next
            End If

            varScore = clsIMS.UpdateRiskAssessmentPersonTable(varID, TblPerson, TblAddresses, TblAlias, TblArticles, TblSanctions, TblNotes, TblLinkedBusinesses, TblLinkedPersons, TblPoliticalPositions)
            LstPersonSearch.Add("Score", varScore)
            If TblPerson.Rows.Count > 0 Then
                LstPersonSearch.Add("isPEP", TblPerson(0)("isPEP"))
                LstPersonSearch.Add("isSanctionsCurrent", TblPerson(0)("isSanctionsCurrent"))
                LstPersonSearch.Add("isSanctionsPrevious", TblPerson(0)("isSanctionsPrevious"))
                LstPersonSearch.Add("isFinancialregulator", TblPerson(0)("isFinancialregulator"))
                LstPersonSearch.Add("isAdverseMedia", TblPerson(0)("isAdverseMedia"))
            Else
                LstPersonSearch.Add("isPEP", False)
                LstPersonSearch.Add("isSanctionsCurrent", False)
                LstPersonSearch.Add("isSanctionsPrevious", False)
                LstPersonSearch.Add("isFinancialregulator", False)
                LstPersonSearch.Add("isAdverseMedia", False)
            End If

            Return LstPersonSearch

        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameRiskAssessment, Err.Description & " - Error in Person Search")
            clsIMS.ActionMovementPR(9, varID, "complianceAPI")
        End Try

    End Function

    Public Async Function BusinessSearchAsync(ByVal varID As Double, ByVal varBankCheck As Boolean, ByVal varThreshold As Integer, ByVal varBusinessName As String,
                                              ByVal varPEP As Boolean, ByVal varPrevSanc As Boolean, ByVal varCurrSanc As Boolean, ByVal varLaw As Boolean, ByVal varFinReg As Boolean,
                                              ByVal varIns As Boolean, ByVal varDisqDir As Boolean, ByVal varAdMedia As Boolean, ByVal varAddress As String, ByVal varCity As String,
                                              ByVal varCounty As String, ByVal varPostCode As String, ByVal varCountry As String, ByVal HTML_Body As HTML) As Task(Of Dictionary(Of String, String))
        Dim TblBusiness As New DataTable
        Dim TblAddresses As New DataTable
        Dim TblAlias As New DataTable
        Dim TblArticles As New DataTable
        Dim TblSanctions As New DataTable
        Dim TblNotes As New DataTable
        Dim TblLinkedBusinesses As New DataTable
        Dim TblLinkedPersons As New DataTable
        Dim TblPoliticalPositions As New DataTable
        Dim HTMLBody As String = String.Empty

        Try
            Dim LstBusinessSearch As New Dictionary(Of String, String)
            Dim varBusinessSearch As Integer = 0
            Dim varRecordsFound As Integer = 0
            Dim LstBusiness As Dictionary(Of String, String) = Nothing
            Dim varJSONBusiness As String = Await GetJSONBusinessSearchAsync(varThreshold, varBusinessName, varPEP, varPrevSanc, varCurrSanc, varLaw, varFinReg, varIns, varDisqDir, varAdMedia, varAddress, varCity, varCounty, varPostCode, varCountry)
            Dim varScore As Integer = 0

            CreateBusinessColumns(TblBusiness)
            CreateAddressesColumns(TblAddresses)
            CreateAliasColumns(TblAlias)
            CreateArticlesColumns(TblArticles)
            CreateSanctionsColumns(TblSanctions)
            CreateNotesColumns(TblNotes)
            CreateLinkedBusinessesColumns(TblLinkedBusinesses)
            CreateLinkedPersonsColumns(TblLinkedPersons)

            If varJSONBusiness <> "" Then
                Dim LstjsonResult = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(varJSONBusiness)

                For j As Integer = 0 To LstjsonResult.Item("matches").count - 1
                    Dim jsonToBusiness = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(LstjsonResult.Item("matches").item(j).ToString())

                    PopulateBusiness(jsonToBusiness, TblBusiness, HTMLBody)
                    PopulateAddresses(jsonToBusiness, TblAddresses, "business", HTMLBody)
                    PopulateAlias(jsonToBusiness, TblAlias, "business", HTMLBody)
                    PopulateArticles(jsonToBusiness, TblArticles, "business", HTMLBody)
                    PopulateSanctions(jsonToBusiness, TblSanctions, "business", HTMLBody)
                    PopulateNotes(jsonToBusiness, TblNotes, "business", HTMLBody)
                    PopulateLinkedBusinesses(jsonToBusiness, TblLinkedBusinesses, "business", HTMLBody)
                    PopulateLinkedPersons(jsonToBusiness, TblLinkedPersons, "business", HTMLBody)
                Next
            End If

            varScore = clsIMS.UpdateRiskAssessmentBusinessTable(varID, varBankCheck, TblBusiness, TblAddresses, TblAlias, TblArticles, TblSanctions, TblNotes, TblLinkedBusinesses, TblLinkedPersons)
            LstBusinessSearch.Add("Score", varScore)
            If TblBusiness.Rows.Count > 0 Then
                LstBusinessSearch.Add("isPEP", TblBusiness(0)("isPEP"))
                LstBusinessSearch.Add("isSanctionsCurrent", TblBusiness(0)("isSanctionsCurrent"))
                LstBusinessSearch.Add("isSanctionsPrevious", TblBusiness(0)("isSanctionsPrevious"))
                LstBusinessSearch.Add("isFinancialregulator", TblBusiness(0)("isFinancialregulator"))
                LstBusinessSearch.Add("isAdverseMedia", TblBusiness(0)("isAdverseMedia"))
            Else
                LstBusinessSearch.Add("isPEP", False)
                LstBusinessSearch.Add("isSanctionsCurrent", False)
                LstBusinessSearch.Add("isSanctionsPrevious", False)
                LstBusinessSearch.Add("isFinancialregulator", False)
                LstBusinessSearch.Add("isAdverseMedia", False)
            End If

            HTML_Body.HtmlBody = HTMLBody
            Return LstBusinessSearch

        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameRiskAssessment, Err.Description & " - Error in Business Search")
            clsIMS.ActionMovementPR(9, varID, "complianceAPI")
        End Try

    End Function

    Private Async Function GetJSONPersonSearchAsync(ByVal _Threshold As Integer, ByVal _PEP As Boolean, ByVal _PrevSanc As Boolean, ByVal _CurrSanc As Boolean,
                                                    ByVal _Law As Boolean, ByVal _FinReg As Boolean, ByVal _Ins As Boolean, ByVal _DisqDir As Boolean,
                                                    ByVal _AdMedia As Boolean, ByVal _Forename As String, ByVal _Middlename As String, ByVal _Surname As String,
                                                    ByVal _DateofBirth As String, ByVal _YearOfBirth As String, ByVal _Address As String, ByVal _City As String,
                                                    ByVal _County As String, ByVal _PostCode As String, ByVal _Country As String) As Task(Of String)

        Dim responseMessage = New HttpResponseMessage()
        Dim ApiKey As String = String.Empty
        Dim requestContent As StringContent

        If _Threshold > 0 And _Threshold < 100 And _Surname <> "" Then
            Try
                Dim clsSearchRequest = New SearchRequest
                With clsSearchRequest
                    .Threshold = _Threshold
                    .Pep = _PEP
                    .PreviousSanctions = _PrevSanc
                    .CurrentSanctions = _CurrSanc
                    .LawEnforcement = _Law
                    .FinancialRegulator = _FinReg
                    .Insolvency = _Ins
                    .DisqualifiedDirector = _DisqDir
                    .AdverseMedia = _AdMedia
                    .Forename = IIf(String.IsNullOrEmpty(_Forename), String.Empty, _Forename)
                    .Middlename = IIf(String.IsNullOrEmpty(_Middlename), String.Empty, _Middlename)
                    .Surname = IIf(String.IsNullOrEmpty(_Surname), String.Empty, _Surname)
                    .DateOfBirth = IIf(String.IsNullOrEmpty(_DateofBirth), String.Empty, _DateofBirth)
                    .YearOfBirth = IIf(String.IsNullOrEmpty(_YearOfBirth), String.Empty, _YearOfBirth)
                    .Address = IIf(String.IsNullOrEmpty(_Address), String.Empty, _Address)
                    .City = IIf(String.IsNullOrEmpty(_City), String.Empty, _City)
                    .County = IIf(String.IsNullOrEmpty(_County), String.Empty, _County)
                    .PostCode = IIf(String.IsNullOrEmpty(_PostCode), String.Empty, _PostCode)
                    .Country = IIf(String.IsNullOrEmpty(_Country), String.Empty, _Country)
                End With

                'Fetch the API key for the client ID stored in the StringResources file.
                ApiKey = Await FetchApiKeyAsync()

                Using client = New HttpClient()
                    client.DefaultRequestHeaders.Add("ApiKey", ApiKey.ToUpper())
                    Dim settings As New JsonSerializerSettings With {
                        .Formatting = Formatting.Indented,
                        .NullValueHandling = NullValueHandling.Ignore
                    }
                    Dim payload As String = JsonConvert.SerializeObject(clsSearchRequest, settings)
                    requestContent = New StringContent(payload, Encoding.UTF8, "application/json")
                    responseMessage = client.PostAsync(String.Format(String.Format(My.Resources.StringResource.SafeSearchUrl, 0, String.Format(String.Format(My.Resources.StringResource.ClientId)))), requestContent).GetAwaiter().GetResult()
                    Dim responseBody As String = responseMessage.Content.ReadAsStringAsync().GetAwaiter().GetResult()
                    Dim responseCode As Integer = Convert.ToInt32(responseMessage.StatusCode)
                    If responseMessage.IsSuccessStatusCode Then
                        Return responseBody
                    Else
                        Return String.Empty
                    End If
                End Using
            Catch ex As Exception
                Return String.Empty
                clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameRiskAssessment, Err.Description & " - Error in GetJSONPersonSearchAsync")
            End Try
        Else
            Return String.Empty
        End If

    End Function

    Private Async Function GetJSONBusinessSearchAsync(ByVal _Threshold As Integer, ByVal _BusinessName As String, ByVal _PEP As Boolean, ByVal _PrevSanc As Boolean,
                                                      ByVal _CurrSanc As Boolean, ByVal _Law As Boolean, ByVal _FinReg As Boolean, ByVal _Ins As Boolean,
                                                      ByVal _DisqDir As Boolean, ByVal _AdMedia As Boolean, ByVal _Address As String, ByVal _City As String,
                                                      ByVal _County As String, ByVal _PostCode As String, ByVal _Country As String) As Task(Of String)

        Dim responseMessage = New HttpResponseMessage()
        Dim ApiKey As String = String.Empty
        Dim requestContent As StringContent

        If _Threshold > 0 And _Threshold < 100 And _BusinessName <> "" Then
            Try
                Dim clsSearchRequest = New SearchRequest
                With clsSearchRequest
                    .Threshold = _Threshold
                    .Pep = _PEP
                    .PreviousSanctions = _PrevSanc
                    .CurrentSanctions = _CurrSanc
                    .LawEnforcement = _Law
                    .FinancialRegulator = _FinReg
                    .Insolvency = _Ins
                    .DisqualifiedDirector = _DisqDir
                    .AdverseMedia = _AdMedia
                    .BusinessName = IIf(String.IsNullOrEmpty(_BusinessName), String.Empty, _BusinessName)
                    .Address = IIf(String.IsNullOrEmpty(_Address), String.Empty, _Address)
                    .City = IIf(String.IsNullOrEmpty(_City), String.Empty, _City)
                    .County = IIf(String.IsNullOrEmpty(_County), String.Empty, _County)
                    .PostCode = IIf(String.IsNullOrEmpty(_PostCode), String.Empty, _PostCode)
                    .Country = IIf(String.IsNullOrEmpty(_Country), String.Empty, _Country)
                End With

                'Fetch the API key for the client ID stored in the StringResources file.
                ApiKey = Await FetchApiKeyAsync()

                Using client = New HttpClient()
                    client.DefaultRequestHeaders.Add("ApiKey", ApiKey.ToUpper())
                    Dim settings As New JsonSerializerSettings With {
                        .Formatting = Formatting.Indented,
                        .NullValueHandling = NullValueHandling.Ignore
                    }
                    Dim payload As String = JsonConvert.SerializeObject(clsSearchRequest, settings)
                    requestContent = New StringContent(payload, Encoding.UTF8, "application/json")
                    responseMessage = Await client.PostAsync(String.Format(String.Format(My.Resources.StringResource.SafeSearchUrl, 1, String.Format(String.Format(My.Resources.StringResource.ClientId)))), requestContent)
                    Dim responseBody As String = responseMessage.Content.ReadAsStringAsync().GetAwaiter().GetResult()
                    Dim responseCode As Integer = Convert.ToInt32(responseMessage.StatusCode)
                    If responseMessage.IsSuccessStatusCode Then
                        Return responseBody
                    Else
                        Return String.Empty
                    End If
                End Using
            Catch ex As Exception
                Return String.Empty
                clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameRiskAssessment, Err.Description & " - Error in GetJSONBusinessSearchAsync")
            End Try
        Else
            Return String.Empty
        End If

    End Function

    Private Async Function FetchApiKeyAsync() As Task(Of String)

        Dim responseMessage = New HttpResponseMessage()
        Dim Url = String.Format(String.Format(My.Resources.StringResource.ClientSearchUrl, String.Format(String.Format(My.Resources.StringResource.ClientId))))

        Try
            Using client = New HttpClient()
                responseMessage = Await client.GetAsync(Url)
                Dim responseBody As String = Await responseMessage.Content.ReadAsStringAsync()
                Dim responseCode As Integer = CInt(responseMessage.StatusCode)
                If responseMessage.IsSuccessStatusCode Then
                    Dim clientDetails As Client = JsonConvert.DeserializeObject(Of Client)(responseBody)
                    Return clientDetails.ApiKey
                End If
            End Using
        Catch ex As Exception
            Return String.Empty
        End Try

    End Function

End Class