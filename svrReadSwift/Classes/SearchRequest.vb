﻿Public Class SearchRequest

    Private Threshold_val As Integer
    Private Pep_val As Boolean
    Private Previous_Sanctions As Boolean
    Private Current_Sanctions As Boolean
    Private Law_Enforcement As Boolean
    Private Financial_Regulator As Boolean
    Private Insolvency_val As Boolean
    Private Disqualified_Director As Boolean
    Private Adverse_Media As Boolean
    Private Fname As String
    Private Mname As String
    Private Sname As String
    Private Business_Name As String
    Private Date_OfBirth As String
    Private Year_OfBirth As String
    Private Date_OfDeath As String
    Private Year_OfDeath As String
    Private Address_val As String
    Private City_val As String
    Private County_val As String
    Private Post_Code As String
    Private Country_val As String

    Public Property Threshold() As Integer
        Get
            Return Threshold_val
        End Get
        Set(ByVal value As Integer)
            Threshold_val = value
        End Set
    End Property

    Public Property Pep() As Boolean
        Get
            Return Pep_val
        End Get
        Set(ByVal value As Boolean)
            Pep_val = value
        End Set
    End Property

    Public Property PreviousSanctions() As Boolean
        Get
            Return Previous_Sanctions
        End Get
        Set(ByVal value As Boolean)
            Previous_Sanctions = value
        End Set
    End Property

    Public Property CurrentSanctions() As Boolean
        Get
            Return Current_Sanctions
        End Get
        Set(ByVal value As Boolean)
            Current_Sanctions = value
        End Set
    End Property

    Public Property LawEnforcement() As Boolean
        Get
            Return Law_Enforcement
        End Get
        Set(ByVal value As Boolean)
            Law_Enforcement = value
        End Set
    End Property

    Public Property FinancialRegulator() As Boolean
        Get
            Return Financial_Regulator
        End Get
        Set(ByVal value As Boolean)
            Financial_Regulator = value
        End Set
    End Property

    Public Property Insolvency() As Boolean
        Get
            Return Insolvency_val
        End Get
        Set(ByVal value As Boolean)
            Insolvency_val = value
        End Set
    End Property

    Public Property DisqualifiedDirector() As Boolean
        Get
            Return Disqualified_Director
        End Get
        Set(ByVal value As Boolean)
            Disqualified_Director = value
        End Set
    End Property

    Public Property AdverseMedia() As Boolean
        Get
            Return Adverse_Media
        End Get
        Set(ByVal value As Boolean)
            Adverse_Media = value
        End Set
    End Property

    Public Property Forename() As String
        Get
            Return Fname
        End Get
        Set(ByVal value As String)
            Fname = value
        End Set
    End Property

    Public Property Middlename() As String
        Get
            Return Mname
        End Get
        Set(ByVal value As String)
            Mname = value
        End Set
    End Property

    Public Property Surname() As String
        Get
            Return Sname
        End Get
        Set(ByVal value As String)
            Sname = value
        End Set
    End Property

    Public Property BusinessName() As String
        Get
            Return Business_Name
        End Get
        Set(ByVal value As String)
            Business_Name = value
        End Set
    End Property

    Public Property DateOfBirth() As String
        Get
            Return Date_OfBirth
        End Get
        Set(ByVal value As String)
            Date_OfBirth = value
        End Set
    End Property

    Public Property YearOfBirth() As String
        Get
            Return Year_OfBirth
        End Get
        Set(ByVal value As String)
            Year_OfBirth = value
        End Set
    End Property

    Public Property DateOfDeath() As String
        Get
            Return Date_OfDeath
        End Get
        Set(ByVal value As String)
            Date_OfDeath = value
        End Set
    End Property

    Public Property YearOfDeath() As String
        Get
            Return Year_OfDeath
        End Get
        Set(ByVal value As String)
            Year_OfDeath = value
        End Set
    End Property

    Public Property Address() As String
        Get
            Return Address_val
        End Get
        Set(ByVal value As String)
            Address_val = value
        End Set
    End Property

    Public Property City() As String
        Get
            Return City_val
        End Get
        Set(ByVal value As String)
            City_val = value
        End Set
    End Property

    Public Property County() As String
        Get
            Return County_val
        End Get
        Set(ByVal value As String)
            County_val = value
        End Set
    End Property

    Public Property PostCode() As String
        Get
            Return Post_Code
        End Get
        Set(ByVal value As String)
            Post_Code = value
        End Set
    End Property

    Public Property Country() As String
        Get
            Return Country_val
        End Get
        Set(ByVal value As String)
            Country_val = value
        End Set
    End Property

End Class
