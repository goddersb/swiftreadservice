﻿Public Class HTML

    'Public class to contain the Html Body, this is to replace
    'the previous ByRef that is invalid in an Await method.
    Private _HtmlBody As String

    Public Property HtmlBody() As String
        Get
            Return _HtmlBody
        End Get
        Set(ByVal value As String)
            _HtmlBody = value
        End Set
    End Property

End Class