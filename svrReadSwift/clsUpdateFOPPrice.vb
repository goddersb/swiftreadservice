﻿Imports System.Data.SqlClient
Imports System.Net
Imports System.Web.Script

Public Class clsUpdateFOPPrice

    Dim _drFileStatus As SqlDataReader
    Dim _titleCode As String
    Dim _bBGTicker As String
    Dim _titleLink As String
    Dim _fromDate As Date
    Dim _toDate As String

    Public Sub UpdateFOPPrices()
        Dim SqlString As String = "EXEC dbo.DolfinPaymentGetFOPInstrumentToPrice" ' Get negative balanaces
        Dim dr As SqlDataReader

        Try ' Process the record
            dr = clsIMS.GetSQLData(SqlString)
            If dr.HasRows Then ' If there's an account to balance, then process
                While dr.Read() ' Open the data and pass the data row to create the file
                    Try ' This will start the treasury balancing process
                        resetGetMoveVariables()
                        _titleCode = dr("T_Code").ToString
                        _bBGTicker = dr("BbgTicker").ToString
                        _titleLink = dr("T_ASCIIsymbol").ToString
                        ' Update the price for the instrument
                        ImportBbgPrices(_bBGTicker, _fromDate, _toDate, clsIMS.GetCurrentConnection())
                        ' Update the RD (45 to 14) so that it's ready for import to IMS
                        clsIMS.ExecuteString(clsIMS.GetCurrentConnection(), "EXEC dbo.DolfinPaymentUpdateRD45ToRD14 " & _titleCode)
                    Catch ex As Exception
                        clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFOPPriceUpdate, "FOP price update: Unsucessful update on: " + _bBGTicker + " Error: " & ex.Message.ToString)
                        Continue While ' Keep processing the rest
                    End Try
                End While
            Else ' Nothing to process
                clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameFOPPriceUpdate, "FOP price update: Nothing to process.")
            End If
        Catch ex As Exception ' Error in getting accounts in negative but should not stop the next run.
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFOPPriceUpdate, "FOP price update: Error in getting affected instruments. " & Err.Description)
        Finally
            dr = Nothing
        End Try
    End Sub

    Private Sub resetGetMoveVariables()
        _titleCode = ""
        _bBGTicker = ""
        _titleLink = ""
        _fromDate = DateAdd(DateInterval.Day, -10, Now())
        _toDate = DateAdd(DateInterval.Day, -1, Now())
    End Sub

    Public Function GetBloombergPrices(ByVal Ticker As String, FromDate As Date, ToDate As Date, Optional ByVal ReturnError As Boolean = False) As Object
        Dim url As String = "http://10.1.15.96:35111/?columns=PX_OPEN,PX_LAST" & "&tickers=" & Replace(Ticker, " ", "%20") & "&dates=" & Format(FromDate, "yyyMMdd") & "-" & Format(ToDate, "yyyMMdd") ' ,PX_HIGH,PX_LOW,PX_VOLUME
        Dim varwebclient As New WebClient()
        Dim JSONString As String = ""
        Dim jss As New Serialization.JavaScriptSerializer
        Dim JSON As Object
        Try
            JSONString = varwebclient.DownloadString(url)
            Dim BBGResult As Dictionary(Of String, Object) = jss.Deserialize(Of Dictionary(Of String, Object))(JSONString)
            ' This will return the collection of prices
            JSON = BBGResult(Ticker)
            Return JSON
        Catch ex As Exception
            ' MsgBox("Error retrieving price details for: {0}. {1}", Ticker, ex.Message)
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - GetBloombergPrices")
            Return Nothing
        End Try
    End Function
    Public Sub ImportBbgPrices(Ticker As String, FromDate As Date, ToDate As Date, conn As SqlClient.SqlConnection)
        Dim PriceData As Object
        Dim PriceLastData As Object
        Dim PriceOpenData As Object

        Dim colPriceDate As List(Of String) = clsIMS.GetMultipleDataToList("SELECT DateValue = FORMAT(Date, 'yyyy-MM-dd') FROM dbo.uftblWorkingDates('" & FromDate.ToString("yyyy-MM-dd") & "', '" & ToDate.ToString("yyyy-MM-dd") & "', 165)")

        Try
            PriceData = GetBloombergPrices(Ticker, FromDate, ToDate)
            PriceLastData = PriceData("PX_LAST") ' This navigates to the PX_LAST set
            PriceOpenData = PriceData("PX_OPEN")

            For Each item In colPriceDate
                Try
                    If PriceLastData(item.ToString) & PriceOpenData(item.ToString) <> "" Then
                        Using Cmd As New SqlCommand("_Dolfin_InsertPrice", conn)
                            With Cmd
                                .CommandType = CommandType.StoredProcedure
                                .Parameters.Clear()
                                .Parameters.Add("@secSymbol", SqlDbType.VarChar).Value = Ticker
                                .Parameters.Add("@prDate", SqlDbType.DateTime).Value = item.ToString ' Date Value
                                .Parameters.Add("@prClose", SqlDbType.Float).Value = CDec(PriceLastData(item.ToString))
                                .Parameters.Add("@prOpen", SqlDbType.Float).Value = CDec(PriceOpenData(item.ToString))
                                .ExecuteNonQuery()
                            End With
                        End Using
                    End If
                Catch ex As Exception
                    clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - ImportBbgPrices (adding price to table)")
                End Try
            Next
            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameReceiveDeliver, $"ImportBbgPrices (added prices to IMS price table) for {Ticker} from: {FromDate} to: {ToDate}.")
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - ImportBbgPrices (general function)")
        End Try
    End Sub

End Class
