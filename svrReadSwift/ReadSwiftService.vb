﻿Imports System.Configuration

Public Class ReadSwiftService

    Private _tmrCheckSwift As Timers.Timer
    Private _tmrSwiftRead As Timers.Timer
    Private _blnShowValues As Boolean
    Private _intSwiftReadIntervalValue As Integer
    Private _intCheckSwiftIntervalValue As Integer

    Protected Overrides Sub OnStart(ByVal args() As String)

        'Used for debugging only!
        '#If DEBUG Then
        '        MyBase.RequestAdditionalTime(6000000)
        '        Debugger.Launch()
        '#End If

        clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameReadSwifts, $"The SwiftRead service has started.")

        'Instantiate a new instance of the timer objects.
        _tmrCheckSwift = New Timers.Timer
        _tmrSwiftRead = New Timers.Timer

        'Retrieve the values from the config file and assign to their respective variables.
        _blnShowValues = ConfigurationManager.AppSettings("ShowValues")
        _intSwiftReadIntervalValue = ConfigurationManager.AppSettings("SwiftReadRefresh")
        _intCheckSwiftIntervalValue = ConfigurationManager.AppSettings("SwiftCheckRefresh")

        If _blnShowValues Then
            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameReadSwifts, $"Swift Read interval value {_intSwiftReadIntervalValue}")
            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameReadSwifts, $"Swift Check interval value {_intCheckSwiftIntervalValue}")
        End If

        'Add the timer handlers
        AddHandler _tmrSwiftRead.Elapsed, AddressOf RunReadSwift
        AddHandler _tmrCheckSwift.Elapsed, AddressOf RunSwiftCheck

        'Define their settings.
        _tmrSwiftRead.Interval = _intSwiftReadIntervalValue
        _tmrSwiftRead.Enabled = IIf(Debugger.IsAttached, False, True)
        _tmrCheckSwift.Interval = _intCheckSwiftIntervalValue
        _tmrCheckSwift.Enabled = IIf(Debugger.IsAttached, False, True)

        'Used for debugging only!
        'MyBase.OnStart(args)

        'Call the RunReadSwift method rather than use the Timer.
        'If Debugger.IsAttached Then
        '    RunReadSwift()
        'End If

    End Sub

    Private Sub RunReadSwift()

        Try
            clsIMS.AddAudit(1, 17, $"SwiftRead - Process Started at {Date.Now}")
            If Not clsIMS.LiveDB() Then
                clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameReadSwifts, "UAT Version.")
            End If

            clsIMS.OpenConnection()
            RunSwiftRead()

            '************************************
            'Only For Testing!
            'clsIMS.UpdateMovementsStatus()
            '************************************

            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameReadSwifts, $"SwiftRead - Process Ended at {Date.Now}")
        Catch Ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameReadSwifts, $"An error occured in RunTimer with the error message : {Ex.Message}")
        End Try

    End Sub

    Private Sub RunSwiftCheck()

        Try
            clsIMS.AddAudit(1, 17, $"SwiftCheck - Process Started at {Date.Now}")
            If Not Debugger.IsAttached Then
                clsIMS.ConfirmSwiftFilesReceiving()
                clsEmail.CorporateActionsAddAppt()
                clsIMS.DirectoryHousekeeping()
            End If
            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameReadSwifts, $"SwiftCheck - Process Ended at {Date.Now.ToString}")
        Catch Ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameReadSwifts, $"An error occured in RunSwiftCheck with the error message : {Ex.Message}")
        End Try

    End Sub

    Protected Overrides Sub OnStop()
        clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameReadSwifts, $"The SwiftRead service has stopped.")
        Me._tmrSwiftRead.Dispose()
        Me._tmrCheckSwift.Dispose()
    End Sub

End Class
