﻿Imports System.Data.SqlClient
Imports System.IO
Public Class clsTreasuryBalancer

    Dim _portfolioCode As Integer = 0
    Dim _brokerCode As Integer = 0
    Dim _currencyCode As Integer = 0
    Dim _tCode As Integer = 0
    Dim _targetBalance As Double
    Dim _balanceDate As String
    Dim _treasuryMoveId As Integer
    Dim _fileName As String = ""
    Dim _isFileSucessful As Boolean = False
    ReadOnly _fileWait As Integer = 60
    Dim i As Integer = 0

    Private Sub resetGetMoveVariables()
        _portfolioCode = 0
        _brokerCode = 0
        _currencyCode = 0
        _tCode = 0
        _targetBalance = 0
        _balanceDate = ""
        i = 0
        _fileName = ""
        _treasuryMoveId = 0
    End Sub

    Public Sub RunTreasuryBalancer()

        Try
            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameOmnibusSwitch, "Starting Treasury Balancer - ReadSwift")
            If IsTreasuryAutoAvailable() = True Then
                ProcessNegativeAccounts()
            End If
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameOmnibusSwitch, "Automatic Balancing: Error in treasury (ReadSwift).  " & ex.Message)
        End Try

    End Sub

    Public Sub ProcessNegativeAccounts()
        Dim SqlString As String = "EXEC dbo.DolfinPaymentGetTreasuryBalancerRequest" ' Get negative balanaces
        Dim dr As SqlDataReader
        Dim drFileStatus As SqlDataReader

        Try ' Process the record
            clsIMS.ExecuteString(clsIMS.GetCurrentConnection(), "EXEC dbo.DolfinPaymentSetTreasuryInProgress 1") ' Makes sure nobody else is doing treasury
            dr = clsIMS.GetSQLData(SqlString)
            If dr.HasRows Then ' If there's an account to balance, then process
                While dr.Read() ' Open the data and pass the data row to create the file
                    Try ' This will start the treasury balancing process
                        resetGetMoveVariables()
                        _portfolioCode = dr("Pf_Code").ToString
                        _brokerCode = dr("B_Code").ToString
                        _currencyCode = dr("CR_Code").ToString
                        _tCode = dr("T_Code").ToString
                        _targetBalance = dr("TargetBalance").ToString
                        _balanceDate = Format(dr("BalanceDate"), "yyyy-MM-dd")
                        _treasuryMoveId = dr("RequestId")
                        CreateTreasuryMoves(_portfolioCode, _brokerCode, _currencyCode, _targetBalance, _tCode, _balanceDate, _treasuryMoveId) ' Target Balance is always 0 for negatives

                        If _fileName <> "" Then
                            For i = 0 To _fileWait ' Filewait = 20 seconds with the sleep
                                System.Threading.Thread.Sleep(1000) ' Wait for a second
                                SqlString = "SELECT NoOfRecInFile, NoOfRecInIMS FROM dbo.vwDolfinPaymentTreasuryMoveStatusCount WHERE [FileName] = '" + _fileName + "'"
                                drFileStatus = clsIMS.GetSQLData(SqlString) ' Single row
                                If drFileStatus.HasRows Then
                                    drFileStatus.Read()
                                    If drFileStatus("NoOfRecInFile") = drFileStatus("NoOfRecInIMS") Then ' Check if the number of rows equals the number of trans in IMS
                                        _isFileSucessful = True
                                        clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameOmnibusSwitch, "Automatic Balancing: " + _fileName + " processed successfully.")
                                        UpdateTreasuryMoveStatus(_treasuryMoveId, _fileName, 2) ' Success
                                        Exit For ' Exit the for loop and get next account to balance
                                    End If
                                Else
                                    Throw New System.Exception("Unable to get " + _fileName + " file status.") ' More likely to be a DB problem from this point unless the record has been deleted
                                End If
                                ' 
                                If i >= _fileWait Then
                                    Throw New System.Exception(_fileName + " file did not process within the time period.") ' More likely to be a DB problem from this point unless the record has been deleted
                                End If
                                i += 1
                            Next
                        End If
                    Catch ex As Exception
                        If _fileName <> "" Then ' there's a file and can't confirm status abort the session
                            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameOmnibusSwitch, "Automatic Balancing: Aborting routine. Please check for file: " + _fileName + " Error: " & ex.Message.ToString)
                            clsIMS.ExecuteString(clsIMS.GetCurrentConnection(), "EXEC dbo.DolfinPaymentSetTreasuryErrorFlag 1") ' Disables treasury automation if there's unverified file
                            UpdateTreasuryMoveStatus(_treasuryMoveId, _fileName, 3) ' Failure
                            Exit Sub
                        Else ' No file is created then continue for next record
                            ' Just sets the request to failure but no file created
                            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameOmnibusSwitch, ex.Message.ToString + " on request ID: " + _treasuryMoveId.ToString)
                            If _treasuryMoveId > 0 Then
                                UpdateTreasuryMoveStatus(_treasuryMoveId, _fileName, 3) ' Failure
                            End If
                            Continue While
                        End If
                    End Try
                End While
                clsIMS.ExecuteString(clsIMS.GetCurrentConnection(), "EXEC dbo.DolfinPaymentSetTreasuryInProgress 0") ' Makes treasury available if all is fine
            Else ' Nothing to process
                clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameOmnibusSwitch, "Automatic Balancing: Nothing to process.")
                clsIMS.ExecuteString(clsIMS.GetCurrentConnection(), "EXEC dbo.DolfinPaymentSetTreasuryInProgress 0") ' Set Treasury to not in progress since there's nothing to process
            End If
        Catch ex As Exception ' Error in getting accounts in negative but should not stop the next run.
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameOmnibusSwitch, "Automatic Balancing: Error in treasury moves processing.  " & Err.Description)
        Finally
            dr = Nothing
        End Try
    End Sub

    Private Sub CreateTreasuryMoves(PortfolioCodeIn As Integer, BrokerCodeIn As Integer, CurrencyCodeIn As Integer, ExtraAmtToPayIn As Double, TCodeIn As Integer, BalanceDateIn As String, TrsyMoveId As Integer)
        Dim SqlString As String = ""
        SqlString = "EXEC dbo.DolfinPaymentGetPortfolioTreasuryMovesToFile " & PortfolioCodeIn.ToString() &
                    ", " & BrokerCodeIn.ToString &
                    ", " & CurrencyCodeIn.ToString &
                    ", " & Math.Abs(ExtraAmtToPayIn).ToString & ' This is now Target Balance
                    ", " & TCodeIn.ToString &
                    ", '" & BalanceDateIn + "'" &
                    ", " & TrsyMoveId.ToString

        Dim dr As SqlDataReader
        Try
            dr = clsIMS.GetSQLData(SqlString)
            If dr.HasRows Then
                dr.Read() ' Open the data and pass the single value dataset to create the file
                _fileName = dr.GetString(1)
                CreateTextFile(dr.GetString(0), clsIMS.GetFilePath("Buffer") & _fileName)
                IO.File.Copy(clsIMS.GetFilePath("Buffer") & _fileName, clsIMS.GetFilePath("IMSExport") & _fileName)
                clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameOmnibusSwitch, "Treasury file created: " + _fileName)
            End If
        Catch ex As Exception
            Throw ex
        Finally
            dr = Nothing
        End Try
    End Sub

    Public Sub UpdateTreasuryMoveStatus(ByVal varTreasuyMoveID As Integer, ByVal varFileName As String, ByVal varMoveStatus As Byte)

        Try
            Using Cmd As New SqlCommand("DolfinPaymentUpdateTreasuryRequestAndFile", clsIMS.GetCurrentConnection)
                Cmd.CommandType = CommandType.StoredProcedure
                Cmd.Parameters.Add("@TreasuryMoveID", SqlDbType.Int).Value = varTreasuyMoveID
                Cmd.Parameters.Add("@FileName", SqlDbType.VarChar).Value = varFileName
                Cmd.Parameters.Add("@MoveStatus", SqlDbType.Int).Value = varMoveStatus
                Cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub CreateTextFile(ByVal x As String, FILE_FULL_PATH As String)
        Dim objWriter As New System.IO.StreamWriter(FILE_FULL_PATH, True)
        Try
            If System.IO.File.Exists(FILE_FULL_PATH) = False Then
                System.IO.File.Create(FILE_FULL_PATH).Dispose()
            End If
            objWriter.WriteLine(x)
            objWriter.Close()
        Catch
            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameOmnibusSwitch, "Automatic Balancing: Error in creating file.", 0)
        Finally
            objWriter.Close()
            objWriter = Nothing
        End Try
    End Sub

    Public Function IsTreasuryAutoAvailable() As Boolean
        Dim SqlString As String = ""
        Dim IsAvailable As Boolean = False
        SqlString = "SELECT AutoTreasury = (SELECT FE_ExportBuffer
                    FROM [dbo].[DolfinPaymentFileExport]
                    WHERE FE_Name = 'Auto Treasury')
                    , TreasuryInProgress = 
                    (SELECT FE_ExportBuffer
                    FROM [dbo].[DolfinPaymentFileExport]
                    WHERE FE_Name = 'Treasury Status')"
        Dim dr As SqlDataReader
        Try
            dr = clsIMS.GetSQLData(SqlString)
            If dr.HasRows Then
                dr.Read()
                If dr("AutoTreasury") = True And dr("TreasuryInProgress") = False Then
                    IsAvailable = True
                End If
            Else
                IsAvailable = False
            End If
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameOmnibusSwitch, "Automatic Balancing: Unable to get automated treasury availability.", 0)
            IsAvailable = False
            Throw ex
        Finally
            dr = Nothing
        End Try
        Return IsAvailable
    End Function

End Class