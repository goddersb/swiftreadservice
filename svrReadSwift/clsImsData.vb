﻿Imports System.Data.SqlClient
Imports System.IO
Public Class clsImsData

    Private _varPaymentTypes As New Dictionary(Of Integer, String)
    Private _varStatusTypes As New Dictionary(Of Integer, String)
    Private _varFilePaths As New Dictionary(Of String, String)
    Public ReadOnly _varIMSConnection As String = "Integrated Security=True;Initial Catalog=DolfinPayment;Server=IMSDB01;MultipleActiveResultSets=true"
    Private _conn As SqlConnection

    Private _varMessageList As New Dictionary(Of String, Tuple(Of Integer, String, Boolean, Boolean))

    Public Sub New()
        _conn = GetNewOpenConnection()
        PopulateMessageTypes()
        PopulatePaymentTypes()
        PopulateStatusTypes()
        PopulateFilePaths()
    End Sub

    Property GetPaymentType(ByVal varPaytypeID As Integer) As String
        Get
            Return _varPaymentTypes(varPaytypeID)
        End Get
        Set(value As String)

        End Set
    End Property

    Property GetStatusType(ByVal varStatusTypeID As Integer) As String
        Get
            Return _varStatusTypes(varStatusTypeID)
        End Get
        Set(value As String)

        End Set
    End Property

    Property GetFilePath(ByVal FilePathName As String) As String
        Get
            Return _varFilePaths(FilePathName)
        End Get
        Set(value As String)

        End Set
    End Property

    Property MessageList() As Dictionary(Of String, Tuple(Of Integer, String, Boolean, Boolean))
        Get
            Return _varMessageList
        End Get
        Set(value As Dictionary(Of String, Tuple(Of Integer, String, Boolean, Boolean)))

        End Set
    End Property

    Public Function GetDataToList(ByVal varSQL As String) As Dictionary(Of String, String)
        Dim reader As SqlDataReader = GetSQLData(varSQL)
        Dim Lst As New Dictionary(Of String, String)

        GetDataToList = Nothing
        Try
            If reader.HasRows Then
                GetDataToList = PopulateAllFieldsReaderToList(reader)
            End If
            reader.Close()
        Catch ex As Exception

        End Try
    End Function

    Public Function PopulateAllFieldsReaderToList(ByVal Reader As SqlDataReader) As Dictionary(Of String, String)
        Dim Lst As New Dictionary(Of String, String)
        Dim vField As Integer, vRow As Integer = 0

        If Reader.HasRows Then
            While (Reader.Read())
                For vField = 0 To Reader.FieldCount - 1
                    If Not Lst.ContainsKey(Reader.GetName(vField).ToString & "_" & vRow.ToString) Then
                        Lst.Add(Reader.GetName(vField).ToString & "_" & vRow.ToString, Reader(vField).ToString)
                    End If
                Next
                vRow = vRow + 1
            End While
        End If
        PopulateAllFieldsReaderToList = Lst
    End Function

    Private Sub PopulatePaymentTypes()
        Dim varPaymentTypeID As Integer = 0
        Dim varPaymentType As String = ""
        Dim ReaderPT As SqlDataReader = GetSQLData("Select PT_ID, PT_PaymentType from vwDolfinPaymentType where PT_Movement = 1 Order by PT_PaymentType")

        If ReaderPT.HasRows Then
            While ReaderPT.Read()
                varPaymentTypeID = IIf(IsDBNull(ReaderPT("PT_ID")), 0, ReaderPT("PT_ID"))
                varPaymentType = IIf(IsDBNull(ReaderPT("PT_PaymentType")), "", ReaderPT("PT_PaymentType"))
                _varPaymentTypes.Add(varPaymentTypeID, varPaymentType)
            End While
        End If
        ReaderPT.Close()
    End Sub

    Private Sub PopulateStatusTypes()
        Dim varStatusTypeID As Integer = 0
        Dim varStatusType As String = ""
        Dim ReaderST As SqlDataReader = GetSQLData("Select S_ID, S_Status from vwDolfinPaymentStatus")

        If ReaderST.HasRows Then
            While ReaderST.Read()
                varStatusTypeID = IIf(IsDBNull(ReaderST("S_ID")), 0, ReaderST("S_ID"))
                varStatusType = IIf(IsDBNull(ReaderST("S_Status")), "", ReaderST("S_Status"))
                _varStatusTypes.Add(varStatusTypeID, varStatusType)
            End While
        End If
        ReaderST.Close()
    End Sub

    Private Sub PopulateMessageTypes()
        Dim varMessageID As Integer = 0
        Dim varMessageType As String = ""
        Dim varMessageIncoming As Boolean = False
        Dim varMessageRD As Boolean = False
        Dim ReaderMT As SqlDataReader = GetSQLData("Select M_ID, M_MessageType, M_Incoming,M_RD from vwDolfinPaymentMessageType")

        Try
            If ReaderMT.HasRows Then
                While ReaderMT.Read()
                    varMessageID = IIf(IsDBNull(ReaderMT("M_ID")), 0, ReaderMT("M_ID"))
                    varMessageType = IIf(IsDBNull(ReaderMT("M_MessageType")), "", ReaderMT("M_MessageType"))
                    varMessageIncoming = IIf(IsDBNull(ReaderMT("M_Incoming")), "", ReaderMT("M_Incoming"))
                    varMessageRD = IIf(IsDBNull(ReaderMT("M_RD")), "", ReaderMT("M_RD"))
                    Dim varMessageTypes = Tuple.Create(varMessageID, varMessageType, varMessageIncoming, varMessageRD)
                    _varMessageList.Add(varMessageType, varMessageTypes)
                End While
            End If
            ReaderMT.Close()
        Catch ex As Exception
            AddAudit(3, 14, Err.Description & " - error in PopulateMessageTypes")
        End Try
    End Sub

    Private Sub PopulateFilePaths()
        Dim varFilePathName As String = ""
        Dim varFilePath As String = ""
        Dim ReaderFP As SqlDataReader = GetSQLData("select Path_Name, Path_FilePath from vwDolfinPaymentFilePath")

        If ReaderFP.HasRows Then
            While ReaderFP.Read()
                varFilePathName = IIf(IsDBNull(ReaderFP("Path_Name")), "", ReaderFP("Path_Name"))
                varFilePath = IIf(IsDBNull(ReaderFP("Path_FilePath")), "", ReaderFP("Path_FilePath"))
                _varFilePaths.Add(varFilePathName, varFilePath)
            End While
        End If
        ReaderFP.Close()
    End Sub

    Private Function GetSQLFunction(ByVal varSQL As String) As String

        Try
            Using Cmd As New SqlCommand(varSQL, _conn)
                Cmd.CommandType = CommandType.Text
                GetSQLFunction = Cmd.ExecuteScalar.ToString
            End Using
        Catch ex As Exception
            GetSQLFunction = Nothing
            AddAudit(3, 14, Err.Description & " - error in GetSQLFunction")
        End Try

    End Function

    Public Function GetSQLItem(ByVal varSQL As String) As String
        Dim reader As SqlDataReader

        Try
            GetSQLItem = ""
            reader = clsIMS.GetSQLData(varSQL)
            If reader.HasRows Then
                While (reader.Read())
                    GetSQLItem = reader(0).ToString()
                End While
            End If
            reader.Close()
        Catch ex As Exception
            GetSQLItem = Nothing
            AddAudit(3, 14, Err.Description & " - error in GetSQLItem")
        End Try
    End Function

    Public Function SendEmail(ByVal varRecipients As String, ByVal varBody As String, ByVal varSubject As String) As Boolean

        Try
            Using Cmd As New SqlCommand("DolfinPaymentSendMail", _conn)
                Cmd.CommandType = CommandType.StoredProcedure
                Cmd.CommandText = "DolfinPaymentSendMail"
                Cmd.Parameters.Add("varRecipients", SqlDbType.NVarChar).Value = varRecipients
                Cmd.Parameters.Add("varBody", SqlDbType.NVarChar).Value = varBody
                Cmd.Parameters.Add("varSubject", SqlDbType.NVarChar).Value = varSubject
                Cmd.Parameters.Add("FileAttachementPath", SqlDbType.NVarChar).Value = ""
                Cmd.ExecuteNonQuery()
                clsIMS.AddAudit(3, 17, "Swift Reader is no longer running")
                SendEmail = True
            End Using
        Catch ex As Exception
            AddAudit(3, 14, Err.Description & " - error in sending email")
            SendEmail = False
        End Try
    End Function

    Public Sub OpenConnection()
        _conn = GetNewOpenConnection()
    End Sub

    Public Function GetNewOpenConnection() As SqlConnection
        Dim newconn = New SqlConnection
        Try
            newconn.ConnectionString = _varIMSConnection

            If newconn.State <> ConnectionState.Open Then
                newconn.Open()
            End If
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            GetNewOpenConnection = newconn
        End Try
    End Function

    Public Function GetSQLData(ByVal varSQL As String) As SqlDataReader

        Try
            Using Cmd As New SqlCommand(varSQL, _conn)
                Cmd.CommandType = CommandType.Text
                GetSQLData = Cmd.ExecuteReader()
                Return GetSQLData
            End Using
        Catch ex As Exception
            GetSQLData = Nothing
            AddAudit(3, 14, Err.Description & " - error in GetSQLData")
            Throw ex
        End Try

    End Function

    Public Sub ExecuteString(ByVal execonn As SqlConnection, ByVal varSQL As String)

        Try
            Using Cmd As New SqlCommand(varSQL, execonn)
                Cmd.CommandType = CommandType.Text
                Cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            AddAudit(3, 14, Err.Description & " - error in executestring")
            Throw ex
        End Try

    End Sub

    Public Sub UpdateAttemptedReadSwiftTime()
        Try
            ExecuteString(_conn, "update DolfinPaymentFileExport set FE_UserModifiedTime = GETDATE() where FE_ID = 4")
        Catch ex As Exception
        End Try
    End Sub

    Public Sub AddAudit(ByVal varAuditStatus As Integer, ByVal varAuditGroupName As Integer, ByVal varAuditdesc As String, Optional ByVal varAuditTransactionID As Double = -1)

        Try
            Using Cmd As New SqlCommand("DolfinPaymentAddAuditToLog", _conn)
                Cmd.CommandType = CommandType.StoredProcedure
                Cmd.Parameters.Add("@varStatus", SqlDbType.Int).Value = varAuditStatus
                Cmd.Parameters.Add("@varGroupName", SqlDbType.Int).Value = varAuditGroupName
                Cmd.Parameters.Add("@Id", SqlDbType.Int).Value = varAuditTransactionID
                Cmd.Parameters.Add("@varDesc", SqlDbType.NVarChar).Value = varAuditdesc
                Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = "SwiftRead"
                Cmd.Parameters.Add("@UserMachine", SqlDbType.NVarChar).Value = "AppServer"
                Cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
        End Try
    End Sub

    Public Sub ConfirmSwiftFilesReceiving()
        If clsIMS.GetSQLItem("select max(sr_FileDateTime),getdate() from DolfinPaymentSwiftRead having datepart(weekday,getdate()) not in (1,7) and dateadd(hour,2,max(sr_FileDateTime))>getdate()") = "" Then
            clsIMS.SendEmail(OpsEmailAddresses, "Swift files not been received for over 2 hours (not inc sat/sun)", "Swift files not been received for over 2 hours")
        End If
    End Sub

    Public Sub DirectoryHousekeeping()
        Dim varDirDate As Date

        Try
            For Each Dir As String In Directory.GetDirectories(clsIMS.GetFilePath("Default"))
                varDirDate = Directory.GetLastWriteTime(Dir)
                If InStr(1, Dir, ".", vbTextCompare) <> 0 And varDirDate.Date < DateAdd(DateInterval.Day, -1, Now.Date) Then
                    Dim Dirfiles As String() = Directory.GetFiles(Dir)
                    For Each DirFile As String In Dirfiles
                        File.Delete(DirFile)
                    Next
                    Directory.Delete(Dir, True)
                End If
            Next
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusWarning, cAuditGroupNameCashMovementsFiles, Err.Description & " - Error on DirectoryHousekeeping")
        End Try
    End Sub

    'GB - Doesn't appear to be called anywhere!
    '**********************************************
    'Public Function HasFundsInPaymentAccount(ByVal varID As Double) As String
    '    HasFundsInPaymentAccount = ""
    '    HasFundsInPaymentAccount = GetSQLFunction("Select dbo.DolfinPaymentHasFundsInPaymentAccount(" & varID & ")")
    'End Function

    Public Sub UpdateMovementsStatus()

        UpdatePendingMovements()
        SendApproveEmail()
        CheckApproveEmailRD()
        CheckApproveEmailCR()
        'CheckVolopaCutOff()

    End Sub

    Private Sub UpdatePendingMovements()
        Dim reader As SqlDataReader = Nothing
        Dim PendingReview As Boolean = False
        Dim PendingBalanceReview As Boolean = False
        Dim varReaderID As Double = 0
        Dim varReaderStatusID As Double = 0
        Dim varReaderSettleDate As Date = Now
        Dim varReaderPaymentTypeID As Double = 0
        Dim varReaderAmount As Double = 0
        Dim varReaderOrderAccountCode As Integer = 0
        Dim varReaderPortfolioCode As Integer = 0
        Dim varReaderOrderBrokerCode As Integer = 0
        Dim varReaderCCYCode As Integer = 0
        Dim varReaderBenAccountNumber As String = ""
        Dim varReaderBenSwift As String = ""
        Dim varReaderBenIBAN As String = ""
        Dim varReaderBenBankSwift As String = ""
        Dim varReaderBenBankIBAN As String = ""
        Dim varReaderOtherID As Integer = 0
        Dim varReaderSendSwift As Boolean = False
        Dim varReaderDefaultSendSwift As Boolean = False
        Dim varReaderOtherDefaultSendSwift As Boolean = False
        Dim varReaderOtherDefaultCheckSwift As Boolean = False
        Dim varPendingReason As String = ""

        Try
            clsIMS.AddAudit(1, 17, "Starting UpdatePendingMovements - ReadSwift")
            reader = GetSQLData("select P_ID,P_statusid,P_PaymentTypeid,P_SettleDate,P_Amount,P_OrderAccountCode,P_PortfolioCode,
                                P_OrderBrokerCode,P_CCYCode,P_BenAccountNumber,P_BenName,P_BenAddress1,P_BenAddress2,P_BenZip,
                                P_BenSwift,P_BenIBAN,P_BenBankSwift,P_BenBankIBAN,P_SendSwift,P_Other_ID,PT_DefaultSendSwift,OTH_DefaultSendSwift,OTH_DefaultCheckSwift
                                from vwDolfinPaymentGridView where p_statusid in (" & cStatusReady & "," & cStatusPendingAuth & "," & cStatusAllocatingFunds & ")")

            If reader.HasRows Then
                While reader.Read
                    PendingReview = False
                    PendingBalanceReview = False
                    If Not IsDBNull(reader("P_id")) Then
                        varReaderID = IIf(IsNumeric(reader("P_id")), CDbl(reader("P_id")), 0)
                    End If
                    varReaderStatusID = IIf(Not IsDBNull(reader("P_Statusid")), reader("P_Statusid").ToString, "")
                    varReaderPaymentTypeID = IIf(Not IsDBNull(reader("P_PaymentTypeid")), reader("P_PaymentTypeid").ToString, "")

                    If Not IsDBNull(reader("P_SettleDate")) Then
                        varReaderSettleDate = IIf(IsDate(reader("P_SettleDate")), CDate(reader("P_SettleDate")), Now)
                    End If

                    If Not IsDBNull(reader("P_Amount")) Then
                        varReaderAmount = IIf(IsNumeric(reader("P_Amount")), CDbl(reader("P_Amount")), 0)
                    End If

                    If Not IsDBNull(reader("P_OrderAccountCode")) Then
                        varReaderOrderAccountCode = IIf(IsNumeric(reader("P_OrderAccountCode")), CInt(reader("P_OrderAccountCode")), 0)
                    End If

                    If Not IsDBNull(reader("P_PortfolioCode")) Then
                        varReaderPortfolioCode = IIf(IsNumeric(reader("P_PortfolioCode")), CInt(reader("P_PortfolioCode")), 0)
                    End If

                    If Not IsDBNull(reader("P_OrderBrokerCode")) Then
                        varReaderOrderBrokerCode = IIf(IsNumeric(reader("P_OrderBrokerCode")), CInt(reader("P_OrderBrokerCode")), 0)
                    End If

                    If Not IsDBNull(reader("P_CCYCode")) Then
                        varReaderCCYCode = IIf(IsNumeric(reader("P_CCYCode")), CInt(reader("P_CCYCode")), 0)
                    End If

                    varReaderBenAccountNumber = IIf(Not IsDBNull(reader("P_BenAccountNumber")), reader("P_BenAccountNumber").ToString, "")
                    varReaderBenSwift = IIf(Not IsDBNull(reader("P_BenSwift")), Trim(reader("P_BenSwift").ToString), "")
                    varReaderBenIBAN = IIf(Not IsDBNull(reader("P_BenIBAN")), Trim(reader("P_BenIBAN").ToString), "")
                    varReaderBenBankSwift = IIf(Not IsDBNull(reader("P_BenBankSwift")), Trim(reader("P_BenBankSwift").ToString), "")
                    varReaderBenBankIBAN = IIf(Not IsDBNull(reader("P_BenBankIBAN")), Trim(reader("P_BenBankIBAN").ToString), "")
                    varReaderSendSwift = IIf(Not IsDBNull(reader("P_SendSwift")), reader("P_SendSwift"), False)

                    If Not IsDBNull(reader("P_Other_ID")) Then
                        varReaderOtherID = IIf(IsNumeric(reader("P_Other_ID")), CInt(reader("P_Other_ID")), 0)
                    End If

                    varReaderDefaultSendSwift = IIf(Not IsDBNull(reader("PT_DefaultSendSwift")), reader("PT_DefaultSendSwift"), False)
                    varReaderOtherDefaultSendSwift = IIf(Not IsDBNull(reader("OTH_DefaultSendSwift")), reader("OTH_DefaultSendSwift"), False)
                    varReaderOtherDefaultCheckSwift = IIf(Not IsDBNull(reader("OTH_DefaultCheckSwift")), reader("OTH_DefaultCheckSwift"), False)

                    If varReaderPaymentTypeID <> cPayDescExIn And varReaderOtherID <> cPayDescInOtherBankCharges Then
                        Dim varBalance As Double = GetBalance(varReaderPortfolioCode, varReaderOrderAccountCode)
                        Dim varPendingAmount As Double = GetPendingAmount(varReaderID, varReaderPortfolioCode, varReaderCCYCode, varReaderOrderAccountCode)

                        If varReaderAmount > Strings.Format(varPendingAmount + varBalance, "#,##0.00") Then
                            varPendingReason = "Not enough in balance inc pending amounts (" & Strings.Format(varPendingAmount + varBalance, "#,##0.00") & ")"
                            PendingBalanceReview = True
                        End If
                    End If

                    If varReaderPaymentTypeID = cPayDescExOut Or varReaderPaymentTypeID = cPayDescExClientOut Or varReaderPaymentTypeID = cPayDescIn Or varReaderPaymentTypeID = cPayDescExClientIn Or varReaderPaymentTypeID = cPayDescInOther Then
                        If varReaderSettleDate = Now.Date Then
                            Dim varReaderCutOff As String = GetSQLItem("select co_cutofftime + ',' + co_cutofftimezone from vwDolfinPaymentCutOffTimes where co_brokercode = " & varReaderOrderBrokerCode & " and CO_CCYCode = " & varReaderCCYCode)

                            If varReaderCutOff <> "" Then
                                Dim varReaderCutOffTime As String = Left(varReaderCutOff, InStr(1, varReaderCutOff, ",", vbTextCompare) - 1)
                                Dim varReaderCutOffTimeZone As String = Right(varReaderCutOff, Len(varReaderCutOff) - InStr(1, varReaderCutOff, ",", vbTextCompare))

                                If CDate(varReaderSettleDate.Date & " " & varReaderCutOffTime) < ConvertTimeZone(varReaderCutOffTimeZone).DateTime Then
                                    varPendingReason = "Cut Off time gone - " & ConvertTimeZone(varReaderCutOffTimeZone).DateTime
                                    'PendingReview = True
                                End If
                            End If
                        End If
                    End If

                    If varReaderSettleDate.Date < Now.Date And varReaderStatusID <> cStatusComplianceRejected And varReaderOtherID <> cPayDescInOtherBankCharges And varReaderOtherID <> cPayDescInOtherBankChargesCovChg _
                    And varReaderOtherID <> cPayDescInOtherBankInterestCreditOrdOnly And varReaderOtherID <> cPayDescInOtherBankInterestDebitOrdOnly _
                    And varReaderOtherID <> cPayDescInOtherBankInterestCreditBenOnly And varReaderOtherID <> cPayDescInOtherBankInterestDebitBenOnly Then
                        RollSettleDate(varReaderID)
                    End If

                    If varReaderAmount = 0 Then
                        varPendingReason = "Cannot pay 0 amount"
                        PendingReview = True
                    End If

                    If ((varReaderDefaultSendSwift And Not varReaderOtherDefaultSendSwift) Or (varReaderDefaultSendSwift And varReaderOtherDefaultSendSwift And varReaderOtherDefaultCheckSwift)) And varReaderPaymentTypeID <> cPayDescExOut And varReaderPaymentTypeID <> cPayDescExClientOut And varReaderBenAccountNumber = "" Then
                        varPendingReason = "Beneficiary account number is empty"
                        PendingReview = True
                    End If

                    If ((varReaderDefaultSendSwift And Not varReaderOtherDefaultSendSwift) Or (varReaderDefaultSendSwift And varReaderOtherDefaultSendSwift And varReaderOtherDefaultCheckSwift)) And varReaderBenSwift = "" And varReaderBenBankSwift = "" And varReaderSendSwift Then
                        varPendingReason = "Beneficiary swift is empty and trying to send a swift message"
                        PendingReview = True
                    End If

                    If ((varReaderDefaultSendSwift And Not varReaderOtherDefaultSendSwift) Or (varReaderDefaultSendSwift And varReaderOtherDefaultSendSwift And varReaderOtherDefaultCheckSwift)) And varReaderBenIBAN = "" And varReaderBenBankIBAN = "" And varReaderSendSwift Then
                        varPendingReason = "Beneficiary IBAN is empty and trying to send a swift message"
                        PendingReview = True
                    End If

                    If PendingReview Then
                        UpdateStatusRow(varReaderID, clsIMS.GetStatusType(cStatusPendingReview))
                        AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, "Pending transaction in UpdatePendingMovements because " & varPendingReason, varReaderID)
                    End If
                End While
            End If
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - error in UpdatePendingMovements", varReaderID)
        Finally
            If Not reader Is Nothing Then
                reader.Close()
            End If
        End Try
    End Sub

    Private Async Sub SendApproveEmail()

        Dim LstPayment As New Dictionary(Of String, String)
        Dim reader As SqlDataReader = Nothing

        Try
            clsIMS.AddAudit(1, 17, "Starting SendApproveEmail - ReadSwift")
            reader = GetSQLData("Select P_ID,P_StatusId,P_PaymentTypeID,P_PaymentType,P_SettleDate,P_Portfolio,P_CCY,P_Amount,P_OrderRef,P_SentApproveEmail,P_SentApproveEmailCompliance,P_SendSwift,P_SendIMS,P_AboveThreshold,P_ModifiedTime,P_AuthorisedTime,
                                    P_Username,P_BenName,P_Benfirstname,P_Benmiddlename,P_Bensurname,P_BenAddress1,P_BenAddress2,P_BenCounty,P_BenZip,P_BenCountry,PR_CheckedBankName,PR_CheckedBankCountry,
                                    PR_IssuesDisclosed,PR_Email,PR_CheckedSantions,PR_CheckedPEP,PR_CheckedCDD,PR_Type,P_BenTypeID,P_BenTypeThreshold,P_BenRelationship,P_PaymentFilePath,P_isPEP,P_isSanctionsCurrent,P_isSanctionsPrevious,
                                    P_isAdverseMedia,P_BankisSanctionsCurrent,P_BankisSanctionsPrevious,P_RAScore
                                    from vwDolfinPaymentSendApprovalEmail where P_StatusId in (" & cStatusPendingAuth & "," & cStatusPendingAccountMgt & "," & cStatusPendingAccountMgtLevel2 & "," & cStatusPendingCompliance & "," & cStatusPendingComplianceLevel2 & "," & cStatusComplianceRejected & "," & cStatusAboveThreshold & "," & cStatusAllocatingFunds & ") And (isnull(P_SentApproveEmail,0) = 1 Or isnull(P_SentApproveEmailCompliance,0) > 0)")

            If reader.HasRows Then
                While reader.Read
                    LstPayment = GetLstPaymentFromReader(reader)
                    Await ScorePayment(LstPayment)
                    If (CInt(LstPayment("StatusID")) = cStatusPendingAuth Or CInt(LstPayment("StatusID")) = cStatusAboveThreshold) And (CBool(LstPayment("SentApproveEmail")) Or CBool(LstPayment("AboveThreshold"))) Then
                        clsEmail.CheckApproveMovementEmail(LstPayment, 0)
                    ElseIf LstPayment("RequestedType") = 8 And CInt(LstPayment("StatusID")) = cStatusPendingAccountMgtLevel2 Then
                        clsEmail.CheckApproveMovementEmail(LstPayment, CInt(LstPayment("SentApproveEmailCompliance")))
                    ElseIf (CInt(LstPayment("StatusID")) = cStatusPendingCompliance Or CInt(LstPayment("StatusID")) = cStatusPendingPaymentsTeam Or CInt(LstPayment("StatusID")) = cStatusPaymentsTeamRejected Or CInt(LstPayment("StatusID")) = cStatusComplianceRejected) Then
                        If CInt(LstPayment("SentApproveEmailCompliance")) = 0 Then
                            Dim LstRecipients As List(Of String) = clsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where U_AuthoriseCompliance = 1")
                            clsEmail.SendApproveMovementEmailCompliance3rdParty(LstPayment, LstRecipients)
                            clsIMS.ActionMovementPR(11, CDbl(LstPayment("ID")), "ComplianceAPI")
                        Else
                            clsEmail.CheckApproveMovementEmail(LstPayment, CInt(LstPayment("SentApproveEmailCompliance")))
                        End If
                    End If
                End While
            End If
            reader.Close()

        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - Error In SendApproveEmail", LstPayment("ID"))
        Finally
            If Not reader Is Nothing Then
                reader.Close()
            End If
        End Try
    End Sub

    Private Async Function ScorePayment(ByVal LstPayment As Dictionary(Of String, String)) As Task(Of Boolean)

        Try
            If CInt(LstPayment("PaymentTypeID")) <> 7 Then
                If clsIMS.GetSQLItem("Select P_ID from DolfinPaymentRA where P_ID = " & LstPayment("ID")) = "" Then
                    clsIMS.AddAudit(1, 17, "Score Payment - " & LstPayment("ID"))
                    If CInt(LstPayment("BenTypeID")) = 0 Then
                        Await RunSearchAsync(0, LstPayment)
                    Else
                        Await RunSearchAsync(1, LstPayment)
                    End If
                    Await RunSearchAsync(2, LstPayment)
                    clsIMS.AddDictionaryItem(LstPayment, "ScorePEP", clsIMS.GetSQLItem("Select P_ScorePEP from DolfinPaymentRA where P_ID = " & LstPayment("ID")))
                    clsIMS.AddDictionaryItem(LstPayment, "ScoreBank", clsIMS.GetSQLItem("Select P_ScoreBank from DolfinPaymentRA where P_ID = " & LstPayment("ID")))
                    clsIMS.AddDictionaryItem(LstPayment, "ScoreRecipient", clsIMS.GetSQLItem("Select P_ScoreRecipient from DolfinPaymentRA where P_ID = " & LstPayment("ID")))
                    clsIMS.AddDictionaryItem(LstPayment, "ScoreCountry", clsIMS.GetSQLItem("Select P_ScoreCountry from DolfinPaymentRA where P_ID = " & LstPayment("ID")))
                    clsIMS.AddDictionaryItem(LstPayment, "ScoreCCY", clsIMS.GetSQLItem("Select P_ScoreCCY from DolfinPaymentRA where P_ID = " & LstPayment("ID")))
                    clsIMS.AddDictionaryItem(LstPayment, "ScoreAmt", clsIMS.GetSQLItem("Select P_ScoreAmt from DolfinPaymentRA where P_ID = " & LstPayment("ID")))
                    clsIMS.AddDictionaryItem(LstPayment, "ScoreCumulativeAmt", clsIMS.GetSQLItem("Select P_ScoreCumulativeAmt from DolfinPaymentRA where P_ID = " & LstPayment("ID")))
                    clsIMS.AddDictionaryItem(LstPayment, "ScoreType", clsIMS.GetSQLItem("Select P_ScoreType from DolfinPaymentRA where P_ID = " & LstPayment("ID")))
                    clsIMS.AddDictionaryItem(LstPayment, "Score", clsIMS.GetSQLItem("Select P_RAScore from DolfinPaymentRA where P_ID = " & LstPayment("ID")))
                    clsIMS.AddDictionaryItem(LstPayment, "RiskScoreDescription", clsIMS.GetSQLItem("Select RR_Description from DolfinPaymentRARangeResult where " & LstPayment("Score") & " between RR_MinScoreRange And RR_MaxScoreRange"))
                    clsIMS.AddDictionaryItem(LstPayment, "RiskScore", clsIMS.GetSQLItem("Select RR_RiskScore from DolfinPaymentRARangeResult where " & LstPayment("Score") & " between RR_MinScoreRange And RR_MaxScoreRange"))
                    RiskAction(LstPayment, False)
                    clsEmail.CheckApproveMovementEmail(LstPayment, LstPayment("RiskScore"))
                Else
                    clsEmail.CheckApproveMovementEmail(LstPayment, CInt(LstPayment("SentApproveEmailCompliance")))
                End If
            Else
                clsEmail.CheckApproveMovementEmail(LstPayment, CInt(LstPayment("SentApproveEmailCompliance")))
            End If
            Return True
        Catch ex As Exception
            Return False
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - Error In Score Payment", LstPayment("ID"))
        End Try

    End Function

    Private Sub CheckApproveEmailRD()
        Dim readerRD As SqlDataReader = Nothing
        Dim LstPaymentRD As New Dictionary(Of String, String)

        Try
            clsIMS.AddAudit(1, 17, "Starting CheckApproveEmailRD - ReadSwift")
            readerRD = GetSQLData("Select * from vwDolfinPaymentReceiveDeliverCheckApprovalEmail")

            If readerRD.HasRows Then
                While readerRD.Read
                    LstPaymentRD = GetLstPaymentRDFromreaderRD(readerRD)
                    clsEmail.CheckApproveMovementEmail(LstPaymentRD, 0)
                End While
            End If
            readerRD.Close()

        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - Error In CheckApproveEmailRD", IIf(IsNumeric(LstPaymentRD("ID")), LstPaymentRD("ID"), 0))
        Finally
            If Not readerRD Is Nothing Then
                readerRD.Close()
            End If
        End Try
    End Sub

    Private Sub CheckApproveEmailCR()
        Dim readerCR As SqlDataReader = Nothing
        Dim LstPaymentCR As New Dictionary(Of String, String)

        Try
            clsIMS.AddAudit(1, 17, "Starting CheckApproveEmailCR - ReadSwift")
            readerCR = GetSQLData("SELECT * FROM vwDolfinPaymentConfirmReceiptsCheckApprovalEmail")

            If readerCR.HasRows Then
                While readerCR.Read
                    LstPaymentCR = GetLstPaymentCRFromreaderCR(readerCR)
                    clsEmail.CheckApproveMovementEmail(LstPaymentCR, 0)
                End While
            End If
            readerCR.Close()

        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - Error In CheckApproveEmailRD", IIf(IsNumeric(LstPaymentCR("ID")), LstPaymentCR("ID"), 0))
        Finally
            If Not readerCR Is Nothing Then
                readerCR.Close()
            End If
        End Try
    End Sub

    'GB - Doesn't appear to be called anywhere!
    '**********************************************
    'Public Sub CheckVolopaCutOff()
    '    Dim varCutOff As String = GetSQLFunction("Select dbo.DolfinPaymentVolopaIsCutOff()")

    '    Try
    '        If varCutOff <> "" Then
    '            If varCutOff = "Volopa Weekly" Then
    '                clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameVolopa, "Sending Volopa Weekly Report - ReadSwift")
    '                Dim reader As SqlClient.SqlDataReader = GetSQLData("select p_id from vwDolfinPaymentVolopa cross apply DolfinPaymentFileExport where FE_ID = 10 And P_StatusID = 13 And P_SettleDate between FE_UserModifiedTime And getdate()")
    '                If reader.HasRows Then
    '                    clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameVolopa, "Sending Volopa Weekly Report Email - ReadSwift")
    '                    clsEmail.SendVolopaEmail("Weekly", "select P_SettleDate,P_Amount,FirstName,Surname,VolopaToken from vwDolfinPaymentVolopa cross apply DolfinPaymentFileExport where FE_ID = 10 And P_StatusID = 13 And P_SettleDate between FE_UserModifiedTime And getdate()")
    '                End If
    '                reader.Close()
    '                clsIMS.ExecuteString(clsIMS.GetNewOpenConnection, "update DolfinPaymentFileExport set FE_UserModifiedTime = getdate() where fe_ID = 10")
    '            Else
    '                clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameVolopa, "Volopa cut off reached for " & varCutOff & " - ReadSwift")
    '                Dim reader As SqlClient.SqlDataReader = GetSQLData("select p_id from vwDolfinPaymentReadySwiftVolopa")
    '                If reader.HasRows Then
    '                    clsEmail.SendVolopaEmail("Daily", "Select P_SettleDate,P_Amount,FirstName,Surname,VolopaToken from vwDolfinPaymentReadySwiftVolopa")
    '                    CreateTransactionsSwiftVolopa()
    '                End If
    '                reader.Close()
    '                clsIMS.ExecuteString(clsIMS.GetNewOpenConnection, "update DolfinPaymentFileExport set FE_UserModifiedTime = getdate() where fe_key = '" & varCutOff & "'")
    '            End If
    '        End If
    '    Catch ex As Exception
    '        clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameVolopa, Err.Description & " - error in CheckVolopaCutOff - ReadSwift")
    '    End Try
    'End Sub

    Public Sub AddDictionaryItem(ByVal Lst As Dictionary(Of String, String), ByVal varKeyName As String, ByVal varItem As Object)
        If Lst.ContainsKey(varKeyName) Then
            Lst.Remove(varKeyName)
        End If
        Lst.Add(varKeyName, varItem)
    End Sub

    Public Async Function RunSearchAsync(SearchType As Integer, LstPayment As Dictionary(Of String, String)) As Task(Of Boolean)

        Dim LstSearchResult As New Dictionary(Of String, String)
        'Dim HTMLBody As String = ""
        Dim _HTML As New HTML

        Try
            If SearchType = 0 Then
                LstSearchResult = Await clsSearch.PersonSearchAsync(CDbl(LstPayment("ID")), CInt(LstPayment("BenTypeThreshold")), True, True, True, False, False, False, False, True, LstPayment("BenFirstname"), LstPayment("BenMiddlename"), LstPayment("BenSurname"), "", "", LstPayment("BenAddress1"), LstPayment("BenAddress2"), LstPayment("BenCounty"), LstPayment("BenZip"), LstPayment("BenCountry"), _HTML)
                If LstSearchResult.Count > 0 Then
                    clsIMS.AddDictionaryItem(LstPayment, "isPEP", LstSearchResult("isPEP"))
                    clsIMS.AddDictionaryItem(LstPayment, "isSanctionsCurrent", LstSearchResult("isSanctionsCurrent"))
                    clsIMS.AddDictionaryItem(LstPayment, "isSanctionsPrevious", LstSearchResult("isSanctionsPrevious"))
                    clsIMS.AddDictionaryItem(LstPayment, "isAdverseMedia", LstSearchResult("isAdverseMedia"))
                    AddHTML("Ind", CDbl(LstPayment("ID")), LstPayment("FilePath"), _HTML.HtmlBody)
                End If
            ElseIf SearchType = 1 Then
                LstSearchResult = Await clsSearch.BusinessSearchAsync(CDbl(LstPayment("ID")), False, CInt(LstPayment("BenTypeThreshold")), LstPayment("BenName"), True, True, True, False, False, False, False, True, LstPayment("BenAddress1"), LstPayment("BenAddress2"), LstPayment("BenCounty"), LstPayment("BenZip"), LstPayment("BenCountry"), _HTML)
                If LstSearchResult.Count > 0 Then
                    clsIMS.AddDictionaryItem(LstPayment, "isPEP", LstSearchResult("isPEP"))
                    clsIMS.AddDictionaryItem(LstPayment, "isSanctionsCurrent", LstSearchResult("isSanctionsCurrent"))
                    clsIMS.AddDictionaryItem(LstPayment, "isSanctionsPrevious", LstSearchResult("isSanctionsPrevious"))
                    clsIMS.AddDictionaryItem(LstPayment, "isAdverseMedia", LstSearchResult("isAdverseMedia"))
                    AddHTML("Bus", CDbl(LstPayment("ID")), LstPayment("FilePath"), _HTML.HtmlBody)
                End If
            ElseIf SearchType = 2 Then
                LstSearchResult = Await clsSearch.BusinessSearchAsync(CDbl(LstPayment("ID")), True, CInt(LstPayment("BenTypeThreshold")), LstPayment("BenBankName"), False, True, True, False, False, False, False, False, "", "", "", "", LstPayment("BenBankCountry"), _HTML)
                If LstSearchResult.Count > 0 Then
                    clsIMS.AddDictionaryItem(LstPayment, "Score", LstSearchResult("Score"))
                    clsIMS.AddDictionaryItem(LstPayment, "BankisSanctionsCurrent", LstSearchResult("isSanctionsCurrent"))
                    clsIMS.AddDictionaryItem(LstPayment, "BankisSanctionsPrevious", LstSearchResult("isSanctionsPrevious"))
                    AddHTML("Bank", CDbl(LstPayment("ID")), LstPayment("FilePath"), _HTML.HtmlBody)
                End If
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function GetCurrentConnection() As SqlConnection
        GetCurrentConnection = Nothing
        If _conn.State = ConnectionState.Open Then
            GetCurrentConnection = _conn
        End If
    End Function

    Public Function GetLstPaymentFromReader(ByVal reader As SqlDataReader) As Dictionary(Of String, String)
        Dim LstPayment As New Dictionary(Of String, String)

        GetLstPaymentFromReader = Nothing
        Try
            LstPayment.Clear()

            If IsDBNull(reader("P_ID")) Then
                LstPayment.Add("ID", 0)
            Else
                clsIMS.AddDictionaryItem(LstPayment, "ID", IIf(IsNumeric(reader("P_ID")), reader("P_ID"), 0))
            End If

            If IsDBNull(reader("P_StatusId")) Then
                LstPayment.Add("StatusID", 0)
            Else
                LstPayment.Add("StatusID", IIf(IsNumeric(reader("P_StatusId")), reader("P_StatusId"), 0))
            End If

            If IsDBNull(reader("P_PaymentTypeID")) Then
                LstPayment.Add("PaymentTypeID", 0)
            Else
                LstPayment.Add("PaymentTypeID", IIf(IsNumeric(reader("P_PaymentTypeID")), reader("P_PaymentTypeID"), 0))
            End If

            If IsDBNull(reader("P_PaymentType")) Then
                LstPayment.Add("PaymentType", "")
            Else
                LstPayment.Add("PaymentType", reader("P_PaymentType").ToString)
            End If

            If IsDBNull(reader("P_SettleDate")) Then
                LstPayment.Add("SettleDate", "")
            Else
                LstPayment.Add("SettleDate", IIf(IsDate(reader("P_SettleDate")), reader("P_SettleDate"), ""))
            End If

            If IsDBNull(reader("P_Portfolio")) Then
                LstPayment.Add("Portfolio", "")
            Else
                LstPayment.Add("Portfolio", reader("P_Portfolio").ToString)
            End If

            If IsDBNull(reader("P_CCY")) Then
                LstPayment.Add("CCY", "")
            Else
                LstPayment.Add("CCY", reader("P_CCY").ToString)
            End If

            If IsDBNull(reader("P_Amount")) Then
                LstPayment.Add("Amount", 0)
            Else
                LstPayment.Add("Amount", IIf(IsNumeric(reader("P_Amount")), reader("P_Amount"), 0))
            End If

            If IsDBNull(reader("P_OrderRef")) Then
                LstPayment.Add("OrderRef", "")
            Else
                LstPayment.Add("OrderRef", reader("P_OrderRef").ToString)
            End If

            If IsDBNull(reader("P_SentApproveEmail")) Then
                LstPayment.Add("SentApproveEmail", False)
            Else
                LstPayment.Add("SentApproveEmail", reader("P_SentApproveEmail"))
            End If

            If IsDBNull(reader("P_SentApproveEmailCompliance")) Then
                LstPayment.Add("SentApproveEmailCompliance", 0)
            Else
                LstPayment.Add("SentApproveEmailCompliance", IIf(IsNumeric(reader("P_SentApproveEmailCompliance")), reader("P_SentApproveEmailCompliance"), 0))
            End If

            If IsDBNull(reader("P_SendSwift")) Then
                LstPayment.Add("SendSwift", False)
            Else
                LstPayment.Add("SendSwift", reader("P_SendSwift"))
            End If

            If IsDBNull(reader("P_SendIMS")) Then
                LstPayment.Add("SendIMS", False)
            Else
                LstPayment.Add("SendIMS", reader("P_SendIMS"))
            End If

            If IsDBNull(reader("P_AboveThreshold")) Then
                LstPayment.Add("AboveThreshold", False)
            Else
                LstPayment.Add("AboveThreshold", reader("P_AboveThreshold"))
            End If

            If IsDBNull(reader("P_ModifiedTime")) Then
                LstPayment.Add("ModifiedTime", "")
            Else
                LstPayment.Add("ModifiedTime", IIf(IsDate(reader("P_ModifiedTime")), reader("P_ModifiedTime"), ""))
            End If

            If IsDBNull(reader("P_AuthorisedTime")) Then
                LstPayment.Add("AuthorisedTime", "")
            Else
                LstPayment.Add("AuthorisedTime", IIf(IsDate(reader("P_AuthorisedTime")), reader("P_AuthorisedTime"), ""))
            End If

            If IsDBNull(reader("P_Username")) Then
                LstPayment.Add("Username", "")
            Else
                LstPayment.Add("Username", reader("P_Username").ToString)
            End If

            If IsDBNull(reader("P_BenName")) Then
                LstPayment.Add("BenName", "")
            Else
                LstPayment.Add("BenName", reader("P_BenName").ToString)
            End If

            If IsDBNull(reader("P_BenFirstname")) Then
                LstPayment.Add("BenFirstname", "")
            Else
                LstPayment.Add("BenFirstname", reader("P_BenFirstname").ToString)
            End If

            If IsDBNull(reader("P_BenMiddlename")) Then
                LstPayment.Add("BenMiddlename", "")
            Else
                LstPayment.Add("BenMiddlename", reader("P_BenMiddlename").ToString)
            End If

            If IsDBNull(reader("P_BenSurname")) Then
                LstPayment.Add("BenSurname", "")
            Else
                LstPayment.Add("BenSurname", reader("P_BenSurname").ToString)
            End If

            If IsDBNull(reader("P_BenAddress1")) Then
                LstPayment.Add("BenAddress1", "")
            Else
                LstPayment.Add("BenAddress1", reader("P_BenAddress1").ToString)
            End If

            If IsDBNull(reader("P_BenAddress2")) Then
                LstPayment.Add("BenAddress2", "")
            Else
                LstPayment.Add("BenAddress2", reader("P_BenAddress2").ToString)
            End If

            If IsDBNull(reader("P_BenCounty")) Then
                LstPayment.Add("BenCounty", "")
            Else
                LstPayment.Add("BenCounty", reader("P_BenCounty").ToString)
            End If

            If IsDBNull(reader("P_BenZip")) Then
                LstPayment.Add("BenZip", "")
            Else
                LstPayment.Add("BenZip", reader("P_BenZip").ToString)
            End If

            If IsDBNull(reader("P_BenCountry")) Then
                LstPayment.Add("BenCountry", "")
            Else
                LstPayment.Add("BenCountry", reader("P_BenCountry").ToString)
            End If

            If IsDBNull(reader("PR_CheckedBankName")) Then
                LstPayment.Add("BenBankName", "")
            Else
                LstPayment.Add("BenBankName", reader("PR_CheckedBankName").ToString)
            End If

            If IsDBNull(reader("PR_CheckedBankCountry")) Then
                LstPayment.Add("BenBankCountry", "")
            Else
                LstPayment.Add("BenBankCountry", reader("PR_CheckedBankCountry").ToString)
            End If

            If IsDBNull(reader("PR_CheckedSantions")) Then
                LstPayment.Add("CheckedSanctions", False)
            Else
                LstPayment.Add("CheckedSanctions", reader("PR_CheckedSantions"))
            End If

            If IsDBNull(reader("PR_CheckedPEP")) Then
                LstPayment.Add("CheckedPEP", False)
            Else
                LstPayment.Add("CheckedPEP", reader("PR_CheckedPEP"))
            End If

            If IsDBNull(reader("PR_CheckedCDD")) Then
                LstPayment.Add("CheckedCDD", False)
            Else
                LstPayment.Add("CheckedCDD", reader("PR_CheckedCDD"))
            End If

            If IsDBNull(reader("PR_IssuesDisclosed")) Then
                LstPayment.Add("IssuesDisclosed", "")
            Else
                LstPayment.Add("IssuesDisclosed", reader("PR_IssuesDisclosed").ToString)
            End If

            If IsDBNull(reader("PR_Email")) Then
                LstPayment.Add("RequestedEmail", "")
            Else
                LstPayment.Add("RequestedEmail", reader("PR_Email").ToString)
            End If

            If IsDBNull(reader("PR_Type")) Then
                LstPayment.Add("RequestedType", 0)
            Else
                LstPayment.Add("RequestedType", reader("PR_Type").ToString)
            End If

            If IsDBNull(reader("P_BenTypeID")) Then
                LstPayment.Add("BenTypeID", 0)
            Else
                LstPayment.Add("BenTypeID", reader("P_BenTypeID").ToString)
            End If

            If IsDBNull(reader("P_BenTypeThreshold")) Then
                LstPayment.Add("BenTypeThreshold", 100)
            Else
                LstPayment.Add("BenTypeThreshold", reader("P_BenTypeThreshold").ToString)
            End If

            If IsDBNull(reader("P_BenRelationship")) Then
                LstPayment.Add("BenRelationship", 5)
            Else
                LstPayment.Add("BenRelationship", reader("P_BenRelationship").ToString)
            End If

            If IsDBNull(reader("P_PaymentFilePath")) Then
                LstPayment.Add("FilePath", "")
            Else
                LstPayment.Add("FilePath", reader("P_PaymentFilePath").ToString)
            End If

            If IsDBNull(reader("P_isPEP")) Then
                LstPayment.Add("isPEP", False)
            Else
                LstPayment.Add("isPEP", reader("P_isPEP"))
            End If

            If IsDBNull(reader("P_isSanctionsCurrent")) Then
                LstPayment.Add("isSanctionsCurrent", False)
            Else
                LstPayment.Add("isSanctionsCurrent", reader("P_isSanctionsCurrent"))
            End If

            If IsDBNull(reader("P_isSanctionsPrevious")) Then
                LstPayment.Add("isSanctionsPrevious", False)
            Else
                LstPayment.Add("isSanctionsPrevious", reader("P_isSanctionsPrevious"))
            End If

            If IsDBNull(reader("P_isAdverseMedia")) Then
                LstPayment.Add("isAdverseMedia", False)
            Else
                LstPayment.Add("isAdverseMedia", reader("P_isAdverseMedia"))
            End If

            If IsDBNull(reader("P_BankisSanctionsCurrent")) Then
                LstPayment.Add("BankisSanctionsCurrent", False)
            Else
                LstPayment.Add("BankisSanctionsCurrent", reader("P_BankisSanctionsCurrent"))
            End If

            If IsDBNull(reader("P_BankisSanctionsPrevious")) Then
                LstPayment.Add("BankisSanctionsPrevious", False)
            Else
                LstPayment.Add("BankisSanctionsPrevious", reader("P_BankisSanctionsPrevious"))
            End If

            If IsDBNull(reader("P_RAScore")) Then
                LstPayment.Add("Score", 0)
            Else
                LstPayment.Add("Score", IIf(IsNumeric(reader("P_RAScore")), CStr(reader("P_RAScore")), 0))
            End If

            GetLstPaymentFromReader = LstPayment
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameRiskAssessment, Err.Description & " - Error in GetLstPaymentFromReader")
        End Try
    End Function

    Public Function GetLstPaymentRDFromreaderRD(ByVal readerRD As SqlDataReader) As Dictionary(Of String, String)
        Dim LstPaymentRD As New Dictionary(Of String, String)

        GetLstPaymentRDFromreaderRD = Nothing
        Try
            LstPaymentRD.Clear()

            If IsDBNull(readerRD("PRD_ID")) Then
                LstPaymentRD.Add("ID", "")
            Else
                LstPaymentRD.Add("ID", readerRD("PRD_ID").ToString)
            End If

            If IsDBNull(readerRD("PRD_StatusID")) Then
                LstPaymentRD.Add("StatusID", 0)
            Else
                LstPaymentRD.Add("StatusID", IIf(IsNumeric(readerRD("PRD_StatusID")), readerRD("PRD_StatusID"), 0))
            End If

            If IsDBNull(readerRD("PRD_PaymentTypeID")) Then
                LstPaymentRD.Add("PaymentTypeID", 0)
            Else
                LstPaymentRD.Add("PaymentTypeID", IIf(IsNumeric(readerRD("PRD_PaymentTypeID")), readerRD("PRD_PaymentTypeID"), 0))
            End If

            If IsDBNull(readerRD("PRD_SendSwift")) Then
                LstPaymentRD.Add("SendSwift", False)
            Else
                LstPaymentRD.Add("SendSwift", readerRD("PRD_SendSwift"))
            End If

            If IsDBNull(readerRD("PRD_SendIMS")) Then
                LstPaymentRD.Add("SendIMS", False)
            Else
                LstPaymentRD.Add("SendIMS", readerRD("PRD_SendIMS"))
            End If

            If IsDBNull(readerRD("PRD_AboveThreshold")) Then
                LstPaymentRD.Add("AboveThreshold", False)
            Else
                LstPaymentRD.Add("AboveThreshold", readerRD("PRD_AboveThreshold"))
            End If

            If IsDBNull(readerRD("PRD_Username")) Then
                LstPaymentRD.Add("Username", "")
            Else
                LstPaymentRD.Add("Username", readerRD("PRD_Username"))
            End If

            If IsDBNull(readerRD("PRD_ModifiedTime")) Then
                LstPaymentRD.Add("ModifiedTime", "")
            Else
                LstPaymentRD.Add("ModifiedTime", IIf(IsDate(readerRD("PRD_ModifiedTime")), readerRD("PRD_ModifiedTime"), ""))
            End If

            GetLstPaymentRDFromreaderRD = LstPaymentRD
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - Error in GetLstPaymentRDFromreaderRD")
        End Try
    End Function

    Public Function GetLstPaymentCRFromreaderCR(ByVal readerCR As SqlDataReader) As Dictionary(Of String, String)
        Dim LstPaymentCR As New Dictionary(Of String, String)

        GetLstPaymentCRFromreaderCR = Nothing
        Try
            LstPaymentCR.Clear()

            If IsDBNull(readerCR("CRM_ID")) Then
                LstPaymentCR.Add("ID", "")
            Else
                LstPaymentCR.Add("ID", readerCR("CRM_ID").ToString)
            End If

            If IsDBNull(readerCR("CRM_StatusID")) Then
                LstPaymentCR.Add("StatusID", 0)
            Else
                LstPaymentCR.Add("StatusID", IIf(IsNumeric(readerCR("CRM_StatusID")), readerCR("CRM_StatusID"), 0))
            End If

            If IsDBNull(readerCR("CRM_PaymentTypeID")) Then
                LstPaymentCR.Add("PaymentTypeID", 0)
            Else
                LstPaymentCR.Add("PaymentTypeID", IIf(IsNumeric(readerCR("CRM_PaymentTypeID")), readerCR("CRM_PaymentTypeID"), 0))
            End If

            If IsDBNull(readerCR("CRM_SendSwift")) Then
                LstPaymentCR.Add("SendSwift", False)
            Else
                LstPaymentCR.Add("SendSwift", readerCR("CRM_SendSwift"))
            End If

            If IsDBNull(readerCR("CRM_SendIMS")) Then
                LstPaymentCR.Add("SendIMS", False)
            Else
                LstPaymentCR.Add("SendIMS", readerCR("CRM_SendIMS"))
            End If

            If IsDBNull(readerCR("CRM_AboveThreshold")) Then
                LstPaymentCR.Add("AboveThreshold", False)
            Else
                LstPaymentCR.Add("AboveThreshold", readerCR("CRM_AboveThreshold"))
            End If

            If IsDBNull(readerCR("CRM_Username")) Then
                LstPaymentCR.Add("Username", "")
            Else
                LstPaymentCR.Add("Username", readerCR("CRM_Username"))
            End If

            If IsDBNull(readerCR("CRM_ModifiedTime")) Then
                LstPaymentCR.Add("ModifiedTime", "")
            Else
                LstPaymentCR.Add("ModifiedTime", IIf(IsDate(readerCR("CRM_ModifiedTime")), readerCR("CRM_ModifiedTime"), ""))
            End If

            GetLstPaymentCRFromreaderCR = LstPaymentCR
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - Error in GetLstPaymentCRFromreaderCR")
        End Try
    End Function

    Public Sub AddHTML(ByRef SearchTypeShortName As String, ByRef varID As Integer, ByRef varFilePath As String, ByRef HTMLBody As String)
        Try
            If HTMLBody <> "" Then
                clsEmail.FinishHTML(HTMLBody)
                clsEmail.CreateHTMLFile(SearchTypeShortName, varID, varFilePath, HTMLBody)
            End If
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameRiskAssessment, Err.Description & " - Error in PersonSearch (AddHTML)")
            clsIMS.ActionMovementPR(9, varID, "complianceAPI")
        End Try
    End Sub

    Public Sub RiskAction(ByVal LstPayment As Dictionary(Of String, String), ByVal Escalated As Boolean)
        Dim LstRecipients As New List(Of String)

        Try
            If IsNumeric(LstPayment("RiskScore")) Then
                clsIMS.AddAudit(1, 17, "Sending mail to compliance - " & LstPayment("ID"))
                LstRecipients = clsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where U_Username = 'compliance'")
                clsEmail.SendApproveMovementEmailCompliance(LstPayment, Escalated, LstRecipients, LstPayment("RiskScore"))
            Else
                clsIMS.ActionMovementPR(5, CDbl(LstPayment("ID")), "ComplianceAPI")
                clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Compliance refused transaction " & LstPayment("ID") & " by " & "ComplianceAPI (RiskAction)", LstPayment("ID"))
            End If
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameRiskAssessment, Err.Description & " - Error in RiskAction")
            clsIMS.ActionMovementPR(9, LstPayment("ID"), "complianceAPI")
        End Try
    End Sub

    Public Function CanUserAuthoriseCompliance(ByVal varUsername As String, ByVal varComplianceScore As Integer) As Boolean
        CanUserAuthoriseCompliance = False
        If varComplianceScore <= 2 Then
            If clsIMS.CanUser(varUsername, cUserAuthoriseAccountMgt) Then
                CanUserAuthoriseCompliance = True
            End If
        ElseIf varComplianceScore = 3 Then
            If clsIMS.CanUser(varUsername, cUserAuthoriseAccountMgtLevel2) Then
                CanUserAuthoriseCompliance = True
            End If
        ElseIf varComplianceScore = 4 Then
            If clsIMS.CanUser(varUsername, cUserAuthoriseCompliance) Then
                CanUserAuthoriseCompliance = True
            End If
        ElseIf varComplianceScore = 5 Then
            If clsIMS.CanUser(varUsername, cUserAuthoriseComplianceLevel2) Then
                CanUserAuthoriseCompliance = True
            End If
        End If
    End Function

    Public Function CanUser(ByVal varUsername As String, ByVal varPermissionName As String) As Boolean
        Dim reader As SqlClient.SqlDataReader = Nothing

        CanUser = False
        Try
            'reader = GetSQLData("Select U_UserName from vwDolfinPaymentUsers where lower(U_Username) = '" & LCase(varUsername) & "' and " & varPermissionName & " = 1")
            reader = GetSQLData("Select U_UserName from vwDolfinPaymentUsers where lower(replace(U_Username,'RMSCAPITAL\','')) = case when charindex('\','" & LCase(varUsername) & "') <> 0 then right('" & LCase(varUsername) & "',len('" & LCase(varUsername) & "')-charindex('\','" & LCase(varUsername) & "')) else '" & LCase(varUsername) & "' end and " & varPermissionName & " = 1")

            If reader.HasRows Then
                CanUser = True
            End If
        Catch ex As Exception
        Finally
            reader.Close()
        End Try
    End Function

    Public Function GetMultipleDataToList(ByVal varSQL As String) As List(Of String)
        Dim reader As SqlDataReader = clsIMS.GetSQLData(varSQL)
        Dim Lst As New List(Of String)

        If reader.HasRows Then
            GetMultipleDataToList = PopulateMultipleReaderToList(reader)
        Else
            GetMultipleDataToList = Nothing
        End If
        reader.Close()
    End Function

    Public Function PopulateMultipleReaderToList(ByVal Reader As SqlDataReader) As List(Of String)
        Dim Lst As New List(Of String)
        Dim vField As Integer

        If Reader.HasRows Then
            While (Reader.Read())
                For vField = 0 To Reader.FieldCount - 1
                    Lst.Add(Reader(vField).ToString)
                Next
            End While
        End If
        PopulateMultipleReaderToList = Lst
    End Function

    Public Function GetPendingAmount(CurrentID As Integer, PortfolioCode As Integer, CCYCode As Integer, AccountCode As Integer) As Double
        Dim GetPendingStr As String

        GetPendingAmount = 0
        GetPendingStr = GetSQLFunction("Select dbo.DolfinPaymentGetPendingAmount(" & CurrentID & ", " & PortfolioCode & ", " & CCYCode & ", " & AccountCode & ")")
        If IsNumeric(GetPendingStr) Then
            GetPendingAmount = CDbl(GetPendingStr)
        End If
    End Function

    Public Function GetBalance(ByVal PortfolioCode As Integer, ByVal AccountCode As Integer) As Double
        Dim GetBalanceStr As String

        GetBalance = 0
        GetBalanceStr = GetSQLFunction("Select dbo.DolfinPaymentGetBalances(1, " & AccountCode & ", " & PortfolioCode & ", getdate())")

        If IsNumeric(GetBalanceStr) Then
            GetBalance = CDbl(GetBalanceStr)
        End If
    End Function

    Public Function UpdateStatusRow(ByVal varID As Double, ByVal varStatus As String) As Boolean

        Try
            Using Cmd As New SqlCommand("DolfinPaymentPostUpdateMovementsRowStatus", _conn)
                Cmd.CommandType = CommandType.StoredProcedure
                Cmd.Parameters.Add("varID", SqlDbType.Int).Value = varID
                Cmd.Parameters.Add("varStatus", SqlDbType.NVarChar).Value = varStatus
                Cmd.ExecuteNonQuery()
                UpdateStatusRow = True
            End Using
        Catch ex As Exception
            UpdateStatusRow = False
        End Try

    End Function

    Public Function RollSettleDate(ByVal varID As Double) As Boolean

        Try
            Using Cmd As New SqlCommand("DolfinPaymentRollSettleDate", _conn)
                Cmd.CommandType = CommandType.StoredProcedure
                Cmd.Parameters.Add("varID", SqlDbType.Int).Value = varID
                Cmd.ExecuteNonQuery()
                RollSettleDate = True
            End Using
        Catch ex As Exception
            RollSettleDate = False
        End Try

    End Function

    Public Sub ActionMovement(ByVal varOpt As Integer, ByVal varID As Double, ByVal varSendSwift As Boolean, ByVal varSendIMS As Boolean, ByVal varAuthorisedUserName As String)

        Try
            Using Cmd As New SqlCommand("DolfinPaymentAction", _conn)
                Cmd.CommandType = CommandType.StoredProcedure
                Cmd.Parameters.Add("@Opt", SqlDbType.Int).Value = varOpt
                Cmd.Parameters.Add("@ID", SqlDbType.Int).Value = varID
                Cmd.Parameters.Add("@Swift", SqlDbType.Bit).Value = varSendSwift
                Cmd.Parameters.Add("@IMS", SqlDbType.Bit).Value = varSendIMS
                Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = varAuthorisedUserName
                Cmd.ExecuteNonQuery()
                AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovements, $"ActionMovement: {varOpt},Movement ID: {varID}.", varID)
            End Using
        Catch ex As SqlException
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & $" - Error on ActionMovement:{varOpt}, Movement ID: {varID}.", varID)
        End Try

    End Sub

    Public Sub ActionMovementPR(ByVal varOpt As Integer, ByVal varID As Double, ByVal varAuthorisedUserName As String)

        Try
            Using Cmd As New SqlCommand("DolfinPaymentActionPR", _conn)
                Cmd.CommandType = CommandType.StoredProcedure
                Cmd.Parameters.Add("@Opt", SqlDbType.Int).Value = varOpt
                Cmd.Parameters.Add("@ID", SqlDbType.Int).Value = varID
                Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = varAuthorisedUserName
                Cmd.ExecuteNonQuery()
                AddAudit(cAuditStatusSuccess, cAuditGroupNamePaymentRequest, $"ActionMovementPR: {varOpt},Movement ID: {varID}.", varID)
            End Using
        Catch ex As SqlException
            AddAudit(cAuditStatusError, cAuditGroupNamePaymentRequest, Err.Description & $" - Error on ActionMovementPR: {varOpt}, Movement ID: {varID}.", varID)
        End Try

    End Sub

    Public Sub ActionMovementRD(ByVal varOpt As Integer, ByVal varID As Double, ByVal varAuthorisedUserName As String)

        Try
            Using Cmd As New SqlCommand("DolfinPaymentActionRD", _conn)
                Cmd.CommandType = CommandType.StoredProcedure
                Cmd.Parameters.Add("@Opt", SqlDbType.Int).Value = varOpt
                Cmd.Parameters.Add("@ID", SqlDbType.Int).Value = varID
                Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = varAuthorisedUserName
                Cmd.ExecuteNonQuery()
                AddAudit(cAuditStatusSuccess, cAuditGroupNamePaymentRequest, $"ActionMovementRD: {varOpt},Movement ID: {varID}.", varID)
            End Using
        Catch ex As SqlException
            AddAudit(cAuditStatusError, cAuditGroupNamePaymentRequest, Err.Description & $" - Error on ActionMovementRD: {varOpt}, Movement ID: {varID}.", varID)
        End Try

    End Sub

    Public Sub ActionMovementCR(ByVal varOpt As Integer, ByVal varID As Double, ByVal varAuthorisedUserName As String)

        Try
            Using Cmd As New SqlCommand("DolfinPaymentActionRD", _conn)
                Cmd.CommandType = CommandType.StoredProcedure
                Cmd.CommandText = "DolfinPaymentActionCR"
                Cmd.Parameters.Add("@Opt", SqlDbType.Int).Value = varOpt
                Cmd.Parameters.Add("@ID", SqlDbType.Int).Value = varID
                Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = varAuthorisedUserName
                Cmd.ExecuteNonQuery()
                AddAudit(cAuditStatusSuccess, cAuditGroupNamePaymentRequest, $"ActionMovementCR: {varOpt},Movement ID: {varID}.", varID)
            End Using
        Catch ex As SqlException
            AddAudit(cAuditStatusError, cAuditGroupNamePaymentRequest, Err.Description & $" - Error on ActionMovementCR: {varOpt}, Movement ID: {varID}.", varID)
        End Try

    End Sub

    Public Function UpdateRiskAssessmentPersonTable(ByVal varID As Double, ByVal TblPerson As DataTable, ByVal TblAddresses As DataTable, ByVal TblAlias As DataTable,
                                               ByVal TblArticles As DataTable, ByVal TblSanctions As DataTable, ByVal TblNotes As DataTable,
                                               ByVal TblLinkedBusinesses As DataTable, ByVal TblLinkedPersons As DataTable, ByVal TblPoliticalPositions As DataTable) As Integer

        Dim vID As String = ""
        UpdateRiskAssessmentPersonTable = 0
        Try
            Using Cmd As New SqlCommand("DolfinPaymentAddRAPerson", _conn)
                Cmd.CommandType = CommandType.StoredProcedure
                Cmd.Parameters.Add("@PaymentID", SqlDbType.Int).Value = varID
                Cmd.Parameters.Add("@TblPerson", SqlDbType.Structured).Value = TblPerson
                Cmd.Parameters.Add("@TblAddress", SqlDbType.Structured).Value = TblAddresses
                Cmd.Parameters.Add("@TblAlias", SqlDbType.Structured).Value = TblAlias
                Cmd.Parameters.Add("@TblArticle", SqlDbType.Structured).Value = TblArticles
                Cmd.Parameters.Add("@TblSanctions", SqlDbType.Structured).Value = TblSanctions
                Cmd.Parameters.Add("@TblNotes", SqlDbType.Structured).Value = TblNotes
                Cmd.Parameters.Add("@TblLinkedBusiness", SqlDbType.Structured).Value = TblLinkedBusinesses
                Cmd.Parameters.Add("@TblLinkedPerson", SqlDbType.Structured).Value = TblLinkedPersons
                Cmd.Parameters.Add("@TblPoliticalPosition", SqlDbType.Structured).Value = TblPoliticalPositions
                Cmd.Parameters.Add("@Score", SqlDbType.Int)
                Cmd.Parameters("@Score").Direction = ParameterDirection.Output
                Cmd.ExecuteNonQuery()
                If Not IsDBNull(Cmd.Parameters("@Score").Value) Then
                    UpdateRiskAssessmentPersonTable = Cmd.Parameters("@Score").Value
                Else
                    AddAudit(cAuditStatusError, cAuditGroupNameRiskAssessment, "Error on UpdateRiskAssessmentPersonTable - No update score returned from DolfinPaymentAddRAPerson")
                    clsIMS.ActionMovementPR(9, varID, "complianceAPI")
                End If
            End Using
        Catch ex As SqlException
            AddAudit(cAuditStatusError, cAuditGroupNameRiskAssessment, Err.Description & " - Error on UpdateRiskAssessmentPersonTable")
            clsIMS.ActionMovementPR(9, varID, "complianceAPI")
        End Try

    End Function

    Public Function UpdateRiskAssessmentBusinessTable(ByVal varID As Double, ByVal varBankCheck As Boolean, ByVal TblBusiness As DataTable, ByVal TblAddresses As DataTable, ByVal TblAlias As DataTable,
                                               ByVal TblArticles As DataTable, ByVal TblSanctions As DataTable, ByVal TblNotes As DataTable,
                                               ByVal TblLinkedBusinesses As DataTable, ByVal TblLinkedPersons As DataTable) As Integer

        Dim vID As String = ""
        UpdateRiskAssessmentBusinessTable = 0

        Try
            Using Cmd As New SqlCommand("DolfinPaymentAddRABusiness", _conn)
                Cmd.CommandType = CommandType.StoredProcedure
                Cmd.Parameters.Add("@PaymentID", SqlDbType.Int).Value = varID
                Cmd.Parameters.Add("@BankCheck", SqlDbType.Bit).Value = varBankCheck
                Cmd.Parameters.Add("@TblBusiness", SqlDbType.Structured).Value = TblBusiness
                Cmd.Parameters.Add("@TblAddress", SqlDbType.Structured).Value = TblAddresses
                Cmd.Parameters.Add("@TblAlias", SqlDbType.Structured).Value = TblAlias
                Cmd.Parameters.Add("@TblArticle", SqlDbType.Structured).Value = TblArticles
                Cmd.Parameters.Add("@TblSanctions", SqlDbType.Structured).Value = TblSanctions
                Cmd.Parameters.Add("@TblNotes", SqlDbType.Structured).Value = TblNotes
                Cmd.Parameters.Add("@TblLinkedBusiness", SqlDbType.Structured).Value = TblLinkedBusinesses
                Cmd.Parameters.Add("@TblLinkedPerson", SqlDbType.Structured).Value = TblLinkedPersons
                Cmd.Parameters.Add("@Score", SqlDbType.Int)
                Cmd.Parameters("@Score").Direction = ParameterDirection.Output
                Cmd.ExecuteNonQuery()
                If Not IsDBNull(Cmd.Parameters("@Score").Value) Then
                    UpdateRiskAssessmentBusinessTable = Cmd.Parameters("@Score").Value
                Else
                    AddAudit(cAuditStatusError, cAuditGroupNameRiskAssessment, "Error on UpdateRiskAssessmentBusinessTable - No update score returned from DolfinPaymentAddRABusiness")
                    clsIMS.ActionMovementPR(9, varID, "complianceAPI")
                End If
            End Using
        Catch ex As SqlException
            AddAudit(cAuditStatusError, cAuditGroupNameRiskAssessment, Err.Description & " - Error on UpdateRiskAssessmentBusinessTable")
            clsIMS.ActionMovementPR(9, varID, "complianceAPI")
        End Try

    End Function

    Public Function CreateTransactionsSwiftAuto() As Boolean

        Dim ExportFileCount As New Dictionary(Of String, Integer)
        Dim ExportFilesList As New Dictionary(Of String, String)
        Dim TransConn As New SqlConnection

        clsIMS.AddAudit(1, 17, "Starting CreateTransactionsSwiftAuto - ReadSwift")
        TransConn.ConnectionString = _varIMSConnection
        TransConn.Open()
        Dim transaction As SqlTransaction = TransConn.BeginTransaction

        Dim reader As SqlClient.SqlDataReader = GetSQLData("select p_id from vwDolfinPaymentReadySwiftAutoReady")
        Try
            Dim vReviewCount As Integer = 0
            If reader.HasRows Then
                If LiveDB() Then
                    RunSQLJob(cSwiftJobNameAutoReady)
                Else
                    RunSQLJob(cSwiftJobNameAutoReady & " UAT")
                End If

                ExportFileCount.Add(GetStatusType(cStatusPendingReview), vReviewCount)
                CopyExportFiles(ExportFilesList, ExportFileCount)

                transaction.Commit()
                AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "CreateTransactionsSwiftAuto: Created transactions files", 0)
            End If
        Catch ex As Exception
            CreateTransactionsSwiftAuto = False
            Try
                transaction.Rollback()
                AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - error in CreateTransactionsSwiftAuto - Rollback transactions")
            Catch ex2 As Exception
                AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - error in CreateTransactionsSwiftAuto closing trans cannot rollback")
            End Try
        Finally
            reader.Close()
            TransConn.Close()
            CreateTransactionsSwiftAuto = True
        End Try

    End Function

    Public Function CopyExportFiles(ByRef ExportFilesList As Dictionary(Of String, String), Optional ByRef ExportFileCount As Dictionary(Of String, Integer) = Nothing) As Boolean
        Dim ExportFileName As String = ""
        Dim varSwiftCount As Integer, varIMSCount As Integer
        Try
            If Not IO.Directory.Exists(clsIMS.GetFilePath("SWIFTExport")) Then
                IO.Directory.CreateDirectory(clsIMS.GetFilePath("SWIFTExport"))
            End If
            If Not IO.Directory.Exists(clsIMS.GetFilePath("IMSExport")) Then
                IO.Directory.CreateDirectory(clsIMS.GetFilePath("IMSExport"))
            End If

            For exportFile As Integer = 0 To ExportFilesList.Count - 1
                ExportFileName = ExportFilesList.Item(ExportFilesList.Keys(exportFile)).ToString
                If InStr(1, ExportFilesList.Keys(exportFile).ToString, clsIMS.GetSQLItem("select FE_Name from vwDolfinPaymentFileExport where FE_Key = 'SwiftName'"), vbTextCompare) <> 0 Then
                    IO.File.Copy(clsIMS.GetFilePath("SWIFTExport") & ExportFileName, clsIMS.GetFilePath("Buffer") & ExportFileName)
                    clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "File saved to:" & clsIMS.GetFilePath("Buffer") & Dir(ExportFileName), 0)
                    varSwiftCount = varSwiftCount + 1
                ElseIf InStr(1, ExportFilesList.Keys(exportFile).ToString, clsIMS.GetSQLItem("select FE_Name from vwDolfinPaymentFileExport where FE_Key = 'IMSName'"), vbTextCompare) <> 0 Then
                    IO.File.Copy(ExportFileName, clsIMS.GetFilePath("IMSExport") & Dir(ExportFileName))
                    clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "Copied file:  " & ExportFileName & " to " & clsIMS.GetFilePath("IMSExport") & Dir(ExportFileName), 0)
                    varIMSCount = varIMSCount + 1
                End If
            Next

            If Not ExportFileCount Is Nothing Then
                ExportFileCount.Add(clsIMS.GetSQLItem("select FE_Name from vwDolfinPaymentFileExport where FE_Key = 'SwiftName'"), varSwiftCount)
                ExportFileCount.Add(clsIMS.GetSQLItem("select FE_Name from vwDolfinPaymentFileExport where FE_Key = 'IMSName'"), varIMSCount)
            End If
            CopyExportFiles = True
            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "CopyExportFiles: Copied all files", 0)
        Catch ex As Exception
            CopyExportFiles = False
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - CopyExportFiles: could not copy transaction files - exportfilesList(exportfile): " & ExportFileName)
        End Try

    End Function

    Public Function CreateTransactionsSwiftVolopa() As Boolean

        Dim ExportFileCount As New Dictionary(Of String, Integer)
        Dim ExportFilesList As New Dictionary(Of String, String)

        Dim TransConn As New SqlConnection
        TransConn.ConnectionString = _varIMSConnection
        TransConn.Open()
        Dim transaction As SqlTransaction = TransConn.BeginTransaction

        Dim reader As SqlClient.SqlDataReader = GetSQLData("select p_id from vwDolfinPaymentReadySwiftVolopa")
        Try
            Dim vReviewCount As Integer = 0
            If reader.HasRows Then
                If LiveDB() Then
                    RunSQLJob(cSwiftJobNameVolopa)
                Else
                    RunSQLJob(cSwiftJobNameVolopa & " UAT")
                End If

                ExportFileCount.Add(GetStatusType(cStatusPendingReview), vReviewCount)
                CopyExportFiles(ExportFilesList, ExportFileCount)

                transaction.Commit()
                AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "CreateTransactionsSwiftVolopa (ReadSwift): Created transactions files", 0)
            End If
        Catch ex As Exception
            CreateTransactionsSwiftVolopa = False
            Try
                transaction.Rollback()
                AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - error in CreateTransactionsSwiftVolopa (ReadSwift) - Rollback transactions")
            Catch ex2 As Exception
                AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - error in CreateTransactionsSwiftVolopa (ReadSwift) - closing trans cannot rollback")
            End Try
        Finally
            reader.Close()
            TransConn.Close()
            CreateTransactionsSwiftVolopa = True
        End Try

    End Function

    Public Function RunSQLJob(ByVal varJobName As String) As Boolean

        Try
            Using Cmd As New SqlCommand("DolfinPaymentRunJob", _conn)
                Cmd.CommandType = CommandType.StoredProcedure
                Cmd.Parameters.Add("@varJobName", SqlDbType.NVarChar).Value = varJobName
                Cmd.ExecuteNonQuery()
            End Using
        Catch ex As SqlException
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & $" - Error on RunSQLJob for {varJobName}.")
        Finally
            AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, $"RunSQLJob for {varJobName}.")
        End Try
        RunSQLJob = True

    End Function

    Public Function GetGPPdata(ByRef varDocNo As String, ByRef varData As String) As Boolean

        Try
            Using Cmd As New SqlCommand("DolfinPaymentGetGPPData", _conn)
                Cmd.CommandType = CommandType.StoredProcedure
                Cmd.Parameters.Add("@varDocNo", SqlDbType.NVarChar).Value = varDocNo
                Cmd.Parameters.Add("@varData", SqlDbType.NVarChar).Value = varData
                Dim GPPreader As SqlClient.SqlDataReader = Cmd.ExecuteReader()
                If GPPreader.HasRows Then
                    GPPreader.Read()
                    varData = GPPreader(0).ToString
                    If varData <> "" Then
                        GetGPPdata = True
                    Else
                        GetGPPdata = False
                    End If
                Else
                    GetGPPdata = False
                End If
                GPPreader.Close()
            End Using
        Catch ex As SqlException
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & $" - Error on GetGPPdata for DocNo: {varDocNo}.")
            GetGPPdata = False
        End Try

    End Function

    Public Function GetSwiftFileIMS(ByRef varSettledStatus As Integer, ByRef varDocNo As String, ByRef varSwiftFile As String) As Boolean

        Try
            Using Cmd As New SqlCommand("DolfinPaymentCreateSwiftFileIMS", _conn)
                Cmd.CommandType = CommandType.StoredProcedure
                Cmd.Parameters.Add("@varSettledStatus", SqlDbType.Int).Value = varSettledStatus
                Cmd.Parameters.Add("@varDocNo", SqlDbType.NVarChar).Value = varDocNo
                Cmd.Parameters.Add("@varSwiftFile", SqlDbType.NVarChar).Value = varSwiftFile

                Dim Swiftreader As SqlClient.SqlDataReader = Cmd.ExecuteReader()
                If Swiftreader.HasRows Then
                    Swiftreader.Read()
                    varSwiftFile = Swiftreader(0).ToString
                    If varSwiftFile <> "" Then
                        GetSwiftFileIMS = True
                    Else
                        GetSwiftFileIMS = False
                    End If
                Else
                    GetSwiftFileIMS = False
                End If
                Swiftreader.Close()
            End Using
        Catch ex As SqlException
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & $" - Error on GetGPPdata for DocNo: {GetSwiftFileIMS}, Swift File: {varSwiftFile}.")
            GetSwiftFileIMS = False
        End Try

    End Function

    Public Function LiveDB() As Boolean
        LiveDB = False
        If _conn.Database.ToString = "DolfinPayment" Then
            LiveDB = True
        End If
    End Function

    Public Sub CloseConnection()
        _conn.Close()
    End Sub

End Class