﻿Imports System.Data.SqlClient

Public Class clsSwiftRead

    Private Const _varConnection As String = "Integrated Security=True;Initial Catalog=DolfinPayment;Server=IMSDB01;MultipleActiveResultSets=true"
    Private Const _cDefaultDate As Date = #1800-01-01#
    Private ReadOnly _conn As SqlConnection
    'Private ReadOnly GetNextLine As Boolean
    Private _varSwiftFileName As String
    Private _varSwiftFileNameDate As Date
    Private _varSwiftFileDateTime As Date
    Private _varSwiftType As String
    Private _varSwiftAcknowledged As Boolean
    Private _varSwiftErrorCode As String
    Private _varSwiftCode As String
    Private _varFunctionType As String
    Private _varSwiftUserReference As String
    Private _varReference As String
    Private _varReference2 As String ' This is to cater for 535 where varReference is cleared.
    Private _varRelatedReference As String
    Private _varRelatedReference2 As String ' This is to cater for 535 where varRelatedReference is cleared.
    Private _varLinkReference As String
    Private _varLinkReference2 As String ' This is to cater for 535 where varLinkReference is cleared.
    Private _varAccountNo As String
    Private _varAccountNo2 As String ' This is to cater for 535 where varAccountNo is cleared.
    Private _varAccountCCY As String
    Private _varIndicator1 As String
    Private _varIndicator2 As String
    Private _varTradeDate As Date
    Private _varSettleDate As Date
    Private _varExDate As Date = _cDefaultDate
    Private _varRecDate As Date = _cDefaultDate
    Private _varAnnouDate As Date = _cDefaultDate
    Private _varEffectiveDate As Date = _cDefaultDate
    Private _varClearer As String
    Private _varISIN As String
    Private _varOpeningAmount As Double
    Private _varOpeningAmountCCY As String
    Private _varOpeningAmountQualifier As String
    Private _varAmount As Double
    Private _varAmountCCY As String
    Private _varAmountQualifier As String
    Private _varPrice As Double
    Private _varPriceCCY As String
    Private _varPriceQualifier As String
    Private _varSettledAmount As Double
    Private _varSettledAmountCCY As String
    Private _varSettledAmountQualifier As String
    Private _varClosingAmount As Double
    Private _varClosingAmountCCY As String
    Private _varClosingAmountQualifier As String
    Private _varFaceValue As Double
    Private _varSettleValue As Double
    Private _varFaceValueAggregate As Double
    Private _varFaceValuePendingDelivered As Double
    Private _varFaceValuePendingReceived As Double
    Private _varGrossAmount As Double
    Private _varGrossAmountCCY As String
    Private _varGrossAmountQualifier As String
    Private _varNetAmount As Double
    Private _varNetAmountCCY As String
    Private _varNetAmountQualifier As String
    Private _varCommission As Double
    Private _varCommissionCCY As String
    Private _varCommissionQualifier As String
    Private _varAccrued As Double
    Private _varAccruedCCY As String
    Private _varAccruedQualifier As String
    Private _varRate As Double
    Private _varRateQualifier As String
    Private _varOrderIBAN As String
    Private _varOrderInstitution As String
    Private _varOrderBankIBAN As String
    Private _varOrderBankInstitution As String
    Private _varIntermediaryIBAN As String
    Private _varIntermediary As String
    Private _varBeneficiaryIBAN As String
    Private _varBeneficiaryInstitution As String
    Private _varBeneficiaryBankIBAN As String
    Private _varBeneficiaryBankInstitution As String
    Private _varSafeKeeping As String
    Private _varBuyer As String
    Private _varSeller As String
    Private _varReceivingAgent As String
    Private _varDeliveringAgent As String
    Private _varPlaceOfSettlement As String
    Private _varDefaultValue As String
    Private _varReason1 As String
    Private _varReason2 As String
    Private _varDescription As String
    Private _varHasBlockEnded As Boolean = True ' RA 2019-05-08

    Private ReadOnly _varSwiftFilePath As String = clsIMS.GetFilePath("SWIFTImport")
    Private ReadOnly _varSwiftFilePathErrors As String = clsIMS.GetFilePath("SWIFTError")
    Private ReadOnly _varSwiftFileEditImportPath As String = clsIMS.GetFilePath("SWIFTEditImport")
    Private ReadOnly _varGPPFilePath As String = clsIMS.GetFilePath("GPP")

    Public Sub New()
        _conn = GetNewOpenConnection()
    End Sub

    Public Sub ClearClass()
        _varSwiftFileName = ""
        _varSwiftFileNameDate = Now
        _varSwiftFileDateTime = Now
        _varSwiftType = ""
        _varSwiftAcknowledged = False
        _varSwiftErrorCode = ""
        _varSwiftCode = ""
        _varFunctionType = ""
        _varSwiftUserReference = ""
        ClearClassData()
    End Sub

    Public Sub ClearClassData()
        _varReference = ""
        _varRelatedReference = ""
        _varLinkReference = ""
        _varAccountNo = ""
        _varAccountCCY = ""
        _varIndicator1 = ""
        _varIndicator2 = ""
        _varTradeDate = Now
        _varSettleDate = Now
        _varExDate = _cDefaultDate
        _varRecDate = _cDefaultDate
        _varAnnouDate = _cDefaultDate
        _varEffectiveDate = _cDefaultDate
        _varClearer = ""
        _varISIN = ""
        _varOpeningAmount = 0
        _varOpeningAmountCCY = ""
        _varOpeningAmountQualifier = ""
        _varAmount = 0
        _varAmountCCY = ""
        _varAmountQualifier = ""
        _varPrice = 0
        _varPriceCCY = ""
        _varPriceQualifier = ""
        _varSettledAmount = 0
        _varSettledAmountCCY = ""
        _varSettledAmountQualifier = ""
        _varClosingAmount = 0
        _varClosingAmountCCY = ""
        _varClosingAmountQualifier = ""
        _varFaceValue = 0
        _varSettleValue = 0
        _varFaceValueAggregate = 0
        _varFaceValuePendingDelivered = 0
        _varFaceValuePendingReceived = 0
        _varGrossAmount = 0
        _varGrossAmountCCY = ""
        _varGrossAmountQualifier = ""
        _varNetAmount = 0
        _varNetAmountCCY = ""
        _varNetAmountQualifier = ""
        _varCommission = 0
        _varCommissionCCY = ""
        _varCommissionQualifier = ""
        _varAccrued = 0
        _varAccruedCCY = ""
        _varAccruedQualifier = ""
        _varRate = 0
        _varRateQualifier = ""
        _varOrderIBAN = ""
        _varOrderInstitution = ""
        _varOrderBankIBAN = ""
        _varOrderBankInstitution = ""
        _varIntermediaryIBAN = ""
        _varIntermediary = ""
        _varBeneficiaryIBAN = ""
        _varBeneficiaryInstitution = ""
        _varBeneficiaryBankIBAN = ""
        _varBeneficiaryBankInstitution = ""
        _varSafeKeeping = ""
        _varBuyer = ""
        _varSeller = ""
        _varReceivingAgent = ""
        _varDeliveringAgent = ""
        _varPlaceOfSettlement = ""
        _varDefaultValue = ""
        _varReason1 = ""
        _varReason2 = ""
        _varDescription = ""
        _varHasBlockEnded = True ' Roger addition 2019-05-08
    End Sub

    Public Function getSearchInterval() As Integer
        getSearchInterval = IIf(Now.DayOfWeek = 3, 5, 3)
    End Function

    Public Function getSearchString(ByVal DaysSearch As Integer) As String
        Dim SearchString As String = "*_"

        If DaysSearch = 4 Then
            DaysSearch = 5
        End If

        If Now.Year = DateAdd(DateInterval.Day, -DaysSearch, Now).Year Then
            SearchString &= Now.ToString("yyyy")
        Else
            SearchString &= DateAdd(DateInterval.Day, -DaysSearch, Now).ToString("yyyy")
        End If

        If Now.Month = DateAdd(DateInterval.Day, -DaysSearch, Now).Month Then
            SearchString &= Now.ToString("MM")
        End If

        If DaysSearch = 0 Then
            SearchString &= Now.ToString("dd")
        End If

        getSearchString = SearchString & "*.fin"

        'getSearchString = "20180118_*.fin"

    End Function

    Public Sub ProcessHeader(ByVal varLine As String)
        Try
            _varSwiftUserReference = "" : _varSwiftCode = "" : _varSwiftType = ""
            Call GetFileTypeFromSwift(varLine)
            Dim Status As Boolean = GetStatusFromSwift(varLine)
            Call GetSwiftCode(varLine)
            Call GetUserReference(varLine)
            'If varSwiftType = "103" Then
            'MsgBox("")
            'End If

            'If clsIMS.MessageList.ContainsKey("MT" & varSwiftType) Then
            'Dim MsgType As Tuple(Of Integer, String, Boolean, Boolean) = clsIMS.MessageList.Item("MT" & varSwiftType)
            'If MsgType.Item3 = False And Not Status Then
            'ProcessHeader = False
            'End If
            'End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot ProcessHeader, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub ProcessLine(ByVal varLine As String, ByRef SwiftFinFile As String, ByRef writeLineToTextFile As String, ByRef varJoinSection As String,
                           ByRef LstAccountInfo As List(Of Dictionary(Of String, String)), ByRef ColAddInfo As Dictionary(Of String, String),
                           ByRef LstCorpActionTermInfo As List(Of Dictionary(Of String, String)), ByRef ColCorpActionTermInfo As Dictionary(Of String, String),
                           ByRef TempDescription As String)

        If _varSwiftType = "101" Then
            Process101(varLine, varJoinSection)
        ElseIf _varSwiftType = "103" Then
            Process103(varLine, varJoinSection)
        ElseIf _varSwiftType = "200" Then
            Process200(varLine, varJoinSection)
        ElseIf _varSwiftType = "202" Then
            Process202(varLine, varJoinSection)
        ElseIf _varSwiftType = "210" Then
            Process210(varLine)
        ElseIf _varSwiftType = "502" Then
            Process502(varLine)
        ElseIf _varSwiftType = "509" Then
            Process509(varLine)
        ElseIf _varSwiftType = "515" Then
            Process515(varLine)
        ElseIf _varSwiftType = "535" Then
            If _varSwiftCode = "IRVTBEBB" Or _varSwiftCode = "IRVTGB2X" Or _varSwiftCode = "IRVTJPJX848" Or _varSwiftCode = "IRVTLULXLTA" Or _varSwiftCode = "IRVTUS3N" Or _varSwiftCode = "IRVTUS3NIBK" Then
                Process535WithSubBal(varLine, writeLineToTextFile)
            Else
                Process535(varLine, writeLineToTextFile)
            End If
        ElseIf _varSwiftType = "536" Or _varSwiftType = "537" Or _varSwiftType = "538" Then
            Process536(varLine, writeLineToTextFile, varJoinSection)
        ElseIf _varSwiftType = "540" Or _varSwiftType = "541" Or _varSwiftType = "542" Or _varSwiftType = "543" Or _varSwiftType = "544" Then
            Process540(varLine)
        ElseIf _varSwiftType = "545" Or _varSwiftType = "546" Or _varSwiftType = "547" Or _varSwiftType = "548" Then
            Process548(varLine, varJoinSection)
        ElseIf _varSwiftType = "564" Then
            Process564(varLine, varJoinSection, LstCorpActionTermInfo, ColCorpActionTermInfo)
        ElseIf _varSwiftType = "566" Then
            Process566(varLine, varJoinSection, LstCorpActionTermInfo, ColCorpActionTermInfo)
        ElseIf _varSwiftType = "568" Then
            Process568(varLine, varJoinSection)
        ElseIf _varSwiftType = "900" Then
            Process900(varLine, varJoinSection)
        ElseIf _varSwiftType = "910" Then
            Process910(varLine, varJoinSection)
        ElseIf _varSwiftType = "920" Then
            Process920(varLine)
        ElseIf _varSwiftType = "940" Then
            Process940(varLine, varJoinSection, LstAccountInfo, ColAddInfo)
        ElseIf _varSwiftType = "942" Then
            Process942(varLine, varJoinSection, LstAccountInfo, ColAddInfo)
        ElseIf _varSwiftType = "950" Then
            Process950(varLine, varJoinSection, LstAccountInfo, ColAddInfo)
        Else
            ProcessAll(varLine, TempDescription)
        End If
    End Sub

    Public Sub ReadSwift()
        Dim varLine As String = ""
        Dim writeLineToTextFile As String = ""
        Dim SwiftFinFileDate As Date

        clsIMS.AddAudit(1, 17, "Starting ReadSwift - ReadSwift")
        Try
            clsIMS.UpdateAttemptedReadSwiftTime()
            Dim SwiftFileNames As New List(Of String)
            If IO.Directory.Exists(_varSwiftFilePath) Then
                Dim fileDir As New System.IO.DirectoryInfo(_varSwiftFilePath)
                Dim swiftfiles = fileDir.GetFiles.ToList
                Dim LatestFileFromSwift = (From file In swiftfiles Select file Order By file.CreationTime Descending).FirstOrDefault
                Dim LatestFileDateTimeFromSwift As Date = LatestFileFromSwift.LastWriteTime

                swiftfiles = Nothing
                LatestFileFromSwift = Nothing
                Dim DaysSearch As Integer = 3
                Dim fileHistoryDate As Date = DateAdd(DateInterval.Day, -DaysSearch, LatestFileDateTimeFromSwift)
                'DaysSearch = 0
                SwiftFileNames = GetSwiftFileNamesInDictionary("select sr_filename from DolfinPaymentSwiftRead where cast(sr_filedatetime as date) between cast('" & fileHistoryDate.ToString("yyyy-MM-dd") & "' as date) and cast(getdate() as date) group by sr_filename")

                If IsNothing(SwiftFileNames) Then
                    SwiftFileNames = New List(Of String)
                    SwiftFileNames.Add("")
                End If

                Dim files As Generic.IEnumerable(Of Object)
                files = From f In fileDir.EnumerateFiles() Where f.LastWriteTime > fileHistoryDate
                Dim SwiftFinFile As IO.FileInfo

                For Each SwiftFinFile In files
                    SwiftFinFileDate = IO.File.GetLastWriteTime(SwiftFinFile.FullName)
                    If DateDiff("d", SwiftFinFileDate.Date, Now.Date) <= DaysSearch And Len(Dir(SwiftFinFile.FullName)) = 30 Then
                        ClearClass()
                        Dim LstAccountInfo As New List(Of Dictionary(Of String, String))
                        Dim ColAddInfo As New Dictionary(Of String, String)
                        Dim LstCorpActionTermInfo As New List(Of Dictionary(Of String, String))
                        Dim ColCorpActionTermInfo As New Dictionary(Of String, String)
                        Dim TempDescription As String = ""
                        If Not SwiftFileNames.Contains(Dir(SwiftFinFile.FullName)) Then
                            writeLineToTextFile = ""
                            Dim objSwiftReader As IO.StreamReader = GetSwiftFile(SwiftFinFile.FullName, SwiftFinFileDate)

                            Dim vLineNum As Integer = 1
                            Dim varJoinSection As String = ""
                            Do While objSwiftReader.Peek() >= 0
                                varLine = objSwiftReader.ReadLine()
                                If vLineNum = 1 Then
                                    Call ProcessHeader(varLine)
                                End If

                                Call ProcessLine(varLine, SwiftFinFile.FullName, writeLineToTextFile, varJoinSection, LstAccountInfo, ColAddInfo, LstCorpActionTermInfo, ColCorpActionTermInfo, TempDescription)

                                TempDescription = ""
                                If (InStr(1, varLine, "{5:", vbTextCompare) <> 0 And InStr(1, varLine, "{1:", vbTextCompare) <> 0) Then
                                    ClearClassData()
                                    Call ProcessHeader(varLine)
                                    TempDescription = Right(varLine, Len(varLine) - InStr(1, varLine, "{1:", vbTextCompare) + 1)
                                End If

                                vLineNum += 1
                            Loop

                            If vLineNum = 2 Then
                                _varDescription = varLine
                                Call SaveSwiftRowToDB("Processed Swift Type")
                            End If

                            If clsIMS.GetSQLItem("select sr_filename from DolfinPaymentSwiftRead where sr_filename = '" & Dir(SwiftFinFile.FullName) & "'") = "" Then
                                Call SaveUnprocessedSwiftRowToDB()
                            End If

                            objSwiftReader.Close()
                            objSwiftReader = Nothing
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 14, Err.Description & " - error in ReadSwift")
        End Try
    End Sub

    Public Sub ReadSwiftold()
        Dim varLine As String = ""
        Dim writeLineToTextFile As String = ""
        Dim SwiftFinFileDate As Date

        Try
            clsIMS.UpdateAttemptedReadSwiftTime()
            Dim SwiftFileNames As New List(Of String)
            If IO.Directory.Exists(_varSwiftFilePath) Then
                Dim fileDir As New System.IO.DirectoryInfo(_varSwiftFilePath)
                Dim swiftfiles = fileDir.GetFiles.ToList
                Dim LatestFileFromSwift = (From file In swiftfiles Select file Order By file.CreationTime Descending).FirstOrDefault
                Dim LatestFileDateTimeFromSwift As Date = LatestFileFromSwift.LastWriteTime

                fileDir = Nothing
                swiftfiles = Nothing
                LatestFileFromSwift = Nothing
                Dim DaysSearch As Integer = DateDiff(DateInterval.Day, LatestFileDateTimeFromSwift, Now) + 3
                'DaysSearch = 0
                SwiftFileNames = GetSwiftFileNamesInDictionary("select sr_filename from DolfinPaymentSwiftRead where cast(sr_filedatetime as date) between cast(getdate()-" & DaysSearch & " as date) and cast(getdate() as date) group by sr_filename")

                If IsNothing(SwiftFileNames) Then
                    SwiftFileNames = New List(Of String)
                    SwiftFileNames.Add("")
                End If

                For Each SwiftFinFile As String In System.IO.Directory.GetFiles(_varSwiftFilePath, getSearchString(DaysSearch))
                    SwiftFinFileDate = IO.File.GetLastWriteTime(SwiftFinFile)
                    If DateDiff("d", SwiftFinFileDate.Date, Now.Date) <= DaysSearch And Len(Dir(SwiftFinFile)) = 30 Then
                        ClearClass()
                        Dim LstAccountInfo As New List(Of Dictionary(Of String, String))
                        Dim ColAddInfo As New Dictionary(Of String, String)
                        Dim LstCorpActionTermInfo As New List(Of Dictionary(Of String, String))
                        Dim ColCorpActionTermInfo As New Dictionary(Of String, String)
                        Dim TempDescription As String = ""
                        If Not SwiftFileNames.Contains(Dir(SwiftFinFile)) Then
                            writeLineToTextFile = ""
                            Dim objSwiftReader As IO.StreamReader = GetSwiftFile(SwiftFinFile, SwiftFinFileDate)

                            Dim vLineNum As Integer = 1
                            Dim varJoinSection As String = ""
                            Do While objSwiftReader.Peek() >= 0
                                varLine = objSwiftReader.ReadLine()
                                If vLineNum = 1 Then
                                    Call ProcessHeader(varLine)
                                End If

                                Call ProcessLine(varLine, SwiftFinFile, writeLineToTextFile, varJoinSection, LstAccountInfo, ColAddInfo, LstCorpActionTermInfo, ColCorpActionTermInfo, TempDescription)

                                TempDescription = ""
                                If (InStr(1, varLine, "{5:", vbTextCompare) <> 0 And InStr(1, varLine, "{1:", vbTextCompare) <> 0) Then
                                    Call ProcessHeader(varLine)
                                    TempDescription = Right(varLine, Len(varLine) - InStr(1, varLine, "{1:", vbTextCompare) + 1)
                                End If

                                vLineNum += 1
                            Loop

                            If vLineNum = 2 Then
                                _varDescription = varLine
                                Call SaveSwiftRowToDB("Processed Swift Type")
                            End If

                            If clsIMS.GetSQLItem("select sr_filename from DolfinPaymentSwiftRead where sr_filename = '" & Dir(SwiftFinFile) & "'") = "" Then
                                Call SaveUnprocessedSwiftRowToDB()
                            End If

                            objSwiftReader.Close()
                            objSwiftReader = Nothing
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 14, Err.Description & " - error in ReadSwift")
        End Try
    End Sub

    Public Sub ReadSwiftErrors()

        Dim varLine As String = ""
        Dim varSaveLine As String = ""
        Dim varErrorSwiftType As String = ""
        Dim SwiftErrorFinFileDate As Date

        clsIMS.AddAudit(1, 17, "Starting ReadSwiftErrors - ReadSwift")
        Try
            Dim DaysSearch As Integer = 0
            Dim SwiftErrorFileNames As New List(Of String)
            SwiftErrorFileNames = GetSwiftFileNamesInDictionary("select sre_filename from DolfinPaymentSwiftReadErrors where cast(sre_filedatetime as date) between cast(getdate()-" & DaysSearch & " as date) and cast(getdate() as date) group by sre_filename")

            If IsNothing(SwiftErrorFileNames) Then
                SwiftErrorFileNames = New List(Of String)
                SwiftErrorFileNames.Add("")
            End If

            If IO.Directory.Exists(_varSwiftFilePathErrors) Then
                For Each SwiftErrorFinFile As String In System.IO.Directory.GetFiles(_varSwiftFilePathErrors, getSearchString(DaysSearch))
                    SwiftErrorFinFileDate = IO.File.GetLastWriteTime(SwiftErrorFinFile)
                    If DateDiff("d", SwiftErrorFinFileDate.Date, Now.Date) <= DaysSearch Then
                        If Not SwiftErrorFileNames.Contains(Dir(SwiftErrorFinFile)) Then
                            Dim varErrorReference As String = ""
                            Dim objSwiftReader As IO.StreamReader = GetSwiftFile(SwiftErrorFinFile, SwiftErrorFinFileDate)
                            Dim vLineNum As Integer = 1

                            Do While objSwiftReader.Peek() >= 0
                                varLine = objSwiftReader.ReadLine()
                                If vLineNum = 1 Then
                                    varErrorSwiftType = GetFileTypeFromSwift(varLine)
                                End If
                                If Left(varLine, 3) = ":20" Then
                                    If Left(varLine, 4) = ":20:" Then
                                        varErrorReference = Right(varLine, Len(varLine) - 4)
                                    Else
                                        varErrorReference = Right(varLine, Len(varLine) - 12)
                                    End If
                                End If

                                If varLine = "-}" Then
                                    varSaveLine &= varLine
                                Else
                                    varSaveLine = varSaveLine & varLine & vbNewLine
                                End If

                                vLineNum += 1
                            Loop

                            Dim varError As String = GetSwiftError(Dir(SwiftErrorFinFile), SwiftErrorFinFileDate)
                            Call SaveSwiftErrorRowToDB(Dir(SwiftErrorFinFile), SwiftErrorFinFileDate, varErrorSwiftType, varErrorReference, varError, varSaveLine)

                            objSwiftReader.Close()
                            objSwiftReader = Nothing
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 14, Err.Description & " - error in ReadSwiftErrors")
        End Try
    End Sub

    Private Function GetSwiftError(ByVal SwiftFinFile As String, ByVal SwiftFinFileDate As Date) As String
        GetSwiftError = ""
        Dim vLine As String = ""
        Dim varErrorCode As String = ""
        Dim varID As String = ""

        Try
            For Each SwiftFinErrorFile As String In System.IO.Directory.GetFiles(_varSwiftFilePathErrors, Left(SwiftFinFile, InStr(SwiftFinFile, ".", vbTextCompare) - 1) & "*.fin.err")
                Dim SwiftErrorSearch = Left(Dir(SwiftFinErrorFile), Len(Dir(SwiftFinErrorFile)) - 15)

                If InStr(1, SwiftFinFile, SwiftErrorSearch, vbTextCompare) <> 0 Then
                    Dim objSwiftErrorReader As IO.StreamReader = GetSwiftFile(SwiftFinErrorFile, SwiftFinFileDate)
                    If objSwiftErrorReader IsNot Nothing Then
                        If objSwiftErrorReader.Peek() >= 0 Then
                            GetSwiftError = objSwiftErrorReader.ReadLine()
                            Exit For
                        End If
                    End If
                    objSwiftErrorReader.Close()
                End If
            Next
        Catch ex As Exception
            clsIMS.AddAudit(3, 14, Err.Description & " - no return for GetSwiftError")
        End Try
    End Function

    Private Function GetSwiftFile(ByVal SwiftFinFile As String, ByVal SwiftFinFileDate As Date) As IO.StreamReader
        GetSwiftFile = Nothing
        Try
            Dim swiftstream As New IO.FileStream(SwiftFinFile, IO.FileMode.Open, IO.FileAccess.Read)
            GetSwiftFile = New IO.StreamReader(swiftstream)
            _varSwiftFileName = Dir(SwiftFinFile)
            _varSwiftFileDateTime = SwiftFinFileDate
            Dim varSwiftFileNameDateStr As String = Left(Right(_varSwiftFileName, Len(_varSwiftFileName) - InStr(_varSwiftFileName, "_", vbTextCompare)), 8)
            If IsDate(Left(varSwiftFileNameDateStr, 4) & "-" & Mid(varSwiftFileNameDateStr, 5, 2) & "-" & Right(varSwiftFileNameDateStr, 2)) Then
                _varSwiftFileNameDate = Left(varSwiftFileNameDateStr, 4) & "-" & Mid(varSwiftFileNameDateStr, 5, 2) & "-" & Right(varSwiftFileNameDateStr, 2)
            End If
        Catch ex As Exception
            GetSwiftFile = Nothing
            clsIMS.AddAudit(3, 14, Err.Description & " - no return for GetSwiftFile")
        End Try
    End Function

    Public Function GetStatusFromSwift(ByRef varLine As String) As Boolean
        GetStatusFromSwift = False
        Try
            If InStr(1, varLine, "{451:0}", vbTextCompare) <> 0 Then
                _varSwiftAcknowledged = True
                GetStatusFromSwift = True
            ElseIf InStr(1, varLine, "{451:1}", vbTextCompare) <> 0 Then
                _varSwiftAcknowledged = False
                GetStatusFromSwift = True
                Dim varErrorStart = InStr(1, varLine, "{405:", vbTextCompare)
                If varErrorStart <> 0 Then
                    _varSwiftErrorCode = GetMessageBlock(varErrorStart, varLine)
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 14, Err.Description & " - no return for GetStatusFromSwift")
        End Try
    End Function

    Public Sub GetUserReference(ByRef varLine As String)
        Try
            Dim varUserReferenceStart = InStr(1, varLine, "{3:{108:", vbTextCompare)
            If varUserReferenceStart <> 0 Then
                _varSwiftUserReference = Replace(Replace(GetMessageBlock(varUserReferenceStart, varLine), "{3:{108:", ""), "}", "")
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 14, Err.Description & " - no return for GetUserReference")
        End Try
    End Sub

    Public Sub GetSwiftCode(ByRef varLine As String)
        Try
            Dim varSwiftCodeStart = InStr(1, varLine, "{2:", vbTextCompare)
            If varSwiftCodeStart <> 0 Then
                _varSwiftCode = Mid(varLine, varSwiftCodeStart + 17, 8)
                If clsIMS.MessageList.ContainsKey("MT" & _varSwiftType) Then
                    Dim MsgType As Tuple(Of Integer, String, Boolean, Boolean) = clsIMS.MessageList.Item("MT" & _varSwiftType)

                    If MsgType.Item3 = False Then
                        _varSwiftCode = Mid(varLine, varSwiftCodeStart + 7, 8)
                    End If
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 14, Err.Description & " - no return for GetSwiftCode")
        End Try
    End Sub

    Private Function GetFileTypeFromSwift(ByRef varLine As String) As String
        Dim varPos As Integer = 0
        GetFileTypeFromSwift = ""
        Try
            varPos = InStr(1, varLine, "{2:I", vbTextCompare)
            If varPos <> 0 Then
                _varSwiftType = Right(Replace(UCase(Mid(varLine, varPos, 7)), "{2:I", ""), 3)
            Else
                varPos = InStr(1, varLine, "{2:O", vbTextCompare)
                If varPos <> 0 Then
                    _varSwiftType = Right(Replace(UCase(Mid(varLine, varPos, 7)), "{2:O", ""), 3)
                End If
            End If
            GetFileTypeFromSwift = _varSwiftType
        Catch ex As Exception
            clsIMS.AddAudit(3, 14, Err.Description & " - no return for GetFileTypeFromSwift")
        End Try
    End Function

    Private Function GetMessageBlock(ByVal varStart As Integer, ByVal varLine As String) As String
        GetMessageBlock = ""
        Try
            If Mid(varLine, varStart, 1) = "{" Then
                For i As Integer = varStart To Len(varLine)
                    If Mid(varLine, i, 1) = "}" Then
                        GetMessageBlock &= "}"
                        Exit For
                    ElseIf Mid(varLine, i, 1) <> "}" Then
                        GetMessageBlock &= Mid(varLine, i, 1)
                    End If
                Next
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 14, Err.Description & " - no return for GetMessageBlock")
        End Try
    End Function

    Public Sub Process101(ByRef varLine As String, ByRef varJoinSection As String)
        Const cSwift101Reference As String = ":20:"
        Const cSwift101FunctionType As String = ":23B:"
        Const cSwift101Detail As String = ":32"
        Const cSwift101AccOrder As String = ":50"
        Const cSwift101AccOrderBank As String = ":52"
        Const cSwift101AccIntBankIBAN As String = ":53"
        Const cSwift101AccIntBank As String = ":56"
        Const cSwift101AccBenBank As String = ":57"
        Const cSwift101AccBen As String = ":59:"
        Const cSwift101Description As String = ":70"

        Dim var101DetailStr As String = ""
        Dim varOrderInstitutionStr As String = ""
        Dim varOrderBankInstitutionStr As String = ""
        Dim varIntermediaryInstitutionStr As String = ""
        Dim varBeneficiaryInstitutionStr As String = ""
        Dim varBeneficiaryBankInstitutionStr As String = ""
        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If Left(varLine, 1) = ":" Then
                    varJoinSection = ""
                End If
                If Left(varLine, 4) = cSwift101Reference Then
                    _varReference = Right(varLine, Len(varLine) - 4)
                End If
                If Left(varLine, 5) = cSwift101FunctionType Then
                    _varFunctionType = Right(varLine, Len(varLine) - 5)
                End If
                If Left(varLine, 3) = cSwift101Detail Then
                    var101DetailStr = Right(varLine, Len(varLine) - 5)
                    _varAmountCCY = Mid(var101DetailStr, 7, 3)
                    _varAmount = FormatNumber(Right(var101DetailStr, Len(var101DetailStr) - 9))
                End If
                If Left(varLine, 3) = cSwift101AccOrder Or varJoinSection = cSwift101AccOrder Then
                    If Left(varLine, 3) = cSwift101AccOrder Then
                        varOrderInstitutionStr = Right(varLine, Len(varLine) - 5)
                    Else
                        varOrderInstitutionStr = varLine
                    End If
                    If Left(varOrderInstitutionStr, 1) = "/" Then
                        _varOrderIBAN = Right(varOrderInstitutionStr, Len(varOrderInstitutionStr) - 1)
                    Else
                        _varOrderInstitution = _varOrderInstitution & varOrderInstitutionStr & ","
                    End If
                    varJoinSection = cSwift101AccOrder
                End If
                If Left(varLine, 3) = cSwift101AccOrderBank Or varJoinSection = cSwift101AccOrderBank Then
                    If Left(varLine, 3) = cSwift101AccOrderBank Then
                        varOrderBankInstitutionStr = Right(varLine, Len(varLine) - 5)
                    Else
                        varOrderBankInstitutionStr = varLine
                    End If
                    If Left(varOrderBankInstitutionStr, 1) = "/" Then
                        _varOrderBankIBAN = Right(varOrderBankInstitutionStr, Len(varOrderBankInstitutionStr) - 1)
                    Else
                        _varOrderBankInstitution = _varOrderBankInstitution & varOrderBankInstitutionStr & ","
                    End If
                    varJoinSection = cSwift101AccOrderBank
                End If
                If Left(varLine, 3) = cSwift101AccIntBankIBAN Then
                    _varIntermediaryIBAN = Right(varLine, Len(varLine) - 5)
                    If Left(_varIntermediaryIBAN, 1) = "/" Then
                        _varIntermediaryIBAN = Right(_varIntermediaryIBAN, Len(_varIntermediaryIBAN) - 1)
                    End If
                End If
                If Left(varLine, 3) = cSwift101AccIntBank Then
                    _varIntermediary = Right(varLine, Len(varLine) - 5)
                    If Left(_varIntermediary, 1) = "/" Then
                        _varIntermediary = Right(_varIntermediary, Len(_varIntermediary) - 1)
                    ElseIf Left(_varIntermediary, 2) = "//" Then
                        _varIntermediary = Right(_varIntermediary, Len(_varIntermediary) - 2)
                    End If
                End If
                If Left(varLine, 3) = cSwift101AccBenBank Or varJoinSection = cSwift101AccBenBank Then
                    If Left(varLine, 3) = cSwift101AccBenBank Then
                        varBeneficiaryBankInstitutionStr = Right(varLine, Len(varLine) - 5)
                    Else
                        varBeneficiaryBankInstitutionStr = varLine
                    End If
                    If Left(varBeneficiaryBankInstitutionStr, 1) = "/" Then
                        _varBeneficiaryBankIBAN = Right(varBeneficiaryBankInstitutionStr, Len(varBeneficiaryBankInstitutionStr) - 1)
                    Else
                        _varBeneficiaryBankInstitution = _varBeneficiaryBankInstitution & varBeneficiaryBankInstitutionStr & ","
                    End If
                    varJoinSection = cSwift101AccBenBank
                End If
                If Left(varLine, 4) = cSwift101AccBen Or varJoinSection = cSwift101AccBen Then
                    If Left(varLine, 4) = cSwift101AccBen Then
                        varBeneficiaryInstitutionStr = Right(varLine, Len(varLine) - 4)
                    Else
                        varBeneficiaryInstitutionStr = varLine
                    End If
                    If Left(varBeneficiaryInstitutionStr, 1) = "/" Then
                        _varBeneficiaryIBAN = Right(varBeneficiaryInstitutionStr, Len(varBeneficiaryInstitutionStr) - 1)
                    Else
                        _varBeneficiaryInstitution = _varBeneficiaryInstitution & varBeneficiaryInstitutionStr & ","
                    End If
                    varJoinSection = cSwift101AccBen
                End If
                If Left(varLine, 4) = cSwift101Description Or varJoinSection = cSwift101Description Then
                    _varDescription &= Right(varLine, Len(varLine) - 4)
                    varJoinSection = cSwift101Description
                End If
            Else
                If _varAmount <> 0 Then
                    SaveSwiftRowToDB("Processed Swift Type")
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process101, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process103(ByRef varLine As String, ByRef varJoinSection As String)
        Const cSwift103Reference As String = ":20:"
        Const cSwift103FunctionType As String = ":23B:"
        Const cSwift103Detail As String = ":32"
        Const cSwift103AccOrder As String = ":50"
        Const cSwift103AccOrderBank As String = ":52"
        Const cSwift103AccIntBankIBAN As String = ":53"
        Const cSwift103AccIntBank As String = ":56"
        Const cSwift103AccBenBank As String = ":57"
        Const cSwift103AccBen As String = ":59:"
        Const cSwift103Description As String = ":70:"
        Const cSwift103Reason As String = ":72:"

        Dim var103DetailStr As String = ""
        Dim varOrderInstitutionStr As String = ""
        Dim varOrderBankInstitutionStr As String = ""
        Dim varIntermediaryInstitutionStr As String = ""
        Dim varBeneficiaryInstitutionStr As String = ""
        Dim varBeneficiaryBankInstitutionStr As String = ""
        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If Left(varLine, 1) = ":" Then
                    varJoinSection = ""
                End If
                If Left(varLine, 4) = cSwift103Reference Then
                    _varReference = Right(varLine, Len(varLine) - 4)
                End If
                If Left(varLine, 5) = cSwift103FunctionType Then
                    _varFunctionType = Right(varLine, Len(varLine) - 5)
                End If
                If Left(varLine, 3) = cSwift103Detail Then
                    var103DetailStr = Right(varLine, Len(varLine) - 5)
                    _varTradeDate = CDate(Mid(var103DetailStr, 5, 2) & "/" & Mid(var103DetailStr, 3, 2) & "/" & Left(var103DetailStr, 2)).ToString("yyyy/MM/dd")
                    _varSettleDate = _varTradeDate
                    _varAmountCCY = Mid(var103DetailStr, 7, 3)
                    _varAmount = FormatNumber(Right(var103DetailStr, Len(var103DetailStr) - 9))
                End If
                If Left(varLine, 3) = cSwift103AccOrder Or varJoinSection = cSwift103AccOrder Then
                    If InStr(1, varLine, cSwift103AccOrder, vbTextCompare) <> 0 Then
                        varOrderInstitutionStr = Right(varLine, Len(varLine) - 5)
                    Else
                        varOrderInstitutionStr = varLine
                    End If
                    If Left(varOrderInstitutionStr, 1) = "/" Then
                        _varOrderIBAN = Right(varOrderInstitutionStr, Len(varOrderInstitutionStr) - 1)
                    Else
                        _varOrderInstitution = _varOrderInstitution & varOrderInstitutionStr & ","
                    End If
                    varJoinSection = cSwift103AccOrder
                End If
                If Left(varLine, 3) = cSwift103AccOrderBank Or varJoinSection = cSwift103AccOrderBank Then
                    If Left(varLine, 3) = cSwift103AccOrderBank Then
                        varOrderBankInstitutionStr = Right(varLine, Len(varLine) - 5)
                    Else
                        varOrderBankInstitutionStr = varLine
                    End If
                    If Left(varOrderBankInstitutionStr, 1) = "/" Then
                        _varOrderBankIBAN = Right(varOrderBankInstitutionStr, Len(varOrderBankInstitutionStr) - 1)
                    Else
                        _varOrderBankInstitution = _varOrderBankInstitution & varOrderBankInstitutionStr & ","
                    End If
                    varJoinSection = cSwift103AccOrderBank
                End If
                If Left(varLine, 3) = cSwift103AccIntBankIBAN Then
                    _varIntermediaryIBAN = Right(varLine, Len(varLine) - 5)
                    If Left(_varIntermediaryIBAN, 1) = "/" Then
                        _varIntermediaryIBAN = Right(_varIntermediaryIBAN, Len(_varIntermediaryIBAN) - 1)
                    End If
                End If
                If Left(varLine, 3) = cSwift103AccIntBank Then
                    _varIntermediary = Right(varLine, Len(varLine) - 5)
                    If Left(_varIntermediary, 1) = "/" Then
                        _varIntermediary = Right(_varIntermediary, Len(_varIntermediary) - 1)
                    ElseIf Left(_varIntermediary, 2) = "//" Then
                        _varIntermediary = Right(_varIntermediary, Len(_varIntermediary) - 2)
                    End If
                End If
                If Left(varLine, 3) = cSwift103AccBenBank Or varJoinSection = cSwift103AccBenBank Then
                    If Left(varLine, 3) = cSwift103AccBenBank Then
                        varBeneficiaryBankInstitutionStr = Right(varLine, Len(varLine) - 5)
                    Else
                        varBeneficiaryBankInstitutionStr = varLine
                    End If
                    If Left(varBeneficiaryBankInstitutionStr, 1) = "/" Then
                        _varBeneficiaryBankIBAN = Right(varBeneficiaryBankInstitutionStr, Len(varBeneficiaryBankInstitutionStr) - 1)
                    Else
                        _varBeneficiaryBankInstitution = _varBeneficiaryBankInstitution & varBeneficiaryBankInstitutionStr & ","
                    End If
                    varJoinSection = cSwift103AccBenBank
                End If
                If Left(varLine, 4) = cSwift103AccBen Or varJoinSection = cSwift103AccBen Then
                    If Left(varLine, 4) = cSwift103AccBen Then
                        varBeneficiaryInstitutionStr = Right(varLine, Len(varLine) - 4)
                    Else
                        varBeneficiaryInstitutionStr = varLine
                    End If
                    If Left(varBeneficiaryInstitutionStr, 1) = "/" Then
                        _varBeneficiaryIBAN = Right(varBeneficiaryInstitutionStr, Len(varBeneficiaryInstitutionStr) - 1)
                    Else
                        _varBeneficiaryInstitution = _varBeneficiaryInstitution & varBeneficiaryInstitutionStr & ","
                    End If
                    varJoinSection = cSwift103AccBen
                End If
                If Left(varLine, 4) = cSwift103Description Or varJoinSection = cSwift103Description Then
                    _varDescription &= Replace(varLine, cSwift103Description, "")
                    varJoinSection = cSwift103Description
                End If
                If Left(varLine, 4) = cSwift103Reason Or varJoinSection = cSwift103Reason Then
                    _varReason1 &= Replace(varLine, cSwift103Reason, "")
                    varJoinSection = cSwift103Reason
                End If
            Else
                If _varAmount <> 0 Then
                    SaveSwiftRowToDB("Processed Swift Type")
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process103, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process200(ByRef varLine As String, ByRef varJoinSection As String)
        Const cSwift200Reference As String = ":20:"
        Const cSwift200Detail As String = ":32"
        Const cSwift200AccOrderIBAN As String = ":53"
        Const cSwift200IntIBAN As String = ":56"
        Const cSwift200AccBenIBAN As String = ":57"

        Dim var200DetailStr As String = ""
        Dim varOrderInstitutionStr As String = ""
        Dim varIntInstitutionStr As String
        Dim varOrderBankInstitutionStr As String = ""

        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If Left(varLine, 1) = ":" Then
                    varJoinSection = ""
                End If
                If Left(varLine, 4) = cSwift200Reference Then
                    _varReference = Right(varLine, Len(varLine) - 4)
                End If
                If Left(varLine, 3) = cSwift200Detail <> 0 Then
                    var200DetailStr = Right(varLine, Len(varLine) - 5)
                    _varTradeDate = CDate(Mid(var200DetailStr, 5, 2) & "/" & Mid(var200DetailStr, 3, 2) & "/" & Left(var200DetailStr, 2)).ToString("yyyy/MM/dd")
                    _varSettleDate = _varTradeDate
                    _varAmountCCY = Mid(var200DetailStr, 7, 3)
                    _varAmount = FormatNumber(Right(var200DetailStr, Len(var200DetailStr) - 9))
                End If
                If Left(varLine, 5) = cSwift200AccOrderIBAN Then
                    varOrderInstitutionStr = Right(varLine, Len(varLine) - 5)
                    If Left(varOrderInstitutionStr, 1) = "/" Then
                        _varOrderIBAN = Right(varOrderInstitutionStr, Len(varOrderInstitutionStr) - 1)
                    Else
                        _varOrderInstitution = _varOrderInstitution & varOrderInstitutionStr & ","
                    End If
                    varJoinSection = cSwift200AccOrderIBAN
                End If
                If Left(varLine, 5) = cSwift200IntIBAN Then
                    varIntInstitutionStr = Right(varLine, Len(varLine) - 5)
                    If Left(varIntInstitutionStr, 1) = "/" Then
                        _varIntermediaryIBAN = Right(varIntInstitutionStr, Len(varIntInstitutionStr) - 1)
                    Else
                        _varIntermediary = _varIntermediary & varIntInstitutionStr & ","
                    End If
                    varJoinSection = cSwift200IntIBAN
                End If
                If Left(varLine, 5) = cSwift200AccBenIBAN Then
                    varOrderBankInstitutionStr = Right(varLine, Len(varLine) - 5)
                    If Left(varOrderBankInstitutionStr, 1) = "/" Then
                        _varOrderBankIBAN = Right(varOrderBankInstitutionStr, Len(varOrderBankInstitutionStr) - 1)
                    Else
                        _varOrderBankInstitution = _varOrderBankInstitution & varOrderBankInstitutionStr & ","
                    End If
                    varJoinSection = cSwift200AccBenIBAN
                End If
            Else
                If _varAmount <> 0 Then
                    SaveSwiftRowToDB("Processed Swift Type")
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process200, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process202(ByRef varLine As String, ByRef varJoinSection As String)
        Const cSwift202Reference As String = ":20:"
        Const cSwift202RelatedReference As String = ":21:"
        Const cSwift202Detail As String = ":32"
        Const cSwift200AccOrderIBANRUB As String = ":52"
        Const cSwift200AccOrderIBAN As String = ":53"
        Const cSwift200IntIBAN As String = ":56"
        Const cSwift200AccBenIBANRUB As String = ":57"
        Const cSwift200AccBenIBAN As String = ":58"
        Const cSwift200Description As String = ":72"

        Dim var202DetailStr As String = ""
        Dim varOrderInstitutionStr As String = ""
        Dim varIntInstitutionStr As String
        Dim varOrderBankInstitutionStr As String = ""

        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If Left(varLine, 1) = ":" Then
                    varJoinSection = ""
                End If
                If Left(varLine, 4) = cSwift202Reference Then
                    _varReference = Right(varLine, Len(varLine) - 4)
                End If
                If Left(varLine, 4) = cSwift202RelatedReference Then
                    _varRelatedReference = Right(varLine, Len(varLine) - 4)
                End If
                If InStr(1, varLine, cSwift202Detail, vbTextCompare) <> 0 Then
                    var202DetailStr = Right(varLine, Len(varLine) - 5)
                    _varTradeDate = CDate(Mid(var202DetailStr, 5, 2) & "/" & Mid(var202DetailStr, 3, 2) & "/" & Left(var202DetailStr, 2)).ToString("yyyy/MM/dd")
                    _varSettleDate = _varTradeDate
                    _varAmountCCY = Mid(var202DetailStr, 7, 3)
                    _varAmount = FormatNumber(Right(var202DetailStr, Len(var202DetailStr) - 9))
                End If
                If Left(varLine, 5) = cSwift200AccOrderIBAN Or Left(varLine, 5) = cSwift200AccOrderIBANRUB Then
                    varOrderInstitutionStr = Right(varLine, Len(varLine) - 5)
                    If Left(varOrderInstitutionStr, 1) = "/" Then
                        _varOrderIBAN = Right(varOrderInstitutionStr, Len(varOrderInstitutionStr) - 1)
                    Else
                        _varOrderInstitution = _varOrderInstitution & varOrderInstitutionStr & ","
                    End If
                    varJoinSection = cSwift200AccOrderIBAN
                End If
                If Left(varLine, 5) = cSwift200IntIBAN Then
                    varIntInstitutionStr = Right(varLine, Len(varLine) - 5)
                    If Left(varIntInstitutionStr, 1) = "//" Then
                        _varIntermediary = Right(varIntInstitutionStr, Len(varIntInstitutionStr) - 2)
                    ElseIf Left(varIntInstitutionStr, 1) = "/" Then
                        _varIntermediary = Right(varIntInstitutionStr, Len(varIntInstitutionStr) - 1)
                    Else
                        _varIntermediary = _varIntermediary & varIntInstitutionStr & ","
                    End If
                    varJoinSection = cSwift200IntIBAN
                End If
                If Left(varLine, 5) = cSwift200AccBenIBAN Or Left(varLine, 5) = cSwift200AccBenIBANRUB Then
                    varOrderBankInstitutionStr = Right(varLine, Len(varLine) - 5)
                    If Left(varOrderBankInstitutionStr, 1) = "/" Then
                        _varOrderBankIBAN = Right(varOrderBankInstitutionStr, Len(varOrderBankInstitutionStr) - 1)
                    Else
                        _varOrderBankInstitution = _varOrderBankInstitution & varOrderBankInstitutionStr & ","
                    End If
                    varJoinSection = cSwift200AccBenIBAN
                End If
                If Left(varLine, 4) = cSwift200Description Or varJoinSection = cSwift200Description Then
                    _varDescription &= Right(varLine, Len(varLine) - 4)
                    varJoinSection = cSwift200Description
                End If
            Else
                If _varAmount <> 0 Then
                    SaveSwiftRowToDB("Processed Swift Type")
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process202, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process210(ByRef varLine As String)
        Const cSwift210Reference As String = ":20:"
        Const cSwift210AccNo As String = ":25"
        Const cSwift210date As String = ":30"
        Const cSwift210RelatedReference As String = ":21"
        Const cSwift210Amount As String = ":32"
        Const cSwift210Sender As String = ":52"
        Const cSwift210Intermediary As String = ":56"

        Dim var210DetailStr As String = ""
        Dim var210DateStr As String = ""

        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If Left(varLine, 4) = cSwift210Reference Then
                    _varReference = Right(varLine, Len(varLine) - 4)
                End If
                If Left(varLine, 3) = cSwift210AccNo Then
                    _varAccountNo = Right(varLine, Len(varLine) - 4)
                End If
                If Left(varLine, 3) = cSwift210date Then
                    var210DateStr = Right(varLine, Len(varLine) - 4)
                    _varTradeDate = CDate(Mid(var210DateStr, 5, 2) & "/" & Mid(var210DateStr, 3, 2) & "/" & Left(var210DateStr, 2)).ToString("yyyy/MM/dd")
                End If
                If Left(varLine, 3) = cSwift210RelatedReference Then
                    _varRelatedReference = Right(varLine, Len(varLine) - 4)
                End If
                If Left(varLine, 3) = cSwift210Amount Then
                    var210DetailStr = Right(varLine, Len(varLine) - 5)
                    _varAmountCCY = Left(var210DetailStr, 3)
                    _varAmount = FormatNumber(Right(var210DetailStr, Len(var210DetailStr) - 3))
                End If
                If Left(varLine, 3) = cSwift210Sender Then
                    _varOrderInstitution = Right(varLine, Len(varLine) - 5)
                End If
                If Left(varLine, 3) = cSwift210Intermediary Then
                    _varIntermediary = Right(varLine, Len(varLine) - 5)
                End If
            Else
                If _varAmount <> 0 Then
                    SaveSwiftRowToDB("Processed Swift Type")
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process202, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process502(ByRef varLine As String)
        Const cSwift502Ref As String = ":20C::SEME//"
        Const cSwift502FunctionType As String = ":23G:"
        Const cSwift502Date As String = ":98C::PREP//"
        Const cSwift502AccNo As String = ":97A::SAFE//"
        Const cSwift502ISIN As String = ":35B:"
        Const cSwift502Indicator1 As String = ":22F::"
        Const cSwift502OrderUnit As String = ":36B::ORDR//"
        Const cSwift502OrderAmt As String = ":19A::ORDR//"
        Const cSwift502Locations As String = ":95"

        Dim varDateStr As String = ""
        Dim varIndicatorStr As String = ""
        Dim varAmtStr As String = ""
        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If Left(varLine, 12) = cSwift502Ref Then
                    _varReference = Right(varLine, Len(varLine) - 12)
                End If
                If Left(varLine, 5) = cSwift502FunctionType Then
                    _varFunctionType = Right(varLine, Len(varLine) - 5)
                End If
                If Left(varLine, 6) = cSwift502Indicator1 Then
                    varIndicatorStr = Right(varLine, Len(varLine) - 6)
                    If varIndicatorStr = "BUSE//SUBS" Or varIndicatorStr = "BUSE//BUYI" Or varIndicatorStr = "BUSE//REDM" Or varIndicatorStr = "BUSE//SELL" Then
                        _varIndicator1 = Right(varLine, Len(varLine) - 6)
                    End If
                End If
                If Left(varLine, 12) = cSwift502Date Then
                    varDateStr = Right(varLine, Len(varLine) - 12)
                    _varTradeDate = CDate(Mid(varDateStr, 1, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)).ToString("yyyy-MM-dd")
                    _varSettleDate = _varTradeDate
                End If
                If Left(varLine, 12) = cSwift502AccNo Then
                    _varAccountNo = Right(varLine, Len(varLine) - 12)
                End If
                If Left(varLine, 5) = cSwift502ISIN Then
                    _varISIN = Replace(Replace(varLine, cSwift502ISIN, ""), "ISIN ", "")
                End If
                If Left(varLine, 12) = cSwift502OrderUnit Then
                    _varAmount = FormatNumber(Right(varLine, Len(varLine) - 17))
                End If
                If Left(varLine, 12) = cSwift502OrderAmt Then
                    varAmtStr = Right(varLine, Len(varLine) - 12)
                    _varAccountCCY = Left(varAmtStr, 3)
                    _varAmount = FormatNumber(Right(varAmtStr, Len(varAmtStr) - 3))
                End If
                If InStr(1, varLine, cSwift502Locations, vbTextCompare) <> 0 Then
                    If InStr(1, varLine, "BUYR", vbTextCompare) <> 0 Then
                        _varBuyer = Right(varLine, Len(varLine) - 12)
                    ElseIf InStr(1, varLine, "SELL", vbTextCompare) <> 0 Then
                        _varSeller = Right(varLine, Len(varLine) - 12)
                    End If
                End If
            Else
                If _varAccountNo <> "" And _varISIN <> "" And _varAmount <> 0 Then
                    SaveSwiftRowToDB("Processed Swift Type")
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process502, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process509(ByRef varLine As String)
        Const cSwift509Ref As String = ":20C::SEME//"
        Const cSwift509RelatedReference As String = ":20C::RELA//"
        Const cSwift509ISIN As String = ":35B:"
        Const cSwift509Indicator1 As String = ":25D::"
        Const cSwift509OrderUnit As String = ":36B::ORDR//"
        Const cSwift509OrderAmt As String = ":19A::ORDR//"

        Dim varAmtStr As String = ""
        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If Left(varLine, 12) = cSwift509Ref Then
                    _varReference = Right(varLine, Len(varLine) - 12)
                End If
                If Left(varLine, 12) = cSwift509RelatedReference Then
                    _varRelatedReference = Right(varLine, Len(varLine) - 12)
                End If
                If Left(varLine, 6) = cSwift509Indicator1 Then
                    _varIndicator1 = Right(varLine, Len(varLine) - 6)
                End If
                If Left(varLine, 5) = cSwift509ISIN Then
                    _varISIN = Replace(Replace(varLine, cSwift509ISIN, ""), "ISIN ", "")
                End If
                If Left(varLine, 12) = cSwift509OrderUnit Then
                    _varAmount = FormatNumber(Right(varLine, Len(varLine) - 17))
                End If
                If Left(varLine, 12) = cSwift509OrderAmt Then
                    varAmtStr = Right(varLine, Len(varLine) - 12)
                    _varAccountCCY = Left(varAmtStr, 3)
                    _varAmount = FormatNumber(Right(varAmtStr, Len(varAmtStr) - 3))
                End If
            Else
                If _varISIN <> "" And _varAmount <> 0 Then
                    SaveSwiftRowToDB("Processed Swift Type")
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process509, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process515(ByRef varLine As String)
        Const cSwift515Ref As String = ":20C::SEME//"
        Const cSwift515RelatedReference As String = ":20C::RELA//"
        Const cSwift515FunctionType As String = ":23G:"
        Const cSwift515AccNo As String = ":97A::SAFE//"
        Const cSwift515ISIN As String = ":35B:"
        Const cSwift515Indicator1 As String = ":22F::"
        Const cSwift515Date As String = ":98A::"
        Const cSwift502OrderUnit As String = ":36B::"
        Const cSwift515OrderAmt As String = ":19A::"
        Const cSwift515Price As String = ":90"
        Const cSwift515Locations As String = ":95"

        Dim varDateStr As String = ""
        Dim varIndicatorStr As String = ""
        Dim varAmtStr As String = ""
        Dim varAmtStr2 As String = ""
        Dim varPriceStr As String = ""
        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If Left(varLine, 12) = cSwift515Ref Then
                    _varReference = Right(varLine, Len(varLine) - 12)
                End If
                If Left(varLine, 12) = cSwift515RelatedReference Then
                    _varRelatedReference = Right(varLine, Len(varLine) - 12)
                End If
                If Left(varLine, 5) = cSwift515FunctionType Then
                    _varFunctionType = Right(varLine, Len(varLine) - 5)
                End If
                If Left(varLine, 6) = cSwift515Indicator1 Then
                    varIndicatorStr = Right(varLine, Len(varLine) - 6)
                    If varIndicatorStr = "BUSE//SUBS" Or varIndicatorStr = "BUSE//BUYI" Or varIndicatorStr = "BUSE//REDM" Or varIndicatorStr = "BUSE//SELL" Then
                        _varIndicator1 = Right(varLine, Len(varLine) - 6)
                    End If
                End If
                If Left(varLine, 12) = cSwift515AccNo Then
                    _varAccountNo = Right(varLine, Len(varLine) - 12)
                End If
                If Left(varLine, 5) = cSwift515ISIN Then
                    _varISIN = Replace(Replace(varLine, cSwift515ISIN, ""), "ISIN ", "")
                End If
                If Left(varLine, 12) = cSwift502OrderUnit Then
                    _varAmount = FormatNumber(Right(varLine, Len(varLine) - 17))
                End If

                If Left(varLine, 3) = cSwift515Price Then
                    varPriceStr = Right(varLine, Len(varLine) - 12)
                    _varPriceQualifier = Left(varPriceStr, 4)
                    If IsNumeric(Replace(Right(varPriceStr, Len(varPriceStr) - 5), ",", ".")) Then
                        _varPrice = FormatNumber(Right(varPriceStr, Len(varPriceStr) - 5))
                    Else
                        _varPriceCCY = Mid(varPriceStr, 6, 3)
                        _varPrice = FormatNumber(Right(varPriceStr, Len(varPriceStr) - 8))
                    End If
                End If
                If Left(varLine, 6) = cSwift515OrderAmt Then
                    varAmtStr = Right(varLine, Len(varLine) - 6)
                    varAmtStr2 = Right(varLine, Len(varLine) - 12)
                    If Left(varAmtStr, 4) = "DEAL" Then
                        _varAccountCCY = Left(varAmtStr2, 3)
                        _varAmount = FormatNumber(Right(varAmtStr2, Len(varAmtStr2) - 3))
                    ElseIf Left(varAmtStr, 4) = "SETT" Then
                        _varSettledAmountCCY = Left(varAmtStr2, 3)
                        _varSettledAmount = FormatNumber(Right(varAmtStr2, Len(varAmtStr2) - 3))
                    ElseIf Left(varAmtStr, 4) = "EXEC" Then
                        _varCommissionCCY = Left(varAmtStr2, 3)
                        _varCommission = FormatNumber(Right(varAmtStr2, Len(varAmtStr2) - 3))
                        _varCommissionQualifier = "EXEC"
                    ElseIf Left(varAmtStr, 4) = "ACRU" Then
                        _varAccruedCCY = Left(varAmtStr2, 3)
                        _varAccrued = FormatNumber(Right(varAmtStr2, Len(varAmtStr2) - 3))
                        _varAccruedQualifier = "ACRU"
                    End If
                End If
                If InStr(1, varLine, cSwift515Date, vbTextCompare) <> 0 Then
                    If InStr(1, varLine, "TRAD//", vbTextCompare) <> 0 Then
                        varDateStr = Replace(varLine, cSwift515Date & "TRAD//", "")
                        _varTradeDate = Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)
                    ElseIf InStr(1, varLine, "SETT//", vbTextCompare) <> 0 Then
                        varDateStr = Replace(varLine, cSwift515Date & "SETT//", "")
                        If IsDate(Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)) Then
                            _varSettleDate = Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)
                        End If
                    End If
                End If

                If InStr(1, varLine, cSwift515Locations, vbTextCompare) <> 0 Then
                    If InStr(1, varLine, "BUYR", vbTextCompare) <> 0 Then
                        _varBuyer = Right(varLine, Len(varLine) - 12)
                    ElseIf InStr(1, varLine, "SELL", vbTextCompare) <> 0 Then
                        _varSeller = Right(varLine, Len(varLine) - 12)
                    End If
                End If
            Else
                If _varISIN <> "" And _varAmount <> 0 Then
                    SaveSwiftRowToDB("Processed Swift Type")
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process515, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process535(ByRef varLine As String, ByRef WriteLineToTextFile As String)
        Const cSwift535Ref As String = ":20C::SEME//"
        Const cSwift535RelatedReference As String = ":20C::RELA//"
        Const cSwift535LinkReference As String = ":20C::PREV//"
        Const cSwift535AccNo As String = ":97"
        Const cSwift535Clearer As String = ":94F::SAFE//"
        Const cSwift535ISIN As String = ":35B:"
        Const cSwift535HoldCCY As String = ":19A::HOLD//"
        Const cSwift535FaceValue As String = ":93B::AGGR//"
        Const cSwift535FaceValuePenD As String = ":93C::PEND//"
        Const cSwift535FaceValuePenR As String = ":93B::PENR//"
        Const cSwift535MarketPrice As String = ":90B::MRKT//ACTU/"
        Const cSwift535IndicativePrice As String = ":90B::INDC//ACTU/"
        Const cSwift535ExercisePrice As String = ":90B::EXER//ACTU/"
        Try
            ' End of block 
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                '  ":97A::SAFE//"
                ' If there's an acct no and acct no is not blank and can't find blank or not the same acct no in the varline 
                If InStr(1, varLine, cSwift535AccNo, vbTextCompare) <> 0 And _varAccountNo <> "" And InStr(1, varLine, _varAccountNo, vbTextCompare) = 0 Then
                    If _varClearer <> "" And _varISIN <> "" And _varAccountCCY <> "" And (_varFaceValue <> 0 Or _varFaceValuePendingDelivered <> 0 Or _varFaceValuePendingReceived <> 0) Then
                        If InStr(1, WriteLineToTextFile, _varSwiftCode, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varAccountNo, vbTextCompare) = 0 Or
                    InStr(1, WriteLineToTextFile, _varClearer, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varISIN, vbTextCompare) = 0 Or
                    InStr(1, WriteLineToTextFile, _varFaceValue, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varFaceValuePendingDelivered, vbTextCompare) = 0 Or
                    InStr(1, WriteLineToTextFile, _varFaceValuePendingReceived, vbTextCompare) = 0 Or WriteLineToTextFile = "" Then
                            WriteLineToTextFile = _varSwiftFileName & ";" & _varSwiftCode & ";" & _varAccountNo & ";" & _varClearer & ";" & _varISIN & ";" & _varAccountCCY & ";" & _varFaceValue & ";" & _varFaceValuePendingDelivered & ";" & _varFaceValuePendingReceived
                            SaveSwiftRowToDB("Processed Swift Type") ' varAccountNo = "" : 
                            _varClearer = "" : _varISIN = "" : _varAccountCCY = "" : _varFaceValue = 0 : _varFaceValuePendingDelivered = 0 : _varFaceValuePendingReceived = 0
                        End If
                    End If
                End If
                'new clearer ":94F::SAFE//"
                If InStr(1, varLine, cSwift535Clearer, vbTextCompare) <> 0 And _varClearer <> "" And InStr(1, varLine, _varClearer, vbTextCompare) = 0 Then
                    If InStr(1, WriteLineToTextFile, _varSwiftCode, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varAccountNo, vbTextCompare) = 0 Or
                    InStr(1, WriteLineToTextFile, _varClearer, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varISIN, vbTextCompare) = 0 Or
                    InStr(1, WriteLineToTextFile, _varFaceValue, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varFaceValuePendingDelivered, vbTextCompare) = 0 Or
                    InStr(1, WriteLineToTextFile, _varFaceValuePendingReceived, vbTextCompare) = 0 Or WriteLineToTextFile = "" Then
                        If _varISIN <> "" And _varAccountCCY <> "" And (_varFaceValue <> 0 Or _varFaceValuePendingDelivered <> 0 Or _varFaceValuePendingReceived <> 0) Then
                            WriteLineToTextFile = _varSwiftFileName & ";" & _varSwiftCode & ";" & _varAccountNo & ";" & _varClearer & ";" & _varISIN & ";" & _varAccountCCY & ";" & _varFaceValue & ";" & _varFaceValuePendingDelivered & ";" & _varFaceValuePendingReceived
                            SaveSwiftRowToDB("Processed Swift Type")
                            _varClearer = "" : _varISIN = "" : _varAccountCCY = "" : _varFaceValue = 0 : _varFaceValuePendingDelivered = 0 : _varFaceValuePendingReceived = 0
                        End If
                    End If
                End If
                'new ISIN ":35B:" 'InStr(1, WriteLineToTextFile, varClearer, vbTextCompare) = 0 Or
                If InStr(1, varLine, cSwift535ISIN, vbTextCompare) <> 0 And _varISIN <> "" And InStr(1, varLine, _varISIN, vbTextCompare) = 0 Then
                    If InStr(1, WriteLineToTextFile, _varSwiftCode, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varAccountNo, vbTextCompare) = 0 Or
                        InStr(1, WriteLineToTextFile, _varISIN, vbTextCompare) = 0 Or WriteLineToTextFile = "" Then
                        If _varAccountCCY = "" And _varPriceCCY <> "" Then
                            _varAccountCCY = _varPriceCCY
                        End If

                        If _varISIN <> "" And _varAccountCCY <> "" And (_varFaceValue <> 0 Or _varFaceValuePendingDelivered <> 0 Or _varFaceValuePendingReceived <> 0) Then
                            WriteLineToTextFile = _varSwiftFileName & ";" & _varSwiftCode & ";" & _varAccountNo & ";" & _varISIN & ";" & _varAccountCCY & ";" & _varFaceValue & ";" & _varFaceValuePendingDelivered & ";" & _varFaceValuePendingReceived
                            SaveSwiftRowToDB("Processed Swift Type")
                            _varISIN = "" : _varAccountCCY = "" : _varFaceValue = 0 : _varFaceValuePendingDelivered = 0 : _varFaceValuePendingReceived = 0
                        End If
                    End If
                End If
                'new CCY'
                'If InStr(1, varLine, cSwift535HoldCCY, vbTextCompare) <> 0 And varAccountCCY <> "" And InStr(1, varLine, varAccountCCY, vbTextCompare) = 0 Then
                'If InStr(1, WriteLineToTextFile, varSwiftCode, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, varAccountNo, vbTextCompare) = 0 Or
                'InStr(1, WriteLineToTextFile, varClearer, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, varISIN, vbTextCompare) = 0 Or
                'InStr(1, WriteLineToTextFile, varFaceValue, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, varFaceValuePendingDelivered, vbTextCompare) = 0 Or
                'InStr(1, WriteLineToTextFile, varFaceValuePendingReceived, vbTextCompare) = 0 Or WriteLineToTextFile = "" Then
                'If varISIN <> "" And varAccountCCY <> "" And (varFaceValue <> 0 Or varFaceValuePendingDelivered <> 0 Or varFaceValuePendingReceived <> 0) Then
                'WriteLineToTextFile = varSwiftFileName & ";" & varSwiftCode & ";" & varAccountNo & ";" & varClearer & ";" & varISIN & ";" & varAccountCCY & ";" & varFaceValue & ";" & varFaceValuePendingDelivered & ";" & varFaceValuePendingReceived
                'SaveSwiftRowToDB("Processed Swift Type")
                'varAccountCCY = "" : varFaceValue = 0 : varFaceValuePendingDelivered = 0 : varFaceValuePendingReceived = 0
                'End If
                'End If
                'End If
                If InStr(1, varLine, cSwift535Ref, vbTextCompare) <> 0 Then
                    _varReference = Replace(varLine, cSwift535Ref, "")
                    If _varReference <> _varReference2 Then
                        _varReference2 = _varReference
                    End If
                End If
                If InStr(1, varLine, cSwift535RelatedReference, vbTextCompare) <> 0 Then
                    _varRelatedReference = Replace(varLine, cSwift535RelatedReference, "")
                    If _varRelatedReference <> _varRelatedReference2 Then
                        _varRelatedReference2 = _varRelatedReference
                    End If
                End If
                If InStr(1, varLine, cSwift535LinkReference, vbTextCompare) <> 0 Then
                    _varLinkReference = Replace(varLine, cSwift535LinkReference, "")
                    If _varLinkReference <> _varLinkReference2 Then
                        _varLinkReference2 = _varLinkReference
                    End If
                End If
                If InStr(1, varLine, cSwift535AccNo, vbTextCompare) <> 0 Then
                    _varAccountNo = StrReverse(Left(StrReverse(varLine), InStr(StrReverse(varLine), "/") - 1))
                    If _varAccountNo <> _varAccountNo2 Then
                        _varAccountNo2 = _varAccountNo
                    End If
                End If
                If InStr(1, varLine, cSwift535Clearer, vbTextCompare) <> 0 Then
                    _varClearer = Replace(varLine, cSwift535Clearer, "")
                End If
                If InStr(1, varLine, cSwift535ISIN, vbTextCompare) <> 0 Then
                    _varISIN = Replace(Replace(varLine, cSwift535ISIN, ""), "ISIN ", "")
                End If
                If InStr(1, varLine, cSwift535HoldCCY, vbTextCompare) <> 0 Then
                    _varAccountCCY = Mid(Replace(varLine, cSwift535HoldCCY, ""), 1, 3)
                End If
                If InStr(1, varLine, cSwift535FaceValue, vbTextCompare) <> 0 Then
                    _varFaceValue = FormatNumber(Right(Replace(varLine, cSwift535FaceValue, ""), Len(Replace(varLine, cSwift535FaceValue, "")) - 5))
                End If
                If InStr(1, varLine, cSwift535FaceValuePenD, vbTextCompare) <> 0 Then
                    _varFaceValuePendingDelivered = FormatNumber(Right(Replace(varLine, cSwift535FaceValuePenD, ""), Len(Replace(varLine, cSwift535FaceValuePenD, "")) - 10))
                End If
                If InStr(1, varLine, cSwift535FaceValuePenR, vbTextCompare) <> 0 Then
                    _varFaceValuePendingReceived = FormatNumber(Right(Replace(varLine, cSwift535FaceValuePenR, ""), Len(Replace(varLine, cSwift535FaceValuePenR, "")) - 5))
                End If
                If InStr(1, varLine, cSwift535MarketPrice, vbTextCompare) <> 0 Then
                    _varPrice = FormatNumber(Mid(StrReverse(Left(StrReverse(varLine), InStr(1, StrReverse(varLine), "/") - 1)), 4, 20))
                    _varPriceCCY = Trim(Left(StrReverse(Left(StrReverse(varLine), InStr(1, StrReverse(varLine), "/") - 1)), 3))
                    _varPriceQualifier = Mid(varLine, 7, 4)
                End If
                If InStr(1, varLine, cSwift535IndicativePrice, vbTextCompare) <> 0 Then
                    _varPrice = FormatNumber(Mid(StrReverse(Left(StrReverse(varLine), InStr(1, StrReverse(varLine), "/") - 1)), 4, 20))
                    _varPriceCCY = Trim(Left(StrReverse(Left(StrReverse(varLine), InStr(1, StrReverse(varLine), "/") - 1)), 3))
                    _varPriceQualifier = Mid(varLine, 7, 4)
                End If
                If InStr(1, varLine, cSwift535ExercisePrice, vbTextCompare) <> 0 Then
                    _varPrice = FormatNumber(Mid(StrReverse(Left(StrReverse(varLine), InStr(1, StrReverse(varLine), "/") - 1)), 4, 20))
                    _varPriceCCY = Trim(Left(StrReverse(Left(StrReverse(varLine), InStr(1, StrReverse(varLine), "/") - 1)), 3))
                    _varPriceQualifier = Mid(varLine, 7, 4)
                End If
            Else
                If InStr(1, WriteLineToTextFile, _varSwiftCode, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varAccountNo, vbTextCompare) = 0 Or
                    InStr(1, WriteLineToTextFile, _varISIN, vbTextCompare) = 0 Or WriteLineToTextFile = "" Then

                    If _varAccountCCY = "" And _varPriceCCY <> "" Then
                        _varAccountCCY = _varPriceCCY
                    End If

                    If _varISIN <> "" And _varAccountCCY <> "" And (_varFaceValue <> 0 Or _varFaceValuePendingDelivered <> 0 Or _varFaceValuePendingReceived <> 0) Then
                        SaveSwiftRowToDB("Processed Swift Type")
                    Else
                        SaveSwiftRowToDB("No data found in Swift")
                    End If
                Else
                    SaveSwiftRowToDB("No data found in Swift")
                End If
            End If
            ' This is to deal with 535s where the global vars are being cleared.
            ' Added so extra vars to deal with fields being blank
            _varAccountNo = IIf(_varAccountNo = "", _varAccountNo2, _varAccountNo)
            _varReference = IIf(_varReference = "", _varReference2, _varReference)
            _varLinkReference = IIf(_varLinkReference = "", _varLinkReference2, _varLinkReference)
            _varRelatedReference = IIf(_varRelatedReference = "", _varRelatedReference2, _varRelatedReference)
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process535, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub


    Public Sub Process535WithSubBal(ByRef varLine As String, ByRef WriteLineToTextFile As String)
        Const cSwift535Ref As String = ":20C::SEME//"
        Const cSwift535RelatedReference As String = ":20C::RELA//"
        Const cSwift535LinkReference As String = ":20C::PREV//"
        Const cSwift535AccNo As String = ":97A::SAFE//"
        Const cSwift535Clearer As String = ":94F::SAFE//"
        Const cSwift535ISIN As String = ":35B:"
        Const cSwift535HoldCCY As String = ":19A::HOLD//"
        Const cSwift535FaceValue As String = ":93"
        Const cSwift535MarketPrice As String = ":90B::MRKT//ACTU/"
        Const cSwift535IndicativePrice As String = ":90B::INDC//ACTU/"
        Const cSwift535ExercisePrice As String = ":90B::EXER//ACTU/"
        Const cSwift535SubBalEnd As String = ":16S:SUBBAL"
        Const cSwift535SubBalBlockEnd As String = ":16S:FIN"

        Try
            ' End of block 
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                '  ":97A::SAFE//"
                ' If there's an acct no and acct no is not blank and can't find blank or not the same acct no in the varline 
                If InStr(1, varLine, cSwift535AccNo, vbTextCompare) <> 0 And _varAccountNo <> "" And InStr(1, varLine, _varAccountNo, vbTextCompare) = 0 Then
                    If _varClearer <> "" And _varISIN <> "" And _varAccountCCY <> "" And (_varFaceValue <> 0 Or _varFaceValuePendingDelivered <> 0 Or _varFaceValuePendingReceived <> 0) Then
                        If InStr(1, WriteLineToTextFile, _varSwiftCode, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varAccountNo, vbTextCompare) = 0 Or
                    InStr(1, WriteLineToTextFile, _varClearer, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varISIN, vbTextCompare) = 0 Or
                    InStr(1, WriteLineToTextFile, _varFaceValue, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varFaceValuePendingDelivered, vbTextCompare) = 0 Or
                    InStr(1, WriteLineToTextFile, _varFaceValuePendingReceived, vbTextCompare) = 0 Or WriteLineToTextFile = "" Then
                            WriteLineToTextFile = _varSwiftFileName & ";" & _varSwiftCode & ";" & _varAccountNo & ";" & _varClearer & ";" & _varISIN & ";" & _varAccountCCY & ";" & _varFaceValue & ";" & _varFaceValuePendingDelivered & ";" & _varFaceValuePendingReceived
                            SaveSwiftRowToDB("Processed Swift Type") ' varAccountNo = "" : 
                            _varClearer = "" : _varISIN = "" : _varAccountCCY = "" : _varFaceValue = 0 : _varFaceValuePendingDelivered = 0 : _varFaceValuePendingReceived = 0 : _varHasBlockEnded = True
                        End If
                    End If
                    _varHasBlockEnded = True
                End If

                'new clearer ":94F::SAFE//"
                If InStr(1, varLine, cSwift535Clearer, vbTextCompare) <> 0 And _varClearer <> "" And InStr(1, varLine, _varClearer, vbTextCompare) = 0 Then
                    If InStr(1, WriteLineToTextFile, _varSwiftCode, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varAccountNo, vbTextCompare) = 0 Or
                    InStr(1, WriteLineToTextFile, _varClearer, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varISIN, vbTextCompare) = 0 Or
                    InStr(1, WriteLineToTextFile, _varFaceValue, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varFaceValuePendingDelivered, vbTextCompare) = 0 Or
                    InStr(1, WriteLineToTextFile, _varFaceValuePendingReceived, vbTextCompare) = 0 Or WriteLineToTextFile = "" Then
                        If _varISIN <> "" And _varAccountCCY <> "" And (_varFaceValue <> 0 Or _varFaceValuePendingDelivered <> 0 Or _varFaceValuePendingReceived <> 0) Then
                            WriteLineToTextFile = _varSwiftFileName & ";" & _varSwiftCode & ";" & _varAccountNo & ";" & _varClearer & ";" & _varISIN & ";" & _varAccountCCY & ";" & _varFaceValue & ";" & _varFaceValuePendingDelivered & ";" & _varFaceValuePendingReceived
                            SaveSwiftRowToDB("Processed Swift Type")
                            _varClearer = "" : _varISIN = "" : _varAccountCCY = "" : _varFaceValue = 0 : _varFaceValuePendingDelivered = 0 : _varFaceValuePendingReceived = 0 : _varHasBlockEnded = True
                        End If
                    End If
                End If

                ' BEGIN: New block to deal 535 and breakdown -- RA CHANGE 2019-05-08
                If varLine = cSwift535SubBalBlockEnd Then
                    _varHasBlockEnded = True
                End If
                If varLine = cSwift535SubBalEnd Then
                    _varHasBlockEnded = False
                    If _varFaceValue <> 0 Then
                        WriteLineToTextFile = _varSwiftFileName & ";" & _varSwiftCode & ";" & _varAccountNo & ";" & _varClearer & ";" & _varISIN & ";" & _varAccountCCY & ";" & _varFaceValue & ";" & _varFaceValuePendingDelivered & ";" & _varFaceValuePendingReceived
                        SaveSwiftRowToDB("Processed Swift Type")
                        _varFaceValue = 0 ' This makes sure that only one record is written where there's breakdown but not a value
                    End If
                End If
                If InStr(1, varLine, cSwift535ISIN, vbTextCompare) <> 0 And _varISIN <> "" And InStr(1, varLine, _varISIN, vbTextCompare) = 0 Then
                    _varISIN = "" : _varAccountCCY = "" : _varFaceValue = 0 : _varFaceValuePendingDelivered = 0 : _varFaceValuePendingReceived = 0 : _varHasBlockEnded = True
                End If
                ' END: New block to deal 535 and breakdown -- RA CHAN
                If InStr(1, varLine, cSwift535Ref, vbTextCompare) <> 0 Then
                    _varReference = Replace(varLine, cSwift535Ref, "")
                    If _varReference <> _varReference2 Then
                        _varReference2 = _varReference
                    End If
                End If
                If InStr(1, varLine, cSwift535RelatedReference, vbTextCompare) <> 0 Then
                    _varRelatedReference = Replace(varLine, cSwift535RelatedReference, "")
                    If _varRelatedReference <> _varRelatedReference2 Then
                        _varRelatedReference2 = _varRelatedReference
                    End If
                End If
                If InStr(1, varLine, cSwift535LinkReference, vbTextCompare) <> 0 Then
                    _varLinkReference = Replace(varLine, cSwift535LinkReference, "")
                    If _varLinkReference <> _varLinkReference2 Then
                        _varLinkReference2 = _varLinkReference
                    End If
                End If
                If InStr(1, varLine, cSwift535AccNo, vbTextCompare) <> 0 Then
                    _varAccountNo = Replace(varLine, cSwift535AccNo, "")
                    If _varAccountNo <> _varAccountNo2 Then
                        _varAccountNo2 = _varAccountNo
                    End If
                End If
                If InStr(1, varLine, cSwift535Clearer, vbTextCompare) <> 0 Then
                    _varClearer = Replace(varLine, cSwift535Clearer, "")
                End If
                If InStr(1, varLine, cSwift535ISIN, vbTextCompare) <> 0 Then
                    _varISIN = Replace(Replace(varLine, cSwift535ISIN, ""), "ISIN ", "")
                End If
                If InStr(1, varLine, cSwift535HoldCCY, vbTextCompare) <> 0 Then
                    _varAccountCCY = Mid(Replace(varLine, cSwift535HoldCCY, ""), 1, 3)
                End If
                If Left(varLine, 3) = cSwift535FaceValue Then
                    If InStr(1, varLine, ":93B::AGGR//", vbTextCompare) <> 0 Then
                        _varFaceValue = FormatNumber(Right(Replace(varLine, ":93B::AGGR//", ""), Len(Replace(varLine, ":93B::AGGR//", "")) - 5))
                    ElseIf InStr(1, varLine, ":93B::PEND//", vbTextCompare) <> 0 Then
                        _varFaceValuePendingDelivered = FormatNumber(Right(Replace(varLine, ":93B::PEND//", ""), Len(Replace(varLine, ":93B::PEND//", "")) - 5))
                    ElseIf InStr(1, varLine, ":93C::PEND//", vbTextCompare) <> 0 Then
                        _varFaceValuePendingDelivered = FormatNumber(Right(Replace(varLine, ":93C::PEND//", ""), Len(Replace(varLine, ":93C::PEND//", "")) - 5))
                    ElseIf InStr(1, varLine, ":93B::PENR//", vbTextCompare) <> 0 Then
                        _varFaceValuePendingReceived = FormatNumber(Right(Replace(varLine, ":93B::PENR//", ""), Len(Replace(varLine, ":93B::PENR//", "")) - 5))
                    ElseIf InStr(1, varLine, ":93C::PENR//", vbTextCompare) <> 0 Then
                        _varFaceValuePendingReceived = FormatNumber(Right(Replace(varLine, ":93C::PENR//", ""), Len(Replace(varLine, ":93C::PENR//", "")) - 5))
                    End If
                End If
                If InStr(1, varLine, cSwift535MarketPrice, vbTextCompare) <> 0 Then
                    _varPrice = FormatNumber(Mid(StrReverse(Left(StrReverse(varLine), InStr(1, StrReverse(varLine), "/") - 1)), 4, 20))
                    _varPriceCCY = Trim(Left(StrReverse(Left(StrReverse(varLine), InStr(1, StrReverse(varLine), "/") - 1)), 3))
                    _varPriceQualifier = Mid(varLine, 7, 4)
                End If
                If InStr(1, varLine, cSwift535IndicativePrice, vbTextCompare) <> 0 Then
                    _varPrice = FormatNumber(Mid(StrReverse(Left(StrReverse(varLine), InStr(1, StrReverse(varLine), "/") - 1)), 4, 20))
                    _varPriceCCY = Trim(Left(StrReverse(Left(StrReverse(varLine), InStr(1, StrReverse(varLine), "/") - 1)), 3))
                    _varPriceQualifier = Mid(varLine, 7, 4)
                End If
                If InStr(1, varLine, cSwift535ExercisePrice, vbTextCompare) <> 0 Then
                    _varPrice = FormatNumber(Mid(StrReverse(Left(StrReverse(varLine), InStr(1, StrReverse(varLine), "/") - 1)), 4, 20))
                    _varPriceCCY = Trim(Left(StrReverse(Left(StrReverse(varLine), InStr(1, StrReverse(varLine), "/") - 1)), 3))
                    _varPriceQualifier = Mid(varLine, 7, 4)
                End If
            Else
                If InStr(1, WriteLineToTextFile, _varSwiftCode, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varAccountNo, vbTextCompare) = 0 Or
                    InStr(1, WriteLineToTextFile, _varClearer, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varISIN, vbTextCompare) = 0 Or
                    InStr(1, WriteLineToTextFile, _varFaceValue, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varFaceValuePendingDelivered, vbTextCompare) = 0 Or
                    InStr(1, WriteLineToTextFile, _varFaceValuePendingReceived, vbTextCompare) = 0 Or WriteLineToTextFile = "" Then
                    If _varISIN <> "" And _varAccountCCY <> "" And (_varFaceValue <> 0 Or _varFaceValuePendingDelivered <> 0 Or _varFaceValuePendingReceived <> 0) Then
                        SaveSwiftRowToDB("Processed Swift Type")
                    End If
                End If
            End If

            ' This is to deal with 535s where the global vars are being cleared.
            ' Added so extra vars to deal with fields being blank
            _varAccountNo = IIf(_varAccountNo = "", _varAccountNo2, _varAccountNo)
            _varReference = IIf(_varReference = "", _varReference2, _varReference)
            _varLinkReference = IIf(_varLinkReference = "", _varLinkReference2, _varLinkReference)
            _varRelatedReference = IIf(_varRelatedReference = "", _varRelatedReference2, _varRelatedReference)
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process535, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process536(ByRef varLine As String, ByRef WriteLineToTextFile As String, ByRef varJoinSection As String)
        Const cSwift536Ref As String = ": 20C::SEME//"
        Const cSwift536RelatedReference As String = ":20C::RELA//"
        Const cSwift536LinkReference As String = ":20C::PREV//"
        Const cSwift536FunctionType As String = ":23G:"
        Const cSwift536AccNo As String = ":97A::SAFE//"
        Const cSwift536Clearer As String = ":94B::SAFE//"
        Const cSwift536ISIN As String = ":35B:"
        Const cSwift536Indicator1 As String = ":22F::"
        Const cSwift536Indicator2 As String = ":22H::"
        Const cSwift536Amount As String = ":19A::PSTA//"
        Const cSwift536FaceValue As String = ":36B::PSTA//"
        Const cSwift536Date As String = ":98A::"
        Const cSwift536SafeKeeping As String = ":94"
        Const cSwift536Locations As String = ":95"
        Const cSwift536Reason As String = ":70D::REAS//"

        Dim varDateStr As String
        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If Left(varLine, 1) = ":" Then
                    varJoinSection = ""
                End If
                If InStr(1, varLine, cSwift536AccNo, vbTextCompare) <> 0 And _varAccountNo <> "" And InStr(1, varLine, _varAccountNo, vbTextCompare) = 0 Then
                    If _varISIN <> "" And _varFaceValue <> 0 Then
                        If InStr(1, WriteLineToTextFile, _varAccountNo, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varISIN, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varFaceValue, vbTextCompare) = 0 Or WriteLineToTextFile = "" Then
                            WriteLineToTextFile = _varAccountNo & ";" & _varISIN & ";" & _varFaceValue
                            SaveSwiftRowToDB("Processed Swift Type")
                            _varAccountNo = "" : _varISIN = "" : _varFaceValue = 0
                        End If
                    End If
                End If

                'new ISIN
                If InStr(1, varLine, cSwift536ISIN, vbTextCompare) <> 0 And _varISIN <> "" And InStr(1, varLine, _varISIN, vbTextCompare) = 0 Then
                    If _varFaceValue <> 0 Then
                        If InStr(1, WriteLineToTextFile, _varAccountNo, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varISIN, vbTextCompare) = 0 Or InStr(1, WriteLineToTextFile, _varFaceValue, vbTextCompare) = 0 Or WriteLineToTextFile = "" Then
                            WriteLineToTextFile = _varAccountNo & ";" & _varISIN & ";" & _varFaceValue
                            SaveSwiftRowToDB("Processed Swift Type")
                            _varISIN = "" : _varFaceValue = 0
                        End If
                    End If
                End If

                If InStr(1, varLine, cSwift536Ref, vbTextCompare) <> 0 Then
                    _varReference = Replace(varLine, cSwift536Ref, "")
                End If
                If InStr(1, varLine, cSwift536RelatedReference, vbTextCompare) <> 0 Then
                    _varRelatedReference = Replace(varLine, cSwift536RelatedReference, "")
                End If
                If InStr(1, varLine, cSwift536LinkReference, vbTextCompare) <> 0 Then
                    _varLinkReference = Replace(varLine, cSwift536LinkReference, "")
                End If
                If InStr(1, varLine, cSwift536FunctionType, vbTextCompare) <> 0 Then
                    _varFunctionType = Replace(varLine, cSwift536FunctionType, "")
                End If
                If InStr(1, varLine, cSwift536Indicator1, vbTextCompare) <> 0 Then
                    _varIndicator1 = Replace(varLine, cSwift536Indicator1, "")
                End If
                If InStr(1, varLine, cSwift536Indicator2, vbTextCompare) <> 0 Then
                    _varIndicator2 = Replace(varLine, cSwift536Indicator2, "")
                End If
                If InStr(1, varLine, cSwift536AccNo, vbTextCompare) <> 0 Then
                    _varAccountNo = Replace(varLine, cSwift536AccNo, "")
                End If
                If InStr(1, varLine, cSwift536Amount, vbTextCompare) <> 0 Then
                    _varAccountCCY = Left(Replace(varLine, cSwift536Amount, ""), 3)
                    _varAmount = Right(Replace(varLine, cSwift536Amount, ""), Len(Replace(varLine, cSwift536Amount, "")) - 3)
                End If
                If InStr(1, varLine, cSwift536Clearer, vbTextCompare) <> 0 Then
                    _varClearer = Replace(varLine, cSwift536Clearer, "")
                End If
                If InStr(1, varLine, cSwift536ISIN, vbTextCompare) <> 0 Then
                    _varISIN = Replace(Replace(varLine, cSwift536ISIN, ""), "ISIN ", "")
                End If
                If InStr(1, varLine, cSwift536FaceValue, vbTextCompare) <> 0 Then
                    _varFaceValue = FormatNumber(Right(Replace(varLine, cSwift536FaceValue, ""), Len(Replace(varLine, cSwift536FaceValue, "")) - 5))
                End If
                If InStr(1, varLine, cSwift536Date, vbTextCompare) <> 0 Then
                    If InStr(1, varLine, "TRAD//", vbTextCompare) <> 0 Then
                        varDateStr = Replace(varLine, cSwift536Date & "TRAD//", "")
                        _varTradeDate = Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)
                    ElseIf InStr(1, varLine, "SETT//", vbTextCompare) <> 0 Then
                        varDateStr = Replace(varLine, cSwift536Date & "SETT//", "")
                        If IsDate(Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)) Then
                            _varSettleDate = Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)
                        End If
                    End If
                End If
                If InStr(1, varLine, cSwift536SafeKeeping, vbTextCompare) <> 0 Then
                    _varSafeKeeping = Right(varLine, Len(varLine) - 12)
                End If
                If InStr(1, varLine, cSwift536Locations, vbTextCompare) <> 0 Then
                    If InStr(1, varLine, "BUYR", vbTextCompare) <> 0 Then
                        _varBuyer = Right(varLine, Len(varLine) - 12)
                    ElseIf InStr(1, varLine, "SELL", vbTextCompare) <> 0 Then
                        _varSeller = Right(varLine, Len(varLine) - 12)
                    ElseIf InStr(1, varLine, "REAG", vbTextCompare) <> 0 Then
                        _varReceivingAgent = Right(varLine, Len(varLine) - 11)
                        If Left(_varReceivingAgent, 1) = "/" Then
                            _varReceivingAgent = Right(_varReceivingAgent, Len(_varReceivingAgent) - 1)
                        End If
                    ElseIf InStr(1, varLine, "DEAG", vbTextCompare) <> 0 Then
                        _varDeliveringAgent = Right(varLine, Len(varLine) - 11)
                        If Left(_varDeliveringAgent, 1) = "/" Then
                            _varDeliveringAgent = Right(_varDeliveringAgent, Len(_varDeliveringAgent) - 1)
                        End If
                    ElseIf InStr(1, varLine, "PSET", vbTextCompare) <> 0 Then
                        _varPlaceOfSettlement = Right(varLine, Len(varLine) - 11)
                        If Left(_varPlaceOfSettlement, 1) = "/" Then
                            _varPlaceOfSettlement = Right(_varPlaceOfSettlement, Len(_varPlaceOfSettlement) - 1)
                        End If
                    End If
                End If
                If InStr(1, varLine, cSwift536Reason, vbTextCompare) <> 0 Or varJoinSection = cSwift536Reason Then
                    _varReason1 = _varReason1 & Replace(varLine, cSwift536Reason, "") & " "
                    varJoinSection = cSwift536Reason
                End If
            Else
                If _varAccountNo <> "" And _varISIN <> "" And _varFaceValue <> 0 Then
                    SaveSwiftRowToDB("Processed Swift Type")
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process536, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process540(ByRef varLine As String)
        Const cSwift540Ref As String = ":20C::SEME//"
        Const cSwift540RelatedReference As String = ":20C::RELA//"
        Const cSwift540LinkReference As String = ":20C::PREV//"
        Const cSwift540FunctionType As String = ":23G:"
        Const cSwift540AccNo As String = ":97A::SAFE//"
        Const cSwift540ISIN As String = ":35B:"
        Const cSwift540Amt As String = ":90A::DEAL//"
        Const cSwift540SettledAmt As String = ":19A::SETT//"
        Const cSwift540FaceValue As String = ":36B"
        Const cSwift540Date As String = ":98A::"
        Const cSwift540SafeKeeping As String = ":94"
        Const cSwift540Locations As String = ":95"

        Dim varDateStr As String
        Dim varAmountStr As String

        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If InStr(1, varLine, cSwift540Ref, vbTextCompare) <> 0 Then
                    _varReference = Replace(varLine, cSwift540Ref, "")
                End If
                If InStr(1, varLine, cSwift540RelatedReference, vbTextCompare) <> 0 Then
                    _varRelatedReference = Replace(varLine, cSwift540RelatedReference, "")
                End If
                If InStr(1, varLine, cSwift540LinkReference, vbTextCompare) <> 0 Then
                    _varLinkReference = Replace(varLine, cSwift540LinkReference, "")
                End If
                If InStr(1, varLine, cSwift540FunctionType, vbTextCompare) <> 0 Then
                    _varFunctionType = Replace(varLine, cSwift540FunctionType, "")
                End If
                If InStr(1, varLine, cSwift540AccNo, vbTextCompare) <> 0 Then
                    _varAccountNo = Replace(varLine, cSwift540AccNo, "")
                End If
                If InStr(1, varLine, cSwift540ISIN, vbTextCompare) <> 0 Then
                    _varISIN = Replace(Replace(varLine, cSwift540ISIN, ""), "ISIN ", "")
                End If
                If InStr(1, varLine, cSwift540FaceValue, vbTextCompare) <> 0 Then
                    _varFaceValue = FormatNumber(Right(Replace(varLine, cSwift540FaceValue, ""), Len(Replace(varLine, cSwift540FaceValue, "")) - 13))
                End If
                If InStr(1, varLine, cSwift540Amt, vbTextCompare) <> 0 Then
                    varAmountStr = Replace(varLine, cSwift540Amt, "")
                    _varAmountQualifier = Left(varAmountStr, 4)
                    _varAmount = FormatNumber(Right(varAmountStr, Len(varAmountStr) - 5))
                End If
                If InStr(1, varLine, cSwift540SettledAmt, vbTextCompare) <> 0 Then
                    varAmountStr = Replace(varLine, cSwift540SettledAmt, "")
                    _varSettledAmountCCY = Left(varAmountStr, 3)
                    _varSettledAmount = FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3))
                End If
                If InStr(1, varLine, cSwift540Date, vbTextCompare) <> 0 Then
                    If InStr(1, varLine, "TRAD//", vbTextCompare) <> 0 Then
                        varDateStr = Replace(varLine, cSwift540Date & "TRAD//", "")
                        _varTradeDate = Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)
                    ElseIf InStr(1, varLine, "SETT//", vbTextCompare) <> 0 Then
                        varDateStr = Replace(varLine, cSwift540Date & "SETT//", "")
                        If IsDate(Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)) Then
                            _varSettleDate = Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)
                        End If
                    End If
                End If
                If InStr(1, varLine, cSwift540SafeKeeping, vbTextCompare) <> 0 Then
                    _varSafeKeeping = Right(varLine, Len(varLine) - 12)
                End If
                If InStr(1, varLine, cSwift540Locations, vbTextCompare) <> 0 Then
                    If InStr(1, varLine, "BUYR", vbTextCompare) <> 0 Then
                        _varBuyer = Right(varLine, Len(varLine) - 12)
                    ElseIf InStr(1, varLine, "SELL", vbTextCompare) <> 0 Then
                        _varSeller = Right(varLine, Len(varLine) - 12)
                    ElseIf InStr(1, varLine, "REAG", vbTextCompare) <> 0 Then
                        _varReceivingAgent = Right(varLine, Len(varLine) - 11)
                        If Left(_varReceivingAgent, 1) = "/" Then
                            _varReceivingAgent = Right(_varReceivingAgent, Len(_varReceivingAgent) - 1)
                        End If
                    ElseIf InStr(1, varLine, "DEAG", vbTextCompare) <> 0 Then
                        _varDeliveringAgent = Right(varLine, Len(varLine) - 11)
                        If Left(_varDeliveringAgent, 1) = "/" Then
                            _varDeliveringAgent = Right(_varDeliveringAgent, Len(_varDeliveringAgent) - 1)
                        End If
                    ElseIf InStr(1, varLine, "PSET", vbTextCompare) <> 0 Then
                        _varPlaceOfSettlement = Right(varLine, Len(varLine) - 11)
                        If Left(_varPlaceOfSettlement, 1) = "/" Then
                            _varPlaceOfSettlement = Right(_varPlaceOfSettlement, Len(_varPlaceOfSettlement) - 1)
                        End If
                    End If
                End If
            Else
                If _varAccountNo <> "" And _varISIN <> "" And _varFaceValue <> 0 Then
                    SaveSwiftRowToDB("Processed Swift Type")
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process540, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process548(ByRef varLine As String, ByRef varJoinSection As String)
        Const cSwift548Reference As String = ":20C::SEME//"
        Const cSwift548RelatedReference As String = ":20C::RELA//"
        Const cSwift548LinkReference As String = ":20C::PREV//"
        Const cSwift548Indicator1 As String = ":25"
        Const cSwift548Indicator2 As String = ":24"
        Const cSwift548FunctionType As String = ":23G:"
        Const cSwift548AccNo As String = ":97A::SAFE//"
        Const cSwift548ISIN As String = ":35B:ISIN "
        Const cSwift548FaceAmount As String = ":36B::"
        Const cSwift548SettledAmount As String = ":19A::"
        Const cSwift548Price As String = ":90"
        Const cSwift548Date As String = ":98A::"
        Const cSwift548SafeKeeping As String = ":94"
        Const cSwift548Locations As String = ":95"
        Const cSwift548Reason As String = ":70D::REAS//"

        Dim var548DetailStr As String
        Dim varDateStr As String
        Dim varPriceStr As String

        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If Left(varLine, 1) = ":" Then
                    varJoinSection = ""
                End If
                If InStr(1, varLine, cSwift548AccNo, vbTextCompare) <> 0 Then
                    _varAccountNo = Replace(varLine, cSwift548AccNo, "")
                End If
                If Left(varLine, 5) = cSwift548FunctionType Then
                    _varFunctionType = Right(varLine, Len(varLine) - 5)
                End If
                If InStr(1, varLine, cSwift548Reference, vbTextCompare) <> 0 Then
                    _varReference = Replace(varLine, cSwift548Reference, "")
                End If
                If InStr(1, varLine, cSwift548RelatedReference, vbTextCompare) <> 0 Then
                    _varRelatedReference = Replace(varLine, cSwift548RelatedReference, "")
                End If
                If InStr(1, varLine, cSwift548LinkReference, vbTextCompare) <> 0 Then
                    _varLinkReference = Replace(varLine, cSwift548LinkReference, "")
                End If
                If Left(varLine, 3) = cSwift548Indicator1 Then
                    _varIndicator1 = Right(varLine, Len(varLine) - 6)
                End If
                If Left(varLine, 3) = cSwift548Indicator2 Then
                    _varIndicator2 = Right(varLine, Len(varLine) - 6)
                End If
                If InStr(1, varLine, cSwift548ISIN, vbTextCompare) <> 0 Then
                    _varISIN = Replace(varLine, cSwift548ISIN, "")
                End If
                If Left(varLine, 6) = cSwift548FaceAmount Then
                    _varFaceValue = FormatNumber(Replace(Replace(Replace(Right(varLine, Len(varLine) - 12), cSwift548FaceAmount, ""), "FAMT/", ""), "UNIT/", ""))
                End If
                If Left(varLine, 6) = cSwift548SettledAmount Then
                    var548DetailStr = Right(varLine, Len(varLine) - 12)
                    _varSettledAmountCCY = Left(var548DetailStr, 3)
                    _varSettledAmount = FormatNumber(Right(var548DetailStr, Len(var548DetailStr) - 3))
                End If
                If InStr(1, varLine, cSwift548Date, vbTextCompare) <> 0 Then
                    If InStr(1, varLine, "TRAD//", vbTextCompare) <> 0 Then
                        varDateStr = Replace(varLine, cSwift548Date & "TRAD//", "")
                        _varTradeDate = Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)
                    ElseIf InStr(1, varLine, "SETT//", vbTextCompare) <> 0 Then
                        varDateStr = Replace(varLine, cSwift548Date & "SETT//", "")
                        If IsDate(Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)) Then
                            _varSettleDate = Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)
                        End If
                    End If
                End If
                If Left(varLine, 3) = cSwift548SafeKeeping Then
                    _varSafeKeeping = Right(varLine, Len(varLine) - 12)
                End If
                If Left(varLine, 3) = cSwift548Price Then
                    varPriceStr = Right(varLine, Len(varLine) - 12)
                    _varPriceQualifier = Left(varPriceStr, 4)
                    If IsNumeric(Replace(Right(varPriceStr, Len(varPriceStr) - 5), ",", ".")) Then
                        _varPrice = FormatNumber(Right(varPriceStr, Len(varPriceStr) - 5))
                    Else
                        _varPriceCCY = Mid(varPriceStr, 6, 3)
                        _varPrice = FormatNumber(Right(varPriceStr, Len(varPriceStr) - 8))
                    End If
                End If
                If Left(varLine, 3) = cSwift548Locations Then
                    If InStr(1, varLine, "BUYR", vbTextCompare) <> 0 Then
                        _varBuyer = Right(varLine, Len(varLine) - 12)
                    ElseIf InStr(1, varLine, "SELL", vbTextCompare) <> 0 Then
                        _varSeller = Right(varLine, Len(varLine) - 12)
                    ElseIf InStr(1, varLine, "REAG", vbTextCompare) <> 0 Then
                        _varReceivingAgent = Right(varLine, Len(varLine) - 12)
                    ElseIf InStr(1, varLine, "DEAG", vbTextCompare) <> 0 Then
                        _varDeliveringAgent = Right(varLine, Len(varLine) - 12)
                    ElseIf InStr(1, varLine, "PSET", vbTextCompare) <> 0 Then
                        _varPlaceOfSettlement = Right(varLine, Len(varLine) - 12)
                    End If
                End If
                If InStr(1, varLine, cSwift548Reason, vbTextCompare) <> 0 Or varJoinSection = cSwift548Reason Then
                    _varReason1 = _varReason1 & Replace(varLine, cSwift548Reason, "") & " "
                    varJoinSection = cSwift548Reason
                End If
            Else
                If _varAccountNo <> "" Then
                    SaveSwiftRowToDB("Processed Swift Type")
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process548, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process564(ByRef varLine As String, ByRef varJoinSection As String, ByRef LstCorpActionTermInfo As List(Of Dictionary(Of String, String)), ByRef ColCorpActionTermInfo As Dictionary(Of String, String))
        Const cSwift564Reference As String = ":20C::CORP//"
        Const cSwift564RelatedReference As String = ":20C::SEME//"
        Const cSwift564LinkReference As String = ":20C::PREV//"
        Const cSwift564AccNo As String = ":97A::SAFE//"
        Const cSwift564FunctionType As String = ":23G:"
        Const cSwift564Indicator1 As String = ":22F::CAEV//"
        Const cSwift564Indicator2 As String = ":22F::CAMV//"
        Const cSwift564ISIN As String = ":35B:ISIN "
        Const cSwift564CCY As String = ":11A::DENO//"
        Const cSwift564Amount As String = ":19"
        Const cSwift564Date As String = ":98A::"
        Const cSwift564Rate As String = ":92A::"
        Const cSwift564SettledAmt As String = ":93"
        Const cSwift564SafeKeeping As String = ":94"
        Const cSwift564Opt As String = ":17B::"
        Const cSwift564Reason As String = ":70E::ADTX//"
        Const cSwift564Desc As String = ":70E::"
        Const cSwift564Term As String = ":13A::"
        Const cSwift564TermOptionCCY As String = ":11A::"
        Const cSwift564Quantity As String = ":36B::"
        Const cSwift564Gross As String = ":92F::GRSS//"
        Const cSwift564Net As String = ":92F::NETT//"
        Const cSwift564TermDates As String = ":98"
        Const cSwift564TermIndicator As String = ":22"
        Const cSwift564StartTerm As String = ":16R:CAOPTN"
        Const cSwift564EndTerm As String = ":16S:CAOPTN"
        Const cSwift564Offer As String = ":90"

        Dim varDateStr As String = ""
        Dim varAmountStr As String
        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If varLine = cSwift564StartTerm Then
                    varJoinSection = cSwift564StartTerm
                ElseIf varLine = cSwift564EndTerm Then
                    AddtoLstInfo(LstCorpActionTermInfo, ColCorpActionTermInfo)
                    SaveSwiftRowToDB("", Nothing, LstCorpActionTermInfo)
                    varJoinSection = ""
                ElseIf varJoinSection = cSwift564StartTerm Then
                    If InStr(1, varLine, cSwift564ISIN, vbTextCompare) <> 0 And Not ColCorpActionTermInfo.ContainsKey("ISIN") Then
                        ColCorpActionTermInfo.Add("ISIN", Right(varLine, Len(varLine) - 10))
                    ElseIf Left(varLine, 6) = cSwift564Term And Not ColCorpActionTermInfo.ContainsKey("Term") Then
                        ColCorpActionTermInfo.Add("Term", Right(varLine, Len(varLine) - 12))
                    ElseIf Left(varLine, 3) = cSwift564TermIndicator Then
                        If ColCorpActionTermInfo.ContainsKey("TermOption") Then
                            If ColCorpActionTermInfo.ContainsKey("Indicator1") Then
                                If Not ColCorpActionTermInfo.ContainsKey("Indicator2") Then
                                    ColCorpActionTermInfo.Add("Indicator2", Right(varLine, Len(varLine) - 6))
                                End If
                            ElseIf Not ColCorpActionTermInfo.ContainsKey("Indicator1") Then
                                ColCorpActionTermInfo.Add("Indicator1", Right(varLine, Len(varLine) - 6))
                            End If
                        ElseIf Not ColCorpActionTermInfo.ContainsKey("TermOption") Then
                            ColCorpActionTermInfo.Add("TermOption", Right(varLine, Len(varLine) - 12))
                        End If
                    ElseIf Left(varLine, 6) = cSwift564TermOptionCCY And Not ColCorpActionTermInfo.ContainsKey("TermOptionCCY") Then
                        ColCorpActionTermInfo.Add("TermOptionCCY", Right(varLine, Len(varLine) - 12))
                    ElseIf Left(varLine, 6) = cSwift564Opt Then
                        varAmountStr = Right(varLine, Len(varLine) - 6)
                        If Left(varAmountStr, 6) = "DFLT//" And Not ColCorpActionTermInfo.ContainsKey("Default") Then
                            ColCorpActionTermInfo.Add("Default", Right(varAmountStr, Len(varAmountStr) - 6))
                        ElseIf Left(varAmountStr, 6) = "WTHD//" And Not ColCorpActionTermInfo.ContainsKey("WTH") Then
                            ColCorpActionTermInfo.Add("WTH", Right(varAmountStr, Len(varAmountStr) - 6))
                        End If
                    ElseIf InStr(1, varLine, cSwift564Quantity, vbTextCompare) <> 0 Then
                        varAmountStr = Right(varLine, Len(varLine) - 6)
                        If Left(varAmountStr, 6) = "MIEX//" Then
                            varAmountStr = Right(varLine, Len(varLine) - 12)
                            If Not ColCorpActionTermInfo.ContainsKey("MinimumQuantityQualifier") Then
                                ColCorpActionTermInfo.Add("MinimumQuantityQualifier", Left(varAmountStr, 4))
                            End If
                            If Not ColCorpActionTermInfo.ContainsKey("MinimumQuantityQualifier") Then
                                ColCorpActionTermInfo.Add("MinimumQuantityQualifier", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 5)))
                            End If
                        ElseIf Left(varAmountStr, 6) = "MILT//" Then
                            varAmountStr = Right(varLine, Len(varLine) - 12)
                            If Not ColCorpActionTermInfo.ContainsKey("MinimumQuantityQualifier") Then
                                ColCorpActionTermInfo.Add("MinimumQuantityQualifier", Left(varAmountStr, 4))
                            End If
                            If Not ColCorpActionTermInfo.ContainsKey("MinimumQuantityQualifier") Then
                                ColCorpActionTermInfo.Add("MinimumQuantityQualifier", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 5)))
                            End If
                        ElseIf Left(varAmountStr, 6) = "MINO//" Then
                            varAmountStr = Right(varLine, Len(varLine) - 12)
                            If Not ColCorpActionTermInfo.ContainsKey("MinimumQuantityQualifier") Then
                                ColCorpActionTermInfo.Add("MinimumQuantityQualifier", Left(varAmountStr, 4))
                            End If
                            If Not ColCorpActionTermInfo.ContainsKey("MinimumQuantityQualifier") Then
                                ColCorpActionTermInfo.Add("MinimumQuantityQualifier", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 5)))
                            End If
                        ElseIf Left(varAmountStr, 6) = "SIZE//" Then
                            varAmountStr = Right(varLine, Len(varLine) - 12)
                            If Not ColCorpActionTermInfo.ContainsKey("ContractSizeQualifier") Then
                                ColCorpActionTermInfo.Add("ContractSizeQualifier", Left(varAmountStr, 4))
                            End If
                            If Not ColCorpActionTermInfo.ContainsKey("ContractSize") Then
                                ColCorpActionTermInfo.Add("ContractSize", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 5)))
                            End If
                        ElseIf Left(varAmountStr, 6) = "ENTL//" Then
                            varAmountStr = Right(varLine, Len(varLine) - 12)
                            If Not ColCorpActionTermInfo.ContainsKey("EntitledQualifier") Then
                                ColCorpActionTermInfo.Add("EntitledQualifier", Left(varAmountStr, 4))
                            End If
                            If Not ColCorpActionTermInfo.ContainsKey("EntitledQuantity") Then
                                ColCorpActionTermInfo.Add("EntitledQuantity", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 5)))
                            End If
                        End If
                    ElseIf Left(varLine, 3) = cSwift564TermDates Then
                        varDateStr = Right(varLine, Len(varLine) - 6)
                        If Left(varDateStr, 6) = "VALU//" And Not ColCorpActionTermInfo.ContainsKey("ValueDate") Then
                            varDateStr = Right(varDateStr, Len(varDateStr) - 6)
                            If Len(varDateStr) = 14 Then
                                ColCorpActionTermInfo.Add("ValueDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2) & " " & Mid(varDateStr, 9, 2) & ":" & Mid(varDateStr, 11, 2) & ":" & Mid(varDateStr, 13, 2))
                            Else
                                ColCorpActionTermInfo.Add("ValueDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2))
                            End If
                        ElseIf Left(varDateStr, 6) = "PAYD//" And Not ColCorpActionTermInfo.ContainsKey("PayDate") Then
                            varDateStr = Right(varDateStr, Len(varDateStr) - 6)
                            If Len(varDateStr) = 14 Then
                                ColCorpActionTermInfo.Add("PayDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2) & " " & Mid(varDateStr, 9, 2) & ":" & Mid(varDateStr, 11, 2) & ":" & Mid(varDateStr, 13, 2))
                            Else
                                ColCorpActionTermInfo.Add("PayDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2))
                            End If
                        ElseIf Left(varDateStr, 6) = "BORD//" And Not ColCorpActionTermInfo.ContainsKey("StockLendingDeadlineDate") Then
                            varDateStr = Right(varDateStr, Len(varDateStr) - 6)
                            If Len(varDateStr) = 14 Then
                                ColCorpActionTermInfo.Add("StockLendingDeadlineDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2) & " " & Mid(varDateStr, 9, 2) & ":" & Mid(varDateStr, 11, 2) & ":" & Mid(varDateStr, 13, 2))
                            Else
                                ColCorpActionTermInfo.Add("StockLendingDeadlineDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2))
                            End If
                        ElseIf Left(varDateStr, 6) = "RDDT//" And Not ColCorpActionTermInfo.ContainsKey("ResponseDeadlineDate") Then
                            varDateStr = Right(varDateStr, Len(varDateStr) - 6)
                            If Len(varDateStr) = 14 Then
                                ColCorpActionTermInfo.Add("ResponseDeadlineDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2) & " " & Mid(varDateStr, 9, 2) & ":" & Mid(varDateStr, 11, 2) & ":" & Mid(varDateStr, 13, 2))
                            Else
                                ColCorpActionTermInfo.Add("ResponseDeadlineDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2))
                            End If
                        ElseIf Left(varDateStr, 6) = "EXPI//" And Not ColCorpActionTermInfo.ContainsKey("ExpiryDate") Then
                            varDateStr = Right(varDateStr, Len(varDateStr) - 6)
                            If Len(varDateStr) = 14 Then
                                ColCorpActionTermInfo.Add("ExpiryDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2) & " " & Mid(varDateStr, 9, 2) & ":" & Mid(varDateStr, 11, 2) & ":" & Mid(varDateStr, 13, 2))
                            Else
                                ColCorpActionTermInfo.Add("ExpiryDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2))
                            End If
                        ElseIf Left(varDateStr, 6) = "POST//" And Not ColCorpActionTermInfo.ContainsKey("PostDate") Then
                            varDateStr = Right(varDateStr, Len(varDateStr) - 6)
                            If Len(varDateStr) = 14 Then
                                ColCorpActionTermInfo.Add("PostDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2) & " " & Mid(varDateStr, 9, 2) & ":" & Mid(varDateStr, 11, 2) & ":" & Mid(varDateStr, 13, 2))
                            Else
                                ColCorpActionTermInfo.Add("PostDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2))
                            End If
                        ElseIf Left(varDateStr, 6) = "MKDT//" And Not ColCorpActionTermInfo.ContainsKey("MarketDate") Then
                            varDateStr = Right(varDateStr, Len(varDateStr) - 6)
                            If Len(varDateStr) = 14 Then
                                ColCorpActionTermInfo.Add("MarketDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2) & " " & Mid(varDateStr, 9, 2) & ":" & Mid(varDateStr, 11, 2) & ":" & Mid(varDateStr, 13, 2))
                            Else
                                ColCorpActionTermInfo.Add("MarketDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2))
                            End If
                        End If
                    ElseIf InStr(1, varLine, cSwift564Rate, vbTextCompare) <> 0 Then
                        If Not ColCorpActionTermInfo.ContainsKey("Rate") Then
                            varAmountStr = Replace(varLine, cSwift564Rate, "")
                            ColCorpActionTermInfo.Add("RateQualifier", Left(varAmountStr, 4))
                            ColCorpActionTermInfo.Add("Rate", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 6)))
                        End If
                    ElseIf Left(varLine, 3) = cSwift564Amount Then
                        varAmountStr = Right(varLine, Len(varLine) - 6)
                        If Left(varAmountStr, 6) = "ENTL//" And Not ColCorpActionTermInfo.ContainsKey("Entitled") Then
                            varAmountStr = Right(varAmountStr, Len(varAmountStr) - 6)
                            ColCorpActionTermInfo.Add("EntitledCCY", Left(varAmountStr, 3))
                            ColCorpActionTermInfo.Add("Entitled", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3)))
                        ElseIf Left(varAmountStr, 6) = "GRSS//" And Not ColCorpActionTermInfo.ContainsKey("Gross") Then
                            varAmountStr = Right(varAmountStr, Len(varAmountStr) - 6)
                            ColCorpActionTermInfo.Add("GrossCCY", Left(varAmountStr, 3))
                            ColCorpActionTermInfo.Add("Gross", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3)))
                        ElseIf Left(varAmountStr, 6) = "NETT//" And Not ColCorpActionTermInfo.ContainsKey("Net") Then
                            varAmountStr = Right(varAmountStr, Len(varAmountStr) - 6)
                            ColCorpActionTermInfo.Add("NetCCY", Left(varAmountStr, 3))
                            ColCorpActionTermInfo.Add("Net", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3)))
                        ElseIf Left(varAmountStr, 6) = "POST//" And Not ColCorpActionTermInfo.ContainsKey("Post") Then
                            varAmountStr = Right(varAmountStr, Len(varAmountStr) - 6)
                            ColCorpActionTermInfo.Add("PostCCY", Left(varAmountStr, 3))
                            ColCorpActionTermInfo.Add("Post", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3)))
                        ElseIf Left(varAmountStr, 6) = "TAXR//" And Not ColCorpActionTermInfo.ContainsKey("Tax") Then
                            varAmountStr = Right(varAmountStr, Len(varAmountStr) - 6)
                            ColCorpActionTermInfo.Add("TaxCCY", Left(varAmountStr, 3))
                            ColCorpActionTermInfo.Add("Tax", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3)))
                        End If
                    ElseIf InStr(1, varLine, cSwift564Gross, vbTextCompare) <> 0 Then
                        If Not ColCorpActionTermInfo.ContainsKey("Gross") Then
                            varAmountStr = Replace(varLine, cSwift564Gross, "")
                            ColCorpActionTermInfo.Add("GrossCCY", Left(varAmountStr, 3))
                            ColCorpActionTermInfo.Add("Gross", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3)))
                        End If
                    ElseIf InStr(1, varLine, cSwift564Net, vbTextCompare) <> 0 Then
                        If Not ColCorpActionTermInfo.ContainsKey("Net") Then
                            varAmountStr = Replace(varLine, cSwift564Net, "")
                            ColCorpActionTermInfo.Add("NetCCY", Left(varAmountStr, 3))
                            ColCorpActionTermInfo.Add("Net", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3)))
                        End If
                    ElseIf Left(varLine, 3) = cSwift564Offer And Not ColCorpActionTermInfo.ContainsKey("Offer") Then
                        If Len(varLine) > 17 Then
                            varAmountStr = Right(varLine, Len(varLine) - 17)
                            If Left(varAmountStr.ToUpper, 3) <> Left(varAmountStr.ToLower, 3) Then
                                ColCorpActionTermInfo.Add("OfferCCY", Left(varAmountStr, 3))
                                ColCorpActionTermInfo.Add("Offer", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3)))
                            Else
                                ColCorpActionTermInfo.Add("Offer", FormatNumber(varAmountStr))
                            End If
                        ElseIf Len(varLine) > 12 Then
                            varAmountStr = Right(varLine, Len(varLine) - 12)
                            If Left(varAmountStr.ToUpper, 3) <> Left(varAmountStr.ToLower, 3) Then
                                ColCorpActionTermInfo.Add("OfferCCY", Left(varAmountStr, 3))
                                ColCorpActionTermInfo.Add("Offer", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3)))
                            Else
                                ColCorpActionTermInfo.Add("Offer", FormatNumber(varAmountStr))
                            End If
                        End If
                    ElseIf Left(varLine, 6) = cSwift564Desc Or varJoinSection = cSwift564Term Then
                        If ColCorpActionTermInfo.ContainsKey("Notes") Then
                            ColCorpActionTermInfo.Add("Notes", ColCorpActionTermInfo.Item("Notes").ToString & Replace(varLine, cSwift564Desc, "") & " ")
                        Else
                            ColCorpActionTermInfo.Add("Notes", Replace(varLine, cSwift564Desc, "") & " ")
                        End If
                        varJoinSection = cSwift564Term
                    End If
                Else
                    If Left(varLine, 1) = ":" Then
                        varJoinSection = ""
                    End If
                    If InStr(1, varLine, cSwift564Reference, vbTextCompare) <> 0 Then
                        _varReference = Replace(varLine, cSwift564Reference, "")
                    End If
                    If InStr(1, varLine, cSwift564RelatedReference, vbTextCompare) <> 0 Then
                        _varRelatedReference = Replace(varLine, cSwift564RelatedReference, "")
                    End If
                    If InStr(1, varLine, cSwift564LinkReference, vbTextCompare) <> 0 Then
                        _varLinkReference = Replace(varLine, cSwift564LinkReference, "")
                    End If
                    If InStr(1, varLine, cSwift564AccNo, vbTextCompare) <> 0 Then
                        _varAccountNo = Replace(varLine, cSwift564AccNo, "")
                    End If
                    If InStr(1, varLine, cSwift564FunctionType, vbTextCompare) <> 0 Then
                        _varFunctionType = Replace(varLine, cSwift564FunctionType, "")
                    End If
                    If InStr(1, varLine, cSwift564Indicator1, vbTextCompare) <> 0 Then
                        _varIndicator1 = Replace(varLine, cSwift564Indicator1, "")
                    End If
                    If InStr(1, varLine, cSwift564Indicator2, vbTextCompare) <> 0 Then
                        _varIndicator2 = Replace(varLine, cSwift564Indicator2, "")
                    End If
                    If InStr(1, varLine, cSwift564ISIN, vbTextCompare) <> 0 Then
                        _varISIN = Replace(varLine, cSwift564ISIN, "")
                    End If
                    If InStr(1, varLine, cSwift564CCY, vbTextCompare) <> 0 Then
                        _varAmountCCY = Replace(varLine, cSwift564CCY, "")
                    End If
                    If Left(varLine, 3) = cSwift564SettledAmt Then
                        If Left(varLine, 4) = ":93C" Then
                            varAmountStr = Replace(varLine, ":93C::SETT//", "")
                            _varSettledAmountQualifier = Left(varAmountStr, 4)
                            _varSettledAmount = FormatNumber(Right(varAmountStr, Len(varAmountStr) - 10))
                        ElseIf Left(varLine, 4) = ":93B" Then
                            If Left(varLine, 12) = ":93B::SETT//" Then
                                varAmountStr = Replace(varLine, ":93B::SETT//", "")
                                _varSettledAmountQualifier = Left(varAmountStr, 4)
                                _varSettledAmount = FormatNumber(Right(varAmountStr, Len(varAmountStr) - 5))
                            ElseIf _varSettledAmount = 0 Then
                                varAmountStr = Right(varLine, Len(varLine) - 12)
                                _varSettledAmountQualifier = Left(varAmountStr, 4)
                                _varSettledAmount = FormatNumber(Right(varAmountStr, Len(varAmountStr) - 5))
                            End If
                        End If
                    End If
                    If Left(varLine, 3) = cSwift564Amount Then
                        varAmountStr = Right(varLine, Len(varLine) - 6)
                        If Left(varDateStr, 6) = "GRSS//" Then
                            varAmountStr = Right(varLine, Len(varLine) - 6)
                            _varGrossAmountCCY = Left(varAmountStr, 3)
                            _varGrossAmount = FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3))
                        ElseIf Left(varDateStr, 6) = "NETT//" Then
                            varAmountStr = Right(varLine, Len(varLine) - 6)
                            _varNetAmountCCY = Left(varAmountStr, 3)
                            _varNetAmount = FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3))
                        End If
                    End If
                    If InStr(1, varLine, cSwift564Rate, vbTextCompare) <> 0 Then
                        varAmountStr = Replace(varLine, cSwift564Rate, "")
                        _varRateQualifier = Left(varAmountStr, 4)
                        _varRate = FormatNumber(Right(varAmountStr, Len(varAmountStr) - 6))
                    End If
                    If Left(varLine, 6) = cSwift564Date Then
                        If InStr(1, varLine, "XDTE//", vbTextCompare) <> 0 Then
                            varDateStr = Replace(varLine, cSwift564Date & "XDTE//", "")
                            _varExDate = Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)
                        ElseIf InStr(1, varLine, "RDTE//", vbTextCompare) <> 0 Then
                            varDateStr = Replace(varLine, cSwift564Date & "RDTE//", "")
                            _varRecDate = Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)
                        ElseIf InStr(1, varLine, "ANOU//", vbTextCompare) <> 0 Then
                            varDateStr = Replace(varLine, cSwift564Date & "ANOU//", "")
                            _varAnnouDate = Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)
                        ElseIf InStr(1, varLine, "EFFD//", vbTextCompare) <> 0 Then
                            varDateStr = Replace(varLine, cSwift564Date & "EFFD//", "")
                            _varEffectiveDate = Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)
                        End If
                    End If
                    If InStr(1, varLine, cSwift564SafeKeeping, vbTextCompare) <> 0 Then
                        _varSafeKeeping = Right(varLine, Len(varLine) - 12)
                    End If
                    If InStr(1, varLine, cSwift564Rate, vbTextCompare) <> 0 Then
                        varAmountStr = Replace(varLine, cSwift564Rate, "")
                        _varRateQualifier = Left(varAmountStr, 4)
                        _varRate = FormatNumber(Right(varAmountStr, Len(varAmountStr) - 6))
                    End If
                    If Left(varLine, 6) = cSwift564Desc Or varJoinSection = cSwift564Desc And Not (varJoinSection = cSwift564Reason) Then
                        _varDescription = _varDescription & Replace(varLine, cSwift564Desc, "") & " "
                        varJoinSection = cSwift564Desc
                    End If
                    If InStr(1, varLine, cSwift564Reason, vbTextCompare) <> 0 Or varJoinSection = cSwift564Reason Then
                        _varReason1 = _varReason1 & Replace(varLine, cSwift564Reason, "") & " "
                        varJoinSection = cSwift564Reason
                    End If
                End If
            Else
                If _varISIN <> "" Then
                    SaveSwiftRowToDB("Processed Swift Type")
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process564, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process566(ByRef varLine As String, ByRef varJoinSection As String, ByRef LstCorpActionTermInfo As List(Of Dictionary(Of String, String)), ByRef ColCorpActionTermInfo As Dictionary(Of String, String))
        Const cSwift566Reference As String = ":20C::CORP//"
        Const cSwift566RelatedReference As String = ":20C::SEME//"
        Const cSwift566LinkReference As String = ":20C::PREV//"
        Const cSwift566AccNo As String = ":97A::SAFE//"
        Const cSwift566FunctionType As String = ":23G:"
        Const cSwift566Indicator1 As String = ":22F::CAEV//"
        Const cSwift566Indicator2 As String = ":22F::CAMV//"
        Const cSwift566ISIN As String = ":35B:ISIN "
        Const cSwift566CCY As String = ":11A::DENO//"
        Const cSwift566Quantity As String = ":93"
        Const cSwift566Amount As String = ":19"
        Const cSwift566Date As String = ":98A::"
        Const cSwift566Rate As String = ":92A::"
        Const cSwift566SafeKeeping As String = ":94"
        Const cSwift566Desc As String = ":70E::"
        Const cSwift566Term As String = ":13A::"
        Const cSwift566TermOptionCCY As String = ":11A::"
        Const cSwift566TermDates As String = ":98"
        Const cSwift566TermAccount As String = ":97"
        Const cSwift566TermIndicator As String = ":22"
        Const cSwift564TermOffer As String = ":90"
        Const cSwift566StartTerm As String = ":16R:CACONF"
        Const cSwift566EndTerm As String = ":16S:CACONF"

        Dim varDateStr As String = ""
        Dim varAmountStr As String
        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If varLine = cSwift566StartTerm Then
                    varJoinSection = cSwift566StartTerm
                ElseIf varLine = cSwift566EndTerm Then
                    AddtoLstInfo(LstCorpActionTermInfo, ColCorpActionTermInfo)
                    SaveSwiftRowToDB("", Nothing, LstCorpActionTermInfo)
                    varJoinSection = ""
                ElseIf varJoinSection = cSwift566StartTerm Then
                    If InStr(1, varLine, cSwift566ISIN, vbTextCompare) <> 0 And Not ColCorpActionTermInfo.ContainsKey("ISIN") Then
                        ColCorpActionTermInfo.Add("ISIN", Right(varLine, Len(varLine) - 10))
                    ElseIf Left(varLine, 6) = cSwift566Term And Not ColCorpActionTermInfo.ContainsKey("Term") Then
                        ColCorpActionTermInfo.Add("Term", Right(varLine, Len(varLine) - 12))
                    ElseIf Left(varLine, 3) = cSwift566TermIndicator Then
                        If ColCorpActionTermInfo.ContainsKey("TermOption") Then
                            If ColCorpActionTermInfo.ContainsKey("Indicator1") Then
                                If Not ColCorpActionTermInfo.ContainsKey("Indicator2") Then
                                    ColCorpActionTermInfo.Add("Indicator2", Right(varLine, Len(varLine) - 6))
                                End If
                            ElseIf Not ColCorpActionTermInfo.ContainsKey("Indicator1") Then
                                ColCorpActionTermInfo.Add("Indicator1", Right(varLine, Len(varLine) - 6))
                            End If
                        ElseIf Not ColCorpActionTermInfo.ContainsKey("TermOption") Then
                            ColCorpActionTermInfo.Add("TermOption", Right(varLine, Len(varLine) - 12))
                        End If
                    ElseIf Left(varLine, 6) = cSwift566TermOptionCCY And Not ColCorpActionTermInfo.ContainsKey("TermOptionCCY") Then
                        ColCorpActionTermInfo.Add("TermOptionCCY", Right(varLine, Len(varLine) - 12))
                    ElseIf Left(varLine, 3) = cSwift566TermDates Then
                        varDateStr = Right(varLine, Len(varLine) - 6)
                        If Left(varDateStr, 6) = "VALU//" And Not ColCorpActionTermInfo.ContainsKey("ValueDate") Then
                            varDateStr = Right(varDateStr, Len(varDateStr) - 6)
                            ColCorpActionTermInfo.Add("ValueDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2))
                        ElseIf Left(varDateStr, 6) = "PAYD//" And Not ColCorpActionTermInfo.ContainsKey("PayDate") Then
                            varDateStr = Right(varDateStr, Len(varDateStr) - 6)
                            ColCorpActionTermInfo.Add("PayDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2))
                        ElseIf Left(varDateStr, 6) = "RDDT//" And Not ColCorpActionTermInfo.ContainsKey("ResponseDeadlineDate") Then
                            varDateStr = Right(varDateStr, Len(varDateStr) - 6)
                            ColCorpActionTermInfo.Add("ResponseDeadlineDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2))
                        ElseIf Left(varDateStr, 6) = "EXPI//" And Not ColCorpActionTermInfo.ContainsKey("ExpiryDate") Then
                            varDateStr = Right(varDateStr, Len(varDateStr) - 6)
                            ColCorpActionTermInfo.Add("ExpiryDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2))
                        ElseIf Left(varDateStr, 6) = "POST//" And Not ColCorpActionTermInfo.ContainsKey("PostDate") Then
                            varDateStr = Right(varDateStr, Len(varDateStr) - 6)
                            ColCorpActionTermInfo.Add("PostDate", Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2))
                        End If
                    ElseIf InStr(1, varLine, cSwift566Rate, vbTextCompare) <> 0 Then
                        If Not ColCorpActionTermInfo.ContainsKey("Rate") Then
                            varAmountStr = Replace(varLine, cSwift566Rate, "")
                            ColCorpActionTermInfo.Add("RateQualifier", Left(varAmountStr, 4))
                            ColCorpActionTermInfo.Add("Rate", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 6)))
                        End If
                    ElseIf Left(varLine, 3) = cSwift566Amount Then
                        varAmountStr = Right(varLine, Len(varLine) - 6)
                        If Left(varAmountStr, 6) = "PSTA//" And Not ColCorpActionTermInfo.ContainsKey("Post") Then
                            varAmountStr = Right(varLine, Len(varLine) - 12)
                            ColCorpActionTermInfo.Add("PostCCY", Left(varAmountStr, 3))
                            ColCorpActionTermInfo.Add("Post", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3)))
                        ElseIf Left(varAmountStr, 6) = "GRSS//" And Not ColCorpActionTermInfo.ContainsKey("Gross") Then
                            varAmountStr = Right(varLine, Len(varLine) - 12)
                            ColCorpActionTermInfo.Add("GrossCCY", Left(varAmountStr, 3))
                            ColCorpActionTermInfo.Add("Gross", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3)))
                        ElseIf Left(varAmountStr, 6) = "NETT//" And Not ColCorpActionTermInfo.ContainsKey("Net") Then
                            varAmountStr = Right(varLine, Len(varLine) - 12)
                            ColCorpActionTermInfo.Add("NetCCY", Left(varAmountStr, 3))
                            ColCorpActionTermInfo.Add("Net", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3)))
                        ElseIf Left(varAmountStr, 6) = "TAXR//" And Not ColCorpActionTermInfo.ContainsKey("Tax") Then
                            varAmountStr = Right(varLine, Len(varLine) - 12)
                            ColCorpActionTermInfo.Add("TaxCCY", Left(varAmountStr, 3))
                            ColCorpActionTermInfo.Add("Tax", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3)))
                        End If
                    ElseIf Left(varLine, 3) = cSwift566TermAccount And Not ColCorpActionTermInfo.ContainsKey("Account") Then
                        varAmountStr = Right(varLine, Len(varLine) - 6)
                        ColCorpActionTermInfo.Add("AccountQualifier", Left(varAmountStr, 4))
                        ColCorpActionTermInfo.Add("Account", Right(varAmountStr, Len(varAmountStr) - 6))
                    ElseIf Left(varLine, 3) = cSwift564TermOffer And Not ColCorpActionTermInfo.ContainsKey("Offer") Then
                        If Len(varLine) > 17 Then
                            varAmountStr = Right(varLine, Len(varLine) - 17)
                            If Left(varAmountStr.ToUpper, 3) <> Left(varAmountStr.ToLower, 3) Then
                                ColCorpActionTermInfo.Add("OfferCCY", Left(varAmountStr, 3))
                                ColCorpActionTermInfo.Add("Offer", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3)))
                            Else
                                ColCorpActionTermInfo.Add("Offer", FormatNumber(varAmountStr))
                            End If
                        ElseIf Len(varLine) > 12 Then
                            varAmountStr = Right(varLine, Len(varLine) - 12)
                            If Left(varAmountStr.ToUpper, 3) <> Left(varAmountStr.ToLower, 3) Then
                                ColCorpActionTermInfo.Add("OfferCCY", Left(varAmountStr, 3))
                                ColCorpActionTermInfo.Add("Offer", FormatNumber(Right(varAmountStr, Len(varAmountStr) - 3)))
                            Else
                                ColCorpActionTermInfo.Add("Offer", FormatNumber(varAmountStr))
                            End If
                        End If
                    ElseIf Left(varLine, 6) = cSwift566Desc Or varJoinSection = cSwift566Term Then
                        If ColCorpActionTermInfo.ContainsKey("Notes") Then
                            ColCorpActionTermInfo.Add("Notes", ColCorpActionTermInfo.Item("Notes").ToString & Replace(varLine, cSwift566Desc, "") & " ")
                        Else
                            ColCorpActionTermInfo.Add("Notes", Replace(varLine, cSwift566Desc, "") & " ")
                        End If
                        varJoinSection = cSwift566Term
                    End If
                Else
                    If Left(varLine, 1) = ":" Then
                        varJoinSection = ""
                    End If
                    If InStr(1, varLine, cSwift566Reference, vbTextCompare) <> 0 Then
                        _varReference = Replace(varLine, cSwift566Reference, "")
                    End If
                    If InStr(1, varLine, cSwift566RelatedReference, vbTextCompare) <> 0 Then
                        _varRelatedReference = Replace(varLine, cSwift566RelatedReference, "")
                    End If
                    If InStr(1, varLine, cSwift566LinkReference, vbTextCompare) <> 0 Then
                        _varLinkReference = Replace(varLine, cSwift566LinkReference, "")
                    End If
                    If InStr(1, varLine, cSwift566AccNo, vbTextCompare) <> 0 Then
                        _varAccountNo = Replace(varLine, cSwift566AccNo, "")
                    End If
                    If InStr(1, varLine, cSwift566FunctionType, vbTextCompare) <> 0 Then
                        _varFunctionType = Replace(varLine, cSwift566FunctionType, "")
                    End If
                    If InStr(1, varLine, cSwift566Indicator1, vbTextCompare) <> 0 Then
                        _varIndicator1 = Replace(varLine, cSwift566Indicator1, "")
                    End If
                    If InStr(1, varLine, cSwift566Indicator2, vbTextCompare) <> 0 Then
                        _varIndicator2 = Replace(varLine, cSwift566Indicator2, "")
                    End If
                    If InStr(1, varLine, cSwift566ISIN, vbTextCompare) <> 0 Then
                        _varISIN = Replace(varLine, cSwift566ISIN, "")
                    End If
                    If InStr(1, varLine, cSwift566CCY, vbTextCompare) <> 0 Then
                        _varAmountCCY = Replace(varLine, cSwift566CCY, "")
                    End If
                    If Left(varLine, 3) = cSwift566Quantity Then
                        varAmountStr = Right(varLine, Len(varLine) - 6)
                        If Left(varAmountStr, 6) = "ELIG//" Then
                            varAmountStr = Right(varLine, Len(varLine) - 12)
                            _varOpeningAmountQualifier = Left(varAmountStr, 4)
                            _varOpeningAmount = FormatNumber(Right(varAmountStr, Len(varAmountStr) - 5))
                        ElseIf Left(varAmountStr, 6) = "SETT//" Then
                            varAmountStr = Right(varLine, Len(varLine) - 12)
                            _varSettledAmountQualifier = Left(varAmountStr, 4)
                            _varSettledAmount = FormatNumber(Right(varAmountStr, Len(varAmountStr) - 5))
                        ElseIf Left(varAmountStr, 6) = "CONB//" Then
                            varAmountStr = Right(varLine, Len(varLine) - 12)
                            _varAmountQualifier = Left(varAmountStr, 4)
                            _varAmount = FormatNumber(Right(varAmountStr, Len(varAmountStr) - 5))
                        End If
                    End If
                    If Left(varLine, 6) = cSwift566Date Then
                        If InStr(1, varLine, "XDTE//", vbTextCompare) <> 0 Then
                            varDateStr = Replace(varLine, cSwift566Date & "XDTE//", "")
                            _varExDate = Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)
                        ElseIf InStr(1, varLine, "RDTE//", vbTextCompare) <> 0 Then
                            varDateStr = Replace(varLine, cSwift566Date & "RDTE//", "")
                            _varRecDate = Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)
                        ElseIf InStr(1, varLine, "EFFD//", vbTextCompare) <> 0 Then
                            varDateStr = Replace(varLine, cSwift566Date & "EFFD//", "")
                            _varEffectiveDate = Left(varDateStr, 4) & "-" & Mid(varDateStr, 5, 2) & "-" & Mid(varDateStr, 7, 2)
                        End If
                    End If
                    If InStr(1, varLine, cSwift566SafeKeeping, vbTextCompare) <> 0 Then
                        _varSafeKeeping = Right(varLine, Len(varLine) - 12)
                    End If
                    If InStr(1, varLine, cSwift566Rate, vbTextCompare) <> 0 Then
                        varAmountStr = Replace(varLine, cSwift566Rate, "")
                        _varRateQualifier = Left(varAmountStr, 4)
                        _varRate = FormatNumber(Right(varAmountStr, Len(varAmountStr) - 6))
                    End If
                    If Left(varLine, 6) = cSwift566Desc Or varJoinSection = cSwift566Desc Then
                        _varDescription = _varDescription & Replace(varLine, cSwift566Desc, "") & " "
                        varJoinSection = cSwift566Desc
                    End If
                End If
            Else
                If _varISIN <> "" Then
                    SaveSwiftRowToDB("Processed Swift Type")
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process566, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process568(ByRef varLine As String, ByRef varJoinSection As String)
        Const cSwift568Reference As String = ":20C::CORP//"
        Const cSwift568RelatedReference As String = ":20C::SEME//"
        Const cSwift568LinkReference As String = ":20C::PREV//"
        Const cSwift568AccNo As String = ":97A::SAFE//"
        Const cSwift568ISIN As String = ":35B:ISIN "
        Const cSwift568Desc As String = ":70"

        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If Left(varLine, 1) = ":" Then
                    varJoinSection = ""
                End If
                If InStr(1, varLine, cSwift568Reference, vbTextCompare) <> 0 Then
                    _varReference = Replace(varLine, cSwift568Reference, "")
                End If
                If InStr(1, varLine, cSwift568RelatedReference, vbTextCompare) <> 0 Then
                    _varRelatedReference = Replace(varLine, cSwift568RelatedReference, "")
                End If
                If InStr(1, varLine, cSwift568LinkReference, vbTextCompare) <> 0 Then
                    _varLinkReference = Replace(varLine, cSwift568LinkReference, "")
                End If
                If InStr(1, varLine, cSwift568AccNo, vbTextCompare) <> 0 Then
                    _varAccountNo = Replace(varLine, cSwift568AccNo, "")
                End If
                If InStr(1, varLine, cSwift568ISIN, vbTextCompare) <> 0 Then
                    _varISIN = Replace(varLine, cSwift568ISIN, "")
                End If
                If Left(varLine, 3) = cSwift568Desc Or varJoinSection = cSwift568Desc Then
                    _varDescription = _varDescription & Replace(varLine, cSwift568Desc, "") & " "
                    varJoinSection = cSwift568Desc
                End If
            Else
                If _varISIN <> "" Then
                    SaveSwiftRowToDB("Processed Swift Type")
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process568, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process900(ByRef varLine As String, ByRef varJoinSection As String)
        Const cSwift900Reference As String = ":20:"
        Const cSwift900RelatedReference As String = ":21:"
        Const cSwift900AccNo As String = ":25:"
        Const cSwift900Detail As String = ":32A:"
        Const cSwift900OrderCustomer As String = ":50"
        Const cSwift900Order As String = ":52"
        Const cSwift900Intermediary As String = ":56"
        Const cSwift900Reason1 As String = ":72:"

        Dim var900DetailStr As String = ""
        Dim varOrderInstitutionStr As String = ""
        Dim varOrderBankInstitutionStr As String = ""
        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If Left(varLine, 1) = ":" Then
                    varJoinSection = ""
                End If
                If InStr(1, varLine, cSwift900Reference, vbTextCompare) <> 0 Then
                    _varReference = Replace(varLine, cSwift900Reference, "")
                End If
                If InStr(1, varLine, cSwift900RelatedReference, vbTextCompare) <> 0 Then
                    _varRelatedReference = Replace(varLine, cSwift900RelatedReference, "")
                End If
                If InStr(1, varLine, cSwift900AccNo, vbTextCompare) <> 0 Then
                    _varAccountNo = Replace(varLine, cSwift900AccNo, "")
                End If
                If InStr(1, varLine, cSwift900Detail, vbTextCompare) <> 0 Then
                    var900DetailStr = Replace(varLine, cSwift900Detail, "")
                    _varAmountCCY = Mid(var900DetailStr, 7, 3)
                    _varTradeDate = CDate(Mid(var900DetailStr, 5, 2) & "/" & Mid(var900DetailStr, 3, 2) & "/" & Left(var900DetailStr, 2)).ToString("yyyy/MM/dd")
                    _varSettleDate = _varTradeDate
                    _varAmount = FormatNumber(Right(var900DetailStr, Len(var900DetailStr) - 9))
                End If
                If InStr(1, varLine, cSwift900OrderCustomer, vbTextCompare) <> 0 Or varJoinSection = cSwift900OrderCustomer Then
                    If InStr(1, varLine, cSwift900OrderCustomer, vbTextCompare) <> 0 Then
                        varOrderInstitutionStr = Right(varLine, Len(varLine) - 5)
                    Else
                        varOrderInstitutionStr = varLine
                    End If
                    If Left(varOrderInstitutionStr, 1) = "/" Then
                        _varOrderIBAN = Right(varOrderInstitutionStr, Len(varOrderInstitutionStr) - 1)
                    Else
                        _varOrderInstitution = _varOrderInstitution & varOrderInstitutionStr & ","
                    End If
                    varJoinSection = cSwift900OrderCustomer
                End If
                If Left(varLine, 3) = cSwift900Order Or varJoinSection = cSwift900Order Then
                    If Left(varLine, 3) = cSwift900Order Then
                        varOrderBankInstitutionStr = Right(varLine, Len(varLine) - 5)
                    Else
                        varOrderBankInstitutionStr = varLine
                    End If
                    If Left(varOrderBankInstitutionStr, 1) = "/" Then
                        _varOrderBankIBAN = Right(varOrderBankInstitutionStr, Len(varOrderBankInstitutionStr) - 1)
                    Else
                        _varOrderBankInstitution = _varOrderBankInstitution & varOrderBankInstitutionStr & ","
                    End If
                    varJoinSection = cSwift900Order
                End If

                If InStr(1, varLine, cSwift900Intermediary, vbTextCompare) <> 0 Then
                    _varIntermediary = Right(varLine, Len(varLine) - 5)
                End If
                If InStr(1, varLine, cSwift900Reason1, vbTextCompare) <> 0 Or varJoinSection = cSwift900Reason1 Then
                    _varReason1 &= Replace(varLine, cSwift900Reason1, "")
                    varJoinSection = cSwift900Reason1
                End If
            Else
                If Len(_varOrderBankInstitution) > 1 Then
                    If Right(_varOrderBankInstitution, 1) = "," Then
                        _varOrderBankInstitution = Left(_varOrderBankInstitution, Len(_varOrderBankInstitution) - 1)
                    End If
                End If

                If _varAccountNo <> "" Then
                    SaveSwiftRowToDB("Processed Swift Type")
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process900, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process910(ByRef varLine As String, ByRef varJoinSection As String)
        Const cSwift910Reference As String = ":20:"
        Const cSwift910RelatedReference As String = ":21:"
        Const cSwift910AccNo As String = ":25:"
        Const cSwift910Detail As String = ":32A:"
        Const cSwift910Ben As String = ":50"
        Const cSwift910BenBank As String = ":52"
        Const cSwift910Intermediary As String = ":56"
        Const cSwift910Reason1 As String = ":72:"

        Dim var910DetailStr As String = ""
        Dim varBeneficiaryInstitutionStr As String = ""
        Dim varBeneficiaryBankInstitutionStr As String = ""
        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If Left(varLine, 1) = ":" Then
                    varJoinSection = ""
                End If
                If InStr(1, varLine, cSwift910Reference, vbTextCompare) <> 0 Then
                    _varReference = Replace(varLine, cSwift910Reference, "")
                End If
                If InStr(1, varLine, cSwift910RelatedReference, vbTextCompare) <> 0 Then
                    _varRelatedReference = Replace(varLine, cSwift910RelatedReference, "")
                End If
                If InStr(1, varLine, cSwift910AccNo, vbTextCompare) <> 0 Then
                    _varAccountNo = Replace(varLine, cSwift910AccNo, "")
                End If
                If InStr(1, varLine, cSwift910Detail, vbTextCompare) <> 0 Then
                    var910DetailStr = Replace(varLine, cSwift910Detail, "")
                    _varAmountCCY = Mid(var910DetailStr, 7, 3)
                    _varTradeDate = CDate(Mid(var910DetailStr, 5, 2) & "/" & Mid(var910DetailStr, 3, 2) & "/" & Left(var910DetailStr, 2)).ToString("yyyy/MM/dd")
                    _varSettleDate = _varTradeDate
                    _varAmount = FormatNumber(Right(var910DetailStr, Len(var910DetailStr) - 9))
                End If

                If InStr(1, varLine, cSwift910Intermediary, vbTextCompare) <> 0 Then
                    _varIntermediary = Right(varLine, Len(varLine) - 5)
                End If
                If Left(varLine, 3) = cSwift910Ben Or varJoinSection = cSwift910Ben Then
                    If Left(varLine, 3) = cSwift910Ben Then
                        varBeneficiaryInstitutionStr = Right(varLine, Len(varLine) - 5)
                    Else
                        varBeneficiaryInstitutionStr = varLine
                    End If
                    If Left(varBeneficiaryInstitutionStr, 1) = "/" Then
                        _varBeneficiaryBankIBAN = Right(varBeneficiaryInstitutionStr, Len(varBeneficiaryInstitutionStr) - 1)
                    Else
                        _varBeneficiaryBankInstitution = _varBeneficiaryBankInstitution & varBeneficiaryInstitutionStr & ","
                    End If
                    varJoinSection = cSwift910Ben
                End If
                If InStr(1, varLine, cSwift910BenBank, vbTextCompare) <> 0 Or varJoinSection = cSwift910BenBank Then
                    If InStr(1, varLine, cSwift910BenBank, vbTextCompare) <> 0 Then
                        varBeneficiaryBankInstitutionStr = Right(varLine, Len(varLine) - 4)
                    Else
                        varBeneficiaryBankInstitutionStr = varLine
                    End If
                    If Left(varBeneficiaryBankInstitutionStr, 1) = "/" Then
                        _varBeneficiaryIBAN = Right(varBeneficiaryBankInstitutionStr, Len(varBeneficiaryBankInstitutionStr) - 1)
                    Else
                        _varBeneficiaryInstitution = _varBeneficiaryInstitution & varBeneficiaryBankInstitutionStr & ","
                    End If
                    varJoinSection = cSwift910BenBank
                End If
                If InStr(1, varLine, cSwift910Reason1, vbTextCompare) <> 0 Or varJoinSection = cSwift910Reason1 Then
                    _varReason1 &= Replace(varLine, cSwift910Reason1, "")
                    varJoinSection = cSwift910Reason1
                End If
            Else
                If Len(_varBeneficiaryInstitution) > 1 Then
                    If Right(_varBeneficiaryInstitution, 1) = "," Then
                        _varBeneficiaryInstitution = Left(_varBeneficiaryInstitution, Len(_varBeneficiaryInstitution) - 1)
                    End If
                End If

                If _varAccountNo <> "" Then
                    SaveSwiftRowToDB("Processed Swift Type")
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process910, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process920(ByRef varLine As String)
        Const cSwift920Reference As String = ":20:"
        Const cSwift920Type As String = ":12:"
        Const cSwift920AccNo As String = ":25:"
        Const cSwift920Ind As String = ":34F:"

        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If InStr(1, varLine, cSwift920Reference, vbTextCompare) <> 0 Then
                    _varReference = Replace(varLine, cSwift920Reference, "")
                End If
                If InStr(1, varLine, cSwift920Type, vbTextCompare) <> 0 Then
                    _varIndicator1 = Replace(varLine, cSwift920Type, "")
                End If
                If InStr(1, varLine, cSwift920AccNo, vbTextCompare) <> 0 Then
                    _varAccountNo = Replace(varLine, cSwift920AccNo, "")
                End If
                If InStr(1, varLine, cSwift920Ind, vbTextCompare) <> 0 Then
                    _varIndicator2 = Replace(varLine, cSwift920Ind, "")
                    _varAccountCCY = Left(_varIndicator2, 3)
                End If
            Else
                If _varAccountNo <> "" Then
                    SaveSwiftRowToDB("Processed Swift Type")
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process920, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process940(ByRef varLine As String, ByRef varJoinSection As String, ByRef LstAccountInfo As List(Of Dictionary(Of String, String)), ByRef ColAddInfo As Dictionary(Of String, String))
        Const cSwift940Reference As String = ":20:"
        Const cSwift940RelatedReference As String = ":21:"
        Const cSwift940AccNo As String = ":25:"
        Const cSwift940Indicator1 As String = ":28C:"
        Const cSwift940OpeningNo As String = ":60"
        Const cSwift940Statement As String = ":61"
        Const cSwift940StatementInfo As String = ":86"
        Const cSwift940ClosingBookedNo As String = ":62"
        Const cSwift940ClosingAvailableNo As String = ":64:"

        Dim varStatementAmountStartPosition As Integer
        Dim var940AccStr As String = ""
        Dim var940OpeningStr As String = ""
        Dim var940StatementStr As String = ""
        Dim var940ClosingBookedStr As String = ""
        Dim var940ClosingAvailableStr As String = ""
        Dim var940AccCCY As String = ""

        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If Left(varLine, 1) = ":" Then
                    varJoinSection = ""
                End If
                If Left(varLine, 4) = cSwift940Reference Then
                    _varReference = Replace(varLine, cSwift940Reference, "")
                End If
                If Left(varLine, 4) = cSwift940RelatedReference Then
                    _varRelatedReference = Replace(varLine, cSwift940RelatedReference, "")
                End If
                If Left(varLine, 4) = cSwift940AccNo Then
                    _varAccountNo = Replace(varLine, cSwift940AccNo, "")
                    If Not IsNumeric(Right(var940AccStr, 3)) Then
                        var940AccCCY = Right(var940AccStr, 3)
                    End If
                End If
                If Left(varLine, 5) = cSwift940Indicator1 Then
                    _varIndicator1 = Replace(varLine, cSwift940Indicator1, "")
                End If
                If Left(varLine, 3) = cSwift940OpeningNo Then
                    var940OpeningStr = Right(varLine, Len(varLine) - 5)
                    _varOpeningAmountCCY = Mid(var940OpeningStr, 8, 3)
                    _varOpeningAmount = FormatNumber(Right(var940OpeningStr, Len(var940OpeningStr) - 10))
                    _varTradeDate = IIf(IsDate(Mid(var940OpeningStr, 6, 2) & "-" & Mid(var940OpeningStr, 4, 2) & "-" & Mid(var940OpeningStr, 2, 2)), Mid(var940OpeningStr, 6, 2) & "-" & Mid(var940OpeningStr, 4, 2) & "-" & Mid(var940OpeningStr, 2, 2), Now)
                    If UCase(Left(var940OpeningStr, 1)) = "D" Or UCase(Left(var940OpeningStr, 2)) = "RC" Then
                        _varOpeningAmount = -_varOpeningAmount
                    End If
                End If
                If Left(varLine, 3) = cSwift940Statement Or Left(varLine, 3) = cSwift940StatementInfo Or varJoinSection = cSwift940Statement Or varJoinSection = cSwift940StatementInfo Then
                    If Left(varLine, 3) = cSwift940Statement Then
                        AddtoLstInfo(LstAccountInfo, ColAddInfo)
                        var940StatementStr = Right(varLine, Len(varLine) - 4)

                        If IsDate("20" & Mid(var940StatementStr, 1, 2) & "-" & Mid(var940StatementStr, 3, 2) & "-" & Mid(var940StatementStr, 5, 2)) Then
                            ColAddInfo.Add("TradeDate", "20" & Mid(var940StatementStr, 1, 2) & "-" & Mid(var940StatementStr, 3, 2) & "-" & Mid(var940StatementStr, 5, 2))
                            ColAddInfo.Add("SettleDate", "20" & Mid(var940StatementStr, 1, 2) & "-" & Mid(var940StatementStr, 3, 2) & "-" & Mid(var940StatementStr, 5, 2))
                        End If

                        Dim varCreditDebitLetter As String = Mid(var940StatementStr, 11, 2)
                        If IsNumeric(Mid(var940StatementStr, 12, 1)) Then
                            varStatementAmountStartPosition = 11
                        Else
                            varStatementAmountStartPosition = 12
                        End If

                        Dim varStatementAmountEndPosition As Integer = varStatementAmountStartPosition + FindNextLetterPositionInString(Right(var940StatementStr, Len(var940StatementStr) - varStatementAmountStartPosition)) + 1
                        Dim varStatementAmountStr As String = Mid(var940StatementStr, varStatementAmountStartPosition + 1, varStatementAmountEndPosition - varStatementAmountStartPosition - 1)
                        ColAddInfo.Add("Type2", IIf(IsNumeric(Right(Left(varCreditDebitLetter, 2), 1)), Left(varCreditDebitLetter, 1), Left(varCreditDebitLetter, 2)))
                        If IsNumeric(FormatNumber(varStatementAmountStr)) Then
                            ColAddInfo.Add("Amount", IIf(Left(varCreditDebitLetter, 1) = "D" Or Left(varCreditDebitLetter, 2) = "RC", -FormatNumber(varStatementAmountStr), FormatNumber(varStatementAmountStr)))
                        End If
                        ColAddInfo.Add("CCY", IIf(var940AccCCY = "", _varOpeningAmountCCY, var940AccCCY))
                        ColAddInfo.Add("Type", Mid(var940StatementStr, varStatementAmountEndPosition, 4))
                        If InStr(varStatementAmountEndPosition, var940StatementStr, "//", vbTextCompare) <> 0 Then
                            Dim varBankreferencestartposition As Integer = InStr(varStatementAmountEndPosition, var940StatementStr, "//", vbTextCompare)
                            ColAddInfo.Add("CustomerReference", Mid(var940StatementStr, varStatementAmountEndPosition + 4, varBankreferencestartposition - (varStatementAmountEndPosition + 4)))
                            ColAddInfo.Add("BankReference", Mid(var940StatementStr, varBankreferencestartposition + 2, 16))
                        Else
                            ColAddInfo.Add("CustomerReference", Right(var940StatementStr, Len(var940StatementStr) - varStatementAmountEndPosition - 3))
                        End If
                        varJoinSection = cSwift940Statement
                    ElseIf Left(varLine, 3) = cSwift940StatementInfo Then
                        AddValueToDictionary(ColAddInfo, "AdditionalInfo2", Right(varLine, Len(varLine) - 4))
                        varJoinSection = cSwift940StatementInfo
                    ElseIf varJoinSection = cSwift940Statement Then
                        AddValueToDictionary(ColAddInfo, "AdditionalInfo1", varLine)
                        varJoinSection = cSwift940Statement
                    ElseIf varJoinSection = cSwift940StatementInfo Then
                        AddValueToDictionary(ColAddInfo, "AdditionalInfo2", varLine)
                        varJoinSection = cSwift940StatementInfo
                    End If
                End If
                If Left(varLine, 3) = cSwift940ClosingBookedNo Then
                    var940ClosingBookedStr = Right(varLine, Len(varLine) - 5)
                    _varAmountCCY = Mid(var940ClosingBookedStr, 8, 3)
                    _varAmount = FormatNumber(Right(var940ClosingBookedStr, Len(var940ClosingBookedStr) - 10))
                    _varSettleDate = IIf(IsDate(Mid(var940ClosingBookedStr, 6, 2) & "-" & Mid(var940ClosingBookedStr, 4, 2) & "-" & Mid(var940ClosingBookedStr, 2, 2)), Mid(var940ClosingBookedStr, 6, 2) & "-" & Mid(var940ClosingBookedStr, 4, 2) & "-" & Mid(var940ClosingBookedStr, 2, 2), Now)
                    If UCase(Left(var940ClosingBookedStr, 1)) = "D" Or UCase(Left(var940ClosingBookedStr, 2)) = "RC" Then
                        _varAmount = -_varAmount
                    End If
                End If
                If Left(varLine, 4) = cSwift940ClosingAvailableNo Then
                    var940ClosingAvailableStr = Right(varLine, Len(varLine) - 4)
                    _varClosingAmountCCY = Mid(var940ClosingAvailableStr, 8, 3)
                    _varClosingAmount = FormatNumber(Right(var940ClosingAvailableStr, Len(var940ClosingAvailableStr) - 10))
                    _varSettleDate = IIf(IsDate(Mid(var940ClosingAvailableStr, 6, 2) & "-" & Mid(var940ClosingAvailableStr, 4, 2) & "-" & Mid(var940ClosingAvailableStr, 2, 2)), Mid(var940ClosingAvailableStr, 6, 2) & "-" & Mid(var940ClosingAvailableStr, 4, 2) & "-" & Mid(var940ClosingAvailableStr, 2, 2), Now)
                    If UCase(Left(var940ClosingAvailableStr, 1)) = "D" Or UCase(Left(var940ClosingBookedStr, 2)) = "RC" Then
                        _varClosingAmount = -_varClosingAmount
                    End If
                End If
            Else
                If _varAccountNo <> "" Then
                    AddtoLstInfo(LstAccountInfo, ColAddInfo)
                    SaveSwiftRowToDB("Processed Swift Type", LstAccountInfo)
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process940, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process942(ByRef varLine As String, ByRef varJoinSection As String, ByRef LstAccountInfo As List(Of Dictionary(Of String, String)), ByRef ColAddInfo As Dictionary(Of String, String))
        Const cSwift942Reference As String = ":20:"
        Const cSwift942RelatedReference As String = ":21:"
        Const cSwift942AccNo As String = ":25:"
        Const cSwift942Indicator1 As String = ":28C:"
        Const cSwift942Indicator2 As String = ":34F:"
        Const cSwift942Statement As String = ":61"
        Const cSwift942StatementInfo As String = ":86"
        Const cSwift942ClosingBalances As String = ":90"

        Dim varStatementAmountStartPosition As Integer
        Dim var942AccStr As String = ""
        Dim var942OpeningStr As String = ""
        Dim var942StatementStr As String = ""
        Dim var942ClosingBalancesStr As String = ""
        Dim var942ClosingAvailableStr As String = ""
        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If Left(varLine, 1) = ":" Then
                    varJoinSection = ""
                End If
                If Left(varLine, 4) = cSwift942Reference Then
                    _varReference = Replace(varLine, cSwift942Reference, "")
                End If
                If Left(varLine, 4) = cSwift942RelatedReference Then
                    _varRelatedReference = Replace(varLine, cSwift942RelatedReference, "")
                End If
                If Left(varLine, 4) = cSwift942AccNo Then
                    _varAccountNo = Replace(varLine, cSwift942AccNo, "")
                    If Not IsNumeric(Right(var942AccStr, 3)) Then
                        _varAccountCCY = Right(var942AccStr, 3)
                    End If
                End If
                If Left(varLine, 5) = cSwift942Indicator1 Then
                    _varIndicator1 = Replace(varLine, cSwift942Indicator1, "")
                End If
                If Left(varLine, 5) = cSwift942Indicator2 Then
                    _varIndicator2 = Replace(varLine, cSwift942Indicator2, "")
                    _varAccountCCY = Left(_varIndicator2, 3)
                End If
                If Left(varLine, 3) = cSwift942Statement Or Left(varLine, 3) = cSwift942StatementInfo Or varJoinSection = cSwift942Statement Or varJoinSection = cSwift942StatementInfo Then
                    If Left(varLine, 3) = cSwift942Statement Then
                        AddtoLstInfo(LstAccountInfo, ColAddInfo)
                        var942StatementStr = Right(varLine, Len(varLine) - 4)

                        If IsDate("20" & Mid(var942StatementStr, 1, 2) & "-" & Mid(var942StatementStr, 3, 2) & "-" & Mid(var942StatementStr, 5, 2)) Then
                            ColAddInfo.Add("TradeDate", "20" & Mid(var942StatementStr, 1, 2) & "-" & Mid(var942StatementStr, 3, 2) & "-" & Mid(var942StatementStr, 5, 2))
                            ColAddInfo.Add("SettleDate", "20" & Mid(var942StatementStr, 1, 2) & "-" & Mid(var942StatementStr, 3, 2) & "-" & Mid(var942StatementStr, 5, 2))
                        End If

                        Dim varCreditDebitLetter As String = Mid(var942StatementStr, 11, 2)
                        If IsNumeric(Mid(var942StatementStr, 12, 1)) Then
                            varStatementAmountStartPosition = 11
                        Else
                            varStatementAmountStartPosition = 12
                        End If

                        Dim varStatementAmountEndPosition As Integer = varStatementAmountStartPosition + FindNextLetterPositionInString(Right(var942StatementStr, Len(var942StatementStr) - varStatementAmountStartPosition)) + 1
                        Dim varStatementAmountStr As String = Mid(var942StatementStr, varStatementAmountStartPosition + 1, varStatementAmountEndPosition - varStatementAmountStartPosition - 1)
                        ColAddInfo.Add("Type2", IIf(IsNumeric(Right(Left(varCreditDebitLetter, 2), 1)), Left(varCreditDebitLetter, 1), Left(varCreditDebitLetter, 2)))
                        If IsNumeric(FormatNumber(varStatementAmountStr)) Then
                            ColAddInfo.Add("Amount", IIf(Left(varCreditDebitLetter, 1) = "D" Or Left(varCreditDebitLetter, 2) = "RC", -FormatNumber(varStatementAmountStr), FormatNumber(varStatementAmountStr)))
                        End If
                        ColAddInfo.Add("CCY", _varAccountCCY)
                        ColAddInfo.Add("Type", Mid(var942StatementStr, varStatementAmountEndPosition, 4))
                        If InStr(varStatementAmountEndPosition, var942StatementStr, "//", vbTextCompare) <> 0 Then
                            Dim varBankreferencestartposition As Integer = InStr(varStatementAmountEndPosition, var942StatementStr, "//", vbTextCompare)
                            ColAddInfo.Add("CustomerReference", Mid(var942StatementStr, varStatementAmountEndPosition + 4, varBankreferencestartposition - (varStatementAmountEndPosition + 4)))
                            ColAddInfo.Add("BankReference", Mid(var942StatementStr, varBankreferencestartposition + 2, 16))
                        Else
                            ColAddInfo.Add("CustomerReference", Right(var942StatementStr, Len(var942StatementStr) - varStatementAmountEndPosition - 3))
                        End If
                        varJoinSection = cSwift942Statement
                    ElseIf Left(varLine, 3) = cSwift942StatementInfo Then
                        AddValueToDictionary(ColAddInfo, "AdditionalInfo2", Right(varLine, Len(varLine) - 4))
                        varJoinSection = cSwift942StatementInfo
                    ElseIf varJoinSection = cSwift942Statement Then
                        AddValueToDictionary(ColAddInfo, "AdditionalInfo1", varLine)
                        varJoinSection = cSwift942Statement
                    ElseIf varJoinSection = cSwift942StatementInfo Then
                        AddValueToDictionary(ColAddInfo, "AdditionalInfo2", varLine)
                        varJoinSection = cSwift942StatementInfo
                    End If
                End If
                If Left(varLine, 3) = cSwift942ClosingBalances Then
                    If Left(varLine, 5) = ":90D:" Then
                        var942ClosingBalancesStr = Replace(varLine, ":90D:", "")
                        _varClosingAmount = FormatNumber(Right(var942ClosingBalancesStr, Len(var942ClosingBalancesStr) - (FindNextChar(var942ClosingBalancesStr) + 2)))
                    ElseIf Left(varLine, 5) = ":90C:" Then
                        var942ClosingBalancesStr = Replace(varLine, ":90C:", "")
                        _varSettledAmount = FormatNumber(Right(var942ClosingBalancesStr, Len(var942ClosingBalancesStr) - (FindNextChar(var942ClosingBalancesStr) + 2)))
                    End If
                End If
            Else
                If _varAccountNo <> "" Then
                    AddtoLstInfo(LstAccountInfo, ColAddInfo)
                    SaveSwiftRowToDB("Processed Swift Type", LstAccountInfo)
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process942, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub Process950(ByRef varLine As String, ByRef varJoinSection As String, ByRef LstAccountInfo As List(Of Dictionary(Of String, String)), ByRef ColAddInfo As Dictionary(Of String, String))
        Const cSwift950Reference As String = ":20:"
        Const cSwift950RelatedReference As String = ":21:"
        Const cSwift950AccNo As String = ":25:"
        Const cSwift950OpeningNo As String = ":60"
        Const cSwift950Statement As String = ":61"
        Const cSwift950StatementInfo As String = ":86"
        Const cSwift950ClosingBookedNo As String = ":62"
        Const cSwift950ClosingAvailableNo As String = ":64:"

        Dim varStatementAmountStartPosition As Integer
        Dim var950AccStr As String = ""
        Dim var950OpeningStr As String = ""
        Dim var950StatementStr As String = ""
        Dim var950ClosingBookedStr As String = ""
        Dim var950ClosingAvailableStr As String = ""
        Dim var950AccCCY As String = ""

        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 Then
                If Left(varLine, 1) = ":" Then
                    varJoinSection = ""
                End If
                If Left(varLine, 4) = cSwift950Reference Then
                    _varReference = Replace(varLine, cSwift950Reference, "")
                End If
                If Left(varLine, 4) = cSwift950RelatedReference Then
                    _varRelatedReference = Replace(varLine, cSwift950RelatedReference, "")
                End If
                If Left(varLine, 4) = cSwift950AccNo Then
                    _varAccountNo = Replace(varLine, cSwift950AccNo, "")
                    If Not IsNumeric(Right(var950AccStr, 3)) Then
                        var950AccCCY = Right(var950AccStr, 3)
                    End If
                End If
                If Left(varLine, 3) = cSwift950OpeningNo Then
                    var950OpeningStr = Right(varLine, Len(varLine) - 5)
                    _varOpeningAmountCCY = Mid(var950OpeningStr, 8, 3)
                    _varOpeningAmount = FormatNumber(Right(var950OpeningStr, Len(var950OpeningStr) - 10))
                    If UCase(Left(var950OpeningStr, 1)) = "D" Or UCase(Left(var950OpeningStr, 2)) = "RC" Then
                        _varOpeningAmount = -_varOpeningAmount
                    End If
                End If
                If Left(varLine, 3) = cSwift950Statement Or Left(varLine, 3) = cSwift950StatementInfo Or varJoinSection = cSwift950Statement Or varJoinSection = cSwift950StatementInfo Then
                    If Left(varLine, 3) = cSwift950Statement Then
                        AddtoLstInfo(LstAccountInfo, ColAddInfo)
                        var950StatementStr = Right(varLine, Len(varLine) - 4)

                        If IsDate("20" & Mid(var950StatementStr, 1, 2) & "-" & Mid(var950StatementStr, 3, 2) & "-" & Mid(var950StatementStr, 5, 2)) Then
                            ColAddInfo.Add("TradeDate", "20" & Mid(var950StatementStr, 1, 2) & "-" & Mid(var950StatementStr, 3, 2) & "-" & Mid(var950StatementStr, 5, 2))
                            ColAddInfo.Add("SettleDate", "20" & Mid(var950StatementStr, 1, 2) & "-" & Mid(var950StatementStr, 3, 2) & "-" & Mid(var950StatementStr, 5, 2))
                        End If

                        Dim varCreditDebitLetter As String = Mid(var950StatementStr, 11, 2)
                        If IsNumeric(Mid(var950StatementStr, 12, 1)) Then
                            varStatementAmountStartPosition = 11
                        Else
                            varStatementAmountStartPosition = 12
                        End If

                        Dim varStatementAmountEndPosition As Integer = varStatementAmountStartPosition + FindNextLetterPositionInString(Right(var950StatementStr, Len(var950StatementStr) - varStatementAmountStartPosition)) + 1
                        Dim varStatementAmountStr As String = Mid(var950StatementStr, varStatementAmountStartPosition + 1, varStatementAmountEndPosition - varStatementAmountStartPosition - 1)
                        ColAddInfo.Add("Type2", IIf(IsNumeric(Right(Left(varCreditDebitLetter, 2), 1)), Left(varCreditDebitLetter, 1), Left(varCreditDebitLetter, 2)))
                        If IsNumeric(FormatNumber(varStatementAmountStr)) Then
                            ColAddInfo.Add("Amount", IIf(Left(varCreditDebitLetter, 1) = "D" Or Left(varCreditDebitLetter, 2) = "RC", -FormatNumber(varStatementAmountStr), FormatNumber(varStatementAmountStr)))
                        End If
                        ColAddInfo.Add("CCY", IIf(var950AccCCY = "", _varOpeningAmountCCY, var950AccCCY))
                        ColAddInfo.Add("Type", Mid(var950StatementStr, varStatementAmountEndPosition, 4))
                        If InStr(varStatementAmountEndPosition, var950StatementStr, "//", vbTextCompare) <> 0 Then
                            Dim varBankreferencestartposition As Integer = InStr(varStatementAmountEndPosition, var950StatementStr, "//", vbTextCompare)
                            ColAddInfo.Add("CustomerReference", Mid(var950StatementStr, varStatementAmountEndPosition + 4, varBankreferencestartposition - (varStatementAmountEndPosition + 4)))
                            ColAddInfo.Add("BankReference", Mid(var950StatementStr, varBankreferencestartposition + 2, 16))
                        Else
                            ColAddInfo.Add("CustomerReference", Right(var950StatementStr, Len(var950StatementStr) - varStatementAmountEndPosition - 3))
                        End If
                        varJoinSection = cSwift950Statement
                    ElseIf Left(varLine, 3) = cSwift950StatementInfo Then
                        AddValueToDictionary(ColAddInfo, "AdditionalInfo2", Right(varLine, Len(varLine) - 4))
                        varJoinSection = cSwift950StatementInfo
                    ElseIf varJoinSection = cSwift950Statement Then
                        AddValueToDictionary(ColAddInfo, "AdditionalInfo1", varLine)
                        varJoinSection = cSwift950Statement
                    ElseIf varJoinSection = cSwift950StatementInfo Then
                        AddValueToDictionary(ColAddInfo, "AdditionalInfo2", varLine)
                        varJoinSection = cSwift950StatementInfo
                    End If
                End If
                If Left(varLine, 3) = cSwift950ClosingBookedNo Then
                    var950ClosingBookedStr = Right(varLine, Len(varLine) - 5)
                    _varAmountCCY = Mid(var950ClosingBookedStr, 8, 3)
                    _varAmount = FormatNumber(Right(var950ClosingBookedStr, Len(var950ClosingBookedStr) - 10))
                    _varSettleDate = IIf(IsDate(Mid(var950ClosingBookedStr, 6, 2) & "-" & Mid(var950ClosingBookedStr, 4, 2) & "-" & Mid(var950ClosingBookedStr, 2, 2)), Mid(var950ClosingBookedStr, 6, 2) & "-" & Mid(var950ClosingBookedStr, 4, 2) & "-" & Mid(var950ClosingBookedStr, 2, 2), Now)
                    If UCase(Left(var950ClosingBookedStr, 1)) = "D" Or UCase(Left(var950ClosingBookedStr, 2)) = "RC" Then
                        _varAmount = -_varAmount
                    End If
                End If
                If Left(varLine, 4) = cSwift950ClosingAvailableNo Then
                    var950ClosingAvailableStr = Right(varLine, Len(varLine) - 4)
                    _varClosingAmountCCY = Mid(var950ClosingAvailableStr, 8, 3)
                    _varClosingAmount = FormatNumber(Right(var950ClosingAvailableStr, Len(var950ClosingAvailableStr) - 10))
                    _varSettleDate = IIf(IsDate(Mid(var950ClosingAvailableStr, 6, 2) & "-" & Mid(var950ClosingAvailableStr, 4, 2) & "-" & Mid(var950ClosingAvailableStr, 2, 2)), Mid(var950ClosingAvailableStr, 6, 2) & "-" & Mid(var950ClosingAvailableStr, 4, 2) & "-" & Mid(var950ClosingAvailableStr, 2, 2), Now)
                    If UCase(Left(var950ClosingAvailableStr, 1)) = "D" Or UCase(Left(var950ClosingAvailableStr, 2)) = "RC" Then
                        _varClosingAmount = -_varClosingAmount
                    End If
                End If
            Else
                If _varAccountNo <> "" Then
                    AddtoLstInfo(LstAccountInfo, ColAddInfo)
                    SaveSwiftRowToDB("Processed Swift Type", LstAccountInfo)
                End If
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Process950, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub ProcessAll(ByRef varLine As String, ByRef TempDescription As String)
        Const cSwiftAllReference As String = ":20C::CORP//"
        Const cSwiftAllRelatedReference As String = ":20C::SEME//"
        Const cSwiftAllLinkReference As String = ":20C::PREV//"
        Const cSwiftAllFunctionType As String = ":23G:"

        Try
            If InStr(1, varLine, "-}", vbTextCompare) = 0 And InStr(1, varLine, "{5:", vbTextCompare) = 0 Then
                If InStr(1, varLine, cSwiftAllReference, vbTextCompare) <> 0 Then
                    _varReference = Replace(varLine, cSwiftAllReference, "")
                End If
                If InStr(1, varLine, cSwiftAllRelatedReference, vbTextCompare) <> 0 Then
                    _varRelatedReference = Replace(varLine, cSwiftAllRelatedReference, "")
                End If
                If InStr(1, varLine, cSwiftAllLinkReference, vbTextCompare) <> 0 Then
                    _varLinkReference = Replace(varLine, cSwiftAllLinkReference, "")
                End If
                If InStr(1, varLine, cSwiftAllFunctionType, vbTextCompare) <> 0 Then
                    _varFunctionType = Replace(varLine, cSwiftAllFunctionType, "")
                End If
                If TempDescription <> "" Then
                    _varDescription &= TempDescription
                End If
                _varDescription = _varDescription & varLine & vbCrLf
            Else
                _varDescription &= "-}"
                SaveSwiftRowToDB("Unprocessed Swift Type")
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot ProcessAll, err:" & Err.Description & ", varLine:" & varLine & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Private Sub AddValueToDictionary(ByRef vardictionary As Dictionary(Of String, String), ByRef varkey As String, ByRef varValue As String)
        Dim TmpVal As String

        If vardictionary.ContainsKey(varkey) Then
            TmpVal = vardictionary.Item(varkey)
            vardictionary.Remove(varkey)
            vardictionary.Add(varkey, TmpVal & " " & varValue)
        Else
            vardictionary.Add(varkey, varValue)
        End If

    End Sub

    Private Sub AddtoLstInfo(ByRef LstInfo As List(Of Dictionary(Of String, String)), ByRef ColInfo As Dictionary(Of String, String))
        If ColInfo IsNot Nothing Then
            If ColInfo.Count <> 0 Then
                LstInfo.Add(ColInfo)
            End If
        End If
        ColInfo = New Dictionary(Of String, String)
    End Sub

    Private Function FindNextLetterPositionInString(ByVal varString As String) As Integer
        FindNextLetterPositionInString = 1
        For i As Integer = 1 To Len(varString)
            If Not IsNumeric(Mid(varString, i, 1)) And Mid(varString, i, 1) <> "," Then
                FindNextLetterPositionInString = i - 1
                Exit For
            End If
        Next
    End Function

    Private Function IsAlpha(ByVal strInputText As String) As Boolean
        Return Text.RegularExpressions.Regex.IsMatch(strInputText, "^[a-zA-Z]")
    End Function

    Public Sub OpenSwiftFile(ByVal varSwiftFile As String)
        If IO.File.Exists(_varSwiftFilePath & varSwiftFile) = True Then
            Process.Start(_varSwiftFilePath & varSwiftFile)
        End If
    End Sub

    Public Sub SaveSwiftRowToDB(ByVal varstatus As String, Optional ByRef LstAccountInfo As List(Of Dictionary(Of String, String)) = Nothing, Optional ByRef LstCorpActionTermInfo As List(Of Dictionary(Of String, String)) = Nothing)
        If LstAccountInfo IsNot Nothing Then
            PopulateSwiftReadDetail(LstAccountInfo)
            LstAccountInfo = New List(Of Dictionary(Of String, String))
        End If
        If LstCorpActionTermInfo IsNot Nothing Then
            PopulateSwiftReadCorpActionTerm(LstCorpActionTermInfo)
            LstCorpActionTermInfo = New List(Of Dictionary(Of String, String))
        End If
        If varstatus <> "" Then
            PopulateSwiftRead(varstatus)
            If _varHasBlockEnded = True Then ' RA 2019-05-08 to deal with 535 breakdown so it doesn't reset details until it's a new ISIN
                ClearClassData()
            End If
        End If
    End Sub

    Public Sub PopulateSwiftRead(ByVal varstatus As String)
        Dim UpdateSQL As String = ""

        Try
            UpdateSQL = "Insert DolfinPaymentSwiftRead(SR_RunDate,SR_FileName,SR_FileNameDate,SR_FileDateTime,SR_SwiftType,SR_Status,SR_SwiftAcknowledged,SR_SwiftErrorCode,SR_SwiftCode,SR_FunctionType,SR_SwiftUserReference,SR_Reference," &
                            "SR_RelatedReference,SR_LinkReference,SR_AccountNo,SR_AccountCCY,SR_Indicator1,SR_Indicator2,SR_TradeDate,SR_SettleDate,SR_ExDate,SR_EffectiveDate,SR_RecDate,SR_AnnouDate,SR_Clearer,SR_ISIN," &
                            "SR_OpeningAmount,SR_OpeningAmountCCY,SR_OpeningAmountQualifier,SR_Amount,SR_AmountCCY,SR_AmountQualifier,SR_Price,SR_PriceCCY,SR_PriceQualifier,SR_SettledAmount,SR_SettledAmountCCY," &
                            "SR_SettledAmountQualifier,SR_ClosingAmount,SR_ClosingAmountCCY,SR_ClosingAmountQualifier,SR_FaceValue,SR_SettleValue,SR_FaceValueAggregate,SR_FaceValuePendingDelivered," &
                            "SR_FaceValuePendingReceived,SR_GrossAmount,SR_GrossAmountCCY,SR_GrossAmountQualifier,SR_NetAmount,SR_NetAmountCCY,SR_NetAmountQualifier,SR_Commission,SR_CommissionCCY," &
                            "SR_CommissionQualifier,SR_Accrued,SR_AccruedCCY,SR_AccruedQualifier,SR_Rate,SR_RateQualifier,SR_OrderIBAN,SR_OrderInstitution,SR_OrderBankIBAN,SR_OrderBankInstitution," &
                            "SR_IntermediaryIBAN,SR_Intermediary,SR_BeneficiaryIBAN,SR_BeneficiaryInstitution,SR_BeneficiaryBankIBAN,SR_BeneficiaryBankInstitution,SR_SafeKeeping,SR_Buyer,SR_Seller,SR_ReceivingAgent," &
                            "SR_DeliveringAgent,SR_PlaceOfSettlement,SR_Default,SR_Reason1,SR_Reason2,SR_Description,SR_EntryDate,SR_LastModifiedDate,SR_ModifiedBy)"

            UpdateSQL &= " Select getdate(), "
            UpdateSQL = UpdateSQL & IIf(_varSwiftFileName = "", "null", "'" & _varSwiftFileName & "'") & ","
            UpdateSQL = UpdateSQL & "'" & _varSwiftFileNameDate.ToString("yyyy-MM-dd") & "',"
            UpdateSQL = UpdateSQL & "'" & _varSwiftFileDateTime.ToString("yyyy-MM-dd HH:mm:ss") & "',"
            UpdateSQL = UpdateSQL & IIf(_varSwiftType = "", "null", "'" & _varSwiftType & "'") & ","
            UpdateSQL = UpdateSQL & IIf(varstatus = "", "null", "'" & varstatus & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varSwiftAcknowledged, 1, 0) & ","
            UpdateSQL = UpdateSQL & IIf(_varSwiftErrorCode = "", "null", "'" & _varSwiftErrorCode & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varSwiftCode = "", "null", "'" & _varSwiftCode & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varFunctionType = "", "null", "'" & _varFunctionType & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varSwiftUserReference = "", "null", "'" & _varSwiftUserReference & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varReference = "", "null", "'" & _varReference & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varRelatedReference = "", "null", "'" & _varRelatedReference & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varLinkReference = "", "null", "'" & _varLinkReference & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varAccountNo = "", "null", "'" & _varAccountNo & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varAccountCCY = "", "null", "'" & _varAccountCCY & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varIndicator1 = "", "null", "'" & _varIndicator1 & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varIndicator2 = "", "null", "'" & _varIndicator2 & "'") & ","
            UpdateSQL = UpdateSQL & "'" & _varTradeDate.ToString("yyyy-MM-dd") & "'" & ","
            UpdateSQL = UpdateSQL & "'" & _varSettleDate.ToString("yyyy-MM-dd") & "'" & ","
            UpdateSQL = UpdateSQL & IIf(_varExDate = _cDefaultDate, "null", "'" & _varExDate.ToString("yyyy-MM-dd") & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varEffectiveDate = _cDefaultDate, "null", "'" & _varEffectiveDate.ToString("yyyy-MM-dd") & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varRecDate = _cDefaultDate, "null", "'" & _varRecDate.ToString("yyyy-MM-dd") & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varAnnouDate = _cDefaultDate, "null", "'" & _varAnnouDate.ToString("yyyy-MM-dd") & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varClearer = "", "null", "'" & _varClearer & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varISIN = "", "null", "'" & _varISIN & "'") & ","
            UpdateSQL = UpdateSQL & _varOpeningAmount & ","
            UpdateSQL = UpdateSQL & IIf(_varOpeningAmountCCY = "", "null", "'" & _varOpeningAmountCCY & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varOpeningAmountQualifier = "", "null", "'" & _varOpeningAmountQualifier & "'") & ","
            UpdateSQL = UpdateSQL & _varAmount & ","
            UpdateSQL = UpdateSQL & IIf(_varAmountCCY = "", "null", "'" & _varAmountCCY & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varAmountQualifier = "", "null", "'" & _varAmountQualifier & "'") & ","
            UpdateSQL = UpdateSQL & _varPrice & ","
            UpdateSQL = UpdateSQL & IIf(_varPriceCCY = "", "null", "'" & _varPriceCCY & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varPriceQualifier = "", "null", "'" & _varPriceQualifier & "'") & ","
            UpdateSQL = UpdateSQL & _varSettledAmount & ","
            UpdateSQL = UpdateSQL & IIf(_varSettledAmountCCY = "", "null", "'" & _varSettledAmountCCY & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varSettledAmountQualifier = "", "null", "'" & _varSettledAmountQualifier & "'") & ","
            UpdateSQL = UpdateSQL & _varClosingAmount & ","
            UpdateSQL = UpdateSQL & IIf(_varClosingAmountCCY = "", "null", "'" & _varClosingAmountCCY & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varClosingAmountQualifier = "", "null", "'" & _varClosingAmountQualifier & "'") & ","
            UpdateSQL = UpdateSQL & _varFaceValue & ","
            UpdateSQL = UpdateSQL & _varSettleValue & ","
            UpdateSQL = UpdateSQL & _varFaceValueAggregate & ","
            UpdateSQL = UpdateSQL & _varFaceValuePendingDelivered & ","
            UpdateSQL = UpdateSQL & _varFaceValuePendingReceived & ","
            UpdateSQL = UpdateSQL & _varGrossAmount & ","
            UpdateSQL = UpdateSQL & IIf(_varGrossAmountCCY = "", "null", "'" & _varGrossAmountCCY & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varGrossAmountQualifier = "", "null", "'" & _varGrossAmountQualifier & "'") & ","
            UpdateSQL = UpdateSQL & _varNetAmount & ","
            UpdateSQL = UpdateSQL & IIf(_varNetAmountCCY = "", "null", "'" & _varNetAmountCCY & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varNetAmountQualifier = "", "null", "'" & _varNetAmountQualifier & "'") & ","
            UpdateSQL = UpdateSQL & _varCommission & ","
            UpdateSQL = UpdateSQL & IIf(_varCommissionCCY = "", "null", "'" & _varCommissionCCY & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varCommissionQualifier = "", "null", "'" & _varCommissionQualifier & "'") & ","
            UpdateSQL = UpdateSQL & _varAccrued & ","
            UpdateSQL = UpdateSQL & IIf(_varAccruedCCY = "", "null", "'" & _varAccruedCCY & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varAccruedQualifier = "", "null", "'" & _varAccruedQualifier & "'") & ","
            UpdateSQL = UpdateSQL & _varRate & ","
            UpdateSQL = UpdateSQL & IIf(_varRateQualifier = "", "null", "'" & _varRateQualifier & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varOrderIBAN = "", "null", "'" & _varOrderIBAN & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varOrderInstitution = "", "null", "'" & Replace(_varOrderInstitution, "'", "") & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varOrderBankIBAN = "", "null", "'" & _varOrderBankIBAN & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varOrderBankInstitution = "", "null", "'" & Replace(_varOrderBankInstitution, ",", "") & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varIntermediaryIBAN = "", "null", "'" & _varIntermediaryIBAN & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varIntermediary = "", "null", "'" & _varIntermediary & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varBeneficiaryIBAN = "", "null", "'" & _varBeneficiaryIBAN & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varBeneficiaryInstitution = "", "null", "'" & Replace(_varBeneficiaryInstitution, "'", "") & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varBeneficiaryBankIBAN = "", "null", "'" & _varBeneficiaryBankIBAN & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varBeneficiaryBankInstitution = "", "null", "'" & Replace(_varBeneficiaryBankInstitution, ",", "") & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varSafeKeeping = "", "null", "'" & _varSafeKeeping & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varBuyer = "", "null", "'" & _varBuyer & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varSeller = "", "null", "'" & _varSeller & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varReceivingAgent = "", "null", "'" & _varReceivingAgent & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varDeliveringAgent = "", "null", "'" & _varDeliveringAgent & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varPlaceOfSettlement = "", "null", "'" & _varPlaceOfSettlement & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varDefaultValue = "", "null", "'" & _varDefaultValue & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varReason1 = "", "null", "'" & Replace(_varReason1, "'", "") & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varReason2 = "", "null", "'" & Replace(_varReason2, "'", "") & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varDescription = "", "null", "'" & Replace(_varDescription, "'", "") & "'") & ","
            UpdateSQL = UpdateSQL & "getdate(),getdate(),'" & Environment.MachineName & "\" & Environment.UserName & "'"

            ExecuteString(_conn, UpdateSQL)
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot SaveSwiftRowToDB - PopulateSwiftRead, err:" & Err.Description & "sql: " & UpdateSQL & ", file:" & _varSwiftFileName)
        End Try
    End Sub


    Public Sub PopulateSwiftReadDetail(ByRef LstAccountInfo As List(Of Dictionary(Of String, String)))
        Dim UpdateSQL As String = ""

        Try
            If LstAccountInfo.Count > 0 Then
                For i As Integer = 0 To LstAccountInfo.Count - 1
                    UpdateSQL = "Insert DolfinPaymentSwiftReadDetail 
                    ([SRA_RunDate],[SRA_FileName],[SRA_FileNameDate],[SRA_SwiftType],[SRA_SwiftCode],[SRA_Reference],[SRA_CustomerReference],
                    [SRA_BankReference],[SRA_AccountNo],[SRA_TradeDate],[SRA_SettleDate],[SRA_Amount],[SRA_CCY],[SRA_Type],[SRA_Type2],[SRA_AdditionalInfo1],[SRA_AdditionalInfo2],[SRA_OrderRef])
                     Select getdate(),"

                    UpdateSQL = UpdateSQL & IIf(_varSwiftFileName = "", "null", "'" & _varSwiftFileName & "'") & ","
                    UpdateSQL = UpdateSQL & "'" & _varSwiftFileNameDate.ToString("yyyy-MM-dd") & "',"
                    UpdateSQL = UpdateSQL & IIf(_varSwiftType = "", "null", "'" & _varSwiftType & "'") & ","
                    UpdateSQL = UpdateSQL & IIf(_varSwiftCode = "", "null", "'" & _varSwiftCode & "'") & ","
                    UpdateSQL = UpdateSQL & IIf(_varReference = "", "null", "'" & _varReference & "'") & ","

                    If LstAccountInfo(i).ContainsKey("CustomerReference") Then
                        UpdateSQL = UpdateSQL & "'" & LstAccountInfo(i).Item("CustomerReference") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstAccountInfo(i).ContainsKey("BankReference") Then
                        UpdateSQL = UpdateSQL & "'" & LstAccountInfo(i).Item("BankReference") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    UpdateSQL = UpdateSQL & IIf(_varAccountNo = "", "null", "'" & _varAccountNo & "'") & ","

                    If LstAccountInfo(i).ContainsKey("TradeDate") Then
                        If IsDate(LstAccountInfo(i).Item("TradeDate")) Then
                            UpdateSQL = UpdateSQL & "'" & LstAccountInfo(i).Item("TradeDate") & "'" & ","
                        Else
                            UpdateSQL &= "null,"
                        End If
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstAccountInfo(i).ContainsKey("SettleDate") Then
                        If IsDate(LstAccountInfo(i).Item("SettleDate")) Then
                            UpdateSQL = UpdateSQL & "'" & LstAccountInfo(i).Item("SettleDate") & "'" & ","
                        Else
                            UpdateSQL &= "null,"
                        End If
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstAccountInfo(i).ContainsKey("Amount") Then
                        UpdateSQL = UpdateSQL & LstAccountInfo(i).Item("Amount") & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstAccountInfo(i).ContainsKey("CCY") Then
                        UpdateSQL = UpdateSQL & "'" & LstAccountInfo(i).Item("CCY") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstAccountInfo(i).ContainsKey("Type") Then
                        UpdateSQL = UpdateSQL & "'" & LstAccountInfo(i).Item("Type") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstAccountInfo(i).ContainsKey("Type2") Then
                        UpdateSQL = UpdateSQL & "'" & LstAccountInfo(i).Item("Type2") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstAccountInfo(i).ContainsKey("AdditionalInfo1") Then
                        UpdateSQL = UpdateSQL & "'" & Replace(LstAccountInfo(i).Item("AdditionalInfo1"), "'", "") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstAccountInfo(i).ContainsKey("AdditionalInfo2") Then
                        UpdateSQL = UpdateSQL & "'" & Replace(LstAccountInfo(i).Item("AdditionalInfo2"), "'", "") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    UpdateSQL &= "'',"
                    ExecuteString(_conn, Left(UpdateSQL, Len(UpdateSQL) - 1))
                Next
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot SaveSwiftRowToDB - PopulateSwiftReadDetail, err:" & Err.Description & "sql: " & UpdateSQL & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub PopulateSwiftReadCorpActionTerm(ByRef LstCorpActionTermInfo As List(Of Dictionary(Of String, String)))
        Dim UpdateSQL As String = ""

        Try
            If LstCorpActionTermInfo.Count > 0 Then
                For i As Integer = 0 To LstCorpActionTermInfo.Count - 1
                    UpdateSQL = "Insert DolfinPaymentSwiftReadCorpActionTerms(SRCA_RunDate,SRCA_FileName,SRCA_FileNameDate,SRCA_FileDateTime,SRCA_SwiftType,SRCA_SwiftCode,SRCA_Reference,SRCA_RelatedReference,SRCA_ISIN,SRCA_Term,SRCA_TermNumber," &
                                    "SRCA_TermOption, SRCA_TermOptionCCY, SRCA_Default, SRCA_WTH, SRCA_Indicator1,SRCA_Indicator2,SRCA_MinimumQuantity, SRCA_MinimumQuantityQualifier, SRCA_MinimumMultipleQuantity, " &
                                    "SRCA_MinimumMultipleQuantityQualifier, SRCA_MinimumNominalQuantity, SRCA_MinimumNominalQuantityQualifier, SRCA_ContractSize, SRCA_ContractSizeQualifier, SRCA_EntitledQuantity, SRCA_EntitledQualifier, " &
                                    "SRCA_StockLendingDeadlineDate, SRCA_ResponseDeadlineDate, SRCA_ExpiryDate, SRCA_ValueDate, SRCA_PayDate, SRCA_PostDate, SRCA_MarketDate, SRCA_GrossAmount, SRCA_GrossAmountCCY, " &
                                    "SRCA_NetAmount,SRCA_NetAmountCCY,SRCA_PostAmount,SRCA_PostAmountCCY,SRCA_EntitledAmount,SRCA_EntitledAmountCCY,SRCA_OfferAmount,SRCA_OfferAmountCCY,SRCA_TaxAmount,SRCA_TaxAmountCCY,SRCA_Rate,SRCA_RateQualifier,SRCA_Account,SRCA_AccountQualifier,SRCA_Notes) Select getdate(),"

                    UpdateSQL = UpdateSQL & IIf(_varSwiftFileName = "", "null", "'" & _varSwiftFileName & "'") & ","
                    UpdateSQL = UpdateSQL & "'" & _varSwiftFileNameDate.ToString("yyyy-MM-dd") & "',"
                    UpdateSQL = UpdateSQL & "'" & _varSwiftFileDateTime.ToString("yyyy-MM-dd HH:mm:ss") & "',"
                    UpdateSQL = UpdateSQL & IIf(_varSwiftType = "", "null", "'" & _varSwiftType & "'") & ","
                    UpdateSQL = UpdateSQL & IIf(_varSwiftCode = "", "null", "'" & _varSwiftCode & "'") & ","
                    UpdateSQL = UpdateSQL & IIf(_varReference = "", "null", "'" & _varReference & "'") & ","
                    UpdateSQL = UpdateSQL & IIf(_varRelatedReference = "", "null", "'" & _varRelatedReference & "'") & ","

                    If LstCorpActionTermInfo(i).ContainsKey("ISIN") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("ISIN") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("Term") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("Term") & "'" & ","
                        If IsNumeric(LstCorpActionTermInfo(i).Item("Term")) Then
                            UpdateSQL = UpdateSQL & CDbl(LstCorpActionTermInfo(i).Item("Term")) & ","
                        Else
                            UpdateSQL &= "null,"
                        End If
                    Else
                        UpdateSQL &= "null,null"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("TermOption") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("TermOption") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("TermOptionCCY") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("TermOptionCCY") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("Default") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("Default") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("WTH") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("WTH") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("Indicator1") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("Indicator1") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("Indicator2") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("Indicator2") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("MinimumQuantity") Then
                        UpdateSQL = UpdateSQL & LstCorpActionTermInfo(i).Item("MinimumQuantity") & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("MinimumQuantityQualifier") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("MinimumQuantityQualifier") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("MinimumMultipleQuantity") Then
                        UpdateSQL = UpdateSQL & LstCorpActionTermInfo(i).Item("MinimumMultipleQuantity") & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("MinimumMultipleQualifier") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("MinimumMultipleQualifier") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("MinimumNominalQuantity") Then
                        UpdateSQL = UpdateSQL & LstCorpActionTermInfo(i).Item("MinimumNominalQuantity") & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("MinimumNominalQualifier") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("MinimumNominalQualifier") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("ContractSize") Then
                        UpdateSQL = UpdateSQL & LstCorpActionTermInfo(i).Item("ContractSize") & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("ContractSizeQualifier") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("ContractSizeQualifier") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("EntitledQuantity") Then
                        UpdateSQL = UpdateSQL & LstCorpActionTermInfo(i).Item("EntitledQuantity") & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("EntitledQualifier") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("EntitledQualifier") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("StockLendingDeadlineDate") Then
                        If IsDate(LstCorpActionTermInfo(i).Item("StockLendingDeadlineDate")) Then
                            UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("StockLendingDeadlineDate") & "'" & ","
                        Else
                            UpdateSQL &= "null,"
                        End If
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("ResponseDeadlineDate") Then
                        If IsDate(LstCorpActionTermInfo(i).Item("ResponseDeadlineDate")) Then
                            UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("ResponseDeadlineDate") & "'" & ","
                        Else
                            UpdateSQL &= "null,"
                        End If
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("ExpiryDate") Then
                        If IsDate(LstCorpActionTermInfo(i).Item("ExpiryDate")) Then
                            UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("ExpiryDate") & "'" & ","
                        Else
                            UpdateSQL &= "null,"
                        End If
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("ValueDate") Then
                        If IsDate(LstCorpActionTermInfo(i).Item("ValueDate")) Then
                            UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("ValueDate") & "'" & ","
                        Else
                            UpdateSQL &= "null,"
                        End If
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("PayDate") Then
                        If IsDate(LstCorpActionTermInfo(i).Item("PayDate")) Then
                            UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("PayDate") & "'" & ","
                        Else
                            UpdateSQL &= "null,"
                        End If
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("PostDate") Then
                        If IsDate(LstCorpActionTermInfo(i).Item("PostDate")) Then
                            UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("PostDate") & "'" & ","
                        Else
                            UpdateSQL &= "null,"
                        End If
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("MarketDate") Then
                        If IsDate(LstCorpActionTermInfo(i).Item("MarketDate")) Then
                            UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("MarketDate") & "'" & ","
                        Else
                            UpdateSQL &= "null,"
                        End If
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("Gross") Then
                        UpdateSQL = UpdateSQL & LstCorpActionTermInfo(i).Item("Gross") & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("GrossCCY") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("GrossCCY") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("Net") Then
                        UpdateSQL = UpdateSQL & LstCorpActionTermInfo(i).Item("Net") & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("NetCCY") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("NetCCY") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("Post") Then
                        UpdateSQL = UpdateSQL & LstCorpActionTermInfo(i).Item("Post") & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("PostCCY") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("PostCCY") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("Entitled") Then
                        UpdateSQL = UpdateSQL & LstCorpActionTermInfo(i).Item("Entitled") & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("EntitledCCY") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("EntitledCCY") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("Offer") Then
                        UpdateSQL = UpdateSQL & LstCorpActionTermInfo(i).Item("Offer") & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("OfferCCY") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("OfferCCY") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("Tax") Then
                        UpdateSQL = UpdateSQL & LstCorpActionTermInfo(i).Item("Tax") & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("TaxCCY") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("TaxCCY") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("Rate") Then
                        UpdateSQL = UpdateSQL & LstCorpActionTermInfo(i).Item("Rate") & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("RateQualifier") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("RateQualifier") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("Account") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("Account") & "',"
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("AccountQualifier") Then
                        UpdateSQL = UpdateSQL & "'" & LstCorpActionTermInfo(i).Item("AccountQualifier") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    If LstCorpActionTermInfo(i).ContainsKey("Notes") Then
                        UpdateSQL = UpdateSQL & "'" & Replace(LstCorpActionTermInfo(i).Item("Notes"), "'", "") & "'" & ","
                    Else
                        UpdateSQL &= "null,"
                    End If

                    ExecuteString(_conn, Left(UpdateSQL, Len(UpdateSQL) - 1))
                Next
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot SaveSwiftRowToDB - PopulateSwiftReadCorpActionTerm, err:" & Err.Description & "sql: " & UpdateSQL & ", file:" & _varSwiftFileName)
        End Try
    End Sub


    Public Sub SaveUnprocessedSwiftRowToDB()
        Dim UpdateSQL As String = ""
        Try
            UpdateSQL = "Insert DolfinPaymentSwiftRead(SR_RunDate,SR_FileName,SR_FileNameDate,SR_FileDateTime,SR_SwiftType,SR_Status,SR_SwiftAcknowledged,SR_SwiftErrorCode,SR_SwiftCode,SR_FunctionType,SR_SwiftUserReference,SR_Reference," &
                            "SR_RelatedReference,SR_LinkReference,SR_AccountNo,SR_AccountCCY,SR_Indicator1,SR_Indicator2,SR_TradeDate,SR_SettleDate,SR_ExDate,SR_EffectiveDate,SR_RecDate,SR_AnnouDate,SR_Clearer,SR_ISIN," &
                            "SR_OpeningAmount,SR_OpeningAmountCCY,SR_OpeningAmountQualifier,SR_Amount,SR_AmountCCY,SR_AmountQualifier,SR_Price,SR_PriceCCY,SR_PriceQualifier,SR_SettledAmount,SR_SettledAmountCCY," &
                            "SR_SettledAmountQualifier,SR_ClosingAmount,SR_ClosingAmountCCY,SR_ClosingAmountQualifier,SR_FaceValue,SR_SettleValue,SR_FaceValueAggregate,SR_FaceValuePendingDelivered," &
                            "SR_FaceValuePendingReceived,SR_GrossAmount,SR_GrossAmountCCY,SR_GrossAmountQualifier,SR_NetAmount,SR_NetAmountCCY,SR_NetAmountQualifier,SR_Commission,SR_CommissionCCY," &
                            "SR_CommissionQualifier,SR_Accrued,SR_AccruedCCY,SR_AccruedQualifier,SR_Rate,SR_RateQualifier,SR_OrderIBAN,SR_OrderInstitution,SR_OrderBankIBAN,SR_OrderBankInstitution," &
                            "SR_IntermediaryIBAN,SR_Intermediary,SR_BeneficiaryIBAN,SR_BeneficiaryInstitution,SR_BeneficiaryBankIBAN,SR_BeneficiaryBankInstitution,SR_SafeKeeping,SR_Buyer,SR_Seller,SR_ReceivingAgent," &
                            "SR_DeliveringAgent,SR_PlaceOfSettlement,SR_Default,SR_Reason1,SR_Reason2,SR_Description,SR_EntryDate,SR_LastModifiedDate,SR_ModifiedBy)"

            UpdateSQL &= " Select getdate(),"
            UpdateSQL = UpdateSQL & IIf(_varSwiftFileName = "", "null", "'" & _varSwiftFileName & "'") & ","
            UpdateSQL = UpdateSQL & "'" & _varSwiftFileNameDate.ToString("yyyy-MM-dd") & "',"
            UpdateSQL = UpdateSQL & "'" & _varSwiftFileDateTime.ToString("yyyy-MM-dd HH:mm:ss") & "',"
            UpdateSQL = UpdateSQL & IIf(_varSwiftType = "", "null", "'" & _varSwiftType & "'") & ","
            UpdateSQL = UpdateSQL & "'No data found in Swift'" & ","
            UpdateSQL = UpdateSQL & IIf(_varSwiftAcknowledged, 1, 0) & ","
            UpdateSQL = UpdateSQL & IIf(_varSwiftErrorCode = "", "null", "'" & _varSwiftErrorCode & "'") & ","
            UpdateSQL = UpdateSQL & IIf(_varSwiftCode = "", "null", "'" & _varSwiftCode & "'") & ","
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL &= "null,"
            UpdateSQL = UpdateSQL & "getdate(),getdate(),'" & Environment.MachineName & "'"

            ExecuteString(_conn, UpdateSQL)
            ClearClassData()
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot SaveUnprocessedSwiftRowToDB, err:" & Err.Description & "sql: " & UpdateSQL & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub SaveSwiftErrorRowToDB(ByVal varSwiftErrorFileName As String, varSwiftErrorFileNameDate As Date, ByVal varErrorSwiftType As String, ByVal varErrorReference As String, ByVal varError As String, ByVal varMessage As String)
        Dim UpdateSQL As String = ""

        Try
            UpdateSQL = "Insert DolfinPaymentSwiftReadErrors Select getdate(),"
            UpdateSQL = UpdateSQL & IIf(varSwiftErrorFileName = "", "null", "'" & varSwiftErrorFileName & "'") & ","
            UpdateSQL = UpdateSQL & "'" & varSwiftErrorFileNameDate.ToString("yyyy-MM-dd") & "',"
            UpdateSQL = UpdateSQL & "'" & varSwiftErrorFileNameDate.ToString("yyyy-MM-dd HH:mm:ss") & "',"
            UpdateSQL = UpdateSQL & "'" & varErrorSwiftType & "',"
            UpdateSQL = UpdateSQL & "'" & varErrorReference & "',"
            UpdateSQL = UpdateSQL & "'" & varError & "',"
            UpdateSQL = UpdateSQL & "'" & varMessage & "',"
            UpdateSQL &= "getdate()"

            ExecuteString(_conn, UpdateSQL)
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot SaveSwiftErrorRowToDB, err:" & Err.Description & "sql: " & UpdateSQL & ", file:" & _varSwiftFileName)
        End Try
    End Sub

    Public Sub CheckBFLFiles()
        clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameReadSwifts, "Starting CheckBFLFiles - ReadSwift")
        If Now.Date.DayOfWeek <> DayOfWeek.Sunday And Now.Date.DayOfWeek <> DayOfWeek.Monday And Now.Hour >= 4 And Now.Hour <= 9 Then
            clsEmail.BFLSave()
        Else
            clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "No files checked on Sunday/Monday and only checked between 4am and 9am - CheckBFLFiles - ReadSwift")
        End If
    End Sub

    Public Sub ReadGGPFiles()
        Dim varSwiftFile As String = ""

        ReadGPPFile(_varGPPFilePath & "Archive\Processed\", _varGPPFilePath & "Sync\Archive\Processed\")
        ReadGPPFile(_varGPPFilePath & "Archive\Failed\", _varGPPFilePath & "Sync\Archive\Failed\")
        ReadGPPFile(_varGPPFilePath & "Reports\", _varGPPFilePath & "Sync\Reports\")
    End Sub

    Public Sub ReadGPPFile(ByVal FilePath As String, ByVal FilePathBak As String)
        Dim varLine As String = ""

        clsIMS.AddAudit(1, 17, "Starting ReadGPPFile: " & FilePath)
        Try
            If IO.Directory.Exists(FilePath) Then
                Dim fileDir As New System.IO.DirectoryInfo(FilePath)
                Dim swiftfiles = fileDir.GetFiles.ToList
                Dim varDocNo As String = ""
                Dim LstHeaders As New List(Of String)
                Dim LstDataRow As New List(Of String)
                Dim varSettled As Boolean = False
                Dim varStatus As Integer = 0

                swiftfiles = Nothing

                Dim files As Generic.IEnumerable(Of Object)
                files = From f In fileDir.EnumerateFiles() Where f.LastWriteTime.ToString("yyyyMMdd") = Now.Date.ToString("yyyyMMdd")
                Dim ProcessedFile As IO.FileInfo

                For Each ProcessedFile In files
                    If Not IO.File.Exists(FilePathBak & Dir(ProcessedFile.FullName)) Then
                        If InStr(1, ProcessedFile.FullName, "_AccountSummaryFO.csv", vbTextCompare) = 0 And InStr(1, ProcessedFile.FullName, "_TradeActivity.csv", vbTextCompare) = 0 Then
                            Dim objSwiftReader As IO.StreamReader = GetGPPFile(ProcessedFile.FullName)

                            'get docno from file, type of file being processed determines whether settled or not
                            If InStr(1, FilePath, "Archive\Failed\", vbTextCompare) <> 0 Or (InStr(1, FilePath, "Reports\", vbTextCompare) <> 0) And LCase(Left(Dir(ProcessedFile.FullName), 3)) = "bnd" Then
                                varStatus = 0
                            ElseIf InStr(1, FilePath, "Archive\Processed\", vbTextCompare) <> 0 Then
                                varStatus = 1
                            ElseIf InStr(1, ProcessedFile.FullName, "_SettledTrades.csv", vbTextCompare) <> 0 Then
                                varStatus = 2
                            Else
                                varStatus = 3
                            End If

                            Dim vLineNum As Integer = 0
                            Do While objSwiftReader.Peek() >= 0
                                varLine = objSwiftReader.ReadLine()

                                If varLine <> "" Then
                                    If vLineNum = 0 Then
                                        LstHeaders = GetDataFromGPPFile(varLine)
                                    Else
                                        LstDataRow = GetDataFromGPPFile(varLine)

                                        Dim varBody As String = ""
                                        Dim varSubject As String = ""
                                        Dim varAccMgtEmailAddress As String = ""

                                        If varStatus = 0 Then
                                            varDocNo = GetFieldData(LstHeaders, LstDataRow, """ClientRef""")
                                            If varDocNo <> "" Then
                                                varAccMgtEmailAddress = clsIMS.GetSQLItem("Select top 1 U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username in ('operationsfiles')")
                                                varBody = "Please see failed DocNo: " & varDocNo
                                                varSubject = varDocNo & " failed to send to GPP"
                                                clsEmail.SendGPPTransactionEmail(varDocNo, ProcessedFile.FullName, varAccMgtEmailAddress, varSubject, varBody)
                                                clsIMS.AddAudit(3, cAuditGroupNameGPP, "Could not Process " & varDocNo & " at GPP, " & ProcessedFile.FullName & " - ReadGPPFile - SwiftRead")
                                            Else
                                                clsIMS.AddAudit(4, cAuditGroupNameGPP, "No DocNo found in file " & ProcessedFile.FullName & " - ReadGPPFile - SwiftRead")
                                            End If
                                        ElseIf varStatus = 1 Then
                                            varDocNo = GetFieldData(LstHeaders, LstDataRow, """ClientRef""")
                                            If varDocNo <> "" Then
                                                varAccMgtEmailAddress = clsIMS.GetSQLItem("Select top 1 U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username in ('operationsfiles')")
                                                varBody = "Successfully processed " & varDocNo
                                                varSubject = varDocNo & " Sent to GPP"
                                                clsEmail.SendGPPTransactionEmail(varDocNo, ProcessedFile.FullName, varAccMgtEmailAddress, varSubject, varBody)
                                                clsIMS.AddAudit(1, cAuditGroupNameGPP, "Processed " & varDocNo & " at GPP, " & ProcessedFile.FullName & " - ReadGPPFile - SwiftRead")
                                            Else
                                                clsIMS.AddAudit(4, cAuditGroupNameGPP, "No DocNo found in file " & ProcessedFile.FullName & " - ReadGPPFile - SwiftRead")
                                            End If
                                        Else
                                            varDocNo = GetFieldData(LstHeaders, LstDataRow, "ClientTransactionRef")
                                            Dim varSettledStatusDesc As String = ""

                                            If varStatus = 2 Then
                                                varSettledStatusDesc = "Settled"
                                            Else
                                                varSettledStatusDesc = GetFieldData(LstHeaders, LstDataRow, "TradeStatus")
                                            End If

                                            If varDocNo <> "" And varSettledStatusDesc <> "" Then
                                                ProcessGPPLine(GetvarSettledStatus(varSettledStatusDesc), varDocNo)
                                                clsIMS.AddAudit(1, cAuditGroupNameGPP, "Processed " & varDocNo & " at GPP, " & ProcessedFile.FullName & " - ReadGPPFile - SwiftRead")
                                            ElseIf varDocNo = "" Then
                                                clsIMS.AddAudit(4, cAuditGroupNameGPP, "No DocNo/Trade Status found in file " & ProcessedFile.FullName & " - ReadGPPFile - SwiftRead")
                                            ElseIf varSettledStatusDesc = "" Then
                                                clsIMS.AddAudit(4, cAuditGroupNameGPP, "No Trade Status found in file " & ProcessedFile.FullName & " for " & varDocNo & " - ReadGPPFile - SwiftRead")
                                            End If
                                        End If
                                    End If
                                End If
                                vLineNum += 1
                            Loop
                            objSwiftReader.Close()
                            objSwiftReader = Nothing
                        End If
                        FileAction(1, ProcessedFile.FullName, FilePathBak & Dir(ProcessedFile.FullName))
                    End If
                Next
            End If
        Catch ex As Exception
            clsIMS.AddAudit(3, cAuditGroupNameGPP, Err.Description & " - error in ReadGPPFile - SwiftRead")
        End Try
    End Sub

    Public Function GetvarSettledStatus(ByVal varSettledStatusDesc As String) As Integer
        GetvarSettledStatus = 0
        If varSettledStatusDesc = "Instructed" Then
            GetvarSettledStatus = 0
        ElseIf varSettledStatusDesc = "Pre-Matched" Then
            GetvarSettledStatus = 1
        ElseIf varSettledStatusDesc = "Matched" Then
            GetvarSettledStatus = 2
        ElseIf varSettledStatusDesc = "Cancelled" Then
            GetvarSettledStatus = 3
        ElseIf varSettledStatusDesc = "Settled" Then
            GetvarSettledStatus = 4
        End If
    End Function

    Public Sub ProcessGPPLine(ByVal varSettledStatus As Integer, ByVal varDocNo As String)
        Dim varSwiftFile As String = ""
        Dim varSwiftFileName As String = ""

        clsIMS.GetSwiftFileIMS(varSettledStatus, varDocNo, varSwiftFile)
        varSwiftFileName = "SwiftRead_" & Now.ToString("_yyyyMMddHHmmssfff") & ".fin"
        clsSE.CreateFile(_varSwiftFileEditImportPath, varSwiftFileName, varSwiftFile)
        clsIMS.AddAudit(1, cAuditGroupNameGPP, "Status " & varSettledStatus & " for DocNo: " & varDocNo & " - sent swift file (ProcessGPPLine)", 0)
    End Sub

    Public Function GetDataFromGPPFile(ByRef varLine As String) As List(Of String)
        Dim ArrayLine() As String = varLine.Split(",")
        GetDataFromGPPFile = ArrayLine.ToList()
    End Function

    Public Function GetFieldData(LstHeaders As List(Of String), LstDataRow As List(Of String), ByVal varFieldName As String) As String
        GetFieldData = ""
        If LstHeaders.IndexOf(varFieldName) > 0 Then
            GetFieldData = LstDataRow(LstHeaders.IndexOf(varFieldName))
        ElseIf LstHeaders.IndexOf(Replace(varFieldName, """", "")) > 0 Then
            GetFieldData = LstDataRow(LstHeaders.IndexOf(Replace(varFieldName, """", "")))
        End If
    End Function

    Private Function GetGPPFile(ByRef GPPFile As String) As IO.StreamReader
        GetGPPFile = Nothing
        Try
            Dim GPPFileStream As New IO.FileStream(GPPFile, IO.FileMode.Open, IO.FileAccess.Read)
            GetGPPFile = New IO.StreamReader(GPPFileStream)
        Catch ex As Exception
            GetGPPFile = Nothing
            clsIMS.AddAudit(3, cAuditGroupNameGPP, Err.Description & " - no return for GetGPPFile - SwiftRead")
        End Try
    End Function

    Public Function GetNewOpenConnection() As SqlConnection
        Dim newconn = New SqlConnection
        Try
            newconn.ConnectionString = _varConnection

            If newconn.State <> ConnectionState.Open Then
                newconn.Open()
            End If
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            GetNewOpenConnection = newconn
        End Try
    End Function

    Public Sub ExecuteString(ByVal execonn As SqlConnection, ByVal varSQL As String)

        Try
            Using Cmd As New SqlCommand(varSQL, execonn)
                Cmd.CommandText = varSQL
                Cmd.CommandType = CommandType.Text
                Cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            clsIMS.AddAudit(3, 17, "Cannot Save Swift to DB, err:" & Err.Description & ", sql:" & varSQL)
        End Try

    End Sub

    Public Function HasData(ByVal varSQL As String) As Boolean
        Dim GetSQLData As SqlDataReader

        Try
            HasData = False
            Using Cmd As New SqlCommand(varSQL, _conn)
                Cmd.CommandType = CommandType.Text
                GetSQLData = Cmd.ExecuteReader()
                If GetSQLData.HasRows Then
                    HasData = True
                End If
            End Using
        Catch ex As Exception
            HasData = False
        End Try

    End Function

    Public Function GetSwiftFileNamesInDictionary(ByVal varSQL As String) As List(Of String)

        Dim GetSQLData As SqlDataReader
        Dim SwiftFileNames As New List(Of String)
        Try
            GetSwiftFileNamesInDictionary = Nothing
            Using Cmd As New SqlCommand(varSQL, _conn)
                Cmd.CommandType = CommandType.Text
                GetSQLData = Cmd.ExecuteReader()
                If GetSQLData.HasRows Then
                    While GetSQLData.Read()
                        SwiftFileNames.Add(GetSQLData(0).ToString)
                    End While
                End If
                GetSwiftFileNamesInDictionary = SwiftFileNames
                GetSQLData.Close()
            End Using
        Catch ex As Exception
            GetSwiftFileNamesInDictionary = Nothing
            clsIMS.AddAudit(3, 17, Err.Description & " - Cannot GetSwiftFileNamesInDictionary")
        End Try
    End Function

    Public Sub CloseConnection()
        _conn.Close()
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

End Class
