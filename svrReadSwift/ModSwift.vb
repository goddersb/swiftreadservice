﻿Imports System.Reflection
Module ModSwift

    Public Const cSwiftJobNameAutoReady As String = "Dolfin Payment CreateSwiftFile AutoReady"
    Public Const cSwiftJobNameVolopa As String = "Dolfin Payment CreateSwiftFileVolopa"
    Public Const OpsEmailAddresses As String = "david.harper@dolfin.com;david.gahagan@dolfin.com;roger.Ababao@dolfin.com"
    Const c940FileName As String = "940.txt"
    Const c950FileName As String = "950.txt"
    Const c535FileName As String = "535.txt"

    Const cPPFSwiftCode As String = "PMBPCZPP"
    Const cABNSwiftCode As String = "ABNCNL2A"
    Const cBONYSwiftCode As String = "IRVTBEBB"

    Const cSwift940AccNo As String = ":25:"
    Const cSwift940OpeningNo As String = ":62F:"
    Const cSwift940ClosingNo As String = ":64:"

    Const cSwift950AccNo As String = ":25:"
    Const cSwift950OpeningNo As String = ":60F:"
    Const cSwift950ClosingNo As String = ":62F:"

    Const cSwift535AccNo As String = ":97A::SAFE//"
    Const cSwift535Clearer As String = ":94F::SAFE//"
    Const cSwift535ISIN As String = ":35B:"
    Const cSwift535HoldCCY As String = ":19A::HOLD//"
    Const cSwiftFaceValue As String = ":93B:"

    Public Const cPayDescInPort As Integer = 1
    Public Const cPayDescIn As Integer = 2
    Public Const cPayDescExClientIn As Integer = 3
    Public Const cPayDescExClientOut As Integer = 4
    Public Const cPayDescExOut As Integer = 5
    Public Const cPayDescExIn As Integer = 6
    Public Const cPayDescInOther As Integer = 7
    Public Const cPayDescReceieveFree As Integer = 8
    Public Const cPayDescDeliverFree As Integer = 9
    Public Const cPayDescMutualFund As Integer = 10
    Public Const cPayDescInPortLoan As Integer = 11
    Public Const cPayDescInMaltaLondon As Integer = 12
    Public Const cPayDescInLondonMalta As Integer = 13
    Public Const cPayDescExVolopaOut As Integer = 14
    Public Const cPayDescExReturnFunds As Integer = 15

    Public Const cPayDescInOtherCovChg As Integer = 1
    Public Const cPayDescInOtherComFee As Integer = 2
    Public Const cPayDescInOtherPayFee As Integer = 3
    Public Const cPayDescInOtherComBenOnly As Integer = 4
    Public Const cPayDescInOtherIntTra As Integer = 5
    Public Const cPayDescInOtherComOrdOnly As Integer = 6
    Public Const cPayDescInOtherCovChgOrdOnly As Integer = 7
    Public Const cPayDescInOtherCovChgBenOnly As Integer = 8
    Public Const cPayDescInOtherNoticetoReceive As Integer = 9
    Public Const cPayDescInOtherGlobalCustodyFee As Integer = 10
    Public Const cPayDescInOtherCustodyFee As Integer = 11
    Public Const cPayDescInOtherManagementFee As Integer = 12
    Public Const cPayDescInOtherBankCharges As Integer = 13
    Public Const cPayDescInOtherBankChargesCovChg As Integer = 14
    Public Const cPayDescInOtherFXCommission As Integer = 15
    Public Const cPayDescInOtherBankInterestCreditOrdOnly As Integer = 16
    Public Const cPayDescInOtherBankInterestDebitOrdOnly As Integer = 17
    Public Const cPayDescInOtherBankInterestCreditBenOnly As Integer = 18
    Public Const cPayDescInOtherBankInterestDebitBenOnly As Integer = 19
    Public Const cPayDescInOtherPayServicesFee As Integer = 20
    Public Const cPayDescInOtherFOPFee As Integer = 21
    Public Const cPayDescInOtherFreeText999 As Integer = 22
    Public Const cPayDescInOtherDolfinFee As Integer = 23
    Public Const cPayDescInOtherStatementRequest As Integer = 24
    Public Const cPayDescInOtherFXInternalOrdOnly As Integer = 25
    Public Const cPayDescInOtherFXInternalBenOnly As Integer = 26
    Public Const cPayDescInOtherPayVolopaFee As Integer = 27
    Public Const cPayDescInOtherFreeText199 As Integer = 28
    Public Const cPayDescInOtherCancelPayment192 As Integer = 29
    Public Const cPayDescInOtherPayVolopaAnnualFee As Integer = 30
    Public Const cPayDescInOtherCustodyFeeBenOnly As Integer = 31
    Public Const cPayDescInOtherManagementFeeBenOnly As Integer = 32
    Public Const cPayDescInOtherCustodyFeeClearClientOwedFees As Integer = 33
    Public Const cPayDescInOtherManagementFeeClearClientOwedFees As Integer = 34
    Public Const cPayDescInOtherFXBuy As Integer = 35
    Public Const cPayDescInOtherFXSell As Integer = 36
    Public Const cPayDescInOtherCustodyFeeFX As Integer = 37
    Public Const cPayDescInOtherManagementFeeFX As Integer = 38
    Public Const cPayDescInOtherMoneyMarketBuy As Integer = 39
    Public Const cPayDescInOtherMoneyMarketSell As Integer = 40
    Public Const cPayDescInOtherFreeText599 As Integer = 41
    Public Const cPayDescInOtherCustodyFeeReversal As Integer = 42
    Public Const cPayDescInOtherManagementFeeReversal As Integer = 43
    Public Const cPayDescInOtherAccountClosingCustodyFee As Integer = 44
    Public Const cPayDescInOtherAccountClosingManagementFee As Integer = 45
    Public Const cPayDescInOtherClientMoneySweep As Integer = 46

    Public Const cStatusReady As Integer = 1
    Public Const cStatusPendingAuth As Integer = 2
    Public Const cStatusPendingReview As Integer = 3
    Public Const cStatusCancelled As Integer = 4
    Public Const cStatusSent As Integer = 5
    Public Const cStatusSwiftAck As Integer = 6
    Public Const cStatusSwiftRej As Integer = 7
    Public Const cStatusSwiftErr As Integer = 8
    Public Const cStatusIMSAck As Integer = 9
    Public Const cStatusIMSErr As Integer = 10
    Public Const cStatusAwaitingSwift As Integer = 11
    Public Const cStatusAwaitingIMS As Integer = 12
    Public Const cStatusPaidAck As Integer = 13
    Public Const cStatusCustodianConfirmAcknowledged As Integer = 14
    Public Const cStatusCustodianMatched As Integer = 15
    Public Const cStatusCustodianNotMatched As Integer = 16
    Public Const cStatusCustodianPending As Integer = 17
    Public Const cStatusCustodianRejected As Integer = 18
    Public Const cStatusCustodianCancelled As Integer = 19
    Public Const cStatusCustodianCancelling As Integer = 20
    Public Const cStatusActive As Integer = 21
    Public Const cStatusNewEvent As Integer = 22
    Public Const cStatusExDate As Integer = 23
    Public Const cStatusPayDate As Integer = 24
    Public Const cStatusDolfinDeadlineDate As Integer = 25
    Public Const cStatusCustodianDeadlineDate As Integer = 26
    Public Const cStatusCustodianDeadlineDateMinusOne As Integer = 27
    Public Const cStatusArchive As Integer = 28
    Public Const cStatusPendingCompliance As Integer = 29
    Public Const cStatusComplianceRejected As Integer = 30
    Public Const cStatusAwaitingPayDate As Integer = 31
    Public Const cStatusExpired As Integer = 32
    Public Const cStatusPendingAccountMgt As Integer = 33
    Public Const cStatusPendingComplianceLevel2 As Integer = 34
    Public Const cStatusPaymentError As Integer = 35
    Public Const cStatusAllocatingFunds As Integer = 36
    Public Const cStatusGenerateInvoice As Integer = 37
    Public Const cStatusNoMatch As Integer = 38
    Public Const cStatusFullMatch As Integer = 39
    Public Const cStatusManualMatch As Integer = 40
    Public Const cStatusReviewMatch As Integer = 41
    Public Const cStatusMiniOMSAcknowledged As Integer = 42
    Public Const cStatusAutoReady As Integer = 43
    Public Const cStatusPendingAccountMgtLevel2 As Integer = 44
    Public Const cStatusPricingInstrument As Integer = 45
    Public Const cStatusAboveThreshold As Integer = 46
    Public Const cStatusSWIFTNoIMSMatch As Integer = 47
    Public Const cStatusSWIFTIMSMatched As Integer = 48
    Public Const cStatusIMSNoSWIFTMatch As Integer = 49
    Public Const cStatusResendToIMS As Integer = 50
    Public Const cStatusPendingPaymentsTeam As Integer = 51
    Public Const cStatusPaymentsTeamRejected As Integer = 52

    Public Const cAuditStatusSuccess As Integer = 1
    Public Const cAuditStatusBlocked As Integer = 2
    Public Const cAuditStatusError As Integer = 3
    Public Const cAuditStatusWarning As Integer = 4

    Public Const cAuditGroupNameViewMovements As Integer = 1
    Public Const cAuditGroupNameCashMovements As Integer = 2
    Public Const cAuditGroupNameCashMovementsFiles As Integer = 3
    Public Const cAuditGroupNameCutOff As Integer = 4
    Public Const cAuditGroupNameTransferFees As Integer = 5
    Public Const cAuditGroupNameTemplates As Integer = 6
    Public Const cAuditGroupNameBalances As Integer = 7
    Public Const cAuditGroupNameUsers As Integer = 8
    Public Const cAuditGroupNameAudit As Integer = 9
    Public Const cAuditGroupNameOmnibusSwitch As Integer = 10
    Public Const cAuditGroupNameSwiftCodes As Integer = 11
    Public Const cAuditGroupNameSwiftCodesIntermediary As Integer = 12
    Public Const cAuditGroupNameSystemSettings As Integer = 13
    Public Const cAuditGroupNameEmail As Integer = 14
    Public Const cAuditGroupNameReceiveDeliver As Integer = 15
    Public Const cAuditGroupNameViewAllSwifts As Integer = 16
    Public Const cAuditGroupNameReadSwifts As Integer = 17
    Public Const cAuditGroupNameConfirmReceipts As Integer = 18
    Public Const cAuditGroupNameFees As Integer = 19
    Public Const cAuditGroupNameCorpAction As Integer = 20
    Public Const cAuditGroupNamePaymentRequest As Integer = 21
    Public Const cAuditGroupNameRiskAssessment As Integer = 22
    Public Const cAuditGroupNameConnectivity As Integer = 23
    Public Const cAuditGroupNameReconciliation As Integer = 24
    Public Const cAuditGroupNameTreasuryBalancing As Integer = 25
    Public Const cAuditGroupNameBatchProcessing As Integer = 26
    Public Const cAuditGroupNameVolopa As Integer = 27
    Public Const cAuditGroupNameComplainceWatchList As Integer = 28
    Public Const cAuditGroupNameFOPPriceUpdate As Integer = 29
    Public Const cAuditGroupNameEditSwift As Integer = 30
    Public Const cAuditGroupNameCASSAccounts As Integer = 31
    Public Const cAuditGroupNameClientEmailAddresses As Integer = 32
    Public Const cAuditGroupNameClientRptCsvConfig As Integer = 33
    Public Const cAuditGroupNameClientStatementGenerator As Integer = 34
    Public Const cAuditGroupNameClientPDFReports As Integer = 35
    Public Const cAuditGroupNameBGMRec As Integer = 36
    Public Const cAuditGroupNameClientSendQtr As Integer = 37
    Public Const cAuditGroupNameWeeklyTradeFiles As Integer = 38
    Public Const cAuditGroupNameRestrictionList As Integer = 39
    Public Const cAuditGroupNameGPP As Integer = 40

    Public Const cUserLogin As String = "U_LoginPayments"
    Public Const cUserPayments As String = "U_UsersPayments"
    Public Const cUserAuthorise As String = "U_AuthorisePayments"
    Public Const cUserAuthoriseMyPayments As String = "U_AuthoriseMyPayments"
    Public Const cUserAuthorise3rdPartyPayments As String = "U_Authorise3rdPartyPayments"
    Public Const cUserEdit As String = "U_EditPayments"
    Public Const cUserDelete As String = "U_DeletePayments"
    Public Const cUserSubmit As String = "U_SubmitPayments"
    Public Const cUserCutoff As String = "U_AuthoriseCutoffTimesPayments"
    Public Const cUserTransferFees As String = "U_TransferFees"
    Public Const cUserAudit As String = "U_AuditLog"
    Public Const cUserOmnibus As String = "U_OmnibusSwitch"
    Public Const cUserOmnibusDeposits As String = "U_OmnibusSwitchTermDeposits"
    Public Const cUserTemplates As String = "U_Templates"
    Public Const cUserBalances As String = "U_Balances"
    Public Const cUserSwift As String = "U_Swift"
    Public Const cUserSwiftIntermediary As String = "U_SwiftIntermediary"
    Public Const cUserSettings As String = "U_SystemSettings"
    Public Const cUserRD As String = "U_ReceiveDeliver"
    Public Const cUserAboveThreshold As String = "U_AuthoriseAboveThreshold"
    Public Const cUserAutoSwift As String = "U_AutoSwift"
    Public Const cUserSwiftRead As String = "U_SwiftRead"
    Public Const cUserConfirmReceipts As String = "U_ConfirmReceipts"
    Public Const cUserFees As String = "U_Fees"
    Public Const cUserCA As String = "U_CorpAction"
    Public Const cUserAuthoriseAccountMgt As String = "U_AuthoriseAccountMgt"
    Public Const cUserAuthoriseAccountMgtLevel2 As String = "U_AuthoriseAccountMgtLevel2"
    Public Const cUserAuthoriseCompliance As String = "U_AuthoriseCompliance"
    Public Const cUserAuthoriseComplianceLevel2 As String = "U_AuthoriseComplianceLevel2"
    Public Const cUserAuthoriseFeeSchedules As String = "U_AuthoriseFeeSchedules"
    Public Const cUserRA As String = "U_RiskAssessment"
    Public Const cUserC As String = "U_Comments"
    Public Const cUserAC As String = "U_AuthoriseComments"
    Public Const cUserPG As String = "U_PasswordGenerator"
    Public Const cUserFeesPaid As String = "U_FeesPaid"
    Public Const cUserAuthoriseFeesPaid As String = "U_AuthoriseFeesPaid"
    Public Const cUserPayFeesPaid As String = "U_PayFeesPaid"
    Public Const cUserTreasuryMgt As String = "U_TreasuryMgt"
    Public Const cUserRec As String = "U_Rec"
    Public Const cUserSwiftAccStatement As String = "U_SwiftAccStatement"
    Public Const cUserBatchProcessing As String = "U_BatchProcessing"
    Public Const cUserComplianceWatchList As String = "U_ComplianceWatchlist"
    Public Const cUserCASS As String = "U_CASSAccounts"
    Public Const cUserClientEmailAddresses As String = "U_ClientEmail"
    Public Const cUserClientStatementGenerator As String = "U_ClientBalCheck"
    Public Const cUserClientSendQtr As String = "U_ClientSendQtr"
    Public Const cUserAuthorisePaymentsTeam As String = "U_AuthorisePaymentsTeam"
    Public Const cUserBGMRec As String = "U_BGMRec"

    Public clsIMS As New clsImsData
    Public clsSR As New clsSwiftRead
    Public clsSE As New clsSwiftEdit
    Public clsEmail As New clsEmailData
    Public clsSearch As New clsC6Search
    Public clsTB As New clsTreasuryBalancer
    Public clsFOPUpdater As New clsUpdateFOPPrice

    Public Sub RunSwiftRead()

        clsSE.EditSwift()
        clsFOPUpdater.UpdateFOPPrices()
        clsTB.RunTreasuryBalancer()
        clsIMS.UpdateMovementsStatus()
        clsIMS.CreateTransactionsSwiftAuto()
        clsSR.ReadSwift()
        clsSR.ReadSwiftErrors()
        clsSR.CheckBFLFiles()
        clsSR.ReadGGPFiles()

    End Sub

    Public Function FindNextChar(ByVal varStr As String) As Integer
        Dim varChar As String
        FindNextChar = 0
        For i As Integer = 1 To Len(varStr)
            varChar = Mid(varStr, i, 1)
            If Not IsNumeric(varChar) Then
                FindNextChar = i
                Exit For
            End If
        Next
    End Function

    Public Function FormatNumber(ByVal varNum As String) As Double
        Dim FormatNumberStr As String
        Try
            FormatNumber = Replace(varNum, ",", ".")
            If FormatNumber = "0." Or FormatNumber = "0.0" Or FormatNumber = "0.00" Then
                FormatNumber = 0
            End If
            If Right(FormatNumber, 1) = "," Or Right(FormatNumber, 1) = "." Then
                FormatNumberStr = Left(FormatNumber, Len(FormatNumber) - 1)
                If IsNumeric(FormatNumberStr) Then
                    FormatNumber = FormatNumberStr
                Else
                    FormatNumber = 0
                End If
            End If
        Catch ex As Exception
            FormatNumber = 0
        End Try
    End Function

    Public Function ConvertTimeZone(ByVal varTimeZone As String) As DateTimeOffset
        Dim varDateTime As DateTime = CDate(DateTime.UtcNow)
        Dim tz As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(varTimeZone)
        Dim utcOffset As New DateTimeOffset(varDateTime, TimeSpan.Zero)
        ConvertTimeZone = utcOffset.ToOffset(tz.GetUtcOffset(utcOffset))
    End Function

    Public Sub AddDataColumn(ByRef Tbl As DataTable, ByRef varName As String, ByRef varType As String)
        Dim newCol As New DataColumn(varName)
        newCol.DataType = System.Type.GetType(varType)
        Tbl.Columns.Add(newCol)
    End Sub

    Public Function GetPaymentDirectory(ByVal varID As Double) As String
        Dim varDirectories As String() = IO.Directory.GetDirectories(clsIMS.GetFilePath("Default"), "*" & varID)
        GetPaymentDirectory = ""

        If varDirectories.Count > 0 Then
            For Each varDirectory In varDirectories
                GetPaymentDirectory = varDirectory
            Next
        End If
    End Function

    Public Sub FileAction(ByVal varAction As Integer, ByVal varFileName As String, Optional ByVal varFullFilePath As String = "")
        Try
            If varAction = 0 Then
                If IO.File.Exists(varFileName) And Not IO.File.Exists(varFullFilePath) Then
                    IO.File.Move(varFileName, varFullFilePath)
                    clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, $"Moved {varFileName} to {varFullFilePath} - SwiftRead (FileAction)")
                End If
            ElseIf varAction = 1 Then
                If IO.File.Exists(varFileName) And Not IO.File.Exists(varFullFilePath) Then
                    IO.File.Copy(varFileName, varFullFilePath)
                    clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, $"Copied {varFileName} to {varFullFilePath} - SwiftRead (FileAction)")
                End If
            ElseIf varAction = 2 Then
                IO.File.Delete(varFileName)
                clsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, $"Removed {varFileName} - SwiftRead (FileAction)")
            End If
        Catch ex As Exception
            clsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, $"Problem with FileAction {varAction} for file {varFileName}, {varFullFilePath} - SwiftRead (FileAction)")
        End Try
    End Sub

End Module